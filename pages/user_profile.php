<?php
	ob_start(); 
	include_once('../assets/includes/header.php');
	// check if user is a staff for not
	 if ($_SESSION['account_name'] != 'staff') {
	  	die();
	  }  
	require_once('../Classes/CourtCase.php'); 
	require_once('../Classes/Staff.php'); 
?>
<div class="panel panel-profile">
	<div class="clearfix">
		<!-- LEFT COLUMN -->
		<div class="profile-left">
			<!-- PROFILE HEADER -->
			<div class="profile-header">
				<div class="overlay"></div>
				<div class="profile-main">
					<!-- <img src="../assets/img/avatar.png" class="img-circle"> -->
					<?php 
						if ($staffDetails[1] == "NONE") {
                    		echo '<img src="../assets/img/user.png" class="img-circle" alt="Avatar">';
                    	}
                    	else {
                			echo '<img src="/lawOffice/uploads/staffpic/'.$staffDetails[1].'" width="250px" height="250px" class="img-circle" alt="Avatar">';
                    	}
                    	// display profile name
                    	echo '<h3 class="name">'. $accountName.'</h3>';
					 ?>
					<!-- <span class="online-status status-available">Available</span> -->
				</div>

			</div>
		</div>
		<!-- END LEFT COLUMN -->
		<!-- RIGHT COLUMN -->
		<div class="profile-right">
			<!-- TABBED CONTENT -->
			<div class="custom-tabs-line tabs-line-bottom left-aligned">
				<ul class="nav" role="tablist">
					<li class="active"><a href="#profileDetails" role="tab" data-toggle="tab">Basic Info</a></li>
					<li><a href="#profileCases" role="tab" data-toggle="tab">Cases </a></li>
				</ul>
			</div>
			<div class="tab-content">
				<?php 
					$objStaff = new Staff;
                    $profileDetails = $objStaff->get_profile_details();
				 ?>
				<!-- profile details -->
				<div class="tab-pane fade in active" id="profileDetails">
					<form id="insert_form">
			            <div class="row">
			                <div class="col-md-12">
			                    <!-- 1 -->
			                    <div class="row">
			                        <div class="col-md-3">
			                            <label for="title" class="col-form-label">First Name</label>
			                        </div>
			                        <div class="col-md-9">
			                            <div class="form-group">
			                               <input type="text" class="form-control" readonly value="<?php echo $profileDetails['staff_firstname']; ?>">
			                            </div>
			                        </div>
			                    </div>
			                    <!--  -->
			                    <div class="row">
			                        <div class="col-md-3">
			                            <label for="title" class="col-form-label">Other Name</label>
			                        </div>
			                        <div class="col-md-9">
			                            <div class="form-group">
			                               <input type="text" class="form-control" readonly value="<?php echo $profileDetails['staff_middlename']; ?>">
			                            </div>
			                        </div>
			                    </div>
			                    <!--  -->
			                    <div class="row">
			                        <div class="col-md-3">
			                            <label for="title" class="col-form-label">Last Name</label>
			                        </div>
			                        <div class="col-md-9">
			                            <div class="form-group">
			                               <input type="text" class="form-control" readonly value="<?php echo $profileDetails['staff_lastname']; ?>">
			                            </div>
			                        </div>
			                    </div>
			                    <!--  -->
			                    <div class="row">
			                        <div class="col-md-3">
			                            <label for="title" class="col-form-label">Date Of Birth</label>
			                        </div>
			                        <div class="col-md-9">
			                            <div class="form-group">
			                               <input type="text" class="form-control" readonly value="<?php echo $profileDetails['staff_dob']; ?>">
			                            </div>
			                        </div>
			                    </div>
			                    <!--  -->
			                    <div class="row">
			                        <div class="col-md-3">
			                            <label for="title" class="col-form-label">Phone Number</label>
			                        </div>
			                        <div class="col-md-9">
			                            <div class="form-group">
			                               <input type="text" class="form-control" readonly value="<?php echo $profileDetails['staff_tel']; ?>">
			                            </div>
			                        </div>
			                    </div>
			              		<!--  -->
			              		<div class="row">
			                        <div class="col-md-3">
			                            <label for="title" class="col-form-label">Emergency Contact</label>
			                        </div>
			                        <div class="col-md-9">
			                            <div class="form-group">
			                               <input type="text" class="form-control" readonly value="<?php echo $profileDetails['staff_emergency']; ?>">
			                            </div>
			                        </div>
			                    </div>
			                    <!--  -->
			                    <div class="row">
			                        <div class="col-md-3">
			                            <label for="title" class="col-form-label">Email</label>
			                        </div>
			                        <div class="col-md-9">
			                            <div class="form-group">
			                               <input type="text" class="form-control" readonly value="<?php echo $profileDetails['staff_email']; ?>">
			                            </div>
			                        </div>
			                    </div>
			                    <!--  -->
			                    <div class="row">
			                        <div class="col-md-3">
			                            <label for="title" class="col-form-label">Location</label>
			                        </div>
			                        <div class="col-md-9">
			                            <div class="form-group">
			                               <textarea class="form-control" readonly><?php echo $profileDetails['staff_houseloc']; ?></textarea>
			                            </div>
			                        </div>
			                    </div>
			                    <!--  -->
			                    <div class="row">
			                        <div class="col-md-3">
			                            <label for="title" class="col-form-label">Date Added</label>
			                        </div>
			                        <div class="col-md-9">
			                            <div class="form-group">
			                               <input type="text" class="form-control" readonly value="<?php echo $profileDetails['added']; ?>">
			                            </div>
			                        </div>
			                    </div>
			                </div>
			            </div>
			        </form>
				</div>
				<!-- cases -->
				<div class="tab-pane fade" id="profileCases">
					<div class="table-responsive">
						<table class="table">
							<thead>
			                    <tr>
			                    	<th></th>
			                        <th>Case Name</th>
			                        <th>Case Number</th>
			                        <th>Date Opened</th>
			                        <th>Stage</th>
			                        <th>Status</th>
			                    </tr>
			                </thead>
			               	<tbody>
			                    <?php
			                        $objCourtCase = new CourtCase;
	                          		$cases = $objCourtCase->get_staff_profile_cases(); 
			                        if (!empty($cases)) {
			                        	print_r($cases);
			                        }
			                       ?>
			                </tbody>
						</table>
					</div>
				</div>
			</div>
			<!-- END TABBED CONTENT -->
		</div>
		<!-- END RIGHT COLUMN -->
	</div>
<?php include('../assets/includes/footer.php'); ?>
