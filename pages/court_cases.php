<?php
	ob_start(); 
	require_once('../includes/header.php'); 

	// refer back to dashboard if page is not in group
	$retrievedFileName = basename($_SERVER['PHP_SELF']);
	if (!in_array($retrievedFileName, $_SESSION['pagesAllowed'])) {
	  header('Location: ../pages/dashboard.php');
	}
	// check if user is a staff for not
	 if ($_SESSION['account_name'] != 'staff') {
	  	die();
	  } 
	
?>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-briefcase"></i> Cases</h2>
         <div class="pull-right">
         	<button data-toggle="modal" data-target="#addCaseModal" data-backdrop="static" data-keyboard="false" class="btn btn-danger"><span class="glyphicon glyphicon-briefcase"></span> Add Case</button>
         </div>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div class="" role="tabpanel" data-example-id="togglable-tabs">
          <ul id="myTab" class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#cases" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true"><strong>Cases </strong> <i class="fa fa-list"></i></a>
            </li>
            <li role="presentation" class=""><a href="#practiceArea" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false"><strong>Practice Area </strong><i class="fa fa-globe"></i></a>
            </li>
          </ul><br>
          <div id="myTabContent" class="tab-content">
            <div role="tabpanel" class="tab-pane fade active in" id="cases" aria-labelledby="home-tab">
              <!-- for filters -->
				<div class="row">
					<div class="col-md-12">
						<div class="panel">
							<div class="panel-body">
								<div class="row">
									<form>
										<div class="col-md-3">
											<div class="form-group">
										      <label for="filter"><b>Practice Area</b></label>
										      <select class="form-control filterPracticeSelect2" style="width:100%;" name="filterPractice" id="filterPractice"> 
						                   	    <option value="" selected="selected" disabled>Select Practice Area</option>
						                   	    <?php
				                                    $objPracticeArea = new PracticeArea;
				                                    $details = $objPracticeArea->get_practice_areas_options(); 
				                                    if (!empty($details)) {
				                                      print_r($details);
				                                    }
				                                    
				                                ?>
							                   </select>
										    </div>
										</div>
										<div class="col-md-3">
											<div class="form-group">
										      <label for="filter"><b>Lead Attorney</b></label>
										      <select class="form-control filterLeadAttorselect2" style="width: 100%;" id="filterLeadAttor" name="filterLeadAttor">
							                   	<option value="" selected="selected" disabled>Select Lead Attorney</option>
							                   	<?php
							                        $staffs = $objStaff->get_staff_by_type("Attorney"); 
							                        if (!empty($staffs)) {
							                        	print_r($staffs);
							                        }
							                    ?>
							                   </select>
										    </div>
										</div>
										<div class="col-md-3">
											<div class="form-group">
										      <label for="filter"><b>Case Stage</b></label>
										      <select class="form-control filtercaseStageselect2" style="width: 100%;" name="filtercaseStage" id="filtercaseStage">
						                   		<option value="" selected="selected" disabled>Select Case Stage</option> 
						                   		<option value="Discovery">Discovery</option>
						                   		<option value="In Trial">In Trial</option>
						                   		<option value="On Hold">On Hold</option>
						                   		<option value="Pleading">Pleading</option>
						                   		<option value="Summon For Direction">Summon For Direction</option>
						                   		<option value="Case Managment Conference">Case Managment Conference</option>
						                   		<option value="Witness Statement">Witness Statement</option>
						                   		<option value="Examination Witness">Examination Witness</option>
						                   		<option value="Address">Address</option>
						                   		<option value="Judgement">Judgement</option>
						                   		<option value="interlocutory injunction">interlocutory injunction</option>
						                   	  </select>
										    </div>
										</div>
										<div class="col-md-3">
											<div class="form-group" style="padding-top: 7%;">
												<button type="button" class=" bg-el" id="applyFilterBtn">Apply Filters <i class="fa fa-filter"></i></button>
												<button type="button" class=" btn-danger" id="clearFilterbtn">Clear Filters <i class="fa fa-eraser"></i></button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			<!-- end of filters -->
			<div class="tab-content">
			    <div id="cases" class="tab-pane fade in active table-responsive">
			        <table class="table tableList">
                   		<thead>
                   			<th width="12%">Case</th>
                   			<th width="10%">Suit No</th>
                   			<th width="6%">Case Code</th>
                   			<th width="10%">Case Stage</th>
                   			<th width="12%">Case Lead Attorney</th>
                   			<th width="10%">Case Added</th>
                   			<th width="12%"></th>
                   		</thead>
                   		<tbody id="courtCaseDisplay">
                   			<?php
                              $objCourtCase = new CourtCase;
                              $details = $objCourtCase->get_cases_all(); 
                              if (!empty($details)) {
                              	print_r($details);
                              }
                             ?>
                   		</tbody>
                   	</table>
			    </div> 
			  </div>
            </div>
			<!-- ///////////////////////////////////////////////////////////////// -->

            <div role="tabpanel" class="tab-pane fade" id="practiceArea" aria-labelledby="profile-tab">
            	<div class="table-responsive">
			      	<table class="table table-hover tableList"> 
	                    <thead>
	                        <tr>
	                            <th>Practice Area</th>
	                            <th>Cases</th>
	                        </tr>
	                    </thead>
	                   	<tbody>
	                         <?php
                              $objPracticeArea = new PracticeArea;
                          	  $details = $objPracticeArea->get_court_case_practice_areas();
                              if (!empty($details)) {
                              	print_r($details);
                              }
                             ?>
	                    </tbody>
	                </table>
			     </div>
			</div>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>



<!-- /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- //////////////// for view court case details-->
<!-- /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->

<div class="modal fade" id="view_case_detailsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header" id="bg">
         <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true" class="asterick btn-default">&times; CLOSE</span></button>
        <h3 class="modal-title viewCaseTitle">Case Details</h3>
      </div>
      <div class="modal-body" id="bg">
          <div class="row">
          	  <div class="col-md-12">
          	  	  <ul class="nav nav-tabs nav-justified" >
				    <li class="active borderRight"><a data-toggle="tab" href="#info"><b><i class="fa fa-info-circle"></i> Info</b></a></li>
				    <li class="borderRight"><a data-toggle="tab" href="#recent_activity"><b><i class="fa fa-rss"></i> Recent Activity</b></a></li>
				    <li class="borderRight"><a data-toggle="tab" href="#contact_staff"><b><i class="fa fa-users"></i> Contact & Staff</b></a></li>
				    <li class="borderRight"><a data-toggle="tab" href="#case_calendar"><b><i class="fa fa-calendar"></i> Calender</b></a></li>
				    <li class="borderRight"><a data-toggle="tab" href="#document"><b><i class="fa fa-folder-open"></i> Documents</b></a></li>
				    <li class="borderRight"><a data-toggle="tab" href="#tasks"><b><i class="fa fa-tasks"></i> Tasks</b></a></li>
				    <li class="borderRight"><a data-toggle="tab" href="#messages"><b><i class="fa fa-comments"></i> Messages</b></a></li>
				    <li><a data-toggle="tab" href="#notes"><b><i class="fa fa-pencil"></i> Notes</b></a></li>
				  </ul><br>
				  <div class="tab-content">
				  	<!-- ////////////////////////////////////////////////////////////////////////////////////////////////////// -->
				  	<!-- information tab -->
				    <div id="info" class="tab-pane fade in active">
				      
				      <div class="row">
				      	<div class="col-md-6 panel" style="border-right: 1px solid black;">
				      		<div class="table-responsive">
						      	<table class="table table-stripped">
						      		<!-- <thead> -->
						      			<th width="35%"></th>
						      			<th width="65%"><h4>Case information</h4></th>
						      		<!-- </thead> -->
						      		<tbody>
						      			<tr>
							      			<td class="bg-el"><i class="fa fa-briefcase"></i> Case Name</td>
							      			<td id="viewCaseName"></td>
							      		</tr>
							      		<tr>
							      			<td class="bg-el"><i class="fa fa-sort-numeric-asc"></i> Case Suit Number</td>
							      			<td id="viewSuitNumber"></td>
							      		</tr>
							      		<tr>
							      			<td class="bg-el"><i class="fa fa-code"></i> Case Code</td>
							      			<td id="viewCaseCode"></td>
							      		</tr>
							      		<tr>
							      			<td class="bg-el"><i class="fa fa-globe"></i> Case Practice Area</td>
							      			<td id="viewPracticeArea"></td>
							      		</tr>
							      		<tr>
							      			<td class="bg-el"><i class="fa fa-level-up"></i> Case Stage</td>
							      			<td id="viewCaseStage"></td>
							      		</tr>
							      		<tr>
							      			<td class="bg-el"><i class="fa fa-university"></i> Case Current Court</td>
							      			<td id="viewCaseCurrentCourt"></td>
							      		</tr>
							      		<tr>
							      			<td class="bg-el"><i class="fa fa-university"></i> Case Next Court</td>
							      			<td id="viewCaseNextCourt"></td>
							      		</tr>
							      		<tr>
							      			<td class="bg-el"><i class="fa fa-calendar"></i> Case Next Court Date</td>
							      			<td id="viewCaseNextCourtDate"></td>
							      		</tr>
							      		<tr>
							      			<td class="bg-el"><i class="fa fa-list"></i> Case Description</td>
							      			<td id="viewCaseDescription"></td>
							      		</tr>
							      		<tr>
							      			<td class="bg-el"><i class="fa fa-exchange"></i> Case Status</td>
							      			<td id="viewCaseStatus"></td>
							      		</tr>
							      		<tr>
							      			<td class="bg-el"><i class="fa fa-calendar"></i> Case Date Opened</td>
							      			<td id="viewCaseOpendDate"></td>
							      		</tr>
							      		<tr>
							      			<td class="bg-el"><i class="fa fa-user"></i> Case Lead Attorney</td>
							      			<td id="viewCaseLeadAttorney"></td>
							      		</tr>
						      		</tbody>
						      		<tfoot>
						      			<tr id="closeCaseBtnDiv">
						      				<td colspan="2">
						      					<button class="btn btn-block closeCaseBtn btn-danger">CLOSE CASE <i class="fa fa-lock"></i></button>
						      				</td>
						      			</tr>
						      		</tfoot>
						      	</table>
						    </div>
						    
				      	</div>
				      	<div class="col-md-6">
				      		
				      	</div>
				      </div>
				    </div>
				    <!-- ////////////////////////////////////////////////////////////////////////////////////////////////////// -->
				    <!-- recent activity -->
				    <div id="recent_activity" class="tab-pane fade">
				      <h4>Recent_activity</h4>
				      <div class="table-responsive">
				      	<table class="table">
				      		<thead>
				      			<th>Activity</th>
				      			<th>Date</th>
				      		</thead>
				      		<tbody id="viewActivityDiv">
				      			
				      		</tbody>
				      	</table>
				      </div>
				    </div>
				    <!-- ////////////////////////////////////////////////////////////////////////////////////////////////////// -->
				    <!-- contact and staff -->
				    <div id="contact_staff" class="tab-pane fade">
				      <div class="row">
				      	<div class="col-md-6 panel">
				      		<div class="x_panel">
						      <div class="x_title">
						        <h4>Case Contacts</h4>
						        <!-- add new button -->
						        <div class="clearfix"></div>
						      </div>
						      <div class="x_content">
						          <div class="table-responsive">
						          	<table class="table">
						          		<thead>
						          			<th></th>
						          			<th>Name</th>
						          			<th>Phone Num</th>
						          			<th>Group</th>
						          		</thead>
						          		<tbody id="viewCaseContactsDiv">
						          			
						          		</tbody>
						          	</table>
						          </div>
						      </div>
						    </div>
				      	</div>
				      	<!-- <div class="col-md-2"></div> -->
				      	<div class="col-md-6 panel">
					      	<div class="x_panel">
						      <div class="x_title">
						        <h4>Case Staff</h4>
						        <!-- add new button -->
						        <div class="clearfix"></div>
						      </div>
						      <div class="x_content">
						      	  <div class="table-responsive">
						          	<table class="table">
						          		<thead>
						          			<th></th>
						          			<th>Name</th>
						          			<th>Phone Num</th>
						          			<th>Staff Type</th>
						          			<!-- <th>Group</th> -->
						          		</thead>
						          		<tbody id="viewCaseStaffDiv">
						          			
						          		</tbody>
						          	</table>
						          </div>

						      </div>
						    </div>
				      	</div>
				      </div>
				    </div>
				    <!-- ////////////////////////////////////////////////////////////////////////////////////////////////////// -->
				    <!-- calendar  OR Events -->
				    <div id="case_calendar" class="tab-pane fade">
				      <h4>Calender</h4>
				      <div class="table-responsive">
				      	<table class="table">
				      		<thead>
				      			<th>Events</th>
				      			<th>Date Added</th>
				      			<th>Location</th>
				      			<th>Event Date/Time</th>
				      			<th>Added User</th>
				      		</thead>
				      		<tbody id="viewEventDiv">
				      			
				      		</tbody>
				      	</table>
				      </div>
				    </div>
				    <!-- ////////////////////////////////////////////////////////////////////////////////////////////////////// -->
				    <!-- documents -->
				    <div id="document" class="tab-pane fade">
				      <h4>Documents</h4>
	                  <div class="form-group input-group">
	                    <input type="text" name="search" class="form-control" placeholder="Search Document name or date  &hellip;" id="viewDocumentSearch" autocomplete="off" autofocus>
	                    <span class="input-group-addon"><i class="fa fa-search"></i></span>
	                  </div>
				      <div class="table-responsive ">
				      	<table class="table ">
				      		<thead>
				      			<tr>
				      				<th>Document Folder</th>
					      			<th>Document Name</th>
					      			<th>Date Uploaded</th>
					      			<th>Added User</th>
					      			<th></th>
				      			</tr>
				      		</thead>
				      		<tbody id="viewdocumentDiv">
				      			
				      		</tbody>
				      	</table>
				      </div>
				    </div>
				    <!-- ////////////////////////////////////////////////////////////////////////////////////////////////////// -->
				    <!-- tasks -->
				    <div id="tasks" class="tab-pane fade">
				      <h4>Task</h4>
				      <div class="form-group input-group">
	                    <input type="text" name="search" class="form-control" placeholder="Search  &hellip;" id="viewtaskSearch" autocomplete="off" autofocus>
	                    <span class="input-group-addon"><i class="fa fa-search"></i></span>
	                  </div>
				      <div class="table-responsive ">
				      	<table class="table ">
				      		<thead>
				      			<tr>
				      				<th width="28%">Tasks</th>
					      			<th width="6%">Priority</th>
					      			<th width="8%">Date Due</th>
					      			<th width="12%">Status</th>
					      			<th width="24%">Completed Date / User</th>
					      			<th width="8%">Date Added</th>
					      			<th width="10%">Added User</th>
				      			</tr>
				      		</thead>
				      		<tbody id="viewTaskDiv">
				      			
				      		</tbody>
				      	</table>
				      </div>
				    </div>
				    
				    <!-- ////////////////////////////////////////////////////////////////////////////////////////////////////// -->
				    <!-- messages -->
				    <div id="messages" class="tab-pane fade">
				      <h4>Message</h4>
				      <div class="table-responsive ">
				      	<table class="table ">
				      		<thead>
				      			<tr>
				      				<th>Subject</th>
					      			<th>Date/Time</th>
					      			<th>Account</th>
					      			<th>Sender</th>
				      			</tr>
				      		</thead>
				      		<tbody id="viewMessagesDiv">
				      			
				      		</tbody>
				      	</table>
				      </div>
				    </div>
				    <!-- ////////////////////////////////////////////////////////////////////////////////////////////////////// -->
				    <!-- Notes -->
				    <div id="notes" class="tab-pane fade">

				    	<div class="row">
						  <div class="col-md-12 col-sm-12 col-xs-12">
						    <div class="x_panel">
						      <div class="x_title">
						        <button id="addNoteBtn" class="btn btn-danger">ADD NEW NOTE <i class="fa fa-plus"></i></button>
						        <!-- end new button -->
						        <div class="clearfix"></div>
						      </div>
						      <div class="x_content">
						          <div class="row">
						              <div class="col-md-12" id="viewCaseNoteDiv"></div>
						          </div>
						      </div>
						    </div>
						  </div>
						</div>

				    </div>
				    <!-- ////////////////////////////////////////////////////////////////////////////////////////////////////// -->
				  </div>
          	  </div>
          </div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<!-- /////////////////////////////Add Note Modal//////////////////////////////////// -->
 <div class="modal fade" id="addNoteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" id="bg">
         <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true" class="asterick btn-default">&times; CLOSE</span></button>
        <h4 class="modal-title caseNoteSubject" >Add Case Note</h4>
      </div>
      <div class="modal-body" id="bg">
     <form id="addNote_form" method="POST"> 
        <div class="row">
          <div class="form-group">
            <div class="col-md-2"><label for="title"><b>Note</b></label></div>
            <div class="col-md-10">
              <textarea type="text" class="form-control" id="viewNoteContent" name="viewNoteContent" rows="10" autocomplete="off" placeholder="Enter Note Details &hellip;" required></textarea>
            </div>
          </div>
        </div>
        <br>
        <!-- case id -->
        <input type="hidden" name="view_note_caseId" id="view_note_caseId" value="">
        <!-- assigning user id -->
        <input type="hidden" id="accountId" value="<?php echo $_SESSION['account_id'];?>">
         <!-- for inserting the page id -->
        <input type="hidden" id="view_note_data_id" name="data_id" value="">
         <!-- for insert query -->
        <input type="hidden" name="mode" id="view_note_mode" value="insert">
        <div class="modal-footer" id="bg">
        	<button type="button" class="btn btn-danger" data-dismiss="modal">Close <i class="fa fa-times"></i></button>
		    <button type="submit" class="btn btn-primary" id="addNoteSubmitBtn">Save <i class="fa fa-floppy-o"></i></button>
        </div>        
        </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<?php include('../includes/footer.php'); ?>
<!-- include court case modal -->
<?php include_once('court_cases_include.php'); ?>

<script src="js/pageScript/court_case_view.js"></script>
<script src="js/pageScript/court_case_view_notes.js"></script>
<?php include_once('viewer_pdfImage.php'); ?>

