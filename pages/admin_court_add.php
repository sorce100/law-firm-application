<?php 
  ob_start();
  include_once('../includes/header.php'); 
  include_once('../Classes/CourtAdd.php'); 
  $objCourtAdd = new CourtAdd;
  // retrive page file name
  $retrievedFileName = basename($_SERVER['PHP_SELF']);
  // if (!in_array($retrievedFileName, $_SESSION['pagesAllowed'])) {
  //     header('Location: dashboard.php');
  // }
?>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Courts Setup</h2>
        <!-- end new button -->
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
          <!-- creating tabs -->
          <div class="" role="tabpanel" data-example-id="togglable-tabs">
            <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
              <li role="presentation" class="active"><a href="#mainCourt" role="tab" data-toggle="tab" aria-expanded="true">Main Court <i class="fa fa-home"></i></a>
              </li>
              <li role="presentation" class=""><a href="#subCourt" role="tab" data-toggle="tab" aria-expanded="false">Sub Court <i class="fa fa-subscript"></i></a>
              </li>
            </ul>
            <div id="myTabContent" class="tab-content">
              <div role="tabpanel" class="tab-pane fade active in" id="mainCourt" aria-labelledby="home-tab">
               <!-- for main court -->
               <div class="row">
                  <button data-toggle="modal" data-target="#mainCourtModal" class="btn btn-danger pull-right"><span class="fa fa-plus"></span> Add Main Court </button>
               </div>
               <hr>
               <!-- display main court list -->
                  <div class="table-responsive">
                    <table class="table table-hover tableList"> 
                      <thead>
                        <tr>
                          <th>Court Name</th>
                          <th>Added</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody id="resultsDisplay">
                        <?php
                          $details = $objCourtAdd->get_main_courts(); 
                          foreach ($details as $detail) {
                            echo '
                                <tr>
                                  <td>'.$detail["main_court_name"].'</td>
                                  <td>'.$detail["added"].'</td>
                                  <td>
                                    <button class="btn-primary main_update_data" id="'.$detail["main_court_id"].'"><i class="fa fa-pencil"></i></button> <button class="btn-danger main_court_del_data" id="'.$detail["main_court_id"].'"><i class="fa fa-trash"></i></button>
                                  </td>
                                </tr>';
                          }
                         ?>
                      </tbody>
                    </table>
                </div>
               <!-- end of main court -->
              </div>
              <div role="tabpanel" class="tab-pane fade" id="subCourt" aria-labelledby="profile-tab">
                <!-- for sub courts -->
                <div class="row">
                  <button data-toggle="modal" data-target="#subCourtModal" class="btn btn-danger pull-right"><span class="fa fa-plus"></span> Add Sub Court </button>
               </div><hr>
               <!-- display sub court list -->
                  <div class="table-responsive">
                    <table class="table table-hover tableList"> 
                      <thead>
                        <tr>
                          <th>Main Cout</th>
                          <th>Sub Court Name</th>
                          <th>Region</th>
                          <th>Added</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody id="resultsDisplay">
                        <?php
                          $details = $objCourtAdd->get_sub_courts(); 
                          foreach ($details as $detail) {
                            echo '
                                <tr>
                                  <td>'.$detail["main_court_name"].'</td>
                                  <td>'.$detail["sub_court_name"].'</td>
                                  <td>'.$detail["sub_court_region"].'</td>
                                  <td>'.$detail["added"].'</td>
                                  <td>
                                    <button class="btn-primary sub_update_data" id="'.$detail["sub_court_id"].'"><i class="fa fa-pencil"></i></button> <button class="btn-danger sub_court_del_data" id="'.$detail["sub_court_id"].'"><i class="fa fa-trash"></i></button>
                                  </td>
                                </tr>';
                          }
                         ?>
                      </tbody>
                    </table>
                </div>
                <!-- end of for sub courts -->
              </div>
            </div>
          </div>
          <!-- end of tabs
          '' -->
      </div>
    </div>
  </div>
</div>

<!-- for main court add modal -->
<div class="modal fade" id="mainCourtModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" id="bg">
         <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true" style="color: red;font-size: 25px;" class="btn-default">&times; </span></button>
        <h4 class="modal-title mainCourtTitle" id="subject">Add Court</h4>
      </div>
      <div class="modal-body" id="bg">
          <form id="mainCourt_form" method="POST">
            <div class="row">
                <div class="col-md-12">
                    <!-- 1 -->
                    <div class="row">
                        <div class="col-md-3">
                            <label for="title" class="col-form-label">Court Name <span class="asterick">*</span></label>
                        </div>
                        <div class="col-md-9">
                            <div class="form-group">
                               <input type="text" name="mainCourtName" id="mainCourtName" class="form-control" placeholder="Eg. Accra Court" autocomplete="off" required>
                            </div>
                        </div>
                    </div>
                    <!-- for inserting the page id -->
                    <input type="hidden" name="data_id" id="mainCourt_data_id" value="">
                    <!-- for insert query -->
                    <input type="hidden" name="mode" id="mainCourtMode" value="mainCourtInsert">

                   <div class="modal-footer">
                      <button type="button" class="btn btn-danger" data-dismiss="modal">Close <i class="fa fa-times"></i></button>
                      <button type="submit" class="btn btn-primary" id="mainCourt_btn">Add Court <i class="fa fa-save"></i></button>
                   </div>
                </div>
            </div>
          </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<!-- for Sub court add modal -->
<div class="modal fade" id="subCourtModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" id="bg">
         <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true" style="color: red;font-size: 25px;" class="btn-default">&times; </span></button>
        <h4 class="modal-title subCourtTitle" id="subject">Add Sub Court</h4>
      </div>
      <div class="modal-body" id="bg">
          <form id="subCourt_form">
            <div class="row">
                <div class="col-md-12">
                    <!-- 1 -->
                    <div class="row">
                        <div class="col-md-3">
                            <label for="title" class="col-form-label">Select Main Court <span class="asterick">*</span></label>
                        </div>
                        <div class="col-md-9">
                            <div class="form-group">
                               <select name="mainCourtSelect" id="mainCourtSelect" class="form-control" required>
                                <option disabled selected>Please Select</option>
                                <?php
                                  $details = $objCourtAdd->get_main_courts(); 
                                  foreach ($details as $detail) {
                                   echo '<option value="'.$detail["main_court_id"].'">'.$detail["main_court_name"].'</option>';
                                  } 
                                ?>
                               </select>
                            </div>
                        </div>
                    </div>
                    <!-- 2-->
                    <div class="row">
                        <div class="col-md-3">
                            <label for="title" class="col-form-label">Sub Name <span class="asterick">*</span></label>
                        </div>
                        <div class="col-md-9">
                            <div class="form-group">
                               <input type="text" name="subCourtName" id="subCourtName" class="form-control" placeholder="Eg. labour Court" autocomplete="off" required>
                            </div>
                        </div>
                    </div>
                    <!-- 3 -->
                    <div class="row">
                        <div class="col-md-3">
                            <label for="title" class="col-form-label">Select Region <span class="asterick">*</span></label>
                        </div>
                        <div class="col-md-9">
                            <div class="form-group">
                               <select name="subCourtRegion" id="subCourtRegion" class="form-control" required>
                                <option disabled selected>Please Select</option>
                                <?php
                                  include_once('../includes/regions.html'); 
                                ?>
                               </select>
                            </div>
                        </div>
                    </div>

                    <!-- for inserting the page id -->
                    <input type="hidden" name="data_id" id="subCourt_data_id" value="">
                    <!-- for insert query -->
                    <input type="hidden" name="mode" id="subCourtMode" value="subCourtInsert">

                   <div class="modal-footer">
                      <button type="button" class="btn btn-danger" data-dismiss="modal">Close <i class="fa fa-times"></i></button>
                      <button type="submit" class="btn btn-primary" id="subCourt_btn">Add Sub Court <i class="fa fa-save"></i></button>
                   </div>
                </div>
            </div>
          </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<?php require_once('../includes/footer.php'); ?>
<!-- scripts -->
<script src="js/pageScript/court_add.js"></script>
