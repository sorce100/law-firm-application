<?php
ob_start(); 
require_once('../includes/header.php');
// refer back to dashboard if page is not in group
$retrievedFileName = basename($_SERVER['PHP_SELF']);
if (!in_array($retrievedFileName, $_SESSION['pagesAllowed'])) {
  header('Location: ../pages/dashboard.php');
}
// check if user is a staff for not
 if ($_SESSION['account_name'] != 'staff') {
  	die();
  }  

$objTasks = new Tasks;
?>



<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-list"> Tasks</i></h2>
        <!-- add new button -->
        <div class="pull-right">
            <button data-toggle="modal" data-target="#addTaskModal" class="btn btn-danger"><span class="fa fa-plus"></span> Add Task </button>
        </div>
        <!-- end new button -->
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
          <!-- creating tabs -->
          <div class="" role="tabpanel" data-example-id="togglable-tabs">
            <ul id="myTab" class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="#allTab" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">All Tasks <i class="fa fa-list"></i></a></li>
              <li role="presentation" class=""><a href="#completeTab" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Completed <i class="fa fa-check"></i></a></li>
              <li role="presentation" class=""><a href="#incompleteTab" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Incomplete <i class="fa fa-times"></i></a></li>
            </ul><br>
            <div id="myTabContent" class="tab-content">
              <!-- ////////////////////////////////////// -->
              <div role="tabpanel" class="tab-pane fade active in" id="allTab" aria-labelledby="home-tab">
               <!--  -->
                  <div class="row">
                    <!-- first col -->
                    <div class="col-md-3 col-sm-12 col-xs-12" style="background: #EBF4FA;border-right: 1px solid #172D44;">
                      <h3>Tasks Filter</h3>
                      <!--  -->
                      <div class="row">
                        <div class="col-md-12">
                          <label for="title" class="col-form-label">Show task Assigned To: </label>
                          <div class="form-group">
                            <select class="form-control" id="taskFilterAssignedTo">
                              <option value="" selected="selected">Please Select</option>
                              <?php 
                                  $Staffs = $objStaff->get_staff(); 
                                  foreach ($Staffs as $Staff) {
                                    if (empty($staff['staff_acc_user_id'])) {
                                      $firstname = $Staff["staff_firstname"];
                                      $lastname = $Staff["staff_lastname"];
                                        echo '<option value="'.trim($Staff["staff_id"]).'" id="'.strtolower(substr($firstname, 0, 1).".".$lastname).'">'.$firstname." ".$lastname.'</option>';
                                    }
                                  }
                             ?>
                            </select>
                            </div>
                        </div>
                      </div>

                      <br>
                      <!--  -->
                      <div class="row">
                        <div class="col-md-12">
                          <label for="title" class="col-form-label">Filter By Completion Status </label>
                          <div class="form-group">
                            <select class="form-control" id="taskFilterCompleteStatus">
                              <option value="" selected="selected">Please Select</option>
                              <option value="INCOMPLETE">INCOMPLETE</option>
                              <option value="COMPLETE">COMPLETE</option>
                            </select>
                            </div>
                        </div>
                      </div>
                      <!--  -->                      
                      <br>
                      <!--  -->
                      <div class="row">
                        <div class="col-md-12">
                          <label for="title" class="col-form-label">Filter By Priority </label>
                          <div class="form-group">
                            <select class="form-control" id="taskFilterPirority">
                              <option value="" selected="selected">Please Select</option>
                              <option value="No Priority">No Priority</option>
                              <option value="Low">Low</option>
                              <option value="Medium">Medium</option>
                              <option value="High">High</option>
                            </select>
                            </div>
                        </div>
                      </div>

                      <br>

                      <!--  -->
                      <div class="row">
                        <div class="col-md-12">
                          <label for="title" class="col-form-label">Filter By Case </label>
                            <div class="form-group">
                              <select class="form-control" id="taskFilterCaseId">
                              <option value="" selected="selected">Please Select</option>
                                <?php
                                    $cases = $objCourtCase->get_cases(); 
                                    if (!empty($cases)) {
                                        print_r($cases);
                                    }
                                ?>
                            </select>
                            </div>
                        </div>
                      </div>

                      <br>
                      <!--  -->
                      <div class="row">
                        <div class="col-md-6">
                          <button class="bg-el btn-block applyTaskFilter"><b>APPLY FILTER </b> <i class="fa fa-gears"></i></button>
                        </div>
                        <div class="col-md-6">
                          <button class="btn-danger btn-block clearTaskFilter"><b>CLEAR FILTER </b> <i class="fa fa-eraser"></i></button>
                        </div>
                      </div>
                      
                      <br>

                    </div>
                    <!-- second col -->
                    <div class="col-md-9 col-sm-12 col-xs-12">
                        <div class="table-responsive">
                          <table class="table table-hover tableList"> 
                            <thead>
                                <tr>
                                    <th width="12%">Status</th>
                                    <th width="26%">Task Name</th>
                                    <th width="8%">Priority</th>
                                    <th width="8%">Due Date</th>
                                    <th width="8%">Case</th>
                                    <th width="10%">Assigned To</th>
                                    <th width="8%">Added</th>
                                    <th width="16%"></th>
                                </tr>
                            </thead>
                            <tbody id="tasksDisplay">
                                 <?php
                                    $results = $objTasks->get_tasks_list(); 
                                    if (!empty($results)) {
                                     print_r($results);
                                    }
                              ?>
                            </tbody>
                          </table>
                      </div>
                    </div>

                  </div>
               <!--  -->
              </div>
              <!-- /////////////////////////////////////// -->
              <div role="tabpanel" class="tab-pane fade" id="completeTab" aria-labelledby="profile-tab">
                <!--  -->
                  <div class="table-responsive">
                      <table class="table table-hover tableList"> 
                        <thead>
                            <tr>
                                <th width="12%">Status</th>
                                <th width="28%">Task Name</th>
                                <th width="8%">Priority</th>
                                <th width="8%">Due Date</th>
                                <th width="8%">Case</th>
                                <th width="8%">Assigned To</th>
                                <th width="8%">Added</th>
                                <th width="16%"></th>
                            </tr>
                        </thead>
                        <tbody>
                             <?php
                                $results = $objTasks->get_tasks_complete_status('COMPLETE'); 
                                if (!empty($results)) {
                                 print_r($results);
                                }
                          ?>
                        </tbody>
                      </table>
                  </div>
                <!--  -->
              </div>
              <!-- ///////////////////////////////////////// -->
              <div role="tabpanel" class="tab-pane fade" id="incompleteTab" aria-labelledby="profile-tab">
                <!--  -->
                  <div class="table-responsive">
                      <table class="table table-hover tableList"> 
                        <thead>
                            <tr>
                                <th width="12%">Status</th>
                                <th width="28%">Task Name</th>
                                <th width="8%">Priority</th>
                                <th width="8%">Due Date</th>
                                <th width="8%">Case</th>
                                <th width="8%">Assigned To</th>
                                <th width="8%">Added</th>
                                <th width="16%"></th>
                            </tr>
                        </thead>
                        <tbody>
                             <?php
                                $results = $objTasks->get_tasks_complete_status('INCOMPLETE'); 
                                if (!empty($results)) {
                                 print_r($results);
                                }
                          ?>
                        </tbody>
                      </table>
                  </div>
                <!--  -->
              </div>
            </div>
          </div>
          <!-- end of tabs
          '' -->
      </div>
    </div>
  </div>
</div>




<?php include_once('../includes/footer.php'); ?>
<?php include_once('tasks_include.php'); ?>

