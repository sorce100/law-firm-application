 $(document).ready(function(){
  // FOR CLICK TO ADD FOR tasks checklist
  let i=1;  
    $('#addChecklist').click(function(){  
         i++;  
         $('.checklistDiv').append('<div class="input-group" id="row'+i+'" style="margin-top:7px;"><span class="input-group-addon"><i class="fa fa-tasks"></i></span><input class="form-control" type="text" id="taskCheckList" name="taskCheckList[]" autocomplete="off"><span class="input-group-addon btn_remove" id="'+i+'" style="background-color:#DE8280;color:#fff;"><i class="fa fa-trash"></i></span></div>');  
    });  
    $(document).on('click', '.btn_remove', function(){  
         let button_id = $(this).prop("id");   
         $('#row'+button_id+'').remove();  
    });
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  $('#addTaskModal').on('hidden.bs.modal', function () {
      $(".taskTitle").html("Add Task");
      $('.checklistDiv').html('<div class="input-group"><span class="input-group-addon"><i class="fa fa-tasks"></i></span><input class="form-control" type="text" id="taskCheckList" name="taskCheckList[]" autocomplete="off"><span class="input-group-addon "><i class="fa fa-trash"></i></span></div>');
      $("#task_data_id").val("");
      $("#taskMode").val("insert");
      $("#task_form")[0].reset();
      $("#taskBtn").replaceWith('<button type="submit" class="btn btn-primary" id="taskBtn">Save Task <i class="fa fa-floppy-o"></i></button>');
      $('#task_form').parsley().reset();
      $('.taskSelect2').val(null).trigger('change');
    });


  $(".taskSelect2").select2({
      dropdownParent: $("#addTaskModal")
    });
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   // parsley
   $('#task_form').parsley();
   $('#task_form').parsley().reset();

  //for inserting 
    $("#task_form").submit(function(e){
      e.preventDefault();
          $.ajax({
              url:"../controllers/taskController.php",
              method:"POST",
              data:$("#task_form").serialize(),
              beforeSend:function(){  
                $('#taskBtn').text("Loading ...").prop("disabled",true); 
              },
              success:function(results){
                // console.log(results);
                  $("#addTaskModal").modal("hide");
                  $("#taskBtn").replaceWith('<button type="submit" class="btn btn-primary" id="taskBtn">Save Task <i class="fa fa-floppy-o"></i></button>');

                  switch(results){
                    case 'success':
                      get_all();
                      toastr.success('Deleted Successfully');
                    break;
                    case 'error':
                      get_all();
                      toastr.error('There was an error');
                    break;
                    default:
                    toastr.error('There was an error');
                  }
              } 

          });  
      });
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

      // for update
      $(document).on('click', '.update_task', function(e){
        e.preventDefault();
         let mode= "updateModal"; 
         let data_id = $(this).prop("id");
          // clear checklist div before updating
          $('.checklistDiv').html(''); 
         $.ajax({  
              url:"../controllers/taskController.php",
              method:"POST",  
              data:{data_id:data_id,mode:mode},  
              success:function(results){
                // console.log(results);
                   let jsonObj = JSON.parse(results);  
                   // changing modal title

                    $(".taskTitle").html("Update Task");
                    $("#taskCourtCase").val(jsonObj[0].court_case_id).trigger('change');
                    // $("#taskCourtCase option[value!='"+jsonObj[0].court_case_id+"']").prop('readonly',true);
                    $("#taskName").val(jsonObj[0].task_name);
                    $("#taskDueDate").val(jsonObj[0].task_due_date);
                    ///////////////////////////////////////////////////////////
                    // add checklist
                    //////////////////////////////////////////////////////////
                    let jsonObjChecklist = JSON.parse(jsonObj[0].task_checklist);
                    for (let i = 0; i < jsonObjChecklist.length; i++) {
                      // console.log(jsonObjChecklist[i]);
                      $('.checklistDiv').append('<div class="input-group" id="row'+i+'" style="margin-top:7px;">'+
                        '<span class="input-group-addon"><i class="fa fa-tasks"></i></span>'+
                        '<input class="form-control" type="text" value="'+jsonObjChecklist[i]+'" id="taskCheckList" name="taskCheckList[]" autocomplete="off">'+
                        '<span class="input-group-addon btn_remove" id="'+i+'" style="background-color:#DE8280;color:#fff;"><i class="fa fa-trash"></i></span></div>');
                    }
                    
                    ///////////////////////////////////////////////////////////
                    $("#taskPriority").val(jsonObj[0].task_priority);
                    $("#taskDescription").val(jsonObj[0].task_description);
                    //////////////////////////////////////////////////////////
                    // check staff
                    let jsonObjStaffIds = JSON.parse(jsonObj[0].task_assign_staff);
                     for (let i = 0; i < jsonObjStaffIds.length; ++i) {

                        $('.taskAssignStaff[value="'+jsonObjStaffIds[i]+'"]').each(function() {
                          $(this).prop('checked',true);
                        });
                     }
                    //////////////////////////////////////////////////
                    $("#task_data_id").val(jsonObj[0].task_id);
                    $("#taskBtn").text("Update Tasks");
                    $("#taskMode").val("update");
                    $("#addTaskModal").modal("show");

                    
              }  
             });  
        });
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
      
// for delete
        $(document).on('click', '.del_data', function(e){
          e.preventDefault();
           if (confirm("ARE YOU SURE YOU WANT TO PROCEED?")) {
                 let mode= "delete"; 
                 let data_id = $(this).prop("id");  
                 $.ajax({  
                      url:"../controllers/taskController.php",  
                      method:"POST",  
                      data:{data_id:data_id,mode:mode},  
                      success:function(results){
                         switch(results){
                          case 'success':
                            get_all();
                            toastr.success('Deleted Successfully');
                          break;
                          case 'error':
                            get_all();
                            toastr.error('There was an error');
                          break;
                          default:
                          toastr.error('There was an error');
                        }

                      }  
                     }); 

             }else{
              return false;
            }  
        });
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// for marking task complete 
       $(document).on('click', '.MarkComplete', function(e){
          e.preventDefault();
           if (confirm("Mark Task Complete ?")) {
                let mode= "taskComplete"; 
                 let data_id = $(this).prop("id");  
                 $.ajax({  
                    url:"../controllers/taskController.php",  
                    method:"POST",  
                    data:{data_id:data_id,mode:mode},  
                    success:function(results){
                      // console.log(results);
                      switch(results){
                        case 'success':
                          get_all();
                          toastr.success('Deleted Successfully');
                        break;
                        case 'error':
                          get_all();
                          toastr.error('There was an error');
                        break;
                        default:
                        toastr.error('There was an error');
                      }

                    }  
                   }); 
            }else{
              return false;
            }
       });
      ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
      // click to view task
      $(document).on('click', '.view_task', function(e){
        e.preventDefault();

         let mode= "updateModal"; 
         let data_id = $(this).prop("id");
          // clear checklist div before updating
          $('.checklistDiv').html(''); 
         $.ajax({  
              url:"../controllers/taskController.php",
              method:"POST",  
              data:{data_id:data_id,mode:mode},  
              success:function(results){
                // console.log(results);
                   let jsonObj = JSON.parse(results);  
                   // changing modal title

                    $(".taskTitle").html("View Assigned Task");
                    $("#taskCourtCase").val(jsonObj[0].court_case_id).trigger('change');
                    // $("#taskCourtCase option[value!='"+jsonObj[0].court_case_id+"']").prop('readonly',true);
                    $("#taskName").val(jsonObj[0].task_name).prop('readonly',true);
                    $("#taskDueDate").val(jsonObj[0].task_due_date);
                    ///////////////////////////////////////////////////////////
                    // add checklist
                    //////////////////////////////////////////////////////////
                    let jsonObjChecklist = JSON.parse(jsonObj[0].task_checklist);
                    for (let i = 0; i < jsonObjChecklist.length; i++) {
                      // console.log(jsonObjChecklist[i]);
                      $('.checklistDiv').append('<div class="input-group" id="row'+i+'" style="margin-top:7px;">'+
                        '<span class="input-group-addon"><i class="fa fa-tasks"></i></span>'+
                        '<input class="form-control" type="text" value="'+jsonObjChecklist[i]+'" id="taskCheckList" name="taskCheckList[]" autocomplete="off" readonly>'+
                        '<span class="input-group-addon btn_remove" id="'+i+'" style="background-color:#DE8280;color:#fff;"><i class="fa fa-trash"></i></span></div>');
                    }
                    
                    ///////////////////////////////////////////////////////////
                    $("#taskPriority").val(jsonObj[0].task_priority);
                    $("#taskDescription").val(jsonObj[0].task_description);
                    //////////////////////////////////////////////////////////
                    // check staff
                    let jsonObjStaffIds = JSON.parse(jsonObj[0].task_assign_staff);
                     for (let i = 0; i < jsonObjStaffIds.length; ++i) {

                        $('.taskAssignStaff[value="'+jsonObjStaffIds[i]+'"]').each(function() {
                          $(this).prop('checked',true);
                        });
                     }
                    //////////////////////////////////////////////////
                    $("#task_data_id").val("");
                    $("#taskBtn").replaceWith('<span id="taskBtn"></span>');
                    $("#taskMode").val("");
                    $("#addTaskModal").modal("show");

                    
              }  
             });  
        });

    // ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // ////////////////////////////// FILTER TASKS //////////////////////////////////////////////////////////////////////// 
        $(document).on('click', '.applyTaskFilter', function(e){
             e.preventDefault();
             let mode = "filterTasks"; 
             let taskFilterAssignedTo = $('option:selected', '#taskFilterAssignedTo').val();
             let taskFilterCompleteStatus = $('option:selected', '#taskFilterCompleteStatus').val();
             let taskFilterPirority = $('option:selected', '#taskFilterPirority').val();
             let taskFilterCaseId = $('option:selected', '#taskFilterCaseId').val();
             if (taskFilterAssignedTo !=="" || taskFilterPirority !=="" || taskFilterCaseId !=="" || taskFilterCompleteStatus !==""  ) {
              $.ajax({  
                url:"../controllers/taskController.php",  
                method:"POST",  
                data:{mode:mode,taskFilterAssignedTo:taskFilterAssignedTo,taskFilterCompleteStatus:taskFilterCompleteStatus,taskFilterPirority:taskFilterPirority,taskFilterCaseId:taskFilterCaseId},  
                success:function(results){
                  console.log(results);
                  switch(results){
                    case 'error':
                      get_all();
                      toastr.error('There was an error');
                    break;
                    default:
                      // $('#tasksDisplay').html('');
                      $('#tasksDisplay').html(results);
                  }

                }  
               });
             }
             else{
              toastr.error('Sorry, Filter cannot be empty');
             } 
        });



      // clear filter fields
      $(document).on('click', '.clearTaskFilter', function(e){
         e.preventDefault();
         let mode = "filterTasks";
         $('#taskFilterAssignedTo option:first').prop('selected',true);
         $('#taskFilterCompleteStatus option:first').prop('selected',true);
         $('#taskFilterPirority option:first').prop('selected',true);
         $('#taskFilterCaseId option:first').prop('selected',true);
         get_all(); 
      });



    // ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // get all data 
         function get_all(){
            let mode = "getAll";
            $.ajax({  
              url:"../controllers/taskController.php",  
              method:"POST",  
              data:{mode:mode},  
              success:function(results){
                $('#tasksDisplay').html('');
                $('#tasksDisplay').html(results);

              }  
            }); 
         }
 });