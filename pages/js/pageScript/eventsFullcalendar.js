 $(document).ready(function() {
   var calendar = $('#calendarDisplay').fullCalendar({
    editable:true,
    header:{
     left:'prev,next,today',
     center:'title',
     right:'month,agendaWeek,agendaDay'
    },
    events: '../dataLoad/load_fullcalendar.php',
    // eventColor: '#D9534F',

    selectable:true,
    selectHelper:true,
    select: function(start, end, allDay){ 
      // update of modal date and time
      var start = $.fullCalendar.formatDate(start, "Y-MM-DD HH:mm:ss");
      var end = $.fullCalendar.formatDate(end, "Y-MM-DD HH:mm:ss");
      $('#eventStartDate').val(start);
      $('#eventEndDate').val(end);
      // insert select case into staff and contacts select
      $('#caseContactsDisplay,#caseStaffsDisplay').html("<tr><td>Select Case</td></tr>");

     $("#eventModal").modal("show");
    },
///////////////////////////////////////////////////////////////////////////////////////

    editable:false,
    eventResize:function(event){
     var startDate = $.fullCalendar.formatDate(event.start, "Y-MM-DD HH:mm:ss");
     var endDate = $.fullCalendar.formatDate(event.end, "Y-MM-DD HH:mm:ss");
     var eventId = event.id;
     var mode = "updateModal";
     $.ajax({  
        url:"../../controllers/eventsController.php",
        method:"POST",  
        data:{data_id:eventId,mode:mode},
        success:function(results){
          // console.log(results);
            let jsonObj = JSON.parse(results);  
             // changing modal title
            $(".modal-title").html("Update Event");
            $("#courtCaseId").val(jsonObj[0].event_court_case_id).trigger('change').prop('disabled',true);
            $("#eventName").val(jsonObj[0].event_name);
            $("#eventStartDate").val(startDate);
            $("#eventEndDate").val(endDate);
            $("#eventLocation").val(jsonObj[0].event_location);
            $("#eventDescription").val(jsonObj[0].event_description);

            let jsonObjeventContactShare = JSON.parse(jsonObj[0].event_contact_share);
              for (var i = 0; i < jsonObjeventContactShare.length; ++i) {
                   $('#eventContactShare[value="'+jsonObjeventContactShare[i]+'"]').each(function() {
                    $(this).prop('checked',true);
                });
              }
            let jsonObjeventContactAttend = JSON.parse(jsonObj[0].event_contact_attend);
            for (let i = 0; i < jsonObjeventContactAttend.length; ++i) {
                 $('#eventContactAttend[value="'+jsonObjeventContactAttend[i]+'"]').each(function() {
                  $(this).prop('checked',true);
                });
              }
            let jsonObjeventStaffShare = JSON.parse(jsonObj[0].event_staff_share);
            for (let i = 0; i < jsonObjeventStaffShare.length; ++i) {
                 $('#eventStaffShare[value="'+jsonObjeventStaffShare[i]+'"]').each(function() {
                  $(this).prop('checked',true);
                });
              }
            let jsonObjeventStaffAttend = JSON.parse(jsonObj[0].event_staff_attend);
            for (let i = 0; i < jsonObjeventStaffAttend.length; ++i) {
                 $('#eventStaffAttend[value="'+jsonObjeventStaffAttend[i]+'"]').each(function() {
                  $(this).prop('checked',true);
                });
              }

            $("#eventMode").val("update");
            $("#event_data_id").val(eventId);
            $("#eventBtn").text("Update Event");
            
            $("#eventModal").modal("show");
        }  
       });
    },

    eventDrop:function(event){
     var start = $.fullCalendar.formatDate(event.start, "Y-MM-DD HH:mm:ss");
     var end = $.fullCalendar.formatDate(event.end, "Y-MM-DD HH:mm:ss");
     var eventId = event.id;

     var event_mode = "event_drop_update";
     $.ajax({
      url:"../../controllers/eventsController.php",
      type:"POST",
      data:{mode:event_mode, eventStartDate:start, eventEndDate:end, data_id:eventId},
      success:function()
      {
       calendar.fullCalendar('refetchEvents');
       toastr.success('Updated Successfully');
      }
     });
    },

    // eventClick:function(event){
    //  if(confirm("Are you sure you want to remove Event?")){
    //   var eventId = event.id;
    //   var event_mode = "delete";
    //   $.ajax({
    //    url:"../../controllers/eventsController.php",
    //    type:"POST",
    //    data:{mode:event_mode,data_id:eventId},
    //    success:function()
    //    {
    //     calendar.fullCalendar('refetchEvents');
    //     toastr.success('Event Removed Successfully');
    //    }
    //   })
    //  }
    // }

   });


});