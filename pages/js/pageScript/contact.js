 $(document).ready(function(){
  /////////////////////////////////////////////////////////////////////////////////////////////
    $('#contactPortal').bootstrapToggle({
      on: 'Enable',
      off: 'Disable',
      onstyle: 'success',
      offstyle: 'danger'
    });
   $('#contactPortal').change(function(){
      if($(this).prop('checked')){
       $('#contactPortal_log').val('YES');
      }else {
       $('#contactPortal_log').val('NO');
      }
   });
  /////////////////////////////////////////////////////////////////////////////////////////////

  $(".companySelect2").select2({
      dropdownParent: $("#addContactModal")
    });
   $(".groupSelect2").select2({
      dropdownParent: $("#addContactModal")
    });
   // parsley
   $('#contact_form').parsley();
   $('#contact_form').parsley().reset();

    $('#addContactModal').on('hidden.bs.modal', function () {
      $(".clientSub").html("Add Contact");
      $(this).find('input:text').css({"border-color":"#808080"});
      $("#contact_form")[0].reset();
      $("#contactsBtn").attr('disabled',false).text("Save Contact");
      $('#contact_form').parsley().reset();
      $('.companySelect2').val(null).trigger('change');
      $('.groupSelect2').val(null).trigger('change');
    });
      //for inserting 
        $("#contact_form").submit(function(e){
            e.preventDefault();
              $.ajax({
              url:"../controllers/contactController.php",
              method:"POST",
              data:$("#contact_form").serialize(),
              beforeSend:function(){  
                    $('#contactsBtn').text("Loading ...").prop("disabled",true); 
               },
              success:function(results){
                // console.log(results);
                    $("#addContactModal").modal("hide");
                   
                   if (results == "success") {
                        get_all();
                        toastr.success('Saved Successfully');
                        $("#contact_form")[0].reset();
                        $('#contactsBtn').attr('disabled',false).text('Save Contact');
                   }
                   else if (results == "error"){
                      toastr.error('There was an error');
                      $("#addContactModal").modal("hide");
                      $("#contact_form")[0].reset();

                   }
              } 

              });  
          });

      // for update
      $(document).on('click', '.contact_update_data', function(){
         let mode= "updateModal"; 
         let data_id = $(this).prop("id");  
         $.ajax({  
              url:"../controllers/contactController.php",
              method:"POST",  
              data:{contactData_id:data_id,contactMode:mode},  
              success:function(results){
                // alert(results);
                   let jsonObj = JSON.parse(results);  
                   // changing modal title
                    $(".clientSub").html("Update Contact");
                    $("#contactFirstName").val(jsonObj[0].contact_First_name);
                    $("#contactMidName").val(jsonObj[0].contact_mid_name);
                    $("#contactLastName").val(jsonObj[0].contact_last_name);
                    $("#contactEmail").val(jsonObj[0].contact_email);
                    $("#contactCompany").val(jsonObj[0].contact_company).trigger('change');
                    $("#contactGroup").val(jsonObj[0].contact_group_id).trigger('change');
                    
                    if(jsonObj[0].contact_portal == "off"){
                      $('#contactPortal').prop('checked', false).change()
                    }
                    $("#contactCell").val(jsonObj[0].contact_cell);
                    $("#contactWrkPhone").val(jsonObj[0].contact_wrk_phone);
                    $("#contactAddress").val(jsonObj[0].contact_address);
                    $("#contactCity").val(jsonObj[0].contact_city);
                    $("#contactRegion").val(jsonObj[0].contact_region);
                    $("#contactCountry").val(jsonObj[0].contact_country);
                    $("#contactData_id").val(jsonObj[0].contact_id);
                    $("#contactsBtn").text("Update Contact");
                    $("#contactMode").val("update");
                    $("#addContactModal").modal("show");
              }  
             });  
        });

      
// for delete
        $(document).on('click', '.contact_del_data', function(){
           if (confirm("ARE YOU SURE YOU WANT TO PROCEED?")) {
                 let mode= "delete"; 
                 let data_id = $(this).prop("id");  
                 $.ajax({  
                      url:"../controllers/contactController.php",  
                      method:"POST",  
                      data:{contactData_id:data_id,contactMode:mode},  
                      success:function(results){
                        console.log(results);
                        if (results == "success") {
                            get_all();
                            toastr.success('Deleted Successfully');
                        }
                        else if (results == "error"){
                            toastr.error('There was an error');
                        }
                      }  
                     }); 

             }else{
              return false;
            }  
        });
    // get all data 
         function get_all(){
            let mode = "getAll";
            $.ajax({  
              url:"../controllers/contactController.php",  
              method:"POST",  
              data:{contactMode:mode},  
              success:function(results){
                $('#contactResultsDisplay').html('');
                let jsonObj = JSON.parse(results);
                for (let i = 0; i < jsonObj.length; i++) {
                    $('#contactResultsDisplay').append('<tr><td>'+jsonObj[i].contact_First_name+'</td><td>'+jsonObj[i].contact_last_name+'</td><td>'+jsonObj[i].contact_group_id+'</td><td>'+jsonObj[i].added+'</td><td><button class="btn-primary contact_update_data" id="'+jsonObj[i].contact_id+'"><i class="fa fa-pencil"></i></button> <button class="btn-danger contact_del_data" id="'+jsonObj[i].contact_id+'"><i class="fa fa-trash"></i></button></td></tr>');
                }
              }  
            }); 
         }
 });