 $(document).ready(function(){
  /////////////////////////////////////////////////////////////////////////////////////////////
    $('#timeEntryBillable').bootstrapToggle({
      on: 'Enable',
      off: 'Disable',
      onstyle: 'success',
      offstyle: 'danger'
    });
   $('#timeEntryBillable').change(function(){
      if($(this).prop('checked')){
       $('#timeEntryBillable_log').val('YES');
      }else {
       $('#timeEntryBillable_log').val('NO');
      }
   });
  /////////////////////////////////////////////////////////////////////////////////////////////


  $(".timeEntryselect2").select2({
      dropdownParent: $("#timeEnteriesModal")
    });
  // timer case select
  $(".Timerselect2").select2({
      dropdownParent: $("#timerModal")
    });

  ///////////////////////////////////////////////////////
  // button toggle
  // active only staff using id
  $('#timeEntryStaff').val($('#accountID').val()).prop('disabled',true);
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   // parsley
   // $('#timeEnteries_form').parsley();
   // $('#timeEnteries_form').parsley().reset();
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // timer count add to time entries
  // $('#timerAdd_btn').click(function(){
  $(document).on('click', '#timerAdd_btn', function(){
    let caseId = $('#timerCase option:selected').prop("value");
    let description = $('#timerNote').val();

    let timerValue = $('.timerCount').html();
    let timerValueSplit = timerValue.split(':');
    var timerValueSeconds = (+timerValueSplit[0]) * 60 * 60 + (+timerValueSplit[1]) * 60 + (+timerValueSplit[2]);
    var timerPerhour = (timerValueSeconds/3600).toFixed(1);
// insert values from timer modal
    $('#timeEntriesCourtCaseId').val(caseId).trigger('change');;
    $('#timeEntryDescribe').val(description);
     $('#timeEntryDuration').val(timerPerhour);

    $("#timeEnteriesModal").modal("show");
  });
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#timeEnteriesModal').on('hidden.bs.modal', function () {
      $("#timerSubject").html("Add Time Entry");
      $(this).find('input:text').css({"border-color":"#808080"});
      $("#timeEnteries_form")[0].reset();
      $("#timeEntry_btn").attr('disabled',false).text("Add");
      $('#timeEnteries_form').parsley().reset();
      $('.timeEntryselect2').val(null).trigger('change');
      $('#timeEntryStaff').val($('#accountId').val()).trigger('change');
    });
      //for inserting 
        $("#timeEnteries_form").submit(function(e){
            e.preventDefault();

              $('#timeEntryStaff').prop('disabled',false);
              $.ajax({
              url:"../controllers/billTimeEntryController.php",
              method:"POST",
              data:$("#timeEnteries_form").serialize(),
              beforeSend:function(){  
                        $('#timeEntry_btn').text("Loading ...").prop("disabled",true); 
                   },
              success:function(results){
                // console.log(results);
                    $("#timeEnteriesModal").modal("hide");
                   if (results == "success") {
                        toastr.success('Saved Successfully');
                        $("#timeEnteries_form")[0].reset();
                        $('#timeEntry_btn').attr('disabled',false).text('Save Contact');
                        // hide timercount modal when successful and clear local storage
                         // localStorage.clear();
                         $("#timerCase").val('');
                         $("#timerNote").html('');
                         $("#timerModal").modal("hide");
                         get_all();
                   }
                   else if (results == "error"){
                      get_all();
                      toastr.error('There was an error');
                      $("#timeEnteriesModal").modal("hide");
                      $("#timeEnteries_form")[0].reset();

                   }
              } 

              });  
          });

      // for update
      $(document).on('click', '.timeEntryupdate_data', function(){
         let mode= "updateModal"; 
         let data_id = $(this).prop("id");  
         $.ajax({  
              url:"../controllers/billTimeEntryController.php",
              method:"POST",  
              data:{data_id:data_id,mode:mode},  
              success:function(results){
                
                   let jsonObj = JSON.parse(results);  
                   // changing modal title
                    $("#timerSubject").html("Update Time Entry");
                    
                    $("#timeEntriesCourtCaseId").val(jsonObj[0].time_entry_case_id).trigger('change');
                    $('#timeEntryStaff').val(jsonObj[0].time_entry_staff_id).prop('disabled',true);
                    $("#timeEntryActivity").val(jsonObj[0].time_entry_activity);
                    if(jsonObj[0].time_entry_billable == "off"){
                      $('#timeEntryBillable').prop('checked', false).change()
                    }
                    $("#timeEntryDescribe").val(jsonObj[0].time_entry_describe);
                    $("#timeEntryDate").val(jsonObj[0].time_entry_date  );
                    $("#timeEntryRate").val(jsonObj[0].time_entry_rate);
                    $("#timeEntryRateType").val(jsonObj[0].time_entry_rate_type);
                    $("#timeEntryDuration").val(jsonObj[0].time_entry_duration);
                    $("#timeEntryInvoiced").val(jsonObj[0].time_entry_invoiced);
                    $("#timeEntry_btn").text("View Time Entry");
                    $("#timeEnteriesModal").modal("show");
              }  
             });  
        });
// data view
  $(document).on('click', '.timeEntryview', function(){
         let mode= "updateModal"; 
         let data_id = $(this).prop("id");  
         $.ajax({  
              url:"../controllers/billTimeEntryController.php",
              method:"POST",  
              data:{data_id:data_id,mode:mode},  
              success:function(results){
                
                   let jsonObj = JSON.parse(results);  
                   // changing modal title
                    $("#timerSubject").html("View Time Entry");
                    
                    $("#timeEntriesCourtCaseId").val(jsonObj[0].time_entry_case_id).trigger('change');
                    $('#timeEntryStaff').val(jsonObj[0].time_entry_staff_id).prop('disabled',true);
                    $("#timeEntryActivity").val(jsonObj[0].time_entry_activity);
                    if(jsonObj[0].time_entry_billable == "off"){
                      $('#timeEntryBillable').prop('checked', false).change()
                    }
                    $("#timeEntryDescribe").val(jsonObj[0].time_entry_describe);
                    $("#timeEntryDate").val(jsonObj[0].time_entry_date  );
                    $("#timeEntryRate").val(jsonObj[0].time_entry_rate);
                    $("#timeEntryRateType").val(jsonObj[0].time_entry_rate_type);
                    $("#timeEntryDuration").val(jsonObj[0].time_entry_duration);
                    $("#timeEntryInvoiced").val(jsonObj[0].time_entry_invoiced);
                    $("#timeEntry_data_id").val(jsonObj[0].time_entry_id);
                    $("#timeEntry_btn").remove();
                    $("#timeEnteriesModal").modal("show");
              }  
             });  
        });
      
// for delete
        $(document).on('click', '.del_data', function(){
           if (confirm("ARE YOU SURE YOU WANT TO PROCEED?")) {
                 let mode= "delete"; 
                 let data_id = $(this).prop("id");  
                 $.ajax({  
                      url:"../controllers/billTimeEntryController.php",  
                      method:"POST",  
                      data:{data_id:data_id,mode:mode},  
                      success:function(results){
                        console.log(results);
                        if (results == "success") {
                            get_all();
                            toastr.success('Deleted Successfully');
                        }
                        else if (results == "error"){
                            get_all();
                            toastr.error('There was an error');
                        }
                      }  
                     }); 

             }else{
              return false;
            }  
        });
  // get all data 
         function get_all(){
            let mode = "getAll";
            $.ajax({  
              url:"../controllers/billTimeEntryController.php",  
              method:"POST",  
              data:{mode:mode},  
              success:function(results){
                // console.log(results);
                $('#timeEntriesDisplay').html('');
                let jsonObj = JSON.parse(results);
                for (let i = 0; i < jsonObj.length; i++) {
                    $('#timeEntriesDisplay').append('<tr><td>'+jsonObj[i].caseName+'</td><td>'+jsonObj[i].time_entry_date+'</td><td>'+jsonObj[i].time_entry_activity+'</td><td>'+jsonObj[i].time_entry_duration+'</td><td>'+jsonObj[i].time_entry_rate+'</td><td>'+jsonObj[i].time_entry_total+'</td><td>'+jsonObj[i].time_entry_invoiced+'</td><td>'+jsonObj[i].time_entry_staff_id+'</td><td><button class="btn-primary timeEntryupdate_data" id="'+jsonObj[i].time_entry_id+'"><i class="fa fa-pencil"></i></button> <button class="btn-danger del_data" id="'+jsonObj[i].time_entry_id+'"><i class="fa fa-trash"></i></button></td></tr>');
                }
              }  
            }); 
         }
 });