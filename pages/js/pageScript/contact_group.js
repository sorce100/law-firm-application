 $(document).ready(function(){
      $('#contactGroup_form').parsley();
      $('#contactGroup_form').parsley().reset();
        // for reset modal when close
      $('#contactGroupModal').on('hidden.bs.modal', function () {
          $(".groupSub").html("Add Contact Group");
          $("#grpData_id").val("");
          $("#grpMode").val("insert");
          $(this).find('input:text').css({"border-color":"#808080"});
          $("#contactGroup_form")[0].reset();
          $("#save_btn").text("Add Page");
          $('#contactGroup_form').parsley().reset();
        });

      //for inserting 
        $("#contactGroup_form").submit(function(e){
            e.preventDefault();
            $(this).find('input[type="text"]').each(function() {
              if( $(this).val() == "" ) {
                e.preventDefault();
                $(this).css({"border-color":"#f35b3f"});
                return false;
              }
              else {
                $(this).css({"border-color":"#808080"});
              }
            });
            // fields validation

              $.ajax({
              url:"../controllers/contactGroupsController.php",
              method:"POST",
              data:$("#contactGroup_form").serialize(),
              beforeSend:function(){  
                        $('#groupBtn').text("Loading ...").prop("disabled",true); 
                   },
              success:function(results){
                   $('#groupBtn').text("Add Contact Group").prop("disabled",false);  
                   $("#contactGroupModal").modal("hide");
                   
                   if (results == "success") {
                        get_all();
                        toastr.success('Saved Successfully');
                        $("#contactGroup_form")[0].reset();
                        $('#userssave_btn').attr('disabled',false).text('Create Account');
                   }
                   else if (results == "error"){
                      toastr.error('There was an error');
                      $("#contactGroupModal").modal("hide");
                      $("#contactGroup_form")[0].reset();

                   }
              } 

              });  
          });

      // for update
      $(document).on('click', '.grp_update_data', function(){
         var mode= "updateModal"; 
         var data_id = $(this).prop("id");  
         $.ajax({  
              url:"../controllers/contactGroupsController.php",
              method:"POST",  
              data:{grpData_id:data_id,grpMode:mode},  
              success:function(results){
                // alert(results);
                   var jsonObj = JSON.parse(results);  
                   // changing modal title
                  $(".groupSub").html("Update Contact Group");
                  $("#contactGroupName").val(jsonObj[0].contact_group_name);
                  $("#grpData_id").val(jsonObj[0].contact_group_id);
                  $("#groupBtn").text("Update Group");
                  $("#grpMode").val("update");
                  $("#contactGroupModal").modal("show");
              }  
             });  
        });
// for delete
        $(document).on('click', '.grp_del_data', function(){
           if (confirm("ARE YOU SURE YOU WANT TO PROCEED?")) {
               
                 var mode= "delete"; 
                 var data_id = $(this).prop("id");  
                 $.ajax({  
                      url:"../controllers/contactGroupsController.php",  
                      method:"POST",  
                      data:{grpData_id:data_id,grpMode:mode},  
                      success:function(results){
                       if (results == "success") {
                            get_all();
                            toastr.success('Deleted Successfully');
                        }
                       else if (results == "error"){
                            toastr.error('There was an error');
                        }
                      }  
                  }); 

             }else{
              return false;
            }  
        });
      // get all data 
         function get_all(){
            var mode = "getAll";
            $.ajax({  
              url:"../controllers/contactGroupsController.php",  
              method:"POST",  
              data:{grpMode:mode},  
              success:function(results){
                $('#grpResultsDisplay').html('');
                var jsonObj = JSON.parse(results);
                for (var i = 0; i < jsonObj.length; i++) {
                    $('#grpResultsDisplay').append('<tr><td>'+jsonObj[i].contact_group_name+'</td><td></td><td>'+jsonObj[i].added+'</td><td><button class="btn-primary grp_update_data" id="'+jsonObj[i].contact_group_id+'"><i class="fa fa-pencil"></i></button> <button class="btn-danger grp_del_data" id="'+jsonObj[i].contact_group_id+'"><i class="fa fa-trash"></i></button></td></tr>');
                }
              }  
            }); 
         }
});