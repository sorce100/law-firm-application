 $(document).ready(function(){
  // check filesize
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // click to upload more files
      let max_fields      = 21; //maximum input boxes allowed
      let wrapper         = $(".input_fields_wrap"); //Fields wrapper
      let add_button      = $(".add_field_button"); //Add button ID
      
      let x = 1; //initlal text box count
      
      $(add_button).click(function(e){ //on add input button click
          e.preventDefault();
          if(x < max_fields){ //max input box allowed
              x++; //text box increment
              $(wrapper).append('<div class="input-group" style="margin-top:7px;"><input type="file" class="form-control" id="file_upload_btn" name="file_array[]" multiple><span class="input-group-addon remove_field" style="background-color:#DE8280;color:#fff;"><i class="fa fa-trash"></i></span></div>'); //add input box
          }
      });
      
      $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
          e.preventDefault(); $(this).parent('div').remove(); x--;;
      });
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // for changing foldername for case document select
    $('#documentCourtCaseId').change(function(){
       $("#docFolderName").val($('option:selected', this).attr('alt'));
    });

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    $(".documentSelect2").select2({
      dropdownParent: $("#addDocumentModal")
    });
    
   // parsley
     $('#document_form').parsley();
     $('#document_form').parsley().reset();

        // for reset modal when close
      $('#addDocumentModal').on('hidden.bs.modal', function () {
          $(".documentTitle").html("Add Case Documents");
          $("#documentMode").val("insert");
          $(".input_fields_wrap").html('');
          $("#docFolderName").val('No Case Selected');
          $("#document_form")[0].reset();
          $('#documentBtn').replaceWith('<button type="submit" class="btn btn-primary" id="documentBtn">Save Document <i class="fa fa-floppy-o"></i></button>');
          // clear dropify and parsley
          $(".dropify-clear").click();
          $('#document_form').parsley().reset();
          // clear select2
          $('.documentSelect2').val(null).trigger('change');
          // reset progress bar
           $('#progressBar').prop('aria-valuemax',0).css('width',0 + '%').text(0 + '%');
      });
        

      //for inserting 
        $("#document_form").submit(function(e){
            e.preventDefault();
              $("#documentCourtCaseId").prop('disabled',false);
              $.ajax({
                // for progress bar
                xhr:function(){
                  let xhr = new XMLHttpRequest();
                  xhr.upload.addEventListener('progress',function(e){
                    // check if upload length is true or false
                    if (e.lengthComputable) {
                      let uploadPercent = Math.round((e.loaded/e.total)*100);
                      // updating progress bar pecentage
                      $('#progressBar').prop('aria-valuemax',uploadPercent).css('width',uploadPercent + '%').text(uploadPercent + '%');
                    }
                  });
                  return xhr;
                },
                url:"../controllers/documentController.php",
                method:"POST",
                enctype: 'multipart/form-data',
                data:new FormData(this),  
                contentType:false,  
                processData:false,
                beforeSend:function(){  
                  $('#documentBtn').prop('disabled','disabled').text('Loading...');  
                },
                success:function(results){  
                // console.log(results);
                   $("#addDocumentModal").modal("hide");

                    switch(results){
                        case 'success':
                          get_all();
                          toastr.success('Saved Successfully');
                          $("#document_form")[0].reset();
                          $('#documentBtn').replaceWith('<button type="submit" class="btn btn-primary" id="documentBtn">Save Document <i class="fa fa-floppy-o"></i></button>');
                        break;
                        case 'error':
                          get_all();
                          toastr.error('There was an error');
                          $("#addDocumentModal").modal("hide");
                          $("#document_form")[0].reset();
                          $('#documentBtn').replaceWith('<button type="submit" class="btn btn-primary" id="documentBtn">Save Document <i class="fa fa-floppy-o"></i></button>');  
                        break;
                        default:
                        toastr.error('There was an error');
                  }
                } 

              });  
          });

      // for update
      $(document).on('click', '.update_docs', function(){
         let mode = "updateModal"; 
         let data_id = $(this).prop("id");
         // disable select option 
         $("#documentCourtCaseId").prop('disabled',true); 
         $('.input_fields_wrap').html('');
         $.ajax({  
              url:"../controllers/documentController.php",
              method:"POST",  
              data:{data_id:data_id,mode:mode},  
              success:function(results){
                // console.log(results);
                  let jsonObj = JSON.parse(results);  
                   // changing modal title
                  $(".modal-title").html("Update Case Documents");
                  $("#documentCourtCaseId").val(jsonObj.doc_court_case_id).trigger('change');
                  $("#docFolderName").val(jsonObj.doc_folder_name);
                  $("#docAssignedDate").val(jsonObj.doc_assigned_date);
                  $("#docDescription").html(jsonObj.doc_description);
                  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                  // add files to add files div
                  
                  if (jsonObj.doc_files_uploaded!='') {
                    let jsonObjFiles = JSON.parse(jsonObj.doc_files_uploaded);
                    for (let i = 0; i < jsonObjFiles.length; i++) {
                      $('.input_fields_wrap').append('<div class="input-group" style="margin-top:7px;">'+
                        '<input type="text" class="form-control" id="file_upload_btn" value="'+jsonObjFiles[i]+'" name="file_array[]" readonly>'+
                        '<span class="input-group-addon remove_field deleteFile" id="'+jsonObjFiles[i]+'" style="background-color:#DE8280;color:#fff;"><i class="fa fa-trash"></i></span></div>');
                    }
                  }
                  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                  $("#document_data_id").val(jsonObj.case_doc_id);
                  $("#documentsArray").val(jsonObj.doc_files_uploaded); 
                  $("#folderName").val(jsonObj.doc_folder_name); 
                  $("#documentMode").val('update');
                 
                  $("#documentBtn").text("Update Docs");
                  $("#addDocumentModal").modal("show");
              }  
             });  
        });

// remove files
        $(document).on('click', '.deleteFile', function(){
           if (confirm("ARE YOU SURE YOU WANT TO PROCEED?")) {
                  let mode= "removeFile"; 
                  let data_id= $("#document_data_id").val(); 

                  let fileName = $(this).prop("id"); 
                  let documentsArray = $("#documentsArray").val();
                  let folderName = $("#folderName").val();
                 $.ajax({  
                      url:"../controllers/documentController.php",  
                      method:"POST",  
                      data:{data_id:data_id,mode:mode,folderName:folderName,documentsArray:documentsArray,fileName:fileName},  
                      success:function(results){
                       
                        switch(results){
                          case 'success':
                            get_all();
                            toastr.success('Deleted Successfully');
                          break;
                          case 'error':
                            get_all();
                            toastr.error('There was an error');
                          break;
                          default:
                          toastr.error('There was an error');
                        }

                      }  
                  }); 

             }else{
              return false;
            }  
        });
      
// for delete
        $(document).on('click', '.del_data', function(){
           if (confirm("ARE YOU SURE YOU WANT TO PROCEED?")) {
               
                 let mode= "delete"; 
                 let data_id = $(this).prop("id"); 
                 $.ajax({  
                      url:"../controllers/documentController.php",  
                      method:"POST",  
                      data:{data_id:data_id,mode:mode},  
                      success:function(results){

                         switch(results){
                          case 'success':
                            get_all();
                            toastr.success('Deleted Successfully');
                          break;
                          case 'error':
                            get_all();
                            toastr.error('There was an error');
                          break;
                          default:
                          toastr.error('There was an error');
                        }
                      }  
                  }); 

             }else{
              return false;
            }  
        });
// document list modal
        $(document).on('click', '.document_list', function(){
          let documentfiles = $(this).prop("id");
          let mode= "documentList"; 
          let data_id = $(this).prop("id"); 

          $('#documentDisplayDiv').html('');

          $.ajax({  
              url:"../controllers/documentController.php",  
              method:"POST",  
              data:{data_id:data_id,mode:mode},  
              success:function(results){
                if (results !="") {
                    $('#documentDisplayDiv').html(results);

                    $('#documentListModal').modal('show'); 
                }
                else{
                    toastr.error('Sorry!!! No files Uploaded');
                }
              }  
          });
        });

// get all data 
     function get_all(){
        let mode= "getAll";
        $.ajax({  
          url:"../controllers/documentController.php",  
          method:"POST",  
          data:{mode:mode},  
          success:function(results){
            // console.log(results);
            $('#documentDisplay').html('');
            $('#documentDisplay').html(results);
            
          }  
        }); 
     }
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

});  