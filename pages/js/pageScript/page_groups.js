$(document).ready(function(){
        // for reset modal when close
      $('#pagesGroupModal').on('hidden.bs.modal', function () {
          $("#subject").html("Add New Practice");
          $("#groupdata_id").val("");
          $("#groupmode").val("insert");
          $("#insert_form")[0].reset();
          $("#groupsave_btn").text("Save Group");
        });

      //for inserting 
        $("#insert_form").submit(function(e){
            $('.tableList').DataTable().destroy();
            e.preventDefault();

              $.ajax({
              url:"../controllers/pagesGroupController.php",
              method:"POST",
              data:$("#insert_form").serialize(),
              beforeSend:function(){  
                       $('#groupsave_btn').prop('disabled','disabled').text('Loading...');  
                   },
              success:function(results){ 
                   $("#pagesGroupModal").modal("hide");
                   
                   if (results == "success") {
                      $('.tableList').dataTable({ordering: false,});
                        get_all();
                        toastr.success('Saved Successfully');
                        $("#insert_form")[0].reset();
                        $('#groupsave_btn').prop('disabled',false);
                        $('#resultsDisplay').prepend(results);
                   }
                    else if (results == "error"){
                      toastr.error('There was an error');
                      $("#pagesGroupModal").modal("hide");
                      $("#insert_form")[0].reset();  
                   }
              } 

              });  
          });

      // for update
        $(document).on('click', '.update_group_data', function(){
             let mode= "updateModal"; 
             let data_id = $(this).prop("id");  
             $.ajax({  
                  url:"../controllers/pagesGroupController.php",
                  method:"POST",  
                  data:{data_id:data_id,mode:mode},  
                  success:function(data){
                      $('.tableList').DataTable().destroy();
                        // passing data from server for particular id selected
                       let jsonObj = JSON.parse(data);
                       // passing the group pages array stored in database
                       let groupPagesArray = JSON.parse(jsonObj[0].pages_id);
                       // console.log(grouppagesArray);
                         //looping through all input id with the checkbox id 
                         let checkbox = $('input[class = "pagesCheckBox"]').each(function(){ 
                                  // grabbing the checkboxes values
                                  let PagesId = $(this).val(); 
                                  // looping througth the array to get the ids
                                  if (groupPagesArray != null) {
                                      for (let i = 0; i < groupPagesArray.length; ++i) {
                                      // for comparing if returned array is contained in the input id's values
                                      if (groupPagesArray[i] == PagesId) {
                                        // select the checkbox if the id's meet
                                            $(this).prop('checked',true);
                                          }
                                      }
                                  }
                                
                               });
                         $('.tableList').dataTable({ordering: false,});
                         // changing modal title
                        $("#subject").html("UPDATE GROUP PAGES");
                        $("#groupdata_id").val(jsonObj[0].pages_group_id);
                        $("#pagesGroupName").val(jsonObj[0].pages_group_name);
                        $("#groupsave_btn").text("Update Group");
                        $("#groupmode").val("update");
                        $("#pagesGroupModal").modal("show");
                  }  
                 });  
          });

      
// for delete
        $(document).on('click', '.del_data', function(){
           if (confirm("ARE YOU SURE YOU WANT TO PROCEED?")) {
               
                 let mode= "delete"; 
                 let data_id = $(this).prop("id");  
                 $.ajax({  
                      url:"../controllers/pagesGroupController.php",  
                      method:"POST",  
                      data:{data_id:data_id,mode:mode},  
                      success:function(results){
                        if (results == "success") {
                            get_all();
                            toastr.success('Deleted Successfully');
                        }
                         else if (results == "error"){
                            toastr.error('There was an error');
                        }
                      }  
                     }); 

             }else{
              return false;
            }  
        });
        // / get all data 
         function get_all(){
            let mode= "getAll";
            $.ajax({  
              url:"../controllers/pagesGroupController.php",  
              method:"POST",  
              data:{mode:mode},  
              success:function(results){
                $('#resultsDisplay').html('');
                $('#resultsDisplay').html(results);
              }  
            }); 
         }

});  