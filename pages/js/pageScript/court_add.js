$(document).ready(function(){
  // //////////////////////////MAIN COURT//////////////////////////////
        // for reset modal when close
      $('#mainCourtModal').on('hidden.bs.modal', function () {
          $(".mainCourtTitle").html("Add Court");
          $("#mainCourt_form")[0].reset();
          $("#mainCourt_btn").text("Add Court");
        });

      //for inserting main court
        $("#mainCourt_form").submit(function(e){
            e.preventDefault();
            // fields validation
              $.ajax({
              url:"../controllers/courtAdd.php",
              method:"POST",
              data:$("#mainCourt_form").serialize(),
              beforeSend:function(){  
                $('#mainCourt_btn').text("Loading ...").prop("disabled",true); 
               },
              success:function(results){ 
                // console.log(results);
                   $('#mainCourt_btn').text("Add Court").prop("disabled",false);
                   $("#mainCourtModal").modal("hide");
                   
                   if (results == "success") {
                      location.reload();
                      toastr.success('Saved Successfully');
                   }
                   else if (results == "error"){
                      toastr.error('There was an error');
                      $("#mainCourtModal").modal("hide");
                      $("#mainCourt_form")[0].reset();  
                   }
              } 

              });  
          });
      // for update
      $(document).on('click', '.main_update_data', function(){
         let mode= "mainUpdateModal"; 
         let data_id = $(this).prop("id");  
         $.ajax({  
              url:"../controllers/courtAdd.php",
              method:"POST",  
              data:{data_id:data_id,mode:mode},  
              success:function(results){
                // console.log(results);
                  let jsonObj = JSON.parse(results);  
                   // changing modal title
                  $(".mainCourtTitle").html("Update Main Court");
                  $("#mainCourtName").val(jsonObj.main_court_name);
                  $("#mainCourt_data_id").val(jsonObj.main_court_id);
                  $("#mainCourt_btn").text("UPDATE ");
                  $("#mainCourtMode").val("mainCourtupdate");
                  $("#mainCourtModal").modal("show");
              }  
             });  
        });
// for delete
        $(document).on('click', '.main_court_del_data', function(){
           if (confirm("ARE YOU SURE YOU WANT TO PROCEED?")) {
                 let mode= "mainCourtdelete"; 
                 let data_id = $(this).prop("id");  
                 $.ajax({  
                      url:"../controllers/courtAdd.php",  
                      method:"POST",  
                      data:{data_id:data_id,mode:mode},  
                      success:function(results){
                        if (results == "success") {
                          location.reload();
                          toastr.success('Deleted Successfully');
                        }
                        else if (results == "error"){
                          toastr.error('There was an error');
                        }
                      }  
                     }); 

             }else{
              return false;
            }  
        });
// ////////////////////////////////END OF MAIN COURT////////////////////////////////////
// //////////////////////////SUB COURT//////////////////////////////
        // for reset modal when close
      $('#subCourtModal').on('hidden.bs.modal', function () {
          $(".subCourtTitle").html("Add Sub Court");
          $("#subCourt_form")[0].reset();
          $("#subCourt_btn").text("Add Sub Court");
        });

      //for inserting main court
        $("#subCourt_form").submit(function(e){
            e.preventDefault();
            // fields validation
              $.ajax({
              url:"../controllers/courtAdd.php",
              method:"POST",
              data:$("#subCourt_form").serialize(),
              beforeSend:function(){  
                $('#subCourt_btn').text("Loading ...").prop("disabled",true); 
               },
              success:function(results){ 
                // console.log(results);
                   $('#subCourt_btn').text("Add Sub Court").prop("disabled",false);
                   $("#subCourtModal").modal("hide");
                   
                   if (results == "success") {
                      location.reload();
                      toastr.success('Saved Successfully');
                   }
                   else if (results == "error"){
                      toastr.error('There was an error');
                      $("#subCourtModal").modal("hide");
                      $("#subCourt_form")[0].reset();  
                   }
              } 

              });  
          });
      // for update
      $(document).on('click', '.sub_update_data', function(){
         let mode= "subUpdateModal"; 
         let data_id = $(this).prop("id");  
         $.ajax({  
              url:"../controllers/courtAdd.php",
              method:"POST",  
              data:{data_id:data_id,mode:mode},  
              success:function(results){
                  let jsonObj = JSON.parse(results);  
                   // changing modal title
                  $(".subCourtTitle").html("Update Main Sub Court");
                  $("#mainCourtSelect").val(jsonObj.main_court_select_id).trigger('change');
                  $("#subCourtName").val(jsonObj.sub_court_name);
                  $("#subCourtRegion").val(jsonObj.sub_court_region).change();
                  $("#subCourt_data_id").val(jsonObj.sub_court_id);
                  $("#subCourt_btn").text("UPDATE");
                  $("#subCourtMode").val("subCourtupdate");
                  $("#subCourtModal").modal("show");
              }  
             });  
        });
// for delete
        $(document).on('click', '.sub_court_del_data', function(){
           if (confirm("ARE YOU SURE YOU WANT TO PROCEED?")) {
                 let mode= "subCourtdelete"; 
                 let data_id = $(this).prop("id");  
                 $.ajax({  
                      url:"../controllers/courtAdd.php",  
                      method:"POST",  
                      data:{data_id:data_id,mode:mode},  
                      success:function(results){
                        if (results == "success") {
                          location.reload();
                          toastr.success('Deleted Successfully');
                        }
                        else if (results == "error"){
                          toastr.error('There was an error');
                        }
                      }  
                     }); 

             }else{
              return false;
            }  
        });
// ////////////////////////////////END OF MAIN COURT////////////////////////////////////
});  