$(document).ready(function(){
        // for reset modal when close
      $('#addNoteModal').on('hidden.bs.modal', function () {
          $(".caseNoteSubject").html("Add Case Note");
          $("#view_note_data_id").val("");
          $("#viewNoteContent").html("");
          $("#view_note_mode").val("insert");
          $("#addNote_form")[0].reset();
          $("#addNoteSubmitBtn").replaceWith('<button type="submit" class="btn btn-primary" id="addNoteSubmitBtn">Save <i class="fa fa-floppy-o"></i></button>');
        });
    // bring up note modal
      $(document).on("click","#addNoteBtn", function(e){ //user click on remove text
         $('#addNoteModal').modal('show');
      });
      //for inserting 
        $("#addNote_form").submit(function(e){
            e.preventDefault();
              $.ajax({
              url:"../controllers/courtCaseNoteController.php",
              method:"POST",
              data:$("#addNote_form").serialize(),
              beforeSend:function(){  
                   $('#addNoteSubmitBtn').prop('disabled','disabled').text('Loading...');  
               },
              success:function(results){ 
                // console.log(results);
                   $("#addNoteModal").modal("hide");
                   
                   switch(results){
                    case 'success':
                        get_all();
                        toastr.success('Saved Successfully');
                        $("#addNote_form")[0].reset();
                        $("#addNoteSubmitBtn").replaceWith('<button type="submit" class="btn btn-primary" id="addNoteSubmitBtn">Save <i class="fa fa-floppy-o"></i></button>');
                    break;
                    case 'error':
                        toastr.error('There was an error');
                        $("#addNote_form")[0].reset();
                    break;
                   }
              } 

              });  
          });

      // for update

      $(document).on('click', '.updateNote', function(){
         let mode= "updateModal"; 
         let data_id = $(this).prop("id");  
         $.ajax({  
              url:"../controllers/courtCaseNoteController.php",
              method:"POST",  
              data:{data_id:data_id,mode:mode},  
              success:function(results){
                  // console.log(results);
                  let jsonObj = JSON.parse(results);  
                   // changing modal title
                  $(".caseNoteSubject").html("Update Case Note");
                  $("#viewNoteContent").html(jsonObj.court_case_note_content);
                  $("#view_note_data_id").val(jsonObj.court_case_note_id);
                  $("#addNoteSubmitBtn").text("Update Note");
                  $("#view_note_mode").val("update");
                  $("#addNoteModal").modal("show");
              }  
             });  
        });


      
// for delete
        $(document).on('click', '.deleteNote', function(){
           if (confirm("ARE YOU SURE YOU WANT TO PROCEED?")) {
               
                 let mode= "delete"; 
                 let data_id = $(this).prop("id");  
                 $.ajax({  
                      url:"../controllers/courtCaseNoteController.php",  
                      method:"POST",  
                      data:{data_id:data_id,mode:mode},  
                      success:function(results){
                        $("#addNoteModal").modal("hide");
                   
                         switch(results){
                          case 'success':
                              get_all();
                              toastr.success('Saved Successfully');
                              $("#addNote_form")[0].reset();
                              $("#addNoteSubmitBtn").replaceWith('<button type="submit" class="btn btn-primary" id="addNoteSubmitBtn">Save <i class="fa fa-floppy-o"></i></button>');
                          break;
                          case 'error':
                              toastr.error('There was an error');
                              $("#addNote_form")[0].reset();
                          break;
                         }
                      }  
                     }); 

             }else{
              return false;
            }  
        });
        // / get all data 
         function get_all(){
            let mode= "getAll";
            let caseId = $('#view_note_caseId').val();
            $.ajax({  
              url:"../controllers/courtCaseNoteController.php",  
              method:"POST",  
              data:{mode:mode,caseId:caseId},  
              success:function(results){
                // console.log(results);
                $('#viewCaseNoteDiv').html('');
                // let jsonObj = JSON.parse(results);
                $('#viewCaseNoteDiv').html(results);
              }  
            }); 
         }

});  