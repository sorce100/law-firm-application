$(document).ready(function(){
        // for reset modal when close
      $('#userGroupModal').on('hidden.bs.modal', function () {
          $(".modal-title").html("Add Pages");
          $(this).find('input:text').css({"border-color":"#808080"});
          $("#insert_form")[0].reset();
          $("#pagesSave_btn").text("Add Page");
        });

      //for inserting 
        $("#insert_form").submit(function(e){
            e.preventDefault();
            $(this).find('input[type="text"]').each(function() {
              if( $(this).val() == "" ) {
                e.preventDefault();
                $(this).css({"border-color":"#f35b3f"});
                return false;
              }
              else {
                $(this).css({"border-color":"#808080"});
              }
            });
            // fields validation
              $.ajax({
              url:"../controllers/pagesController.php",
              method:"POST",
              data:$("#insert_form").serialize(),
              beforeSend:function(){  
                        $('#pagesSave_btn').text("Loading ...").prop("disabled",true); 
                   },
              success:function(results){ 
                   $('#pagesSave_btn').text("Add Page").prop("disabled",false);
                   $("#userGroupModal").modal("hide");
                   
                   if (results == "success") {
                      get_all();
                      toastr.success('Saved Successfully');
                   }
                   else if (results == "error"){
                      toastr.error('There was an error');
                      $("#userGroupModal").modal("hide");
                      $("#insert_form")[0].reset();  
                   }
              } 

              });  
          });
      // for update
      $(document).on('click', '.update_data', function(){
         let mode= "updateModal"; 
         let data_id = $(this).prop("id");  
         $.ajax({  
              url:"../controllers/pagesController.php",
              method:"POST",  
              data:{data_id:data_id,mode:mode},  
              success:function(results){
                  let jsonObj = JSON.parse(results);  
                   // changing modal title
                  $(".modal-title").html("Update Page Details");
                  $("#pageName").val(jsonObj[0].pages_name);
                  $("#pageUrl").val(jsonObj[0].pages_url);
                  $("#pages_data_id").val(jsonObj[0].pages_id);
                  $("#pageFileName").val(jsonObj[0].page_file_name);
                  $("#pagesSave_btn").text("UPDATE PAGE");
                  $("#pagesMode").val("update");
                  $("#userGroupModal").modal("show");
              }  
             });  
        });
// for delete
        $(document).on('click', '.del_data', function(){
           if (confirm("ARE YOU SURE YOU WANT TO PROCEED?")) {
                 let mode= "delete"; 
                 let data_id = $(this).prop("id");  
                 $.ajax({  
                      url:"../controllers/pagesController.php",  
                      method:"POST",  
                      data:{data_id:data_id,mode:mode},  
                      success:function(results){
                        if (results == "success") {
                          get_all();
                          toastr.success('Deleted Successfully');
                        }
                        else if (results == "error"){
                          toastr.error('There was an error');
                        }
                      }  
                     }); 

             }else{
              return false;
            }  
        });
// get all data 
     function get_all(){
        let mode= "getAll";
        $.ajax({  
          url:"../controllers/pagesController.php",  
          method:"POST",  
          data:{mode:mode},  
          success:function(results){
            $('#resultsDisplay').html('');
            $('#resultsDisplay').html(results);
          }  
        }); 
     }
});  