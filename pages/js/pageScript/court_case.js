 $(document).ready(function(){

  $('#addCaseModal').on('hidden.bs.modal', function () {
      $(".casetTitle").html("Add Case");
      $('#contactsAddedDiv').html('');
      $("#courtCase_data_id").val("");
      $("#courtCaseMode").val("insert");
      $("#courtCase_form")[0].reset();
      $("#courtCaseBtn").replaceWith('<button type="submit" class="btn btn-submit btn-success" id="courtCaseBtn">Save & Finish <i class="fa fa-floppy-o"></i></button>');
      $('#courtCase_form').parsley().reset();
      $('.courtCaseSelect2').val(null).trigger('change');

       // set first fieldset and clear the form 
      // $('.f1 fieldset').each(function() {
      //   $(this).css({'display':'none'});
      // });
      $('.fi input[type="text"], input[type="number"], textarea').each(function() {
        $(this).val(''); 
      });

      // $('.f1 fieldset:first-child').css({'display':'block'});
    });


  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// filter form select 2
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  $('.filterPracticeSelect2,.filterLeadAttorselect2,.filtercaseStageselect2').select2();

  // filter by onlick of filter areas
   $('#filterLeadAttor').change(function(){
      let attor = $(this).find(":selected").text();
      // $("#courtCaseDisplay tr").filter(function() {
      //   var dataset = $('table').find('tr');
      //   dataset.show();
      //   dataset.filter(function(index, item) {
      //     return $(item).find('td:first-child').text().split(',').indexOf(selection) === -1;
      //   }).hide();
      // });
   });
  // clear all filter forms
  $(document).on("click","#clearFilterbtn", function(e){ //user click on remove text
     $('#filterPractice,#filterLeadAttor,#filtercaseStage').val(null).trigger('change');
     // reload data if filter cleared
     get_all();
  });

  // for submitting and searching with filter selected values
  $(document).on("click","#applyFilterBtn", function(e){ //user click on remove text
     // grab filter selected values
      let filterPracticeArea = $("#filterPractice").find(":selected").val();
      let filterLeadAttorney = $("#filterLeadAttor").find(":selected").val();
      let filtercaseStage = $("#filtercaseStage").find(":selected").val();

      let mode= "filter_court_case";

      $.ajax({
        url:"../controllers/courtCaseController.php",
        method:"POST",
        data:{filterPracticeArea:filterPracticeArea,filterLeadAttorney:filterLeadAttorney,filtercaseStage:filtercaseStage,mode:mode},
        beforeSend:function(){  
          $('#courtCaseDisplay').html('<tr><td colspan="6"><center>Filtering ...</center></td></tr>'); 
        },
        success:function(results){
          $('#courtCaseDisplay').html('');
          let jsonObj = JSON.parse(results);
          if (jsonObj.length == 0) { 
            $('#courtCaseDisplay').html('<tr><td colspan="6"><center>No Results Found</center></td></tr>');
          }
          else if (jsonObj.length >= 1) {
            for (let i = 0; i < jsonObj.length; i++) {
                $('#courtCaseDisplay').append('<tr><td>'+jsonObj[i].case_name+'</td><td>'+jsonObj[i].case_number+'</td><td>'+jsonObj[i].case_stage+'</td><td>'+jsonObj[i].case_lead_attorney+'</td><td>'+jsonObj[i].added+'<b> by </b>'+jsonObj[i].user_id+'</td><td><button class="btn-default view_case_details" id="'+jsonObj[i].case_id+'"><i class="fa fa-eye"></i></button> <button class="btn-primary update_case_details" id="'+jsonObj[i].case_id+'"><i class="fa fa-pencil"></i></button> <button class="btn-danger del_data" id="'+jsonObj[i].case_id+'"><i class="fa fa-trash"></i></button></td></tr>');
            }
          }
        } 

    });
  });



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// add case modal select2
  $(".contactSelect2,.practiceAreaSelect2,.leadAttorselect2,.caseStageselect2,.currentCourtSelect2,.currentCourtSubSelect2,.nextCourtSelect2,.nextCourtSubSelect2,.billingContactselect2").select2({
      dropdownParent: $("#addCaseModal")
    });
///////////////////////////////////////////////////////
  // Click contacts to add to the contacts
   $('#caseContacts').change(function(){
    let contactSelectName = $('#caseContacts option:selected').text();
    let contactSelectId = $('#caseContacts option:selected').prop("value");

    $('#contactsAddedDiv').append('<div class="input-group" style="margin-top:7px;">'+
      '<input type="text" value="'+contactSelectName+'" class="form-control" id="caseContactSelect"" readonly>'+
      '<span class="input-group-addon remove_field" style="background-color:#DE8280;color:#fff;">'+
      '<input type="hidden" value="'+contactSelectId+'" class="form-control" name="caseContactIds[]">'+
      '<i class="fa fa-trash"></i></span></div>');
   });

   // for removing caseContactsSeletc
    $(document).on("click",".remove_field", function(e){ //user click on remove text
         $(this).parent('div').remove();

      });
///////////////////////////////////////////////////////////////////////////////////////////
// for selecting lead attorney when selected and checking the staff id
    $('#caseLeadAttorney').change(function(){
      let leadAttorneyId = $('#caseLeadAttorney option:selected').prop("value");
      $('.caseStaffIds[value="'+leadAttorneyId+'"]').each(function() {
          $(this).prop('checked',true);
      });

    });

// ////////////////////////////Select current court case sub division///////////////////////
    $('#currentCourtMain').change(function(){

      $('#currentCourtSub option:not(:first)').remove();
      let mainCourtId = $('#currentCourtMain option:selected').prop("value");
      let mode = "get_sub_division";
      $.ajax({
          url:"../controllers/courtAdd.php",
          method:"POST",
          data:{mainCourtId:mainCourtId,mode:mode},  
          success:function(results){
            if (results !="") {
              let jsonObj = JSON.parse(results); 
              $('#currentCourtSub').append(jsonObj);
            }
          } 

      });
    });
// ////////////////////////////Select next court case sub division///////////////////////
    $('#nextCourtMain').change(function(){
      $('#nextCourtSub option:not(:first)').remove();
      let mainCourtId = $('#nextCourtMain option:selected').prop("value");
      let mode = "get_sub_division";
      $.ajax({
          url:"../controllers/courtAdd.php",
          method:"POST",
          data:{mainCourtId:mainCourtId,mode:mode},  
          success:function(results){
            // console.log(results);
            if (results !="") {
              let jsonObj = JSON.parse(results); 
              $('#nextCourtSub').append(jsonObj);
            }
          } 

      });
    });
////////////////////////////////////////////////////////////////////////////////////////////
   // parsley
   $('#courtCase_form').parsley();
   $('#courtCase_form').parsley().reset();

  //for inserting 
    $("#courtCase_form").submit(function(e){
      $('.tableList').DataTable().destroy();
      e.preventDefault();
          $.ajax({
              url:"../controllers/courtCaseController.php",
              method:"POST",
              data:$("#courtCase_form").serialize(),
              beforeSend:function(){  
                $('#courtCaseBtn').text("Loading ...").prop("disabled",true); 
              },
              success:function(results){
                  // console.log(results);
                  $("#addCaseModal").modal("hide");
                 
                 if (results == "success") {
                    $('.tableList').dataTable({ordering: false,});
                      get_all();
                      toastr.success(' Successfully');
                      $("#courtCase_form")[0].reset();
                      $("#courtCaseBtn").replaceWith('<button type="submit" class="btn btn-submit btn-success" id="courtCaseBtn">Save & Finish <i class="fa fa-floppy-o"></i></button>');
                 }
                 else if (results == "error"){
                    get_all();
                    toastr.error('There was an error');
                    $("#addCaseModal").modal("hide");
                    $("#courtCase_form")[0].reset();

                 }
              } 

          });  
      });

      // for update
      $(document).on('click', '.update_case_details', function(){
         let mode= "updateModal"; 
         let data_id = $(this).prop("id");  
         $.ajax({  
              url:"../controllers/courtCaseController.php",
              method:"POST",  
              data:{data_id:data_id,mode:mode},  
              success:function(results){
                 $('.tableList').DataTable().destroy();
                // console.log(results);
                   let jsonObj = JSON.parse(results);  
                   // changing modal title
                    $(".casetTitle").html("Update Case");
                    $('#contactsAddedDiv').html('');
                    // for contacts select
                    let jsonObjContactsIds = JSON.parse(jsonObj.case_contactIds);
                    for (let i = 0; i < jsonObjContactsIds.length; ++i) {
                        $('#contactsAddedDiv').append('<div class="input-group" style="margin-top:7px;">'+
                          '<input type="text" alt="'+jsonObjContactsIds[i]+'" value="'+get_contactName(jsonObjContactsIds[i])+'" class="form-control" id="caseContactSelect"" readonly>'+
                          '<span class="input-group-addon remove_field" style="background-color:#DE8280;color:#fff;">'+
                          '<input type="hidden" value="'+jsonObjContactsIds[i]+'" class="form-control" name="caseContactIds[]">'+
                          '<i class="fa fa-trash"></i></span></div>');
                    }
                    $('.tableList').dataTable({ordering: false,});
                    $("#caseName").val(jsonObj.case_name);
                    $("#caseSuitNumber").val(jsonObj.case_suit_number);
                    $("#caseNumber").val(jsonObj.case_number);
                    $("#casePracticeArea").val(jsonObj.case_practice_area).trigger('change');
                    $("#caseStage").val(jsonObj.case_stage).trigger('change');
                    $("#caseDateOpened").val(jsonObj.case_date_opened);
                    $("#currentCourtMain").val(jsonObj.current_court_main_id).trigger('change');
                    $("#currentCourtSub").val(jsonObj.current_court_sub_id).trigger('change');
                    $("#currentCourtNum").val(jsonObj.current_court_num);
                    $("#nextCourtMain").val(jsonObj.next_court_main_id).trigger('change');
                    $("#nextCourtSub").val(jsonObj.next_court_sub_id).trigger('change');
                    $("#nextCourtNum").val(jsonObj.next_court_num  );
                    $("#nextCourtDate").val(jsonObj.next_court_date);

                    $("#caseDescription").val(jsonObj.case_description);
                    $("#caseStatueLimitation").val(jsonObj.case_statue_limit);
                    $("#caseBillingContact").val(jsonObj.case_billing_contact).trigger('change');
                    $("#caseBillingMethod").val(jsonObj.case_billing_method).trigger('change');
                    $("#caseLeadAttorney").val(jsonObj.case_lead_attorney).trigger('change');
                    // check staff ids
                       let jsonObjStaffIds = JSON.parse(jsonObj.case_staffIds);
                       for (let i = 0; i < jsonObjStaffIds.length; ++i) {

                          $('.caseStaffIds[value="'+jsonObjStaffIds[i]+'"]').each(function() {
                            $(this).prop('checked',true);
                          });
                       }

                    $("#courtCase_data_id").val(jsonObj.case_id);
                    $("#courtCaseBtn").text("Update Case");
                    $("#courtCaseMode").val("update");
                    $("#addCaseModal").modal("show");

                    
              }  
             });  
        });

      
// for delete
        $(document).on('click', '.del_case_data', function(e){
            e.preventDefault();
           if (confirm("ARE YOU SURE YOU WANT TO PROCEED?")) {
                 let mode= "delete"; 
                 let data_id = $(this).prop("id");  
                 $.ajax({  
                      url:"../controllers/courtCaseController.php",  
                      method:"POST",  
                      data:{data_id:data_id,mode:mode},  
                      success:function(results){
                        // console.log(results);
                        if (results == "success") {
                            get_all();
                            toastr.success('Deleted Successfully');
                        }
                        else if (results == "error"){
                            toastr.error('There was an error');
                        }
                      }  
                     }); 

             }else{
              return false;
            }  
        });
    // get all data 
         function get_all(){
            let mode = "getAll";
            $.ajax({  
              url:"../controllers/courtCaseController.php",  
              method:"POST",  
              data:{mode:mode},  
              success:function(results){
                // console.log(results);
                $('#courtCaseDisplay').html('');
                $('#courtCaseDisplay').html(results);
                
              }  
            }); 
         }

    // function to get the name of contacts
      function get_contactName(contactId){
        let mode = "getContactName";
        let data_id = contactId;
        $.ajax({  
          url:"../controllers/contactController.php",  
          method:"POST",  
          data:{contactMode:mode,contactData_id:data_id},  
          success:function(results){
            // search for case contact and insert value
            $('#caseContactSelect[alt="'+contactId+'"]').each(function() {
              $(this).val(results);
            });
           
          }  
        }); 
      }
 });