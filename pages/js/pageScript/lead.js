 $(document).ready(function(){
    $('#lead_form').parsley();
    $('#lead_form').parsley().reset();
        // for reset modal when close
      $('#addLeadModal').on('hidden.bs.modal', function () {
          $(".leadSubject").html("Add New Practice");
          $("#practicedata_id").val("");
          $("#practicemode").val("insert");
          $("#lead_form")[0].reset();
          $("#leadSave_btn").text("Add Page");
          $('#lead_form').parsley().reset();
        });

      //for inserting 
        $("#lead_form").submit(function(e){
           e.preventDefault();
            $.ajax({
            url:"../controllers/leadsController.php",
            method:"POST",
            data:$("#lead_form").serialize(),
            beforeSend:function(){  
               // $('#leadSave_btn').prop('disabled','disabled').text('Loading...');  
           },
            success:function(results){  
              // console.log(results);
              $("#addLeadModal").modal("hide");
               
              if (results == "success") {
                  toastr.success('Saved Successfully');
                  location.reload();
               }
               else if (results == "error"){
                  toastr.error('There was an error');
                  $("#addLeadModal").modal("hide");
                  $("#lead_form")[0].reset();  
               }
            } 

          });  
        });

      // for update
      $(document).on('click', '.update_data', function(e){
          e.preventDefault();
         let mode = "updateModal"; 
         let data_id = $(this).prop("id");  
         $.ajax({  
            url:"../controllers/leadsController.php",
            method:"POST",  
            data:{data_id:data_id,mode:mode},  
            success:function(results){
              // console.log(results);
             let jsonObj = JSON.parse(results);  
             // changing modal title
            $(".leadSubject").html("Update Lead Details");
            $("#leadFirstName").val(jsonObj.lead_first_name);
            $("#leadMidName").val(jsonObj.lead_mid_name);
            $("#leadLastName").val(jsonObj.lead_last_name);
            $("#leadEmail").val(jsonObj.lead_email);
            $("#leadTel").val(jsonObj.lead_tel);
            $("#leadOfficeLocation").text(jsonObj.lead_office_location);
            $("#leadTown").val(jsonObj.lead_town);
            $("#leadRegion").val(jsonObj.lead_region);
            $("#leadCaseMatter").val(jsonObj.lead_case_matter);
            $("#leadCaseDescription").text(jsonObj.lead_case_description);
            $("#leadCaseStatus").val(jsonObj.lead_case_status);
            $("#leadCasePracticeArea").val(jsonObj.lead_case_practice_area);
            $("#leadCaseAssignedTo").val(jsonObj.lead_case_assignedTo);
            $("#leadCaseCompletionDate").val(jsonObj.lead_case_completion_date);
            $("#leadCaseHourlyRate").val(jsonObj.lead_case_hourly_rate);
            $("#leadCaseMonthlyRetainer").val(jsonObj.lead_case_monthly_retainer);
            $("#leadCaseFeeFrequency").val(jsonObj.lead_case_fee_frequency);
            $("#leadCaseSpecialArrangement").val(jsonObj.lead_case_special_arrangement);
            $("#leadCaseAssociatedClients").text(jsonObj.lead_case_associated_clients);
            $("#leadCaseAdverseParties").text(jsonObj.lead_case_adverse_parties);
            $("#leadCaseAssociatedFiles").text(jsonObj.lead_case_associated_files);
            $("#lead_data_id").val(jsonObj.lead_id);
            $("#leadSave_btn").text("Update Lead");
            $("#leadMode").val("update");
            $("#addLeadModal").modal("show");
        }  
       });  
    });

      
// for delete
     $(document).on('click', '.del_data', function(e){
      e.preventDefault();
       if (confirm("ARE YOU SURE YOU WANT TO PROCEED?")) {
           
             let mode= "delete"; 
             let data_id = $(this).prop("id");  
             $.ajax({  
                  url:"../controllers/leadsController.php",  
                  method:"POST",  
                  data:{data_id:data_id,mode:mode},  
                  success:function(results){
                    if (results == "success") {
                      toastr.success('Deleted Successfully');
                      location.reload();
                    }
                    else if (results == "error"){
                      toastr.error('There was an error');
                    }
                  }  
                 }); 

         }else{
          return false;
        }  
    });


  // ///////////////////////////click to convert to case///////////////////////////
  $(document).on('click', '.convert_to_case_btn', function(e){
      e.preventDefault();
     let mode= "convertToCase"; 
     let data_id = $(this).prop("id");

     $("#convertToCaseModal").modal("show");
     // $.ajax({  
     //      url:"../controllers/leadsController.php",  
     //      method:"POST",  
     //      data:{data_id:data_id,mode:mode},  
     //      success:function(results){
     //        if (results == "success") {
     //          toastr.success('Deleted Successfully');
     //          location.reload();
     //        }
     //        else if (results == "error"){
     //          toastr.error('There was an error');
     //        }
     //      }  
     //     }); 

    });
    ///////////////////////do not hire////////////////////
   $(document).on('click', '.not_hire_btn', function(e){
      e.preventDefault();
     let mode= "doNotHire"; 
     let data_id = $(this).prop("id");

     $("#notHireCaseModal").modal("show");
     // $.ajax({  
     //      url:"../controllers/leadsController.php",  
     //      method:"POST",  
     //      data:{data_id:data_id,mode:mode},  
     //      success:function(results){
     //        if (results == "success") {
     //          toastr.success('Deleted Successfully');
     //          location.reload();
     //        }
     //        else if (results == "error"){
     //          toastr.error('There was an error');
     //        }
     //      }  
     //     }); 

    });

   
     
});  