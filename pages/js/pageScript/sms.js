 $(document).ready(function(){
   ////////////////////////////////////////////////////////////////////////
  $(".smsSelect2").select2({
      dropdownParent: $("#smsModal")
    });
   /////////////////////////////////////////////////////////////////////
   // sms content count
    $('#smsContent').keyup(function () {
      var max = 153;
      var len = $(this).val().length;
      if (len >= max) {
        $('#charNum').text(' you have reached the limit');
      } else {
        var char = max - len;
        $('#charNum').text(char + ' characters left');
      }
    });
    //////////////////////////////////////////////////////////////////////
    $('#smsModal').on('hidden.bs.modal', function () {
      $(".modal-title").html("New Sms");
      $(this).find('input:text').css({"border-color":"#808080"});
      $("#messages_form")[0].reset();
      $("#sms_btn").attr('disabled',false).text("Send Sms");
      $('.smsSelect2').val(null).trigger('change');
      $('#messageContent').summernote('reset');
      //set first fieldset and clear the form 
      $('.f1 fieldset').each(function() { $(this).css({'display':'none'}); });
      $('.fi input[type="text"], input[type="number"], textarea').each(function() { $(this).val(''); });
      $('.f1 fieldset:first-child').css({'display':'block'});
      ///////////////////////////////////////////////////
      // clear checkboxes 
      $('#smsContactIds[type="checkbox"]').each(function() { $(this).prop('disabled',false); });
      $('#smsStaffIds[type="checkbox"]').each(function() { $(this).prop('disabled',false); });
      $('#select_all,#select_all_staff').prop('disabled',false);

      //////////////////////////////////////////////////////////
        $("#sms_courtCaseId").val(null).trigger('change').prop('disabled',false);
        $('.btn-next').html('Compose Sms');
        $("#sms_btn").replaceWith('<button type="submit" class="btn btn-primary" id="sms_btn">Send Sms <i class="fa fa-send"></i></button>');
    });
    //////////////////////////////////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////
  // change for case contacts and staff assigned to the case
   $('#sms_courtCaseId').change(function(){
    // deselecting staff and contact checkboxes
      $('#smsContactIds[type="checkbox"]').each(function() { $(this).prop('checked',false); });
      $('#smsStaffIds[type="checkbox"]').each(function() { $(this).prop('checked',false); });
      // get case ID
     let caseId = $('option:selected', this).prop('value');
      // disable table to check checkboxes
      $('.tableList').DataTable().destroy();
       // get staff and case ids associated with case
       get_case_contacts_staff(caseId);
       // enable tablelist after inserting
       $('.tableList').dataTable({ordering: false,});
  });

   // get case contacts and staff by id
  function get_case_contacts_staff(caseId){
    let mode = "case_staffContacts_id";
      $.ajax({  
        url:"../controllers/courtCaseController.php",  
        method:"POST",  
        data:{mode:mode,caseId:caseId},  
        success:function(results){

          let jsonObj = JSON.parse(results);
          let returnContactId = jsonObj['case_contactIds']; 
          let returnstaffId = jsonObj['case_staffIds'];

          let jsonObjContactsId = JSON.parse(returnContactId);
           for (var i = 0; i < jsonObjContactsId.length; i++) {
             $('#smsContactIds[value="'+jsonObjContactsId[i]+'"]').each(function() { $(this).prop('checked',true); });
           }
           // case staff function
      
           let jsonObjStaffsId = JSON.parse(returnstaffId);
            for (var j = 0; j < jsonObjStaffsId.length; j++) {
              $('#smsStaffIds[value="'+jsonObjStaffsId[j]+'"]').each(function() { $(this).prop('checked',true); });
            }
          
        }  
      });
  }


////////////////////////////////////////////////////////////////////////////////
      //for inserting 
        $("#messages_form").submit(function(e){
            e.preventDefault();
              $.ajax({
              url:"../controllers/messagesController.php",
              method:"POST",
              data:$("#messages_form").serialize(),
              beforeSend:function(){  
                        $('#sms_btn').text("Loading ...").prop("disabled",true); 
                   },
              success:function(results){
                // console.log(results);
                    $("#smsModal").modal("hide");
                   
                   if (results == "success") {
                        get_all_sent();
                        get_all_inbox();
                        toastr.success('Saved Successfully');
                        $("#messages_form")[0].reset();
                        $('#sms_btn').attr('disabled',false).text('Save Contact');
                   }
                   else if (results == "error"){
                      get_all_sent();
                      get_all_inbox();
                      toastr.error('There was an error');
                      $("#smsModal").modal("hide");
                      $("#messages_form")[0].reset();

                   }
              } 

              });  
          });
      // for update
      $(document).on('click', '.view_sent_message', function(){
         let mode= "updateModal"; 
         let data_id = $(this).prop("id");
           //make all checkboxes readonly
           $('#smsContactIds[type="checkbox"]').each(function() {
              $(this).prop('disabled',true);
            });
            $('#smsStaffIds[type="checkbox"]').each(function() {
              $(this).prop('disabled',true);
            });
            $('#select_all,#select_all_staff').prop('disabled',true);
         $.ajax({  
              url:"../controllers/messagesController.php",
              method:"POST",  
              data:{data_id:data_id,mode:mode},  
              success:function(results){
                // console.log(results);
                   let jsonObj = JSON.parse(results);  
                   // changing modal title
                    $(".modal-title").html("View Message");
                    ////////////////////////////////////////////////////////////////////////////////////
                    if (jsonObj[0].message_subject !='') {
                      $("#sms_courtCaseId").val(jsonObj[0].sms_courtCaseId).trigger('change').prop('disabled',true);
                    }
                    else{
                      $('.smsSelect2').val(null).trigger('change');
                    }
                     // disable table to check checkboxes
                    $('.tableList').DataTable().destroy();
                    ///////////////////////////////////////////////////////////////////////////////////////
                    let jsonObjContactsId = JSON.parse(jsonObj[0].message_receive_contacts);
                     for (var i = 0; i < jsonObjContactsId.length; i++) {
                       $('#smsContactIds[value="'+jsonObjContactsId[i]+'"]').each(function() {
                          $(this).prop('checked',true);
                        });
                     }
                     // case staff function
                     let jsonObjStaffsId = JSON.parse(jsonObj[0].message_receive_staff);
                      for (var j = 0; j < jsonObjStaffsId.length; j++) {
                        $('#smsStaffIds[value="'+jsonObjStaffsId[j]+'"]').each(function() {
                          $(this).prop('checked',true);
                        });
                      }
                    //////////////////////////////////////////////////////////////////////////////////////////
                    $('#messageContent').summernote('code', jsonObj[0].message_content);
                    $('.btn-next').html('View Message');
                    // enable tablelist after inserting
                    $('.tableList').dataTable({ordering: false,});
                    $("#sms_btn").html('<button type="button" class="btn btn-danger" data-dismiss="modal">Close <i class="fa fa-times"></i></button>');
                    $("#smsModal").modal("show");
              }  
             });  
        });

      
// for delete
        $(document).on('click', '.view_sent_message_del_data', function(){
           if (confirm("ARE YOU SURE YOU WANT TO PROCEED?")) {
                 let mode= "senderDelete"; 
                 let data_id = $(this).prop("id");  
                 $.ajax({  
                      url:"../controllers/messagesController.php",  
                      method:"POST",  
                      data:{data_id:data_id,mode:mode},  
                      success:function(results){
                        console.log(results);
                        if (results == "success") {
                            get_all_sent();
                            toastr.success('Deleted Successfully');
                        }
                        else if (results == "error"){
                            get_all_sent();
                            toastr.error('There was an error');
                        }
                      }  
                     }); 

             }else{
              return false;
            }  
        });

        // inbox delete
        $(document).on('click', '.inbox_del_data', function(){
           if (confirm("ARE YOU SURE YOU WANT TO PROCEED?")) {
                 let mode = "receiverDelete"; 
                 let data_id = $(this).prop("id");  
                 $.ajax({  
                      url:"../controllers/messagesController.php",  
                      method:"POST",  
                      data:{data_id:data_id,mode:mode},  
                      success:function(results){
                        console.log(results);
                        if (results == "success") {
                            get_all_inbox();
                            toastr.success('Deleted Successfully');
                        }
                        else if (results == "error"){
                            get_all_inbox();
                            toastr.error('There was an error');
                        }
                      }  
                     }); 

             }else{
              return false;
            }  
        });
    // get all sent messaages
         function get_all_sent(){
            let mode = "getAll_sent";
            $.ajax({  
              url:"../controllers/messagesController.php",  
              method:"POST",  
              data:{mode:mode},  
              success:function(results){
                // console.log(results);
                $('#sentDisplay').html('');
                let jsonObj = JSON.parse(results);
                for (let i = 0; i < jsonObj.length; i++) {
                    $('#sentDisplay').append('<tr><td>'+jsonObj[i].message_subject+'</td><td>'+jsonObj[i].added+'</td><td><button class="btn-primary view_sent_message" id="'+jsonObj[i].message_id+'"><i class="fa fa-eye"></i></button> <button class="btn-danger view_sent_message_del_data" id="'+jsonObj[i].message_id+'"><i class="fa fa-trash"></i></button></td></tr>');
                }
              }  
            }); 
         }
        // get all received messages
        function get_all_inbox(){
            let mode = "getAll_received";
            $.ajax({  
              url:"../controllers/messagesController.php",  
              method:"POST",  
              data:{mode:mode},  
              success:function(results){
                // console.log(results);
                $('#inboxDisplay').html('');
                let jsonObj = JSON.parse(results);
                for (let i = 0; i < jsonObj.length; i++) {
                    $('#inboxDisplay').append('<tr><td>'+jsonObj[i].message_subject+'</td><td>'+jsonObj[i].message_sender_accounId+'</td><td>'+jsonObj[i].added+'</td><td><button class="btn-primary view_sent_message" id="'+jsonObj[i].message_id+'"><i class="fa fa-eye"></i></button> <button class="btn-danger inbox_del_data" id="'+jsonObj[i].message_id+'"><i class="fa fa-trash"></i></button></td></tr>');
                }
              }  
            }); 
         }
 });