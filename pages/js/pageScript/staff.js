 $(document).ready(function(){
      $('#insert_form').parsley();
      $('#insert_form').parsley().reset();
        // for reset modal when close
      $('#addStaffModal').on('hidden.bs.modal', function () {
          $(".modal-title").html("Add Staff");
          $("#staffdata_id").val("");
          $("#staffmode").val("insert");
          $(this).find('input:text').css({"border-color":"#808080"});
          $("#insert_form")[0].reset();
          $("#staffsave_btn").text("Save & Finish");
          $('#insert_form').parsley().reset();
        });

      //for inserting 
        $("#insert_form").submit(function(e){
            e.preventDefault();
            $(this).find('input[type="text"]').each(function() {
              if( $(this).val() == "" ) {
                e.preventDefault();
                $(this).css({"border-color":"#f35b3f"});
                return false;
              }
              else {
                $(this).css({"border-color":"#808080"});
              }
            });
            // fields validation

              $.ajax({
              url:"../controllers/StaffController.php",
              method:"POST",
              enctype: 'multipart/form-data',
              data:new FormData(this),  
              contentType:false,  
              processData:false,
              beforeSend:function(){  
                       $('#staffsave_btn').prop('disabled','disabled').text('Loading...');  
                   },
              success:function(results){  
                // alert(results);
                   $("#addStaffModal").modal("hide");
                   
                   if (results == "success") {
                        get_all();
                        toastr.success('Saved Successfully');
                        $("#insert_form")[0].reset();
                        $('#staffsave_btn').prop('disabled',false).text('Save & Finish');
                        $('#resultsDisplay').prepend(results);
                   }
                   else if (results == "error"){
                      toastr.error('There was an error');
                      $("#addStaffModal").modal("hide");
                      $("#insert_form")[0].reset();  
                   }
              } 

              });  
          });

      // for update
      $(document).on('click', '.update_data', function(){
         let mode = "updateModal"; 
         let data_id = $(this).prop("id");  
         $.ajax({  
              url:"../controllers/StaffController.php",
              method:"POST",  
              data:{data_id:data_id,mode:mode},  
              success:function(results){
                   let jsonObj = JSON.parse(results);  
                   // changing modal title
                  $(".modal-title").html("Update Staff");
                  $("#staffFirstName").val(jsonObj[0].staff_firstname);
                  $("#staffMidName").val(jsonObj[0].staff_middlename);
                  $("#staffLastName").val(jsonObj[0].staff_lastname);
                  $("#stafftel").val(jsonObj[0].staff_tel);
                  $("#staffemergencytel").val(jsonObj[0].staff_emergency);
                  $("#staffEmail").val(jsonObj[0].staff_email);
                  $("#staffDob").val(jsonObj[0].staff_dob);
                  $("#staffPostal").val(jsonObj[0].staff_postal);
                  $("#staffHouseNum").val(jsonObj[0].staff_housenum);
                  $("#staffHouseLoc").val(jsonObj[0].staff_houseloc);
                  $("#staffType").val(jsonObj[0].staff_type);
                  $("#staffProfNum").val(jsonObj[0].staff_profnum );
                  $("#staffEmployDate").val(jsonObj[0].staff_employdate);
                  $("#staffNote").val(jsonObj[0].staff_note);
                  $("#staffdata_id").val(jsonObj[0].staff_id);
                  if (jsonObj[0].staff_pic != "NONE") {

                      $("#staffPicDiv").html('<img src="../uploads/staffpic/'+jsonObj[0].staff_pic+'" width="255px" height="255px">');
                  }
                  else if (jsonObj[0].staff_pic == "NONE"){
                     $("#staffPicDiv").html('<img src="../assets/img/avatar.png">');
                  }
                  $("#staffstaffsave_btn").text("Update Staff");
                  $("#staffmode").val("update Staff");
                  $("#addStaffModal").modal("show");
              }  
             });  
        });

      
// for delete
        $(document).on('click', '.del_data', function(){
           if (confirm("ARE YOU SURE YOU WANT TO PROCEED?")) {
               
                 let mode= "delete"; 
                 let data_id = $(this).prop("id");  
                 $.ajax({  
                      url:"../controllers/StaffController.php",  
                      method:"POST",  
                      data:{data_id:data_id,mode:mode},  
                      success:function(results){
                        if (results == "success") {
                            get_all();
                            toastr.success('Deleted Successfully');
                        }
                        else if (results == "error"){
                            toastr.error('There was an error');
                        }
                      }  
                  }); 

             }else{
              return false;
            }  
        });

// get all data 
     function get_all(){
        let mode= "getAll";
        $.ajax({  
          url:"../controllers/StaffController.php",  
          method:"POST",  
          data:{mode:mode},  
          success:function(results){
            $('#resultsDisplay').html('');
            let jsonObj = JSON.parse(results);
            for (let i = 0; i < jsonObj.length; i++) {
                $('#resultsDisplay').append('<tr><td>'+jsonObj[i].staff_firstname+'</td><td>'+jsonObj[i].staff_lastname+'</td><td>'+jsonObj[i].staff_tel+'</td><td>'+jsonObj[i].staff_type+'</td><td>'+jsonObj[i].staff_employdate+'</td><td><button class="btn-primary update_data" id="'+jsonObj[i].staff_id+'"><i class="fa fa-pencil"></i></button> <button class="btn-danger del_data" id="'+jsonObj[i].staff_id+'"><i class="fa fa-trash"></i></button></td></tr>');
            }
          }  
        }); 
     }

});  