$(document).ready(function(){

		$('#insert_form').parsley();
      	$('#insert_form').parsley().reset();

	 	 $('#addUserModal').on('hidden.bs.modal', function () {
	 	  $("#usersmode").val("");
	 	  $("#usersmode").val("insert");
          $(".modal-title").html("Add User");
          $("#insert_form")[0].reset();
          $("#userssave_btn").text("Create Account");
          $('#insert_form').parsley().reset();
          $("#accountSelect").prop('disabled',false);
          $('.usersSelect2').val(null).trigger('change');
          $(".accSelectDiv").show();
        });

	 	 $(".usersSelect2").select2({
	      dropdownParent: $("#addUserModal")
	    });

		$("#accountSelect").change(function(){
			 let accountSelect = $('option:selected', this).attr('id');
			 // get username and add
			 $("#userName").val(accountSelect);
		});
		/////////////////////////////////////////////////////////////////////////////////////////////
        $('#accPasswdReset').bootstrapToggle({
          on: 'Direct Login',
          off: 'Reset Pass',
          onstyle: 'success',
          offstyle: 'danger'
        });
       $('#accPasswdReset').change(function(){
          if($(this).prop('checked')){
           $('#accPasswdReset_log').val('NO');
          }else {
           $('#accPasswdReset_log').val('YES');
          }
       });
       // end of password reset

       // eccount status
      $('#accStatus').bootstrapToggle({
        on: 'ACTIVE',
        off: 'DISABLE',
        onstyle: 'success',
        offstyle: 'danger'
      });

      // triggring the check
      $('#accPasswdReset').bootstrapToggle('on');
      $('#accStatus').bootstrapToggle('on');

   $('#accStatus').change(function(){
    if($(this).prop('checked')){
     $('#accStatus_log').val('ACTIVE');
    }else{
     $('#accStatus_log').val('DISABLE');
    }
   });
  /////////////////////////////////////////////////////////////////////////////////////////////////// 
		// for insert
		$("#insert_form").submit(function(e){
	        e.preventDefault();
	        $("#accountSelect").prop('disabled',false);
	          $.ajax({
		          url:"../controllers/usersController.php",
		          method:"POST",
		          data:$("#insert_form").serialize(),
		          beforeSend:function(){  
		                   $('#userssave_btn').attr('disabled','disabled').text('Loading...');  
		               },
		          success:function(results){ 
		          	// console.log(results);
		               $("#addUserModal").modal("hide");
		               if (results == "success") {
                        	get_all();
		                    toastr.success('Saved Successfully');
		                    $("#insert_form")[0].reset();
		                    $('#userssave_btn').attr('disabled',false).text('Create Account');
		                    // $('#resultsDisplay').prepend(results);
		               }
		               else if (results == "error"){
		                  toastr.error('There was an error');
		                  $("#addUserModal").modal("hide");
		                  $("#insert_form")[0].reset();  
		               }
		          } 

	          	});  
	    });
	     // for update
	       $(document).on('click', '.update_data', function(){
	         let mode= "updateModal"; 
	         let data_id = $(this).prop("id");  
	         $.ajax({  
	              url:"../controllers/usersController.php",
	              method:"POST",  
	              data:{data_id:data_id,mode:mode},  
	              success:function(results){
	                   let jsonObj = JSON.parse(results);  
	                   // changing modal title
	                  	$(".modal-title").html("Update User Account");
	                  	///////////////////////////////////////////////////////////////////////////////////////////
	                  	if (jsonObj[0].account_name =="contact") {
	                  		$(".accSelectDiv").hide();
	                  	}
	                  	else if (jsonObj[0].account_name =="staff") {
	                  		$(".accSelectDiv").show();
	                  		// disbale staff ids is saved
	                  		$("#accountSelect").val(jsonObj[0].account_id).trigger('change').prop('disabled',true);
	                  	}
	                  	////////////////////////////////////////////////////////////////////////////////////////////
	                  	// disable account password reset
	                  	if(jsonObj[0].password_reset == "off"){
	                      $('#accPasswdReset').prop('checked', false).change()
	                    }
	                    ///////////////////////////////////////////////////////////////////////////////////////////
	                    // if account status is off
	                    if(jsonObj[0].acc_status == "off"){
	                      $('#accStatus').prop('checked', false).change()
	                    }
	                    //////////////////////////////////////////////////////////////////////////////////////////
						$("#userName").val(jsonObj[0].user_name);
						$("#accGroup").val(jsonObj[0].group_id);
	                    $("#usersdata_id").val(jsonObj[0].users_id);
	                    // turn account status button
	                    $("#userssave_btn").text("Update User");
	                    $("#usersmode").val("update");
	                    $("#addUserModal").modal("show");
	              }  
	             });  
	        });
	      // for delete
	        $(document).on('click', '.del_data', function(){
	           if (confirm("ARE YOU SURE YOU WANT TO PROCEED?")) {
	               
	                 let mode= "delete"; 
	                 let data_id = $(this).prop("id");  
	                 $.ajax({  
	                      url:"../controllers/usersController.php",  
	                      method:"POST",  
	                      data:{data_id:data_id,mode:mode},  
	                      success:function(results){
	                        if (results == "success") {
                        		get_all();
	                            toastr.success('Deleted Successfully');
	                        }
	                        else if (results == "error"){
	                            toastr.error('There was an error');
	                        }
	                      }  
	                     }); 

	             }else{
	              return false;
	            }  
	        });

	       // get all data 
		     function get_all(){
		        let mode= "getAll";
		        $.ajax({  
		          url:"../controllers/usersController.php",  
		          method:"POST",  
		          data:{mode:mode},  
		          success:function(results){
		            $('#resultsDisplay').html('');
		            let jsonObj = JSON.parse(results);
		            for (let i = 0; i < jsonObj.length; i++) {
		                $('#resultsDisplay').append('<tr><td>'+jsonObj[i].user_name+'</td><td>'+jsonObj[i].added+'</td><td>'+jsonObj[i].acc_status+'</td><td>'+jsonObj[i].acc_login_status+'</td><td><button class="btn-primary update_data" id="'+jsonObj[i].users_id+'"><i class="fa fa-pencil"></i></button> <button class="btn-danger del_data" id="'+jsonObj[i].users_id+'"><i class="fa fa-trash"></i></button></td></tr>');
		            }
		          }  
		        }); 
		     }
});