 $(document).ready(function(){
  // for search
  $("#viewDocumentSearch").on("keyup", function() {
      var value = $(this).val().toLowerCase();
      $("#viewdocumentDiv tr").filter(function() {
        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
      });
    });
///////////////////////////////////////////////////////////////////////////
  $("#viewtaskSearch").on("keyup", function() {
      var value = $(this).val().toLowerCase();
      $("#viewTaskDiv tr").filter(function() {
        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
      });
    });
/////////////////////////////////////////////////////////////////////////

// close case btn click
 $(document).on('click', '.closeCaseBtn', function(e){
    e.preventDefault();
    let mode= "close_court_case"; 
    let data_id = $(this).prop("id");
    if (data_id !=="") {
      if (confirm("ARE YOU SURE YOU WANT TO PROCEED?")) {  
         $.ajax({  
              url:"../controllers/courtCaseController.php",  
              method:"POST",  
              data:{data_id:data_id,mode:mode},  
              success:function(results){
                // console.log(results);
                if (results == "success") {
                    $('#view_case_detailsModal').modal('hide');
                    toastr.success('Deleted Successfully');
                }
                else if (results == "error"){
                    $('#view_case_detailsModal').modal('hide');
                    toastr.error('There was an error');
                }
              }  
             }); 
      }
    }
    else{
      toastr.error('Sorry!!! There was an error');
    }
 });
//////////////////////////////////////////////////////////////////////////
  $('#view_case_detailsModal').on('hidden.bs.modal', function () {
    $(".modal-title").html("Add Case");
  });

// for viewing case details
$(document).on('click', '.view_case_details', function(){
  $('#viewCaseContactsDiv').html('');
  $('#viewCaseStaffDiv').html('');
  $('#viewEventDiv').html('');
  $('#viewdocumentDiv').html('');
  $('#viewTaskDiv').html('');
  $('#viewMessagesDiv').html('');
  $('#messageNoteDiv').html('');

  let caseMode= "case_view_details";
  let caseId = $(this).prop("id");
  // insert caseId into case note add modal
  $('#view_note_caseId').val(caseId);
  // all notes about case
  $('#view_case_detailsModal').modal("show");
 
    $.ajax({  
      url:"../controllers/courtCaseController.php",
      method:"POST",  
      data:{caseId:caseId,mode:caseMode},  
      success:function(results){
         // console.log(results);
         let mainJsonObj = JSON.parse(results);
         let jsonObj = mainJsonObj.allResults;

         $('#vieCaseHeader').html('<b>'+jsonObj.case_name+'</b> - [ '+jsonObj.case_number+' ]');
          $('#viewCaseName').html('<b>'+jsonObj.case_name+'</b>');
          $('#viewSuitNumber').html('<b>'+jsonObj.case_suit_number+'</b>');
          $('#viewCaseCode').html('<b>'+jsonObj.case_number+'</b>');
          $('#viewPracticeArea').html('<b>'+jsonObj.practice_area_name+'</b>');
          $('#viewCaseStage').html('<b>'+jsonObj.case_stage+'</b>');
          $('#viewCaseCurrentCourt').html('<b>'+jsonObj.main_current_court+'</b>');
          $('#viewCaseNextCourt').html('<b>'+jsonObj.main_next_court+'</b>');
          $('#viewCaseNextCourtDate').html('<b>'+jsonObj.next_court_date+'</b>');
          $('#viewCaseDescription').html('<b>'+jsonObj.case_description+'</b>');
          $('#viewCaseStatus').html('<b style="color:#B22222;">'+jsonObj.case_status+'</b>');
          // check if case is closed or opened

          //////////////// insert close button id on button ////////////////////////////////
          $('.closeCaseBtn').prop('id',jsonObj.case_id);

          switch(jsonObj.case_status){
            case 'CLOSED':
              $('.closeCaseBtn').replaceWith('<button class="btn btn-block closeCaseBtn bg-green">CASE CLOSED <b> ON '+jsonObj.case_closed_date +'</b></button>');
            break;
            case 'OPEN':
              $('.closeCaseBtn').replaceWith('<button class="btn btn-block closeCaseBtn btn-danger" id="">CLOSE CASE <i class="fa fa-lock"></i></button>');
            break;
          }
          $('#viewCaseOpendDate').html('<b>'+jsonObj.added+'</b>');
          $('#viewCaseLeadAttorney').html('<b>'+jsonObj.staff_firstname +' '+jsonObj.staff_lastname+'</b>');
          // insert contact details
          $('#viewCaseContactsDiv').html(mainJsonObj.caseContacts);
          // insert staff details
          $('#viewCaseStaffDiv').html(mainJsonObj.caseStaffs);
          // insert return events details 
          $('#viewEventDiv').html(mainJsonObj.caseEvents);
          // insert return case doucents
          $('#viewdocumentDiv').html(mainJsonObj.caseDocuments);
          // insert return case Taks
           $('#viewTaskDiv').html(mainJsonObj.caseTasks);
          // insert return case messages
          $('#viewMessagesDiv').html(mainJsonObj.caseMessages);
          // insert return case notes
          $('#viewCaseNoteDiv').html(mainJsonObj.caseNotes);
      }

    });  
});
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  


});

