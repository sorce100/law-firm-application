 $(document).ready(function(){
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  $('#eventModal').on('hidden.bs.modal', function () {
      $(".eventTitle").html("Add Event");
      $("#event_data_id").val("");
      $("#eventMode").val("insert");
      $("#event_form")[0].reset();
      $("#eventBtn").replaceWith('<button type="submit" class="btn btn-primary" id="eventBtn">Save Event <i class="fa fa-floppy-o"></i></button>');
      $('#event_form').parsley().reset();
      $('.eventSelect2').val(null).trigger('change').prop('disabled',false);
      $('#contactIds').val('');
      $('#staffIds').val('');
      $('#caseContactsDisplay,#caseStaffsDisplay').html('<tr><td>Select Case</td></tr>');
    });


  $(".eventSelect2").select2({
      dropdownParent: $("#eventModal")
    });
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   // parsley
   $('#event_form').parsley();
   $('#event_form').parsley().reset();
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// change for case contacts and staff assigned to the case
 $('#eventCourtCaseId').change(function(){

     $('#caseContactsDisplay').html('<tr><td>Loading Case Contacts .....</td></tr>');
     $('#caseStaffsDisplay').html('<tr><td>Loading Case Staff .....</td></tr>');

     let caseId = $('option:selected', this).val();
     // get case contacts and staff by ids of selectd case
     let mode = "case_staffContacts_id";
     // get updated mode to check of insert or update
     let modalMode = $('#eventMode').val();

      $.ajax({  
        url:"../controllers/courtCaseController.php",  
        method:"POST",  
        data:{mode:mode,caseId:caseId},  
        success:function(results){
          $('#caseContactsDisplay,#caseStaffsDisplay').html('');
          if (results !=="") {
            let jsonObj = JSON.parse(results);
            // get contacts checkboxes for share and attend
            $('#contactIds').val(jsonObj['case_contactIds']);
            get_case_contacts(jsonObj['case_contactIds']);
            // get staff checkboxes for share and attend
            $('#staffIds').val(jsonObj['case_staffIds']);
            get_case_staff(jsonObj['case_staffIds']);
          }else{
            toastr.error('Sorry!!! Try again');
          }
          
        }  
      });
  });
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// functions to get the name of contacts and staff
function get_case_contacts(caseContactIds){
    let mode = "getContactData";
    $.ajax({  
      url:"../controllers/contactController.php",  
      method:"POST",  
      data:{contactMode:mode,caseContactIds:caseContactIds},  
      success:function(results){
        $('#caseContactsDisplay').append(results)
      }  
    }); 
 }

 function get_case_staff(caseStaffIds){
    let mode = "getStaffData";
    $.ajax({  
      url:"../controllers/staffController.php",  
      method:"POST",  
      data:{mode:mode,caseStaffIds:caseStaffIds},  
      success:function(results){
        $('#caseStaffsDisplay').append(results);
      }  
    }); 
 }

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //for inserting 
    $("#event_form").submit(function(e){
          e.preventDefault();
          $("#eventCourtCaseId").prop('disabled',false);

          // $('.eventContactShare[type="checkbox"]').each(function() {
          //       if ($(this).is(":checked")) {
          //         $('#eventContactShareHidden').prop('disabled',true);
          //       }
          //   });

          //  $('.eventStaffShare[type="checkbox"]').each(function() {
          //       if ($(this).is(":checked")) {
          //         $('#eventStaffShareHidden').prop('disabled',true);
          //       }
          //   });

          $.ajax({
              url:"../controllers/eventsController.php",
              method:"POST",
              data:$("#event_form").serialize(),
              beforeSend:function(){  
                $('#eventBtn').text("Loading ...").prop("disabled",true); 
              },
              success:function(results){
                // console.log(results);
                  $("#eventModal").modal("hide");
                 
                 if (results == "success") {
                    toastr.success('Saved Successfully');
                    $("#eventBtn").replaceWith('<button type="submit" class="btn btn-primary" id="eventBtn">Save Event <i class="fa fa-floppy-o"></i></button>');
                    get_all();
                    // $("#event_form").reset();
                    // $('#eventBtn').prop('disabled',false).text('Save & Finish');
                 }
                 else if (results == "error"){
                    toastr.error('There was an error');
                    $("#eventBtn").replaceWith('<button type="submit" class="btn btn-primary" id="eventBtn">Save Event <i class="fa fa-floppy-o"></i></button>');
                    get_all();
                    // $("#eventModal").modal("hide");
                    // $("#event_form").reset();

                 }
              } 

          });  
      });
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// UPDATE EVENT DETAILS
$(document).on('click', '.update_event_detail', function(e){
      e.preventDefault();
      var eventId = $(this).prop('id');
      var mode = "updateModal";
     $.ajax({  
        url:"../controllers/eventsController.php",
        method:"POST",  
        data:{data_id:eventId,mode:mode},
        success:function(results){
            $(".eventTitle").html("Update Event");
            let mainJsonObj = JSON.parse(results);
            // console.log(mainJsonObj.eventStaffsData);
              //all results 
            let jsonObj = mainJsonObj.allResults;  
             // changing modal title
            $("#eventCourtCaseId").val(jsonObj.event_court_case_id).trigger('change').prop('disabled',true);
            $("#eventName").val(jsonObj.event_name);
            $("#eventType").val(jsonObj.event_typeId).trigger('change');
            $("#eventStartDate").val(jsonObj.event_start_date);
            $("#eventEndDate").val(jsonObj.event_end_Date);
            $("#eventLocation").val(jsonObj.event_location);
            $("#eventDescription").val(jsonObj.event_description);

            var eventStaffShare = document.getElementsByClassName("eventContactShare");
            // var foundElement = $('#caseContactsDisplay').html();
            console.log(eventStaffShare);

            
            // let jsonObjeventStaffShare = JSON.parse(jsonObj.event_staff_share);
            //   for (let i = 0; i < jsonObjeventStaffShare.length; ++i) {
            //      $('.eventStaffShare[value="'+jsonObjeventStaffShare[i]+'"]').each(function() {
            //         $(this).prop('checked',true);
            //       });
            //   }

            $("#eventMode").val("update");
            $("#event_data_id").val(jsonObj.event_id);
            $("#eventBtn").text("Update Event");
            
            $("#eventModal").modal("show");

       }
    });
});

// view events
$(document).on('click', '.view_event_detail', function(e){
      e.preventDefault();
      var eventId = $(this).prop('id');
      var mode = "updateModal";
     $.ajax({  
        url:"../controllers/eventsController.php",
        method:"POST",  
        data:{data_id:eventId,mode:mode},
        success:function(results){
            $(".eventTitle").html("View Event");
            let mainJsonObj = JSON.parse(results);
            // console.log(mainJsonObj.eventStaffsData);
              //all results 
            let jsonObj = mainJsonObj.allResults;  
             // changing modal title
            $("#eventCourtCaseId").val(jsonObj.event_court_case_id).trigger('change').prop('disabled',true);
            $("#eventName").val(jsonObj.event_name);
            $("#eventType").val(jsonObj.event_typeId).trigger('change');
            $("#eventStartDate").val(jsonObj.event_start_date);
            $("#eventEndDate").val(jsonObj.event_end_Date);
            $("#eventLocation").val(jsonObj.event_location);
            $("#eventDescription").val(jsonObj.event_description);

            // console.log($('#caseStaffsDisplay').html());

            
            let jsonObjeventStaffShare = JSON.parse(jsonObj.event_staff_share);
              for (let i = 0; i < jsonObjeventStaffShare.length; ++i) {
                 $('.eventStaffShare[value="'+jsonObjeventStaffShare[i]+'"]').each(function() {
                    $(this).prop('checked',true);
                  });
              }

            $("#eventMode").val("");
            $("#event_data_id").val("");
            $("#eventBtn").replaceWith('<span id="eventBtn"></span>');
            
            $("#eventModal").modal("show");

       }
    });
});
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
      
// for delete
        $(document).on('click', '.del_data', function(e){
           e.preventDefault();
           if (confirm("ARE YOU SURE YOU WANT TO PROCEED?")) {
                 let mode= "delete"; 
                 let data_id = $(this).prop("id");  
                 $.ajax({  
                      url:"../controllers/eventsController.php",  
                      method:"POST",  
                      data:{data_id:data_id,mode:mode},  
                      success:function(results){
                        // console.log(results);
                        switch(results){
                          case 'success':
                            get_all();
                            toastr.success('Deleted Successfully');
                          break;
                          case 'error':
                            get_all();
                            toastr.error('There was an error');
                          break;
                          default:
                          toastr.error('There was an error');
                        }

                      }  
                  }); 

             }else{
              return false;
            }  
        });

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    // get all data 
     function get_all(){
        let mode = "getAll";
        $.ajax({  
          url:"../controllers/eventsController.php",  
          method:"POST",  
          data:{mode:mode},  
          success:function(results){
            $('#eventResultsDisplay').html('');
            $('#eventResultsDisplay').html(results);
          }  
        }); 
     }
 });