 $(document).ready(function(){
   ////////////////////////////////////////////////////////////////////////
  $(".clientMessageSelect2").select2({
      dropdownParent: $("#clientMessagesModal")
    });
   /////////////////////////////////////////////////////////////////////
    $('#clientMessagesModal').on('hidden.bs.modal', function () {
      $(".modal-title").html("New Message");
      $(this).find('input:text').css({"border-color":"#808080"});
      $("#client_messages_form")[0].reset();
      $("#messages_btn").attr('disabled',false).text("Send Message");
      $('.clientMessageSelect2').val(null).trigger('change');
      $('#messageContent').summernote('reset');
      //set first fieldset and clear the form 
      $('.f1 fieldset').each(function() { $(this).css({'display':'none'}); });
      $('.fi input[type="text"], input[type="number"], textarea').each(function() { $(this).val(''); });
      $('.f1 fieldset:first-child').css({'display':'block'});
      ///////////////////////////////////////////////////
      // clear checkboxes 
      $('#messageContactIds[type="checkbox"]').each(function() { $(this).prop('disabled',false); });
      $('#messageStaffIds[type="checkbox"]').each(function() { $(this).prop('disabled',false); });
      $('#select_all,#select_all_staff').prop('disabled',false);

      //////////////////////////////////////////////////////////
        $("#message_courtCaseId").val(null).trigger('change').prop('disabled',false);
        $('#messageSubject').val('').prop('readonly',false);
        $('.btn-next').html('Compose Message');
        $("#messages_btn").replaceWith('<button type="submit" class="btn btn-primary" id="messages_btn">Send Message <i class="fa fa-send"></i></button>');
    });
    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    // summernote
    $('.summernote').summernote({
      height: 300,
      focus: true,
      disableResizeEditor: true,
      toolbar: [
          // [groupName, [list of button]]
          ['style', ['bold', 'italic', 'underline', 'clear']],
          ['font', ['strikethrough']],
          ['fontsize', ['fontsize']],
          ['color', ['color']],
          ['para', ['ul', 'ol', 'paragraph']],
          ['height', ['height']],
          ['insert', ['table']],
          ['view', ['fullscreen']],
        ]
    });
    // client read message summernote
    $('.readSummernote').summernote({
      height: 350,
      focus: true,
      disableResizeEditor: true,
      toolbar: [
          ['view', ['fullscreen']]
        ]
    });
  ///////////////////////////////////////////////////////////////////////
  // change for case contacts and staff assigned to the case
   $('#message_courtCaseId').change(function(){
    // deselecting staff and contact checkboxes
      $('#messageContactIds[type="checkbox"]').each(function() { $(this).prop('checked',false); });
      $('#messageStaffIds[type="checkbox"]').each(function() { $(this).prop('checked',false); });
      // get case ID
     let caseId = $('option:selected', this).prop('value');
      // disable table to check checkboxes
      $('.tableList').DataTable().destroy();
       // get staff and case ids associated with case
       get_case_contacts_staff(caseId);
       // enable tablelist after inserting
       $('.tableList').dataTable({ordering: false,});
  });

   // get case contacts and staff by id
  function get_case_contacts_staff(caseId){
    let mode = "case_staffContacts_id";
      $.ajax({  
        url:"../controllers/courtCaseController.php",  
        method:"POST",  
        data:{mode:mode,caseId:caseId},  
        success:function(results){

          let jsonObj = JSON.parse(results);
          let returnContactId = jsonObj['case_contactIds']; 
          let returnstaffId = jsonObj['case_staffIds'];

          let jsonObjContactsId = JSON.parse(returnContactId);
           for (var i = 0; i < jsonObjContactsId.length; i++) {
             $('#messageContactIds[value="'+jsonObjContactsId[i]+'"]').each(function() { $(this).prop('checked',true); });
           }
           // case staff function
      
           let jsonObjStaffsId = JSON.parse(returnstaffId);
            for (var j = 0; j < jsonObjStaffsId.length; j++) {
              $('#messageStaffIds[value="'+jsonObjStaffsId[j]+'"]').each(function() { $(this).prop('checked',true); });
            }
          
        }  
      });
  }


////////////////////////////////////////////////////////////////////////////////
      //for inserting 
        $("#client_messages_form").submit(function(e){
            e.preventDefault();
              $.ajax({
              url:"../controllers/messagesController.php",
              method:"POST",
              data:$("#client_messages_form").serialize(),
              beforeSend:function(){  
                        $('#messages_btn').text("Loading ...").prop("disabled",true); 
                   },
              success:function(results){
                // console.log(results);
                    $("#clientMessagesModal").modal("hide");
                   
                   if (results == "success") {
                        get_all_sent();
                        get_all_inbox();
                        toastr.success('Saved Successfully');
                        $("#client_messages_form")[0].reset();
                        $('#messages_btn').attr('disabled',false).text('Save Contact');
                   }
                   else if (results == "error"){
                      get_all_sent();
                      get_all_inbox();
                      toastr.error('There was an error');
                      $("#clientMessagesModal").modal("hide");
                      $("#client_messages_form")[0].reset();

                   }
              } 

              });  
          });
      // for update
      $(document).on('click', '.view_sent_message', function(){
         let mode= "updateModal"; 
         let data_id = $(this).prop("id");
           //make all checkboxes readonly
           $('#messageContactIds[type="checkbox"]').each(function() {
              $(this).prop('disabled',true);
            });
            $('#messageStaffIds[type="checkbox"]').each(function() {
              $(this).prop('disabled',true);
            });
            $('#select_all,#select_all_staff').prop('disabled',true);
         $.ajax({  
              url:"../controllers/messagesController.php",
              method:"POST",  
              data:{data_id:data_id,mode:mode},  
              success:function(results){
                // console.log(results);
                   let jsonObj = JSON.parse(results);  
                   // changing modal title
                    $(".modal-title").html("View Message");
                    ////////////////////////////////////////////////////////////////////////////////////
                    if (jsonObj[0].message_subject !='') {
                      $("#message_courtCaseId").val(jsonObj[0].message_courtCaseId).trigger('change').prop('disabled',true);
                    }
                    else{
                      $('.clientMessageSelect2').val(null).trigger('change');
                    }
                     // disable table to check checkboxes
                    $('.tableList').DataTable().destroy();
                    ///////////////////////////////////////////////////////////////////////////////////////
                    let jsonObjContactsId = JSON.parse(jsonObj[0].message_receive_contacts);
                     for (var i = 0; i < jsonObjContactsId.length; i++) {
                       $('#messageContactIds[value="'+jsonObjContactsId[i]+'"]').each(function() {
                          $(this).prop('checked',true);
                        });
                     }
                     // case staff function
                     let jsonObjStaffsId = JSON.parse(jsonObj[0].message_receive_staff);
                      for (var j = 0; j < jsonObjStaffsId.length; j++) {
                        $('#messageStaffIds[value="'+jsonObjStaffsId[j]+'"]').each(function() {
                          $(this).prop('checked',true);
                        });
                      }
                    //////////////////////////////////////////////////////////////////////////////////////////
                    $('#messageSubject').val(jsonObj[0].message_subject).prop('readonly',true);
                    $('#messageContent').summernote('code', jsonObj[0].message_content);
                    $('.btn-next').html('View Message');
                    // enable tablelist after inserting
                    $('.tableList').dataTable({ordering: false,});
                    $("#messages_btn").html('<button type="button" class="btn btn-danger" data-dismiss="modal">Close <i class="fa fa-times"></i></button>');
                    $("#clientMessagesModal").modal("show");
              }  
             });  
        });
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
              $(document).on('click', '.clientReadMessage', function(){
                 let mode= "updateModal"; 
                 let data_id = $(this).prop("id");
                 $.ajax({  
                      url:"../controllers/messagesController.php",
                      method:"POST",  
                      data:{data_id:data_id,mode:mode},  
                      success:function(results){
                        $('.summernote').summernote('disable');
                        // console.log(results);
                           let jsonObj = JSON.parse(results);  
                           // changing modal title
                            $("#ViewMessagesubject").html("View Message");
                            ////////////////////////////////////////////////////////////////////////////////////
                            $("#message_courtCaseId").val(jsonObj[0].message_courtCaseId).trigger('change').prop('disabled',true);
                            //////////////////////////////////////////////////////////////////////////////////////////
                            $('#readSubject').val(jsonObj[0].message_subject).prop('readonly',true);
                            $('#readContent').summernote('code',jsonObj[0].message_content);
                            // enable tablelist after inserting
                            $("#clientReadMessageModal").modal("show");
                      }  
                     });  
                });
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
      
// for delete
        $(document).on('click', '.view_sent_message_del_data', function(){
           if (confirm("ARE YOU SURE YOU WANT TO PROCEED?")) {
                 let mode= "senderDelete"; 
                 let data_id = $(this).prop("id");  
                 $.ajax({  
                      url:"../controllers/messagesController.php",  
                      method:"POST",  
                      data:{data_id:data_id,mode:mode},  
                      success:function(results){
                        console.log(results);
                        if (results == "success") {
                            get_all_sent();
                            toastr.success('Deleted Successfully');
                        }
                        else if (results == "error"){
                            get_all_sent();
                            toastr.error('There was an error');
                        }
                      }  
                     }); 

             }else{
              return false;
            }  
        });

        // inbox delete
        $(document).on('click', '.inbox_del_data', function(){
           if (confirm("ARE YOU SURE YOU WANT TO PROCEED?")) {
                 let mode = "receiverDelete"; 
                 let data_id = $(this).prop("id");  
                 $.ajax({  
                      url:"../controllers/messagesController.php",  
                      method:"POST",  
                      data:{data_id:data_id,mode:mode},  
                      success:function(results){
                        console.log(results);
                        if (results == "success") {
                            get_all_inbox();
                            toastr.success('Deleted Successfully');
                        }
                        else if (results == "error"){
                            get_all_inbox();
                            toastr.error('There was an error');
                        }
                      }  
                     }); 

             }else{
              return false;
            }  
        });
    // get all sent messaages
         function get_all_sent(){
            let mode = "getAll_sent";
            $.ajax({  
              url:"../controllers/messagesController.php",  
              method:"POST",  
              data:{mode:mode},  
              success:function(results){
                // console.log(results);
                $('#sentDisplay').html('');
                let jsonObj = JSON.parse(results);
                for (let i = 0; i < jsonObj.length; i++) {
                    $('#sentDisplay').append('<tr><td>'+jsonObj[i].message_subject+'</td><td>'+jsonObj[i].added+'</td><td><button class="btn-primary view_sent_message" id="'+jsonObj[i].message_id+'"><i class="fa fa-eye"></i></button> <button class="btn-danger view_sent_message_del_data" id="'+jsonObj[i].message_id+'"><i class="fa fa-trash"></i></button></td></tr>');
                }
              }  
            }); 
         }
        // get all received messages
        function get_all_inbox(){
            let mode = "getAll_received";
            $.ajax({  
              url:"../controllers/messagesController.php",  
              method:"POST",  
              data:{mode:mode},  
              success:function(results){
                // console.log(results);
                $('#inboxDisplay').html('');
                let jsonObj = JSON.parse(results);
                for (let i = 0; i < jsonObj.length; i++) {
                    $('#inboxDisplay').append('<tr><td>'+jsonObj[i].message_subject+'</td><td>'+jsonObj[i].message_sender_accounId+'</td><td>'+jsonObj[i].added+'</td><td><button class="btn-primary view_sent_message" id="'+jsonObj[i].message_id+'"><i class="fa fa-eye"></i></button> <button class="btn-danger inbox_del_data" id="'+jsonObj[i].message_id+'"><i class="fa fa-trash"></i></button></td></tr>');
                }
              }  
            }); 
         }
 });