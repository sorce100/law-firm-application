$(document).ready(function(){
  /////////////////////////////////////////////////////////////////////////////////////////////
    $('#expensesBillable').bootstrapToggle({
      on: 'Enable',
      off: 'Disable',
      onstyle: 'success',
      offstyle: 'danger'
    });
   $('#expensesBillable').change(function(){
      if($(this).prop('checked')){
       $('#expensesBillable_log').val('YES');
      }else {
       $('#expensesBillable_log').val('NO');
      }
   });
  /////////////////////////////////////////////////////////////////////////////////////////////


  $(".Expensesselect2").select2({
      dropdownParent: $("#expensesModal")
    });

    // active only staff using id
  $('#expensesStaff').val($('#StaffAccountID').val()).prop('disabled',true);
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#expensesModal').on('hidden.bs.modal', function () {
      $("#Expensesubject").html("Add Expenses");
      $(this).find('input:text').css({"border-color":"#808080"});
      $("#expenses_form")[0].reset();
      $("#expenses_btn").attr('disabled',false).text("Add");
      $('#expenses_form').parsley().reset();
      $('.Expensesselect2').val(null).trigger('change');
      $('#expensesStaff').val($('#accountId').val()).trigger('change');
    });
      //for inserting 
        $("#expenses_form").submit(function(e){
            e.preventDefault();
            $('#expensesStaff').prop('disabled',false);
              $.ajax({
              url:"../controllers/billExpensesController.php",
              method:"POST",
              data:$("#expenses_form").serialize(),
              beforeSend:function(){  
                        $('#expenses_btn').text("Loading ...").prop("disabled",true); 
                   },
              success:function(results){
                // console.log(results);
                    $("#expensesModal").modal("hide");
                   
                   if (results == "success") {
                        get_all();
                        toastr.success('Saved Successfully');
                        $("#expenses_form")[0].reset();
                        $('#expenses_btn').attr('disabled',false).text('Add');
                   }
                   else if (results == "error"){
                      get_all();
                      toastr.error('There was an error');
                      $("#expensesModal").modal("hide");
                      $("#expenses_form")[0].reset();

                   }
              } 

              });  
          });

      // for update
      $(document).on('click', '.expenses_update_data', function(){
         let mode= "updateModal"; 
         let data_id = $(this).prop("id");  
         $.ajax({  
              url:"../controllers/billExpensesController.php",
              method:"POST",  
              data:{data_id:data_id,mode:mode},  
              success:function(results){
                // console.log(results);
                   let jsonObj = JSON.parse(results);  
                   // changing modal title
                    $("#Expensesubject").html("Update Expenses");
                    $("#expenseCourtCaseId").val(jsonObj[0].bill_expenses_case_id).trigger('change');
                    $("#expensesStaff").val(jsonObj[0].bill_expenses_staff_id).prop('disabled',true);;
                    $("#expensesActivity").val(jsonObj[0].bill_expenses_activity);
                    if(jsonObj[0].bill_expenses_billable == "off"){
                      $('#expensesBillable').prop('checked', false).change()
                    }
                    $("#expensesDescribe").val(jsonObj[0].bill_expenses_describe);
                    $("#expensesDate").val(jsonObj[0].bill_expenses_date);
                    $("#expensesCost").val(jsonObj[0].bill_expenses_cost);
                    $("#expensesQuantity").val(jsonObj[0].bill_expenses_quantity);
                    $("#expenses_data_id").val(jsonObj[0].bill_expenses_id);
                    $("#expenses_btn").text("Update Expenses");
                    $("#expensesMode").val("update");
                    $("#expensesModal").modal("show");
              }  
             });  
        });
// view data
    $(document).on('click', '.expenses_view', function(){
         let mode= "updateModal"; 
         let data_id = $(this).prop("id");  
         $.ajax({  
              url:"../controllers/billExpensesController.php",
              method:"POST",  
              data:{data_id:data_id,mode:mode},  
              success:function(results){
                // console.log(results);
                   let jsonObj = JSON.parse(results);  
                   // changing modal title
                    $("#Expensesubject").html("View Expenses");
                    $("#expenseCourtCaseId").val(jsonObj[0].bill_expenses_case_id).trigger('change');
                    $("#expensesStaff").val(jsonObj[0].bill_expenses_staff_id).prop('disabled',true);;
                    $("#expensesActivity").val(jsonObj[0].bill_expenses_activity);
                    if(jsonObj[0].bill_expenses_billable == "off"){
                      $('#expensesBillable').prop('checked', false).change()
                    }
                    $("#expensesDescribe").val(jsonObj[0].bill_expenses_describe);
                    $("#expensesDate").val(jsonObj[0].bill_expenses_date);
                    $("#expensesCost").val(jsonObj[0].bill_expenses_cost);
                    $("#expensesQuantity").val(jsonObj[0].bill_expenses_quantity);
                    $("#expenses_data_id").val(jsonObj[0].bill_expenses_id);
                    $("#expenses_btn").text("View Expenses");
                    $("#expensesModal").modal("show");
              }  
             });  
        });
      
// for delete
        $(document).on('click', '.expenses_del_data', function(){
           if (confirm("ARE YOU SURE YOU WANT TO PROCEED?")) {
                 let mode= "delete"; 
                 let data_id = $(this).prop("id");  
                 $.ajax({  
                      url:"../controllers/billExpensesController.php",  
                      method:"POST",  
                      data:{data_id:data_id,mode:mode},  
                      success:function(results){
                        // console.log(results);
                        if (results == "success") {
                            get_all();
                            toastr.success('Deleted Successfully');
                        }
                        else if (results == "error"){
                            get_all();
                            toastr.error('There was an error');
                        }
                      }  
                     }); 

             }else{
              return false;
            }  
        });

  // get all data 
         function get_all(){
            let mode = "getAll";
            $.ajax({  
              url:"../controllers/billExpensesController.php",  
              method:"POST",  
              data:{mode:mode},  
              success:function(results){
                // console.log(results);
                $('#expensesDisplay').html('');
                let jsonObj = JSON.parse(results);
                for (let i = 0; i < jsonObj.length; i++) {
                    $('#expensesDisplay').append('<tr><td>'+jsonObj[i].caseName+'</td><td>'+jsonObj[i].bill_expenses_date+'</td><td>'+jsonObj[i].bill_expenses_activity+'</td><td>'+jsonObj[i].bill_expenses_quantity+'</td><td>'+jsonObj[i].bill_expenses_cost+'</td><td>'+jsonObj[i].bill_expenses_total+'</td><td>'+jsonObj[i].bill_expenses_invoiced+'</td><td>'+jsonObj[i].bill_expenses_staff_id+'</td><td><button class="btn-primary expenses_update_data" id="'+jsonObj[i].bill_expenses_id+'"><i class="fa fa-pencil"></i></button> <button class="btn-danger del_data" id="'+jsonObj[i].bill_expenses_id+'"><i class="fa fa-trash"></i></button></td></tr>');
                }
              }  
            }); 
         }  
});