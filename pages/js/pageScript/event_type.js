$(document).ready(function(){
        // for reset modal when close
      $('#eventTypeModal').on('hidden.bs.modal', function () {
          $(".modal-title").html("Add Pages");
          $("#eventType_form")[0].reset();
          $("#eventTypeSave_btn").text("Add Event Type");
        });

      //for inserting 
        $("#eventType_form").submit(function(e){
            e.preventDefault();
            // fields validation
              $.ajax({
              url:"../controllers/eventTypeController.php",
              method:"POST",
              data:$("#eventType_form").serialize(),
              beforeSend:function(){  
                    $('#eventTypeSave_btn').text("Loading ...").prop("disabled",true); 
               },
              success:function(results){
                  console.log(results); 
                   $('#eventTypeSave_btn').text("Add Event Type").prop("disabled",false);
                   $("#eventTypeModal").modal("hide");
                   
                   if (results == "success") {
                      get_all();
                      toastr.success('Saved Successfully');
                   }
                   else if (results == "error"){
                      toastr.error('There was an error');
                      $("#eventTypeModal").modal("hide");
                      $("#eventType_form")[0].reset();  
                   }
              } 

              });  
          });
      // for update
      $(document).on('click', '.update_data', function(){
         let mode= "updateModal"; 
         let data_id = $(this).prop("id");  
         $.ajax({  
              url:"../controllers/eventTypeController.php",
              method:"POST",  
              data:{data_id:data_id,mode:mode},  
              success:function(results){
                  let jsonObj = JSON.parse(results);  
                   // changing modal title
                  $(".modal-title").html("Update Event Type");
                  $("#eventType").val(jsonObj.event_type_name);
                  $("#eventColorPicker").val(jsonObj.event_type_color);
                  $("#eventType_data_id").val(jsonObj.event_type_id);
                  $("#eventTypeSave_btn").text("UPDATE EVENT TYPE");
                  $("#eventTypeMode").val("update");
                  $("#eventTypeModal").modal("show");
              }  
             });  
        });
// for delete
        $(document).on('click', '.del_data', function(){
           if (confirm("ARE YOU SURE YOU WANT TO PROCEED?")) {
                 let mode= "delete"; 
                 let data_id = $(this).prop("id");  
                 $.ajax({  
                      url:"../controllers/eventTypeController.php",  
                      method:"POST",  
                      data:{data_id:data_id,mode:mode},  
                      success:function(results){
                        if (results == "success") {
                          get_all();
                          toastr.success('Deleted Successfully');
                        }
                        else if (results == "error"){
                          toastr.error('There was an error');
                        }
                      }  
                     }); 

             }else{
              return false;
            }  
        });
// get all data 
     function get_all(){
        let mode= "getAll";
        $.ajax({  
          url:"../controllers/eventTypeController.php",  
          method:"POST",  
          data:{mode:mode},  
          success:function(results){
            $('#resultsDisplay').html('');
            let jsonObj = JSON.parse(results);
            for (let i = 0; i < jsonObj.length; i++) {
                $('#resultsDisplay').append('<tr><td>'+jsonObj[i].event_type_name+'</td><td><div class="smallBox" style="background:'+jsonObj[i].event_type_color+';"></div></td><td>'+jsonObj[i].added+'</td><td><button class="btn-primary update_data" id="'+jsonObj[i].event_type_id+'"><i class="fa fa-pencil"></i></button> <button class="btn-danger del_data" id="'+jsonObj[i].event_type_id+'"><i class="fa fa-trash"></i></button></td></tr>');
            }
          }  
        }); 
     }
});  