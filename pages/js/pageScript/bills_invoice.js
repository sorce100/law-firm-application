 $(document).ready(function(){

  $('#invoiceModal').on('hidden.bs.modal', function () {
      $("#invoicesubject").html("Add Invoice");
      $(this).find('input:text').css({"border-color":"#808080"});
      $("#invoice_form")[0].reset();
      $("#invoice_btn").prop('disabled',false).text("Add");
      $('#invoice_form').parsley().reset();
      $('.Invoiceselect2').val(null).trigger('change');
    });
  /////////////////////////////////////////////////////////////////////////////////////////////
    $('#invoiceBillable').bootstrapToggle({
      on: 'Enable',
      off: 'Disable',
      onstyle: 'success',
      offstyle: 'danger'
    });
   $('#invoiceBillable').change(function(){
      if($(this).prop('checked')){
       $('#invoiceBillable_log').val('YES');
      }else {
       $('#invoiceBillable_log').val('NO');
      }
   });
  /////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////

  $(".Invoiceselect2").select2({
      dropdownParent: $("#invoiceModal")
    });
   // parsley
   $('#invoice_form').parsley();
   $('#invoice_form').parsley().reset();

  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // declaring variables to hold subtotals for timeentry total and expenses total
  let sumTotalTimeEntries=0;
  let sumTotalExpenses=0;
  let mainTotal=0;
  // click to change and fill all entries
  $('#invoiceCourtCaseId').change(function(){
    // counters reset to 0
    sumTotalTimeEntries=0;
    sumTotalExpenses=0;
    mainTotal=0;
    // clear all tds for inserting total and subtotal
    $('#timeEntryTotalTd,#expensesTotalTd,#mainTotalTd').html('');
    $('#timeEntryTotal,#expensesTotal,#mainTotal').val('');

     let caseId = $('option:selected', this).prop('value');
     let caseName = $('option:selected', this).text();
     // clear case contacts
     $('#invoiceContact').html('');
     $('#invoiceContact').append('<option disabled selected>Select Contact</option>');
     // clear add time entries and expenses div
     $('#timeEntriesDiv').html('');
     $('#expensesDiv').html('');
     // get contacts associated with case
     get_case_contactsIds(caseId);
     // get case time entries
     get_case_time_entries(caseId,caseName);
     // get expenses
     get_case_expenses(caseId,caseName);
  });

  
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // function for grabing contacts of a case
  function get_case_contactsIds(caseId){
    let mode = "updateModal";
    $.ajax({  
      url:"../controllers/courtCaseController.php",  
      method:"POST",  
      data:{mode:mode,data_id:caseId},  
      success:function(results){
        let jsonObj = JSON.parse(results);
        let jsonObjcaseId = JSON.parse(jsonObj[0].case_contactIds);
        // loop through contact 
        for (var i = 0; i < jsonObjcaseId.length; i++) {
          get_contact_name(jsonObjcaseId[i])
        }
      }  
    });
  }
  // function to get the name of contacts
  function get_contact_name(contactId){
    let mode = "updateModal";
    let data_id = contactId;
    $.ajax({  
      url:"../controllers/contactController.php",  
      method:"POST",  
      data:{contactMode:mode,contactData_id:data_id},  
      success:function(results){
        let jsonObj = JSON.parse(results);
        $('#invoiceContact').append('<option id="'+jsonObj[0].contact_address+'" value="'+jsonObj[0].contact_id+'">'+jsonObj[0].contact_First_name+' '+jsonObj[0].contact_last_name+'</option>');
      }  
    }); 
  }

  // get contact address and insert into address
  $('#invoiceContact').change(function(){
    let contactAddress = $('option:selected', this).prop('id');
    // insert into invoice address
    $('#invoiceAddress').html(contactAddress);
  });


  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // Getting Time Entries for a case

  function get_case_time_entries(caseId,caseName){
    // total time entries
    let mode = "getTimeEntries_by_caseId";
    $.ajax({  
      url:"../controllers/billTimeEntryController.php",  
      method:"POST",  
      data:{mode:mode,caseId:caseId},  
      success:function(results){
        // console.log(results);
        let jsonObj = JSON.parse(results);

        // loop through contact 
        for (var i = 0; i < jsonObj.length; i++) {
          sumTotalTimeEntries = (parseFloat(sumTotalTimeEntries) + parseFloat(jsonObj[i].time_entry_total));
          $('#timeEntriesDiv').append(
            '<tr>'+
              '<td>'+caseName+'</td>'+
              '<td>'+jsonObj[i].time_entry_date+'</td>'+
              '<td>'+jsonObj[i].time_entry_staff_id+'</td>'+
              '<td>'+jsonObj[i].time_entry_activity+'</td>'+
              '<td>'+jsonObj[i].time_entry_rate+'</td>'+
              '<td>'+jsonObj[i].time_entry_duration+'</td>'+
              '<td>'+jsonObj[i].time_entry_total+'</td>'+
              '<td><input type="checkbox" checked alt="'+jsonObj[i].time_entry_total+'" id="timeEnteryInvoiceAdd" name="timeEnteryInvoiceAdd[]" value="'+jsonObj[i].time_entry_id+'"></td>'+
            '</tr>'
          );
        }
        // insert time entries total 
        $('#timeEntryTotalTd').html(sumTotalTimeEntries);
          // insert value into dom
        $('#timeEntryTotal').val(sumTotalTimeEntries);
      }  
    });
  }

  //Getting expenses entries
  function get_case_expenses(caseId,caseName){
    let mode = "getExpenses_by_caseId";
    $.ajax({  
      url:"../controllers/billExpensesController.php",  
      method:"POST",  
      data:{mode:mode,caseId:caseId},  
      success:function(results){
        let jsonObj = JSON.parse(results);
        // loop through contact 
        for (var i = 0; i < jsonObj.length; i++) {
           sumTotalExpenses = (parseFloat(sumTotalExpenses) + parseFloat(jsonObj[i].bill_expenses_total));
          $('#expensesDiv').append(
            '<tr>'+
              '<td>'+caseName+'</td>'+
              '<td>'+jsonObj[i].bill_expenses_date+'</td>'+
              '<td>'+jsonObj[i].bill_expenses_staff_id+'</td>'+
              '<td>'+jsonObj[i].bill_expenses_cost+'</td>'+
              '<td>'+jsonObj[i].bill_expenses_quantity+'</td>'+
              '<td>'+jsonObj[i].bill_expenses_total+'</td>'+
              '<td><input type="checkbox" checked alt="'+jsonObj[i].bill_expenses_total+'" id="expensesInvoiceAdd" name="expensesInvoiceAdd[]" value="'+jsonObj[i].bill_expenses_id+'"></td>'+
            '</tr>'

          );
        }
        // insert time entries total 
        $('#expensesTotalTd').html(sumTotalExpenses);
        // insert value into dom
        $('#expensesTotal').val(sumTotalExpenses);
        // insert total into dom
        // get total of time entry subtotal and expenses subtotal and insert into total
        mainTotal = (parseFloat(sumTotalTimeEntries) + parseFloat(sumTotalExpenses));
        $('#mainTotalTd').html(mainTotal);
        // insert main total into dom
        $('#mainTotal').val(mainTotal);
      }  
    });
  } 
///////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////
// function for Staff Name
function get_staff_name(staffId){
    let mode = "staff_name_only";
    $.ajax({  
      url:"../controllers/StaffController.php",  
      method:"POST",  
      data:{mode:mode,data_id:staffId},  
      success:function(results){
      return function () { return results; };
      }  
    });
  }
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////Select calculate///////////////////////////////////////////////////////////////////////////////////////
 $(document).on('click', '#timeEnteryInvoiceAdd', function(){
  let timeEntryId = $(this).val();
  let timeEntryTotal = $(this).prop('alt');
  // if checbox is checked
  if($(this).prop('checked') == true){
      // insert total of all selected time entry into the totaltime
       sumTotalTimeEntries = (parseFloat(sumTotalTimeEntries) + parseFloat(timeEntryTotal));
       // calulate and insert into td and input
       $('#timeEntryTotalTd').html(sumTotalTimeEntries);
       $('#timeEntryTotal').val(sumTotalTimeEntries);
      // grab total and subtract from total and insert 
       mainTotal = (parseFloat(mainTotal) + parseFloat(timeEntryTotal));
       $('#mainTotalTd').html(mainTotal);
       $('#mainTotal').val(mainTotal);
  }
  // if checkbox is unchecked
  else if($(this).prop('checked') == false){
      // insert total of all selected time entry into the totaltime
       sumTotalTimeEntries = (parseFloat(sumTotalTimeEntries) - parseFloat(timeEntryTotal));
       // calulate and insert into td and input
       $('#timeEntryTotalTd').html(sumTotalTimeEntries);
       $('#timeEntryTotal').val(sumTotalTimeEntries);
      // grab total and subtract from total and insert 
       mainTotal = (parseFloat(mainTotal) - parseFloat(timeEntryTotal));
       $('#mainTotalTd').html(mainTotal);
       $('#mainTotal').val(mainTotal);
  }
  
});
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////Select calculate///////////////////////////////////////////////////////////////////////////////////////
 $(document).on('click', '#expensesInvoiceAdd', function(){
  let expensesId = $(this).val();
  let expensesTotal = $(this).prop('alt');
  // if checbox is checked
  if($(this).prop('checked') == true){
      // insert total of all selected time entry into the totaltime
       sumTotalExpenses = (parseFloat(sumTotalExpenses) + parseFloat(expensesTotal));
       // calulate and insert into td and input
       $('#expensesTotalTd').html(sumTotalExpenses);
       $('#expensesTotal').val(sumTotalExpenses);
      // grab total and subtract from total and insert 
       mainTotal = (parseFloat(mainTotal) + parseFloat(expensesTotal));
       $('#mainTotalTd').html(mainTotal);
       $('#mainTotal').val(mainTotal);
  }
  // if checkbox is unchecked
  else if($(this).prop('checked') == false){
      // insert total of all selected time entry into the totaltime
       sumTotalExpenses = (parseFloat(sumTotalExpenses) - parseFloat(expensesTotal));
       // calulate and insert into td and input
       $('#expensesTotalTd').html(sumTotalExpenses);
       $('#expensesTotal').val(sumTotalExpenses);
      // grab total and subtract from total and insert 
       mainTotal = (parseFloat(mainTotal) - parseFloat(expensesTotal));
       $('#mainTotalTd').html(mainTotal);
       $('#mainTotal').val(mainTotal);
  }
  
});
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    //for inserting 
    $("#invoice_form").submit(function(e){
        e.preventDefault();
          $.ajax({
          url:"../controllers/billInvoiceController.php",
          method:"POST",
          data:$("#invoice_form").serialize(),
          beforeSend:function(){  
                    $('#invoice_btn').text("Loading ...").prop("disabled",true); 
               },
          success:function(results){
            console.log(results);
                $("#invoiceModal").modal("hide");
               
               if (results == "success") {
                    // get_all();
                    toastr.success('Saved Successfully');
                    $("#invoice_form")[0].reset();
                    $('#invoice_btn').prop('disabled',false).text('Add');
               }
               else if (results == "error"){
                    // get_all();
                    toastr.error('There was an error');
                    $("#invoiceModal").modal("hide");
                    $("#invoice_form")[0].reset();
                    $('#invoice_btn').prop('disabled',false).text('Add');

               }
          } 

          });  
      });

      // for update
      $(document).on('click', '.invoice_update_data', function(){
         let mode= "updateModal"; 
         let data_id = $(this).prop("id");  
         $.ajax({  
              url:"../controllers/billInvoiceController.php",
              method:"POST",  
              data:{contactData_id:data_id,contactMode:mode},  
              success:function(results){
                // alert(results);
                   let jsonObj = JSON.parse(results);  
                   // changing modal title
                    $(".clientSub").html("Update Contact");
                    $("#contactFirstName").val(jsonObj[0].contact_First_name);
                    $("#contactMidName").val(jsonObj[0].contact_mid_name);
                    $("#contactLastName").val(jsonObj[0].contact_last_name);
                    $("#contactEmail").val(jsonObj[0].contact_email);
                    $("#contactCompany").val(jsonObj[0].contact_company).trigger('change');;
                    $("#contactGroup").val(jsonObj[0].contact_group_id).trigger('change');;
                    // $("#contactPortal").bootstrapToggle('destroy');
                    $("#contactCell").val(jsonObj[0].contact_cell);
                    $("#contactWrkPhone").val(jsonObj[0].contact_wrk_phone);
                    $("#contactAddress").val(jsonObj[0].contact_address);
                    $("#contactCity").val(jsonObj[0].contact_city);
                    $("#contactRegion").val(jsonObj[0].contact_region);
                    $("#contactCountry").val(jsonObj[0].contact_country);
                    $("#contactData_id").val(jsonObj[0].contact_id);
                    $("#invoice_btn").text("Update Company");
                    $("#contactMode").val("update");
                    $("#invoiceModal").modal("show");
              }  
             });  
        });

      
// for delete
        $(document).on('click', '.contact_del_data', function(){
           if (confirm("ARE YOU SURE YOU WANT TO PROCEED?")) {
                 let mode= "delete"; 
                 let data_id = $(this).prop("id");  
                 $.ajax({  
                      url:"../controllers/billInvoiceController.php",  
                      method:"POST",  
                      data:{contactData_id:data_id,contactMode:mode},  
                      success:function(results){
                        console.log(results);
                        if (results == "success") {
                            get_all();
                            toastr.success('Deleted Successfully');
                        }
                        else if (results == "error"){
                            toastr.error('There was an error');
                        }
                      }  
                     }); 

             }else{
              return false;
            }  
        });
    // get all data 
         function get_all(){
            let mode = "getAll";
            $.ajax({  
              url:"../controllers/billInvoiceController.php",  
              method:"POST",  
              data:{contactMode:mode},  
              success:function(results){
                $('#contactResultsDisplay').html('');
                let jsonObj = JSON.parse(results);
                for (let i = 0; i < jsonObj.length; i++) {
                    $('#contactResultsDisplay').append('<tr><td>'+jsonObj[i].contact_First_name+'</td><td>'+jsonObj[i].contact_last_name+'</td><td>'+jsonObj[i].contact_group_id+'</td><td>'+jsonObj[i].added+'</td><td><button class="btn-primary contact_update_data" id="'+jsonObj[i].contact_id+'"><i class="fa fa-pencil"></i></button> <button class="btn-danger contact_del_data" id="'+jsonObj[i].contact_id+'"><i class="fa fa-trash"></i></button></td></tr>');
                }
              }  
            }); 
         }
 });