
 $(document).ready(function(){
    $('#insert_form').parsley();
    $('#insert_form').parsley().reset();
        // for reset modal when close
      $('#addPracticeModal').on('hidden.bs.modal', function () {
          $("#subject").html("Add New Practice");
          $("#practicedata_id").val("");
          $("#practicemode").val("insert");
          $(this).find('input:text').css({"border-color":"#808080"});
          $("#insert_form")[0].reset();
          $("#practicesave_btn").text("Add Page");
          $('#insert_form').parsley().reset();
        });

      //for inserting 
        $("#insert_form").submit(function(e){
              $.ajax({
              url:"../controllers/practiceAreaController.php",
              method:"POST",
              data:$("#insert_form").serialize(),
              beforeSend:function(){  
                       $('#practicesave_btn').prop('disabled','disabled').text('Loading...');  
                   },
              success:function(results){  
                   $("#addPracticeModal").modal("hide");
                   
                  if (results == "success") {
                      get_all();
                        toastr.success('Saved Successfully');
                        $("#insert_form")[0].reset();
                        $('#practicesave_btn').prop('disabled',false).text('Save');;
                        $('#resultsDisplay').prepend(results);
                   }
                   else if (results == "error"){
                      toastr.error('There was an error');
                      $("#addPracticeModal").modal("hide");
                      $("#insert_form")[0].reset();  
                   }
              } 

              });  
          });

      // for update
      $(document).on('click', '.update_data', function(){
         let mode = "updateModal"; 
         let data_id = $(this).prop("id");  
         $.ajax({  
              url:"../controllers/practiceAreaController.php",
              method:"POST",  
              data:{data_id:data_id,mode:mode},  
              success:function(results){
                console.log(results);
                   let jsonObj = JSON.parse(results);  
                   // changing modal title
                  $("#subject").html("Update Practice Area");
                  $("#practiceArea").val(jsonObj[0].practice_area_name);
                  $("#practicedata_id").val(jsonObj[0].practice_area_id);
                  $("#practicesave_btn").text("Update Area");
                  $("#practicemode").val("update");
                  $("#addPracticeModal").modal("show");
              }  
             });  
        });

      
// for delete
         $(document).on('click', '.del_data', function(){
           if (confirm("ARE YOU SURE YOU WANT TO PROCEED?")) {
               
                 let mode= "delete"; 
                 let data_id = $(this).prop("id");  
                 $.ajax({  
                      url:"../controllers/practiceAreaController.php",  
                      method:"POST",  
                      data:{data_id:data_id,mode:mode},  
                      success:function(results){
                        if (results == "success") {
                          get_all();
                          toastr.success('Deleted Successfully');
                        }
                        else if (results == "error"){
                          toastr.error('There was an error');
                        }
                      }  
                     }); 

             }else{
              return false;
            }  
        });


        // get all data 
     function get_all(){
        let mode= "getAll";
        $.ajax({  
          url:"../controllers/practiceAreaController.php",  
          method:"POST",  
          data:{mode:mode},  
          success:function(results){
            $('#resultsDisplay').html('');
            $('#resultsDisplay').html(results);
          }  
        }); 
     }
});  