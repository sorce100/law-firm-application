 $(document).ready(function(){
    $('#addCompany_form').parsley();
    $('#addCompany_form').parsley().reset();

    $('#addCompanyModal').on('hidden.bs.modal', function () {
      $(".companySub").html("Add Company");
      $(this).find('input:text').css({"border-color":"#808080"});
      $("#comData_id").val("");
      $("#comMode").val("insert");
      $("#addCompany_form")[0].reset();
      $("#companyBtn").text("Save Company");
      $('#addCompany_form').parsley().reset();
    });
  //for inserting 
    $("#addCompany_form").submit(function(e){

        e.preventDefault();
        $(this).find('input[type="text"]').each(function() {
          if( $(this).val() == "" ) {
            e.preventDefault();
            $(this).css({"border-color":"#f35b3f"});
            return false;
          }
          else {
            $(this).css({"border-color":"#808080"});
          }
        });
        // fields validation

          $.ajax({
          url:"../controllers/ContactCompanysController.php",
          method:"POST",
          data:$("#addCompany_form").serialize(),
          beforeSend:function(){  
                    $('#companyBtn').text("Loading ...").prop("disabled",true); 
               },
          success:function(results){
                $("#addCompanyModal").modal("hide");
               
               if (results == "success") {
                    get_all();
                    toastr.success('Saved Successfully');
                    $("#addCompany_form")[0].reset();
                    $('#companyBtn').attr('disabled',false).text('Save Company');
               }
               else if (results == "error"){
                  toastr.error('There was an error');
                  $("#addCompanyModal").modal("hide");
                  $("#addCompany_form")[0].reset();

               }
          } 

          });  
      });

      // for update
      $(document).on('click', '.cmp_update_data', function(){
         let mode= "updateModal"; 
         let data_id = $(this).prop("id");  
         $.ajax({  
              url:"../controllers/ContactCompanysController.php",
              method:"POST",  
              data:{comData_id:data_id,comMode:mode},  
              success:function(results){
                // alert(results);
                   let jsonObj = JSON.parse(results);  
                   // changing modal title
                  $(".companySub").html("Contact Company");
                    $("#ConCompanyName").val(jsonObj[0].con_company_name);
                    $("#ConCompanyEmail").val(jsonObj[0].con_company_email);
                    $("#ConCompanyWebsite").val(jsonObj[0].con_company_website);
                    $("#ConCompanyMainPhone").val(jsonObj[0].con_company_mainphone);
                    $("#ConCompanyAddress").val(jsonObj[0].con_company_address);
                    $("#ConCompanyCity").val(jsonObj[0].con_company_city);
                    $("#ConCompanyRegion").val(jsonObj[0].con_company_region);
                    $("#ConCompanyCountry").val(jsonObj[0].con_company_country);
                    $("#ConCompanyPrivateNote").html(jsonObj[0].con_company_privatenote);
                    $("#comData_id").val(jsonObj[0].con_company_id);
                    $("#companyBtn").text("Update Company");
                    $("#comMode").val("update");
                    $("#addCompanyModal").modal("show");
              }  
             });  
        });

      
// for delete
        $(document).on('click', '.cmp_del_data', function(){
           if (confirm("ARE YOU SURE YOU WANT TO PROCEED?")) {
                 let mode= "delete"; 
                 let data_id = $(this).prop("id");  
                 $.ajax({  
                      url:"../controllers/ContactCompanysController.php",  
                      method:"POST",  
                      data:{comData_id:data_id,comMode:mode},  
                      success:function(results){
                        if (results == "success") {
                            get_all();
                            toastr.success('Deleted Successfully');
                        }
                        else if (results == "error"){
                            toastr.error('There was an error');
                        }
                      }  
                     }); 

             }else{
              return false;
            }  
        });
    // get all data 
         function get_all(){
            let mode = "getAll";
            $.ajax({  
              url:"../controllers/ContactCompanysController.php",  
              method:"POST",  
              data:{comMode:mode},  
              success:function(results){
                $('#cmpResultsDisplay').html('');
                let jsonObj = JSON.parse(results);
                for (let i = 0; i < jsonObj.length; i++) {
                    $('#cmpResultsDisplay').append('<tr><td>'+jsonObj[i].con_company_name+'</td><td></td><td></td><td>'+jsonObj[i].added+'</td><td><button class="btn-primary cmp_update_data" id="'+jsonObj[i].con_company_id+'"><i class="fa fa-pencil"></i></button> <button class="btn-danger cmp_del_data" id="'+jsonObj[i].con_company_id+'"><i class="fa fa-trash"></i></button></td></tr>');
                }
              }  
            }); 
         }
 });