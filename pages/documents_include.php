
<!-- for case document -->
<div class="modal fade" id="addDocumentModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header" id="bg">
         <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true" style="color: red;font-size: 25px;" class="btn-default">&times; </span></button>
        <h4 class="modal-title documentTitle">Add Case Documents</h4>
      </div>
      <div class="modal-body" id="bg">
      	 <form id="document_form" method="POST" enctype="multipart/form-data">
      		<div class="row">
      			<div class="col-md-7" style="border-right: 1px solid #e5eae6;">
      				<!-- 1 -->
      				<div class="row">
		     			<div class="col-md-2">
		     				<label for="title" class="col-form-label">Case Link <span class="asterick">*</span></label>
		     			</div>
		     			<div class="col-md-10">
		     				<div class="form-group">
		                        <select class="form-control documentSelect2" style="width: 100%;" id="documentCourtCaseId" name="documentCourtCaseId" required>
		                        	<option value="" selected="selected">Select Case</option>
		                        	<?php
	                                    $cases = $objCourtCase->get_cases(); 
	                                    if (!empty($cases)) {
	                                        print_r($cases);
	                                    }
	                                ?>
		                        </select>
		                    </div>
		     			</div>
		     		</div>
		     		<!-- 2 -->
		     		<div class="row">
		     			<div class="col-md-2">
		     				<label for="title" class="col-form-label">Folder Name</label>
		     			</div>
		     			<div class="col-md-5">
		     				<div class="form-group input-group">
	                           <input type="text" name="docFolderName" id="docFolderName" class="form-control" value="No Case Selected" readonly required>
	                           <span class="input-group-addon"><i class="fa fa-folder"></i></span>
	                        </div>
		     			</div>
		     		</div><br>
		     		<!-- 3 -->
		     		<div class="row">
		     			<div class="col-md-2">
		     				<label for="title" class="col-form-label">Assigned Date</label>
		     			</div>
		     			<div class="col-md-5">
		     				<div class="form-group input-group">
			                   <input type="text" class="form-control" id="docAssignedDate" name="docAssignedDate" data-toggle="datepicker" readonly required>
			                   <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
			                </div>
		     			</div>
		     		</div>
		     		<!-- 4 -->
		     		<div class="row">
		     			<div class="col-md-2">
		     				<label for="title" class="col-form-label">Description</label>
		     			</div>
		     			<div class="col-md-10">
		     				<div class="form-group ">
			                   <textarea class="form-control" rows="7" name="docDescription" id="docDescription" placeholder="Enter description of documents &hellip;"></textarea>
			                </div>
		     			</div>
		     		</div>
		     		
      			</div>
      			<div class="col-md-5" style="background-color: #f4f4f4f4;">
      				<div>
		                <button class="add_field_button btn-block btn-info" style="padding: 2px;"> ADD FILE <span class="glyphicon glyphicon-plus"></span> </button><br>
		                <!-- for document input forms -->
		                <div class="input_fields_wrap"></div>
		            </div>
      			</div>
      		</div>
      		<!-- ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
      		<!-- progress bar -->
      		<div class="progress">
			  <div id="progressBar" class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%">0%</div>
			</div>
      		<!-- ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
      		<!-- insert document array -->
      		<input type="hidden" name="documentsArray" id="documentsArray" value="">
      		<!-- folderName -->
      		<input type="hidden" name="folderName" id="folderName" value="">
      		<!-- for inserting the page id -->
            <input type="hidden" name="data_id" id="document_data_id" value="">
            <!-- for insert query -->
            <input type="hidden" name="mode" id="documentMode" value="insert">
      		<div class="modal-footer">
		        <button type="button" class="btn btn-danger" data-dismiss="modal">Close <i class="fa fa-times"></i></button>
		        <button type="submit" class="btn btn-primary" id="documentBtn">Save Document <i class="fa fa-floppy-o"></i></button>
		     </div>
		 </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script src="js/pageScript/document.js"></script>