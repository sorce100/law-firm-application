<?php
	ob_start(); 
    include_once('../includes/header.php');
    if ($_SESSION['account_name'] != 'contact') {
	  	die();
	  } 
?>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h3 class="modal-title text-center">WELCOME TO EL AGBEMAVA LAW OFFICE , <strong><?php echo $accountName; ?></strong></h3>
        <!-- end new button -->
        <div class="clearfix"></div>

      </div>
      <div class="x_content">
      	  <a class="btn btn-app-client" href="client_cases.php">
            <i class="fa fa-briefcase"></i> Cases <span class="badge bg-green">12</span>
          </a>
          <a class="btn btn-app-client" href="client_messages.php">
            <i class="fa fa-envelope-o"></i> Messages <span class="badge bg-green">12</span>
          </a>
          <a class="btn btn-app-client" href="client_documents.php">
            <i class="fa fa-book"></i> Documents <span class="badge bg-green">12</span>
          </a>
          <a class="btn btn-app-client" href="client_events.php">
            <i class="fa fa-calendar"></i> Events <span class="badge bg-green">12</span>
          </a>
          <a class="btn btn-app-client" href="client_billing.php">
            <i class="fa fa-credit-card"></i> Billing <span class="badge bg-green">12</span>
          </a>
          <a class="btn btn-app-client" href="client_service_request.php">
            <i class="fa fa-bullhorn"></i> Service Request <span class="badge bg-green">12</span>
          </a>
      </div>
    </div>
  </div>
</div>


<!-- for upcoming events -->
<div class="row">
	<div class="col-md-6 col-sm-12 col-xs-12">
		<div class="row">
		  <div class="col-md-12 col-sm-12 col-xs-12">
		    <div class="x_panel">
		      <div class="x_title">
		        <h3><i class="fa fa-calendar"></i> Upcoming Events</h3>
		        <!-- end new button -->
		        <div class="clearfix"></div>

		      </div>
		      <div class="x_content">
		          Add content to the page ...
		      </div>
		    </div>
		  </div>
		</div>
	</div>
<!-- for recent activity -->
	<div class="col-md-6 col-sm-12 col-xs-12">
		<div class="row">
		  <div class="col-md-12 col-sm-12 col-xs-12">
		    <div class="x_panel">
		      <div class="x_title">
		        <h3><i class="fa fa-exchange"></i> Recent Activity</h3>
		        <!-- end new button -->
		        <div class="clearfix"></div>

		      </div>
		      <div class="x_content">
		          Add content to the page ...
		      </div>
		    </div>
		  </div>
		</div>
	</div>
</div>

<?php require_once('../includes/footer.php');?>

