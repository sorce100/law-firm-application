<?php 
     $contactGroups = new ContactGroups;
     $contactCompany = new ContactCompanys;
 ?>
<!-- modal for contact add -->
<div class="modal fade" id="addContactModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header" id="bg">
         <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true" class=" asterick btn-default">&times; </span></button>
        <h4 class="modal-title clientSub">Add Contact</h4>
      </div>
      <div class="modal-body" id="bg">
      <form id="contact_form" method="POST">
      	<!-- 1 -->
     		<div class="row">
     			<div class="col-md-2">
     				<label for="title" class="col-form-label">Name <span class="asterick">*</span></label>
     			</div>
     			<div class="col-md-4">
     				<div class="form-group">
                        <input type="text" class="form-control input-sm" id="contactFirstName" name="contactFirstName" placeholder="First Name" autocomplete="off" required>
                    </div>
     			</div>
     			<div class="col-md-2">
     				<div class="form-group">
                        <input type="text" class="form-control" id="contactMidName" name="contactMidName" placeholder="Middle" autocomplete="off">
                    </div>
     			</div>
     			<div class="col-md-4">
     				<div class="form-group">
                        <input type="text" class="form-control" id="contactLastName" name="contactLastName" placeholder="Last Name" autocomplete="off" required>
                    </div>
     			</div>
     		</div>
     	<!-- 2 -->
     		<div class="row">
     			<div class="col-md-2">
     				<label for="title" class="col-form-label">Email <span class="asterick">*</span></label>
     			</div>
     			<div class="col-md-10">
     				<div class="form-group">
                        <input type="email" class="form-control" id="contactEmail" name="contactEmail" placeholder="example@email.com" autocomplete="off" required>
                    </div>
     			</div>
     		</div>
     	<!-- 3 -->
     		<div class="row">
     			<div class="col-md-2">
     				<label for="title" class="col-form-label">Company <span class="asterick">*</span></label>
     			</div>
     			<div class="col-md-10">
     				<div class="form-group">
                        <select class="form-control companySelect2" style="width: 100%;" id="contactCompany" name="contactCompany" required>
                        	<option value="" selected="selected">Select Company</option>
                            <?php 
                                $contactCompany = new ContactCompanys;
                                  $companys = $contactCompany->get_contactCompany(); 
                                  foreach ($companys as $company) {
                                    echo "<option value=".$company["con_company_id"].">".$company["con_company_name"]."</option>";
                                  }
                            ?>
                        </select>
                    </div>
     			</div>
     		</div>
     	<!-- 4 -->
       		<div class="row">
     			<div class="col-md-2">
     				<label for="title" class="col-form-label">Contact Group <span class="asterick">*</span></label>
     			</div>
     			<div class="col-md-4">
     				<div class="form-group">
                        <select class="form-control groupSelect2" style="width: 100%;" id="contactGroup" name="contactGroup" required>
                        	<option value="" selected="selected">Select Contact Group</option>
                            <?php 
                                $contactGroups = new ContactGroups;
                                  $groups = $contactGroups->get_contactGroups(); 
                                  foreach ($groups as $group) {
                                    echo "<option value=".$group["contact_group_id"].">".$group["contact_group_name"]."</option>";
                                  }
                            ?>
                        </select>
                    </div>
     			</div>
     		</div>
     	<!-- 5 -->
     		<div class="row">
     			<div class="col-md-2">
     				<label for="title" class="col-form-label">Enable Client Portal</label>
     			</div>
     			<div class="col-md-5">
                    <label class="switch">
                        <input type="checkbox" checked id="contactPortal" name="contactPortal" data-width="150">
                        <input type="hidden" name="contactPortal_log" id="contactPortal_log" value="YES" />
                   </label>
     			</div>
     		</div><br>
     	<!-- 6 -->
     		<div class="row">
     			<div class="col-md-2">
     				<label for="title" class="col-form-label">Cell Phone <span class="asterick">*</span></label>
     			</div>
     			<div class="col-md-10">
     				<div class="form-group">
	                    <input type="number" class="form-control" id="contactCell" name="contactCell" placeholder="xxx xxx xxxx" autocomplete="off" required>
	                </div>
     			</div>
     		</div>
     	<!-- 7 -->
     		<div class="row">
     			<div class="col-md-2">
     				<label for="title" class="col-form-label">Work Phone</label>
     			</div>
     			<div class="col-md-10">
     				<div class="form-group">
	                    <input type="number" class="form-control" id="contactWrkPhone" name="contactWrkPhone" placeholder="xxx xxx xxxx" autocomplete="off" >
	                </div>
     			</div>
     		</div>
     	<!-- 8 -->
     		<div class="row">
     			<div class="col-md-2">
     				<label for="title" class="col-form-label">Address</label>
     			</div>
     			<div class="col-md-10">
     				<div class="form-group">
	                    <input type="text" class="form-control" id="contactAddress" name="contactAddress" placeholder="Address of Contact" autocomplete="off" >
	                </div>
     			</div>
     		</div>
     	<!-- 9 -->
     		<div class="row">
     			<div class="col-md-2">
     				<label for="title" class="col-form-label">City, Region</label>
     			</div>
     			<div class="col-md-5">
     				<div class="form-group">
	                    <input type="text" class="form-control" id="contactCity" name="contactCity" placeholder="Contact City" autocomplete="off" >
	                </div>
     			</div>
     			<div class="col-md-5">
     				<div class="form-group">
                         <select class="form-control" id="contactRegion" name="contactRegion">
                            <?php include('../includes/regions.html'); ?>
                        </select>
	                </div>
     			</div>
     		</div>
     	<!-- 10 -->
     		<div class="row">
     			<div class="col-md-2">
     				<label for="title" class="col-form-label">Country <span class="asterick">*</span></label>
     			</div>
     			<div class="col-md-10">
     				<div class="form-group">
	                    <select class="form-control" id="contactCountry" name="contactCountry" required>
	                    	<?php include('../includes/countries.html'); ?>
	                    </select>
	                </div>
     			</div>
     		</div>
            <!-- for inserting the page id -->
            <input type="hidden" name="contactData_id" id="contactData_id" value="">
            <!-- for insert query -->
            <input type="hidden" name="contactMode" id="contactMode" value="insert">

	         <div class="modal-footer">
		        <button type="button" class="btn btn-danger" data-dismiss="modal">Close <i class="fa fa-times"></i></button>
		        <button type="submit" class="btn btn-primary" id="contactsBtn">Save Contact <i class="fa fa-floppy-o"></i></button>
		     </div>
		 </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script src="js/pageScript/contact.js"></script>