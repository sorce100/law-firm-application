<?php 
	require_once('../includes/header.php');
 ?>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Plain Page</h2>
        <!-- add new button -->
        <div class="pull-right">
            <button class="btn btn-danger">Add New <i class="fa fa-plus"></i></button>
        </div>
        <!-- end new button -->
        <div class="clearfix"></div>

      </div>
      <div class="x_content">
          Add content to the page ...
      </div>
    </div>
  </div>
</div>




<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Courts Setup</h2>
        <!-- end new button -->
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
          <!-- creating tabs -->
          <div class="" role="tabpanel" data-example-id="togglable-tabs">
            <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
              <li role="presentation" class="active"><a href="#mainCourt" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Main Court <i class="fa fa-home"></i></a>
              </li>
              <li role="presentation" class=""><a href="#subCourt" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Sub Court <i class="fa fa-subscript"></i></a>
              </li>
            </ul>
            <div id="myTabContent" class="tab-content">
              <div role="tabpanel" class="tab-pane fade active in" id="mainCourt" aria-labelledby="home-tab">
               <!-- for main court -->
               <div class="row"> </div>

               <!-- end of main court -->
              </div>
              <div role="tabpanel" class="tab-pane fade" id="subCourt" aria-labelledby="profile-tab">
                <!-- for sub courts -->

                <!-- end of for sub courts -->
              </div>
            </div>
          </div>
          <!-- end of tabs
          '' -->
      </div>
    </div>
  </div>
</div>


 <?php require_once('../includes/footer.php'); ?>

  switch(results){
    case 'success':
      get_all();
      toastr.success('Deleted Successfully');
    break;
    case 'error':
      get_all();
      toastr.error('There was an error');
    break;
    default:
    toastr.error('There was an error');
  }

  try {
                
 } catch (PDOException $e) {echo $e;}

<!-- for preventing modal from closing -->

  data-backdrop="static" data-keyboard="false" 