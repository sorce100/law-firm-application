<?php
  ob_start();  
  include('../includes/header.php');
  
  $retrievedFileName = basename($_SERVER['PHP_SELF']);
  if (!in_array($retrievedFileName, $_SESSION['pagesAllowed'])) {
    header('Location: ../pages/dashboard.php');
  }
  // check if user is a staff for not
 if ($_SESSION['account_name'] != 'staff') {
    die();
  }  
?>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-users"></i> Groups</h2>
        <!-- add new button -->
        <div class="pull-right">
            <button data-toggle="modal" data-target="#pagesGroupModal" class="btn btn-danger input-sm"><span class="glyphicon glyphicon-globe"></span> Add Group</button>
        </div>
        <!-- end new button -->
        <div class="clearfix"></div>

      </div>
      <div class="x_content">
        <div class="table-responsive">
              <table class="table table-hover tableList"> 
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Added</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody id="resultsDisplay">
                  <?php
                      $obPagesGroup = new PagesGroup;
                      $groups = $obPagesGroup->get_pages_groups_list(); 
                      if (!empty($groups)) {
                         print_r($groups);
                      }
                     ?>
                </tbody>
              </table>
          </div>
      </div>
    </div>
  </div>
</div>


<!-- for modal -->
<div class="modal fade" id="pagesGroupModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header" id="bg">
         <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true" style="color: red;font-size: 25px;" class="btn-default">&times; </span></button>
        <h4 class="modal-title" id="subject">Add New Practice</h4>
      </div>
      <div class="modal-body" id="bg">
        <form id="insert_form" method="POST">
          <!-- 1 -->
          <div class="row">
              <div class="col-md-2">
                  <label for="title" class="col-form-label">Name <span class="asterick">*</span></label>
              </div>
              <div class="col-md-10">
                  <div class="form-group">
                     <input type="text" name="pagesGroupName" id="pagesGroupName" class="form-control" placeholder="User Group Name &hellip;" autocomplete="off" required>
                  </div>
              </div>
          </div><br>
          <!-- 2 -->
          <div class="row">
              <div class="col-md-2">
                  <label for="title" class="col-form-label">Add Pages <span class="asterick">*</span></label>
              </div>
              <div class="col-md-10">
                 <div class="table-responsive">
                    <table class="table table-hover tableList"> 
                        <thead>
                            <tr>
                              <th><input type="checkbox" id="select_all"/></th>
                              <th>Page Name</th>
                            </tr>
                        </thead>
                        <tbody id="resultsDisplay">
                            <?php
                                $objPages = new Pages;
                                $pages = $objPages->get_pages(); 
                                foreach ($pages as $page) {
                                      echo '
                                          <tr>
                                            <td><input type="checkbox" class="pagesCheckBox" name="pagesId[]" value="'.trim($page["pages_id"]).'"></td>
                                            <td>'.$page["pages_name"].'</td>
                                          </tr>
                                        ';
                                    }
                               ?>
                        </tbody>
                    </table>
                </div>
              </div>
          </div>
            <!-- for inserting the page id -->
            <input type="hidden" name="data_id" id="groupdata_id" value="">
            <!-- for insert query -->
            <input type="hidden" name="mode" id="groupmode" value="insert">

             <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary" id="groupsave_btn">Save Group <i class="fa fa-floppy-o"></i></button>
           </div>
      </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<?php include('../includes/footer.php'); ?>
<!-- scripts -->
<script src="js/pageScript/page_groups.js"></script>
