
<!-- add invoice modal -->
<div class="modal fade" id="invoiceModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header" id="bg">
         <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true" style="color: red;font-size: 25px;" class="btn-default">&times; </span></button>
        <h4 class="modal-title" id="invoicesubject">Add Invoice</h4>
      </div>
      <div class="modal-body" id="bg">
          <form id="invoice_form">
            <div class="row">
                <div class="col-md-12">
                  <!-- for invoice number and invoice date -->
                  <div class="row">
                     <div class="col-md-2"></div>
                    <div class="col-md-5">
                      <label for="title"><b>Invoice #</b></label>
                      <div class="form-group input-group">
                        <span class="input-group-addon">#</span>
                        <input type="number" class="form-control" id="invoiceNum" name="invoiceNum" readonly value="<?php echo '0'.date('ym').rand(0,50);?>" required>
                      </div>
                    </div>
                    <div class="col-md-5">
                      <label for="title"><b>Invoice Date <span class="asterick">*</span></b></label>
                      <div class="form-group input-group">
                        <input type="text" class="form-control" id="invoiceDate" name="invoiceDate" data-toggle="datepicker" readonly="" placeholder="select" required>
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                      </div>
                    </div>
                  </div>
                  <!-- 1 -->
                    <div class="row">
                        <div class="col-md-2">
                            <label for="title" class="col-form-label">Case Link <span class="asterick">*</span></label>
                        </div>
                        <div class="col-md-10">
                            <div class="form-group">
                              <select class="form-control Invoiceselect2" style="width: 100%;" id="invoiceCourtCaseId" name="invoiceCourtCaseId" required>
                                <option value="" selected="selected">Select Case</option>
                                <?php
                                    $cases = $objCourtCase->get_cases(); 
                                    if (!empty($cases)) {
                                        print_r($cases);
                                    }
                                ?>
                              </select>
                          </div>
                        </div>
                    </div>
                    <!-- 2 -->
                    <div class="row">
                        <div class="col-md-2">
                            <label for="title" class="col-form-label">Select Contact <span class="asterick">*</span></label>
                        </div>
                        <div class="col-md-10">
                            <div class="form-group">
                               <select class="form-control Invoiceselect2" style="width: 100%;" id="invoiceContact" name="invoiceContact" required>
                                
                               </select>
                            </div>
                        </div>
                    </div>
                    
                    <!-- 3 -->
                    <div class="row">
                        <div class="col-md-2">
                            <label for="title" class="col-form-label">Address</label>
                        </div>
                        <div class="col-md-10">
                            <textarea class="form-control" id="invoiceAddress" name="invoiceAddress"></textarea>
                        </div>
                    </div>
                    <br>
                    <!-- share invoice via client portal -->
                    <div class="row">
                        <div class="col-md-2">
                            <label for="title" class="col-form-label"><b>Share Invoice Via Client Portal</b></label>
                        </div>
                        <div class="col-md-5">
                          <label class="switch">
                            <input type="checkbox" checked id="invoiceBillable" name="invoiceBillable" data-width="150">
                            <input type="hidden" name="invoiceBillable_log" id="invoiceBillable_log" value="YES" />

                          </label>

                        </div>
                    </div>

                   <hr>
                   <!-- for listing time entries  -->
                   <div class="row">
                     <div class="col-md-12 table-responsive">
                      <center><h3>Case Time Enteries</h3></center>
                       <table class="table table-hover"> 
                          <thead>
                              <tr>
                                  <th>Case</th>
                                  <th>Date</th>
                                  <th>Employee</th>
                                  <th>Activity</th>
                                  <th>Rate (₵)</th>
                                  <th>Hours</th>
                                  <th>Total(₵)</th>
                                  <th>Billable</th>
                              </tr>
                          </thead>
                          <tbody id="timeEntriesDiv">
                              <tr><td><b>Select Case</b></td></tr>
                          </tbody>
                        </table>
                     </div>
                   </div>

                   <hr>

                   <!-- for listing Expenses  -->
                   <div class="row">
                     <div class="col-md-12 table-responsive">
                      <center><h3>Case Expenses</h3></center>
                       <table class="table table-hover"> 
                          <thead>
                              <tr>
                                  <th>Case</th>
                                  <th>Date</th>
                                  <th>Employee</th>
                                  <th>Cost (₵)</th>
                                  <th>Quantity</th>
                                  <th>Total (₵)</th>
                                  <th>Billable</th>
                              </tr>
                          </thead>
                          <tbody id="expensesDiv">
                              <tr><td><b>Select Case</b></td></tr>
                          </tbody>
                        </table>
                     </div>
                   </div>
                   
                   <hr>
                   
                   <div class="row well">
                     <div class="row">
                       <div class="col-md-7">
                         <h3>Invoice Totals</h3>
                       </div>
                       <div class="col-md-5 table-responsive">
                         <table class="table">
                           <tr>
                             <td>Time Entry Sub-Total (₵) :</td>
                             <input type="hidden" name="timeEntryTotal" id="timeEntryTotal" value="">
                             <td id="timeEntryTotalTd"></td>
                           </tr>
                           <tr>
                             <td>Expense Sub-Total (₵) :</td>
                             <input type="hidden" name="expensesTotal" id="expensesTotal" value="">
                             <td id="expensesTotalTd"></td>
                           </tr>
                           <hr>
                           <tr>
                             <td><b>Total (₵) :</b></td>
                             <input type="hidden" name="mainTotal" id="mainTotal" value="">
                             <td id="mainTotalTd"></td>
                           </tr>
                         </table>
                       </div>
                     </div>
                   </div>
                    <!-- for inserting the page id -->
                    <input type="hidden" name="data_id" id="invoice_data_id" value="">
                    <!-- for insert query -->
                    <input type="hidden" name="mode" id="invoiceMode" value="insert">

                   <div class="modal-footer">
                      <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                      <button type="submit" class="btn btn-primary" id="invoice_btn">Add <i class="fa fa-floppy-o"></i></button>
                   </div>
                </div>
            </div>
          </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<!-- for recording payment -->
<div class="modal fade" id="recordPaymentModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header" id="bg">
         <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true" class="btn-default asterick">&times; </span></button>
        <h4 class="modal-title recordPaymentTitle">Record Payment</h4>
      </div>
      <div class="modal-body" id="bg">
          <form id="recordPayment_form">
            <div class="row">
                <div class="col-md-12">
                    <!-- 1 -->
                    <div class="row">
                        <div class="col-md-3">
                            <label for="title" class="col-form-label">Select Invoice <span class="asterick">*</span></label>
                        </div>
                        <div class="col-md-9">
                            <div class="form-group">
                               <select class="form-control" id="recordPaymentInvoice" class="recordPaymentInvoice" required>
                                  <?php 
                                    $details = $objBillInvoice->get_unpayed_invoices(); 
                                    foreach ($details as $detail) {
                                     echo '<option value="'.$detail["bill_invoice_id"].'">'.$detail["bill_invoice_number"].'</option>';
                                    }
                                  ?>
                                 <option></option>
                               </select>
                            </div>
                        </div>
                    </div>

                    <!-- for inserting the page id -->
                    <input type="hidden" name="data_id" id="pages_data_id" value="">
                    <!-- for insert query -->
                    <input type="hidden" name="mode" id="recordPaymentMode" value="recordPay">

                   <div class="modal-footer">
                      <button type="button" class="btn btn-danger" data-dismiss="modal">Close <i class="fa fa-times"></i></button>
                      <button type="submit" class="btn btn-primary" id="recordPayment_btn">Save Payment <i class="fa fa-save"></i></button>
                   </div>
                </div>
            </div>
          </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script src="js/pageScript/bills_invoice.js"></script>