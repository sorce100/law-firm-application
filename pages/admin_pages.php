<?php 
  ob_start();
  include_once('../includes/header.php'); 
  include_once('../Classes/Pages.php'); 
  // retrive page file name
  $retrievedFileName = basename($_SERVER['PHP_SELF']);
  if (!in_array($retrievedFileName, $_SESSION['pagesAllowed'])) {
      header('Location: dashboard.php');
  }
?>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-file"></i> Pages Setup</h2>
        <!-- add new button -->
        <div class="pull-right">
            <button data-toggle="modal" data-target="#userGroupModal" class="btn btn-danger"><span class="fa fa-plus"></span> Add Page</button>
        </div>
        <!-- end new button -->
        <div class="clearfix"></div>

      </div>
      <div class="x_content">
          <div class="table-responsive">
              <table class="table table-hover tableList"> 
                <thead>
                  <tr>
                    <th width="25%">Name</th>
                    <th width="30%">Url</th>
                    <th width="25%">Added</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody id="resultsDisplay">
                  <?php
                    $objPages = new Pages;
                    $pages = $objPages->get_pages_list(); 
                    if (!empty($pages)) {
                        print_r($pages);
                    }
                    ?>
                </tbody>
              </table>
          </div>
      </div>
    </div>
  </div>
</div>

<!-- for modal -->
<div class="modal fade" id="userGroupModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" id="bg">
         <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true" style="color: red;font-size: 25px;" class="btn-default">&times; </span></button>
        <h4 class="modal-title" id="subject">Add Pages</h4>
      </div>
      <div class="modal-body" id="bg">
          <form id="insert_form">
            <div class="row">
                <div class="col-md-12">
                    <!-- 1 -->
                    <div class="row">
                        <div class="col-md-3">
                            <label for="title" class="col-form-label">Page Name <span class="asterick">*</span></label>
                        </div>
                        <div class="col-md-9">
                            <div class="form-group">
                               <input type="text" name="pageName" id="pageName" class="form-control" placeholder="XYZ Setup" autocomplete="off" required>
                            </div>
                        </div>
                    </div>
                    <!-- 2 -->
                    <div class="row">
                        <div class="col-md-3">
                            <label for="title" class="col-form-label">Page URL <span class="asterick">*</span></label>
                        </div>
                        <div class="col-md-9">
                            <div class="form-group">
                              <textarea rows="5" name="pageUrl" id="pageUrl" class="form-control" placeholder="Page url &hellip;" autocomplete="off"></textarea>
                               <!-- <input type="text" name="pageUrl" id="pageUrl" class="form-control" placeholder="Page url &hellip;" autocomplete="off"> -->
                            </div>
                        </div>
                    </div>
                    <!-- page file name -->
                    <div class="row">
                        <div class="col-md-3">
                            <label for="title" class="col-form-label">Page File Name <span class="asterick">*</span></label>
                        </div>
                        <div class="col-md-9">
                            <div class="form-group">
                               <input type="text" name="pageFileName" id="pageFileName" class="form-control" placeholder="xyz.php" autocomplete="off" required>
                            </div>
                        </div>
                    </div>
                    <!-- for inserting the page id -->
                    <input type="hidden" name="data_id" id="pages_data_id" value="">
                    <!-- for insert query -->
                    <input type="hidden" name="mode" id="pagesMode" value="insert">

                   <div class="modal-footer">
                      <button type="button" class="btn btn-danger" data-dismiss="modal">Close <i class="fa fa-times"></i></button>
                      <button type="submit" class="btn btn-primary" id="pagesSave_btn">Add Page <i class="fa fa-save"></i></button>
                   </div>
                </div>
            </div>
          </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<?php require_once('../includes/footer.php'); ?>
<!-- scripts -->
<script src="js/pageScript/pages.js"></script>
