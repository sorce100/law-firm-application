<!-- //////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div class="modal fade" id="expensesModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header" id="bg">
         <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true" style="color: red;font-size: 25px;" class="btn-default">&times; </span></button>
        <h4 class="modal-title" id="Expensesubject">Add Expenses</h4>
      </div>
      <div class="modal-body" id="bg">
          <form id="expenses_form">
            <div class="row">
                <div class="col-md-12">
                    <!-- 1 -->
                    <div class="row">
                        <div class="col-md-2">
                            <label for="title" class="col-form-label">Case Link <span class="asterick">*</span></label>
                        </div>
                        <div class="col-md-10">
                            <div class="form-group">
                              <select class="form-control Expensesselect2" style="width: 100%;" id="expenseCourtCaseId" name="expenseCourtCaseId" required>
                                <option value="" selected="selected">Select Case</option>
                                <?php
                                    $cases = $objCourtCase->get_cases(); 
                                    if (!empty($cases)) {
                                        print_r($cases);
                                    }
                                ?>
                              </select>
                          </div>
                        </div>
                    </div>
                     <!-- grab staff id to select staff -->
                                <input type="hidden" id="StaffAccountID" value="<?php echo $_SESSION['account_id'];?>">
                    <!-- 2 -->
                    <div class="row">
                        <div class="col-md-2">
                            <label for="title" class="col-form-label">User <span class="asterick">*</span></label>
                        </div>
                        <div class="col-md-10">
                            <div class="form-group">
                               <select class="form-control" id="expensesStaff" name="expensesStaff">
                                <?php
                                  foreach ($objStaff->get_staff() as $staff) {
                                     echo '<option value="'.$staff['staff_id'].'">'.$staff['staff_firstname'].' '.$staff['staff_lastname'].'</option>';
                                  }
                                ?>
                               </select>
                            </div>
                        </div>
                    </div>
                    <!-- 3 -->
                    <div class="row">
                        <div class="col-md-2">
                            <label for="title" class="col-form-label">Activity </label>
                        </div>
                        <div class="col-md-10">
                            <div class="form-group">
                               <input type="text" name="expensesActivity" id="expensesActivity" class="form-control" placeholder="Enter Activity &hellip;" autocomplete="off" required>
                            </div>
                        </div>
                    </div>
                    <!--  -->
                    <div class="row">
                        <div class="col-md-2">
                          <label for="title" class="col-form-label">Time Billable</label>
                        </div>
                        <div class="col-md-5">
                          <label class="switch">
                            <input type="checkbox" checked id="expensesBillable" name="expensesBillable" data-width="150">
                            <input type="hidden" name="expensesBillable_log" id="expensesBillable_log" value="YES" />
                          </label>
                        </div>
                    </div>
                    <br>
                    <!-- 4 -->
                    <div class="row">
                        <div class="col-md-2">
                            <label for="title" class="col-form-label">Description</label>
                        </div>
                        <div class="col-md-10">
                            <div class="form-group">
                              <textarea class="form-control" name="expensesDescribe" id="expensesDescribe" placeholder=""></textarea>
                            </div>
                        </div>
                    </div>
                    <!-- 5 -->
                    <div class="row well">
                      <div class="col-md-4">
                        <label for="title"><b>Date <span class="asterick">*</span></b></label>
                        <div class="form-group input-group">
                          <input type="text" class="form-control" id="expensesDate" name="expensesDate" data-toggle="datepicker" readonly="" placeholder="select" required>
                          <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <label for="title"><b>Cost <span class="asterick">*</span></b></label>
                        <div class="form-group input-group">
                          <span class="input-group-addon">₵</span>
                          <input type="number" class="form-control" id="expensesCost" name="expensesCost" required>
                        </div>
                      </div>
                      <div class="col-md-4">
                         <label for="title"><b>Quantity <span class="asterick">*</span></b></label>
                         <div class="form-group">
                          <input type="number" class="form-control" id="expensesQuantity" name="expensesQuantity" required>
                        </div>
                      </div>
                    </div>
                    <!-- for inserting the page id -->
                    <input type="hidden" name="data_id" id="expenses_data_id" value="">
                    <!-- for insert query -->
                    <input type="hidden" name="mode" id="expensesMode" value="insert">

                   <div class="modal-footer">
                      <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                      <button type="submit" class="btn btn-primary" id="expenses_btn">Add <i class="fa fa-floppy-o"></i></button>
                   </div>
                </div>
            </div>
          </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script src="js/pageScript/bills_expenses.js"></script>