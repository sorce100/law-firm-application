<?php
	ob_start(); 
	include('../includes/header.php');
	// refer back to dashboard if page is not in group
	$retrievedFileName = basename($_SERVER['PHP_SELF']);
	if (!in_array($retrievedFileName, $_SESSION['pagesAllowed'])) {
	  header('Location: ../pages/dashboard.php');
	}

	// check if user is a staff for not
	 if ($_SESSION['account_name'] != 'staff') {
	  	die();
	  } 
?>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-globe"></i> Practice Areas</h2>
        <!-- add new button -->
        <div class="pull-right">
            <button data-toggle="modal" data-target="#addPracticeModal" class="btn btn-danger input-sm"><span class="glyphicon glyphicon-globe"></span> Add New</button>
        </div>
        <!-- end new button -->
        <div class="clearfix"></div>

      </div>
      <div class="x_content">
        <div class="table-responsive">
              <table class="table table-hover tableList"> 
                <thead>
	                <tr>
	                    <th>Name</th>
	                    <th>Added</th>
	                    <th></th>
	                </tr>
	            </thead>
	           	<tbody id="resultsDisplay">
	                <?php
	                  $objPracticeArea = new PracticeArea;
	                  $areas = $objPracticeArea->get_practice_areas_list(); 
	                  if (!empty($areas)) {
	                  	print_r($areas);
	                  }
	                 ?>
	            </tbody>
              </table>
          </div>
      </div>
    </div>
  </div>
</div>


<!-- for modal -->
<div class="modal fade" id="addPracticeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" id="bg">
         <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true" style="color: red;font-size: 25px;" class="btn-default">&times; </span></button>
        <h4 class="modal-title" id="subject">Add New Practice</h4>
      </div>
      <div class="modal-body" id="bg">
	      <form id="insert_form" method="POST">
     			<!-- 1 -->
	     		<div class="row">
	     			<div class="col-md-2">
	     				<label for="title" class="col-form-label">Name</label>
	     			</div>
	     			<div class="col-md-10">
	     				<div class="form-group">
	                       <input type="text" name="practiceArea" id="practiceArea" class="form-control" placeholder="Practice Area Name &hellip;" autocomplete="off" required>
	                    </div>
	     			</div>
	     		</div>
	     		<!-- for inserting the page id -->
                <input type="hidden" name="data_id" id="practicedata_id" value="">
                <!-- for insert query -->
                <input type="hidden" name="mode" id="practicemode" value="insert">

		         <div class="modal-footer">
			        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			        <button type="submit" class="btn btn-primary" id="practicesave_btn">Save <i class="fa fa-floppy-o"></i></button>
			     </div>
		  </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<?php include('../includes/footer.php'); ?>
<script src="js/pageScript/practice_area.js"></script>
<script>  
 
 </script>