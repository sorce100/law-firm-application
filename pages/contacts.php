<?php
    ob_start(); 
    require_once('../includes/header.php');
    // refer back to dashboard if page is not in group
    $retrievedFileName = basename($_SERVER['PHP_SELF']);
    if (!in_array($retrievedFileName, $_SESSION['pagesAllowed'])) {
      header('Location: ../pages/dashboard.php');
    }
    // check if user is a staff for not
     if ($_SESSION['account_name'] != 'staff') {
        die();
      }  
    $contactGroups = new ContactGroups;
    $contactCompany = new ContactCompanys;
    $contactobj= new Contact; 
?>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-users"> Contacts & Clients</i></h2>
        <!-- end new button -->
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <ul class="nav nav-tabs">
            <li role="presentation" class="active"><a href="#contacts" id="home-tabb" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="true">Contacts <i class="fa fa-users"></i></a></li>
            <li role="presentation" class=""><a href="#companies" role="tab" id="profile-tabb" data-toggle="tab" aria-controls="profile" aria-expanded="false">Companies <i class="fa fa-building"></i></a></li>
            <li role="presentation" class=""><a href="#contactgroup" role="tab" id="profile-tabb3" data-toggle="tab" aria-controls="profile" aria-expanded="false">Contact Groups <i class="fa fa-object-group"></i></a></li>
        </ul><br>

        <div class="tab-content">
          <!-- for contacts -->
            <div id="contacts" class="tab-pane fade in active">
              <!-- button -->
              <button class="btn btn-danger" data-toggle="dropdown"  data-target="#addContactModal"><span class="fa fa-plus"></span> Add New Contact</button><hr>
                <!--  -->
              <div class="table-responsive">
                <table class="table table-hover tableList"> 
                    <thead>
                        <tr>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Cases</th>
                            <th>Company</th>
                            <th>Group</th>
                            <th>Added</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody id="contactResultsDisplay">
                        <?php
                          $contacts = $contactobj->get_contacts(); 
                          foreach ($contacts as $contact) {
                                  echo '
                                      <tr>
                                        <td>'.$contact["contact_First_name"].'</td>
                                        <td>'.$contact["contact_last_name"].'</td><td><b>';

                                            foreach ($contactobj->get_contacts_cases($contact["contact_id"]) as $case) {
                                            echo $case;
                                        }
                                        echo '</b></td><td>'.$contactCompany->get_company_Name($contact["contact_company"]).'</td>
                                        <td>'.$contactGroups->get_contactGroup_name($contact["contact_group_id"]).'</td>
                                        <td>'.$contact["added"].'</td>
                                        <td>
                                          <button class="btn-primary contact_update_data" id="'.$contact["contact_id"].'"><i class="fa fa-pencil"></i></button> <button class="btn-danger contact_del_data" id="'.$contact["contact_id"].'"><i class="fa fa-trash"></i></button>
                                        </td>
                                      </tr>
                                    ';
                              }
                         ?>
                    </tbody>
                </table>
              </div>
            </div>
          <!-- for companies -->
            <div id="companies" class="tab-pane fade">
                <!-- button -->
                <button class="btn btn-danger" data-toggle="dropdown"  data-target="#addCompanyModal"><span class="fa fa-plus"></span> Add New Company</button><hr>
                <!--  -->
                <div class="table-responsive">
                    <table class="table table-hover tableList"> 
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Contacts</th>
                                <th>Added</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody id="cmpResultsDisplay">
                            <?php
                              $companys = $contactCompany->get_contactCompany(); 
                              foreach ($companys as $company) {
                                      echo '
                                          <tr>
                                            <td>'.$company["con_company_name"].'</td>
                                            <td>'.$contactobj->get_company_contacts($company["con_company_id"]).'</td>
                                            <td>'.$company["added"].'</td>
                                            <td>
                                              <button class="btn-primary cmp_update_data" id="'.$company["con_company_id"].'"><i class="fa fa-pencil"></i></button> <button class="btn-danger cmp_del_data" id="'.$company["con_company_id"].'"><i class="fa fa-trash"></i></button>
                                            </td>
                                          </tr>
                                        ';
                                  }
                             ?>
                        </tbody>
                    </table>
              </div>
            </div>
          <!-- for groups -->
            <div id="contactgroup" class="tab-pane fade">
            <!-- button -->
            <button class="btn btn-danger" data-toggle="dropdown"  data-target="#contactGroupModal"><span class="fa fa-plus"></span> Add New Group</button><hr>
            <!--  -->
              <div class="table-responsive">
                <table class="table table-hover tableList"> 
                    <thead>
                        <tr>
                            <th>Group</th>
                            <th>Contact</th>
                            <th>Added</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody id="grpResultsDisplay">
                        <?php
                          $groups = $contactGroups->get_contactGroups(); 
                          foreach ($groups as $group) {
                              echo '
                                  <tr>
                                    <td>'.$group["contact_group_name"].'</td>
                                    <td>'.$contactobj->get_contactGroup_contacts($group["contact_group_id"]).'</td>
                                    <td>'.$group["added"].'</td>
                                    <td>
                                      <button class="btn-primary grp_update_data" id="'.$group["contact_group_id"].'"><i class="fa fa-pencil"></i></button> <button class="btn-danger grp_del_data" id="'.$group["contact_group_id"].'"><i class="fa fa-trash"></i></button>
                                    </td>
                                  </tr>
                                ';
                              }
                         ?>

                    </tbody>
                </table>
              </div>
            </div>
            <!--  -->
        </div>
      </div>
    </div>
  </div>
</div>



<!-- for adding company -->
<div class="modal fade" id="addCompanyModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header" id="bg">
         <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true" class=" asterick btn-default">&times; </span></button>
        <h4 class="modal-title companySub">Add Company</h4>
      </div>
      <div class="modal-body" id="bg">
      <form id="addCompany_form" method="POST">
        <!-- 1 -->
            <div class="row">
                <div class="col-md-2">
                    <label for="title" class="col-form-label">Name</label>
                </div>
                <div class="col-md-10">
                    <div class="form-group">
                        <input type="text" class="form-control" id="ConCompanyName" name="ConCompanyName" placeholder="Company Name" autocomplete="off" required>
                    </div>
                </div>
            </div>
        <!-- 2 -->
            <div class="row">
                <div class="col-md-2">
                    <label for="title" class="col-form-label">Email</label>
                </div>
                <div class="col-md-10">
                    <div class="form-group">
                        <input type="email" class="form-control" id="ConCompanyEmail" name="ConCompanyEmail" placeholder="example@email.com" autocomplete="off">
                    </div>
                </div>
            </div>
        <!-- 3 -->
            <div class="row">
                <div class="col-md-2">
                    <label for="title" class="col-form-label">Website</label>
                </div>
                <div class="col-md-10">
                    <div class="form-group">
                       <input type="text" class="form-control" id="ConCompanyWebsite" name="ConCompanyWebsite" placeholder="www.example.com" autocomplete="off">
                    </div>
                </div>
            </div>
        <!-- 4 -->
            <div class="row">
                <div class="col-md-2">
                    <label for="title" class="col-form-label">Main Phone</label>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <input type="tel" class="form-control" id="ConCompanyMainPhone" name="ConCompanyMainPhone" placeholder="xxx xxx xxxx" autocomplete="off">
                    </div>
                </div>
            </div>
        <!-- 5 -->

        <!-- 6 -->
            <div class="row">
                <div class="col-md-2">
                    <label for="title" class="col-form-label">Address</label>
                </div>
                <div class="col-md-10">
                    <div class="form-group">
                        <input type="text" class="form-control" id="ConCompanyAddress" name="ConCompanyAddress" placeholder="Company Address" autocomplete="off" >
                    </div>
                </div>
            </div>
        <!-- 9 -->
            <div class="row">
                <div class="col-md-2">
                    <label for="title" class="col-form-label">City, Region</label>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <input type="text" class="form-control" id="ConCompanyCity" name="ConCompanyCity" placeholder="Company City" autocomplete="off">
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <input type="text" class="form-control" id="ConCompanyRegion" name="ConCompanyRegion" placeholder="Contact Region" autocomplete="off">
                    </div>
                </div>
            </div>
        <!-- 10 -->
            <div class="row">
                <div class="col-md-2">
                    <label for="title" class="col-form-label">Country</label>
                </div>
                <div class="col-md-10">
                    <div class="form-group">
                        <select class="form-control" id="ConCompanyCountry" name="ConCompanyCountry" required>
                            <?php include('../includes/countries.html'); ?>
                        </select>
                    </div>
                </div>
            </div>
        <!-- 11 -->
            <div class="row">
                <div class="col-md-2">
                    <label for="title" class="col-form-label">Private Note</label>
                </div>
                <div class="col-md-10">
                    <div class="form-group ">
                       <textarea class="form-control" rows="6" name="ConCompanyPrivateNote" id="ConCompanyPrivateNote"></textarea>
                    </div>
                </div>
            </div>
            <!-- for inserting the page id -->
            <input type="hidden" name="comData_id" id="comData_id" value="">
            <!-- for insert query -->
            <input type="hidden" name="comMode" id="comMode" value="insert">
             <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close <i class="fa fa-times"></i></button>
                <button type="submit" class="btn btn-primary" id="companyBtn">Save Company <i class="fa fa-floppy-o"></i></button>
             </div>
         </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- //////////////////////////////////////////////
//////////////////// Add Contact Groups
///////////////////////////////////////////// -->
<!-- for modal -->
<div class="modal fade" id="contactGroupModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" id="bg">
         <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true" class=" asterick btn-default">&times; </span></button>
        <h4 class="modal-title groupSub">Add Contact Group</h4>
      </div>
      <div class="modal-body" id="bg">
          <form id="contactGroup_form" method="POST">
            <div class="row">
                <div class="col-md-12" style="border-right:1px solid #bec4be; ">
                    <!-- 1 -->
                    <div class="row">
                        <div class="col-md-2">
                            <label for="title" class="col-form-label">Name</label>
                        </div>
                        <div class="col-md-10">
                            <div class="form-group">
                               <input type="text" name="contactGroupName" id="contactGroupName" class="form-control" placeholder="Contact Group Name &hellip;" autocomplete="off" required>
                            </div>
                        </div>
                    </div>
                    <!-- for inserting the page id -->
                    <input type="hidden" name="grpData_id" id="grpData_id" value="">
                    <!-- for insert query -->
                    <input type="hidden" name="grpMode" id="grpMode" value="insert">

                     <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close <i class="fa fa-times"></i></button>
                        <button type="submit" class="btn btn-primary" id="groupBtn">Add Contact Group <i class="fa fa-floppy-o"></i></button>
                     </div>
                </div>
            </div>
          </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<?php include('../includes/footer.php'); ?>
<?php include_once('contacts_include.php'); ?>

<script src="js/pageScript/contact_company.js"></script>
<script src="js/pageScript/contact_group.js"></script>

