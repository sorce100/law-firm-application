<?php
	ob_start(); 
	include_once('../includes/header.php');
	// refer back to dashboard if page is not in group
	$retrievedFileName = basename($_SERVER['PHP_SELF']);
	if ((!in_array($retrievedFileName, $_SESSION['pagesAllowed'])) || ($_SESSION['account_name'] != 'staff')) {
	  header('Location: ../pages/dashboard.php');
	}

	include_once('../Classes/Staff.php');
	include_once('../Classes/PagesGroup.php');
	include_once('../Classes/Users.php'); 
	include_once('../Classes/SessionLogs.php'); 
?>
<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-users"> </i>Actors Setup</h2>
        <button data-toggle="modal" data-target="#addUserModal" class="btn btn-danger pull-right"><span class="glyphicon glyphicon-user"></span> Add User</button>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div class="" role="tabpanel" data-example-id="togglable-tabs">
          <ul id="myTab" class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#users" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Staff Accounts <i class="fa fa-users"></i></a>
            </li>
            <li role="presentation" class=""><a href="#clients" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Client Accounts <i class="fa fa-users"></i></a>
            </li>
            <li role="presentation" class=""><a href="#userLogs" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Account logs <i class="fa fa-cog"></i></a>
            </li>
          </ul><br>
          <div id="myTabContent" class="tab-content">
            <div id="users" class="tab-pane fade in active">
		      <div class="table-responsive">
                	<table class="table table-hover tableList"> 
	                    <thead>
	                        <tr>
	                            <th>UserName</th>
	                            <th>Added</th>
	                            <th>Status</th>
	                            <th>Online / Offline</th>
	                            <th></th>
	                        </tr>
	                    </thead>
	                   	<tbody id="resultsDisplay">
	                        <?php
                              $objUsers = new Users;
                              $users = $objUsers->get_users_staff(); 
                              foreach ($users as $user) {
                                      echo '
                                          <tr>
                                            <td>'.$user["user_name"].'</td>
                                            <td>'.$user["added"].'</td>
                                            <td>'.$user["acc_status"].'</td>
                                            <td>'.$user["acc_login_status"].'</td>
                                            <td>
                                              <button class="btn-primary update_data" id="'.$user["users_id"].'"><i class="fa fa-pencil"></i></button> <button class="btn-danger del_data" id="'.$user["users_id"].'"><i class="fa fa-trash"></i></button>
                                            </td>
                                          </tr>
                                        ';
                                  }
                             ?>
	                    </tbody>
                	</table>
              </div>
		    </div>
		    <!-- clients accounts tab -->
		    <div id="clients" class="tab-pane fade">
		      <div class="table-responsive">
                	<table class="table table-hover tableList"> 
	                    <thead>
	                        <tr>
	                            <th>UserName</th>
	                            <th>Added</th>
	                            <th>Status</th>
	                            <th>Online / Offline</th>
	                            <th></th>
	                        </tr>
	                    </thead>
	                   	<tbody id="resultsDisplay">
	                        <?php
                              $objUsers = new Users;
                              $users = $objUsers->get_users_contact(); 
                              foreach ($users as $user) {
                                      echo '
                                          <tr>
                                            <td>'.$user["user_name"].'</td>
                                            <td>'.$user["added"].'</td>
                                            <td>'.$user["acc_status"].'</td>
                                            <td>'.$user["acc_login_status"].'</td>
                                            <td>
                                              <button class="btn-primary update_data" id="'.$user["users_id"].'"><i class="fa fa-pencil"></i></button> <button class="btn-danger del_data" id="'.$user["users_id"].'"><i class="fa fa-trash"></i></button>
                                            </td>
                                          </tr>
                                        ';
                                  }
                             ?>
	                    </tbody>
                	</table>
              </div>
		    </div>
		    <!-- users logs tab -->
		    <div id="userLogs" class="tab-pane fade">
		      <div class="table-responsive">
                	<table class="table table-hover tableList"> 
	                    <thead>
	                        <tr>
	                            <th>UserName</th>
	                            <th>Log in</th>
	                            <th>Log out</th>
	                        </tr>
	                    </thead>
	                   	<tbody>
	                         <?php
                              $objSessionLogs = new SessionLogs;
                              $userLogs = $objSessionLogs->get_session(); 
                              foreach ($userLogs as $userLog) {
                                      echo '
                                          <tr>
                                            <td>'.$objSessionLogs->get_session_username($userLog["user_id"]).'</td>
                                            <td>'.$userLog["session_start"].'</td>
                                            <td>'.$userLog["session_end"].'</td>
                                          </tr>
                                        ';
                                  }
                             ?>
	                    </tbody>
                	</table>
              </div>
		    </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<!-- modal -->
<div class="modal fade" id="addUserModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" id="bg">
         <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true" style="color: red;font-size: 25px;" class="btn-default">&times; </span></button>
        <h4 class="modal-title">Add User</h4>
      </div>
      <div class="modal-body" id="bg">
	      <form id="insert_form" method="POST">
	     			<!-- 1 -->
		     		<div class="row accSelectDiv">
		     			<div class="col-md-3">
		     				<label for="title" class="col-form-label">Staff <span class="asterick">*</span></label>
		     			</div>
		     			<div class="col-md-9">
		     				<div class="form-group">
		                       <select class="form-control usersSelect2" style="width: 100%;" name="accountSelect" id="accountSelect" required="true">
		                       		<option disabled selected>Select Staff</option>
		                       		<?php 
			                            $Staffs = $objStaff->get_staff(); 
			                            foreach ($Staffs as $Staff) {
			                            	if (empty($staff['staff_acc_user_id'])) {
			                            		$firstname = $Staff["staff_firstname"];
				                            	$lastname = $Staff["staff_lastname"];
				                                echo '<option value="'.trim($Staff["staff_id"]).'" id="'.strtolower(substr($firstname, 0, 1).".".$lastname).'">'.$firstname." ".$lastname.'</option>';
			                            	}
			                            }
			                       ?>
		                       </select>
		                    </div>
		     			</div>
		     		</div>
		     	<!-- 2 -->
		     		<div class="row">
		     			<div class="col-md-3">
		     				<label for="title" class="col-form-label">Username</label>
		     			</div>
		     			<div class="col-md-9">
		     				<div class="form-group">
		                        <input type="text" name="userName" id="userName" class="form-control" readonly>
		                    </div>
		     			</div>
		     		</div>
		     	<!-- 3 -->
		     		<div class="row">
		     			<div class="col-md-3">
		     				<label for="title" class="col-form-label">Password</label>
		     			</div>
		     			<div class="col-md-6">
		     				<div class="form-group">
		                        <input type="Password" name="userPassword" id="userPassword" class="form-control" placeholder="User Account Password" minlength="4" title="Must be 4 characters or more and contain at least 1 lower case letter" required="true">
		                    </div>
		     			</div>
		     		</div>
		     	<!-- reset password -->
		     		<div class="row">
		     			<div class="col-md-3">
		     				<label for="title" class="col-form-label">Reset Password</label>
		     			</div>
		     			<div class="col-md-3">
		                    <input type="checkbox" id="accPasswdReset" data-width="150"/>
                         	<input type="hidden" name="accPasswdReset_log" id="accPasswdReset_log" value="reset" />
		     			</div>
		     		</div>
		     		<br>
		     	<!-- 4 -->
		     		<div class="row">
		     			<div class="col-md-3">
		     				<label for="title" class="col-form-label">Group <span class="asterick">*</span></label>
		     			</div>
		     			<div class="col-md-6">
		     				<div class="form-group">
		                        <select class="form-control" id="accGroup" name="accGroup" required>
		                        	<option value="" selected="selected">Select Group</option>
		                        	<?php 
			                            $objPagesGroup = new PagesGroup;
			                            $Groups = $objPagesGroup->get_pages_groups(); 
			                            foreach ($Groups as $Group) {
			                                echo '<option value="'.trim($Group["pages_group_id"]).'">'.$Group["pages_group_name"].'</option>';
			                            }
			                       ?>
		                        </select>
		                    </div>
		     			</div>
		     		</div>
		     	<!-- 5 -->
		     		<div class="row">
		     			<div class="col-md-3">
		     				<label for="title" class="col-form-label">Account Status</label>
		     			</div>
		     			<div class="col-md-5">
		                    <input type="checkbox" id="accStatus" data-width="100"/>
                        	<input type="hidden" name="accStatus_log" id="accStatus_log" value="active" />
		     			</div>
		     		</div><br>
		     			<!-- for inserting the page id -->
		                <input type="hidden" name="data_id" id="usersdata_id" value="">
		                <!-- for insert query -->
		                <input type="hidden" name="mode" id="usersmode" value="insert">

			         <div class="modal-footer">
				        <button type="button" class="btn btn-danger" data-dismiss="modal">Close <i class="fa fa-close"></i></button>
				        <button type="submit" class="btn btn-primary" id="userssave_btn">Create Account <i class="fa fa-floppy-o"></i></button>
				     </div>
		  </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<?php include('../includes/footer.php'); ?>
<!-- scripts -->
<script src="js/pageScript/users.js"></script>

