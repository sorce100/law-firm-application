<?php 
  ob_start();
  include_once('../includes/header.php');  
  // retrive page file name
  $retrievedFileName = basename($_SERVER['PHP_SELF']);
  // if (!in_array($retrievedFileName, $_SESSION['pagesAllowed'])) {
  //     header('Location: dashboard.php');
  // }


  $objLeads = new Leads;
?>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Leads</h2>
        <!--  new button -->
        <div class="pull-right">
            <button data-toggle="modal" data-target="#addLeadModal" class="btn btn-danger"><span class="fa fa-plus"></span> Add Lead</button>
        </div>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <!-- <div class="col-md-6 col-sm-6 col-xs-12"> -->
          <!-- creating tabs -->
          <div class="" role="tabpanel" data-example-id="togglable-tabs">
            <ul id="myTab" class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="#leadDashboard" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Leads Dashboard <i class="fa fa-home"></i></a></li>
              <li role="presentation" class=""><a href="#leadAll" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">All Leads <i class="fa fa-puzzle-piece"></i></a></li>
              <li role="presentation" class=""><a href="#leadApproved" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Approved Leads <i class="fa fa-thumbs-o-up"></i></a></li>
              <li role="presentation" class=""><a href="#leadDisapproved" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Did Not Hire <i class="fa fa-eject"></i></a></li>
            </ul> <br>
            <div id="myTabContent" class="tab-content">
              <div role="tabpanel" class="tab-pane fade active in" id="leadDashboard" aria-labelledby="home-tab">
               <!-- for dashboard -->
                <!--  -->
                <div class="row">
                  <div class="col-md-3 col-sm-12 col-xs-12">
                    <div class="x_panel leadsDash">
                      <div class="x_title">
                        <h3>New <span class="badge bg-red"><?php echo $objLeads->count_status("New"); ?></span></h3>
                      </div>
                      <div class="x_content">
                        <?php 
                          $details = $objLeads->status_details("New");
                          if (!empty($details)) {
                            foreach ($details as $detail) {
                            echo '<div class="well">
                                    <h3><span class="fa fa-user"></span> '. $detail["fullName"].'</h3>
                                    <h5><span class="fa fa-envelope"></span> '. $detail["lead_email"].'</h5>
                                    <h5><span class="fa fa-phone"></span> '. $detail["lead_tel"].'</h5>
                                    <h5><span class="fa fa-calendar"></span> '. $detail["added"].'</h5>
                                  </div>';
                            }
                          }

                         ?>
                      </div>
                    </div>
                  </div>
                
                <!--  -->
                
                  <div class="col-md-3 col-sm-12 col-xs-12">
                    <div class="x_panel leadsDash">
                      <div class="x_title">
                        <h3>Contacted <span class="badge bg-red"><?php echo $objLeads->count_status("Contacted"); ?></span></h3>
                      </div>
                      <div class="x_content">
                        <?php 
                          $details = $objLeads->status_details("Contacted");
                          if (!empty($details)) {
                            foreach ($details as $detail) {
                            echo '<div class="well">
                                    <h3><span class="fa fa-user"></span> '. $detail["fullName"].'</h3>
                                    <h5><span class="fa fa-envelope"></span> '. $detail["lead_email"].'</h5>
                                    <h5><span class="fa fa-phone"></span> '. $detail["lead_tel"].'</h5>
                                    <h5><span class="fa fa-calendar"></span> '. $detail["added"].'</h5>
                                  </div>';
                            }
                          }

                         ?>
                      </div>
                    </div>
                  </div>
                
                <!--  -->
                
                  <div class="col-md-3 col-sm-12 col-xs-12">
                    <div class="x_panel leadsDash">
                      <div class="x_title">
                        <h3>Consult Scheduled <span class="badge bg-red"><?php echo $objLeads->count_status("Consulting"); ?></span></h3>
                      </div>
                      <div class="x_content">
                        <?php 
                          $details = $objLeads->status_details("Consulting");
                          if (!empty($details)) {
                            foreach ($details as $detail) {
                            echo '<div class="well">
                                    <h3><span class="fa fa-user"></span> '. $detail["fullName"].'</h3>
                                    <h5><span class="fa fa-envelope"></span> '. $detail["lead_email"].'</h5>
                                    <h5><span class="fa fa-phone"></span> '. $detail["lead_tel"].'</h5>
                                    <h5><span class="fa fa-calendar"></span> '. $detail["added"].'</h5>
                                  </div>';
                            }
                          }

                         ?>
                      </div>
                    </div>
                  </div>
                
                <!--  -->
               
                  <div class="col-md-3 col-sm-12 col-xs-12">
                    <div class="x_panel leadsDash">
                      <div class="x_title">
                        <h3>Pending <span class="badge bg-red"><?php echo $objLeads->count_status("Pending"); ?></span></h3>
                      </div>
                      <div class="x_content">
                        <?php 
                          $details = $objLeads->status_details("Pending");
                          if (!empty($details)) {
                            foreach ($details as $detail) {
                            echo '<div class="well">
                                    <h3><span class="fa fa-user"></span> '. $detail["fullName"].'</h3>
                                    <h5><span class="fa fa-envelope"></span> '. $detail["lead_email"].'</h5>
                                    <h5><span class="fa fa-phone"></span> '. $detail["lead_tel"].'</h5>
                                    <h5><span class="fa fa-calendar"></span> '. $detail["added"].'</h5>
                                  </div>';
                            }
                          }

                         ?>
                      </div>
                    </div>
                  </div>
                </div>
               <!-- end of dashboard -->
              </div>
              <div role="tabpanel" class="tab-pane fade" id="leadAll" aria-labelledby="home-tab">
               <!-- for all -->
                <div class="table-responsive">
                    <table class="table table-hover tableList"> 
                      <thead>
                        <tr>
                          <th>Name</th>
                          <th>Case</th>
                          <th>Tel No</th>
                          <th>Region</th>
                          <th>Status</th>
                          <th>Practice Area</th>
                          <th>Hourly Rate</th>
                          <th>Assigned To</th>
                          <th>Added</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody id="resultsDisplay">
                        <?php
                          $leads = $objLeads->get_leads();
                          // print_r($leads);
                          // exit(); 
                          foreach ($leads as $lead) {
                            echo '
                                <tr>
                                  <td>'.$lead["clientName"].'</td>
                                  <td>'.$lead["lead_case_matter"].'</td>
                                  <td>'.$lead["lead_tel"].'</td>
                                  <td>'.$lead["lead_region"].'</td>
                                  <td>'.$lead["lead_case_status"].'</td>
                                  <td>'.$lead["practice_area_name"].'</td>
                                  <td>'.$lead["lead_case_hourly_rate"].'</td>
                                  <td>'.$lead["staffName"].'</td>
                                  <td>'.$lead["added"].'</td>
                                  <td>
                                    <button class="bg-green convert_to_case_btn" id="'.$lead["lead_id"].'"><i class="fa fa-exchange"></i> Convert to Case</button> 
                                    <button class="btn-primary update_data" id="'.$lead["lead_id"].'"><i class="fa fa-pencil"></i></button> 
                                    <button class="bg-purple not_hire_btn" id="'.$lead["lead_id"].'"><i class="fa fa-minus-circle"></i></button> 
                                    <button class="btn-danger del_data" id="'.$lead["lead_id"].'"><i class="fa fa-trash"></i></button>
                                  </td>
                                </tr>';
                          }
                         ?>
                      </tbody>
                    </table>
                </div>

               <!-- end of all -->
              </div>
              <div role="tabpanel" class="tab-pane fade" id="leadApproved" aria-labelledby="profile-tab">
                <!-- for approved leads -->

                <!-- end of approved leads -->
              </div>
              <div role="tabpanel" class="tab-pane fade" id="leadDisapproved" aria-labelledby="profile-tab">
                <!-- for unapproved -->

                <!-- end of unapproved -->
              </div>
            </div>
          </div>
          <!-- end of tabs
          '' -->
        <!-- </div> -->
      </div>
    </div>
  </div>
</div>

<!-- modal for convert to case -->
<div class="modal fade" id="convertToCaseModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" id="bg">
         <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true" class="btn-default asterick">&times; </span></button>
        <h4 class="modal-title convertToCase">Add Case</h4>
      </div>
      <div class="modal-body" id="bg">
          <form id="converttocase_form">
            <div class="row">
                
                    <!-- for inserting the page id -->
                    <input type="hidden" name="data_id" id="convertcase_data_id" value="">
                    <!-- for insert query -->
                    <input type="hidden" name="mode" id="convertcaseMode" value="insert">
              </div>      
               <div class="modal-footer">
                  <button type="button" class="btn btn-danger" data-dismiss="modal">Close <i class="fa fa-times"></i></button>
                  <button type="submit" class="btn btn-primary" id="convertcase_btn">Convert Case <i class="fa fa-save"></i></button>
               </div>
          </form>
        </div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- modal for did not hire -->
<div class="modal fade" id="notHireCaseModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" id="bg">
         <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true" class="btn-default asterick">&times; </span></button>
        <h4 class="modal-title notHireTitle">Are you sure you want to mark this lead as no hire?</h4>
      </div>
      <div class="modal-body">
          <form id="notHire_form">
            <div class="row">
              <div class="col-md-2">
                <label for="title" class="col-form-label">Reason <span class="asterick">*</span></label>
              </div>
              <div class="col-md-10">
                <div class="form-group">
                  <select class="form-control" id="leadNotHireReason" name="leadNotHireReason" required>
                    <option selected disabled>Please Select</option>
                    <option value="Conflict">Conflict</option>
                    <option value="Duplicated Lead">Duplicated Lead</option>
                    <option value="No_case">No Case</option>
                    <option value="Not_a_fit">Not a fit for the firm</option>
                    <option value="Timing">Timing</option>
                    <option value="Unresponsive">Unresponsive</option>
                    <option value="another_firm">Went with another firm</option>
                    <option value="Other">Other</option>
                  </select>
                </div>
              </div>
            </div>
            <!--  -->
            <div class="row">
              <div class="col-md-2">
                <label for="title" class="col-form-label">Description</label>
              </div>
              <div class="col-md-10">
                <div class="form-group">
                    <textarea rows="4" class="form-control" id="leadNotHireDescription" name="leadNotHireDescription" placeholder="Additional notes for reason" autocomplete="off"></textarea>
                </div>
              </div>
            </div>
            <!-- for inserting the page id -->
            <input type="hidden" name="data_id" id="notHire_data_id" value="">
            <!-- for insert query -->
            <input type="hidden" name="mode" id="notHireMode" value="insert">      
           <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal">Close <i class="fa fa-times"></i></button>
              <button type="submit" class="btn btn-primary" id="notHire_btn">Mark As No Hire <i class="fa fa-save"></i></button>
           </div>
          </form>
      </div>
    </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<?php require_once('../includes/footer.php'); ?>
<?php require_once('admin_lead_include.php'); ?>
