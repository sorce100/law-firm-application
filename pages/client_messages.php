<?php
ob_start();
include_once('../includes/header.php');
 // check if user is a client for not
 if ($_SESSION['account_name'] != 'contact') {
  	die();
  } 
// ////////////////
 include_once('../Classes/Messages.php');
 $objMessages = new Messages;
 
?>
<div class="col-md-12 col-sm-12 col-xs-12">
	<div class="x_panel">
	  <div class="x_title">
	    <h2><i class="fa fa-envelope"> Messages </i></h2>
	    <div class="clearfix"></div>
	  </div>
	  <div class="x_content">
	    <div class="col-md-2 leftcol-md2">
	      <!-- required for floating -->
	      <!-- Nav tabs -->
	      <ul class="nav nav-tabs tabs-left">
	        <li>
	        	<a href="#home" data-toggle="tab">
	        		<button id="compose" data-toggle="modal" data-target="#clientMessagesModal" class="btn btn-sm btn-danger btn-block" type="button">COMPOSE <i class="fa fa-pencil-square-o"></i></button>
	        	</a>
	        </li>
	        <li class="active"><a href="#inbox" data-toggle="tab"><i class="fa fa-inbox"></i> Inbox <span class="pull-right badge bg-green">5</span></a></li>
	        <li><a href="#sent" data-toggle="tab"><i class="fa fa-send"></i> Sent</a></li>
	      </ul>
	    </div>

	    <div class="col-xs-10">
	      <!-- Tab panes -->
	      <div class="tab-content">
	        <div id="inbox" class="tab-pane fade in active">
			     <div class="table-responsive">
	                	<table class="table table-hover tableList"> 
		                    <thead>
		                        <tr>
		                            <th>Subject</th>
		                            <th>Sender</th>
		                            <th>Date</th>
		                            <th></th>
		                        </tr>
		                    </thead>
		                   	<tbody id="inboxDisplay">
		                        <?php
	                              $receivedMessages = $objMessages->get_contact_received_messages(); 
	                              foreach ($receivedMessages as $receivedMessage) {
		                              	if ($receivedMessage["message_status"] == "UNREAD") {
		                              		echo '
	                                          <tr style="font-weight:bold;background-color:#f4f4f4;">
	                                            <td>'.$receivedMessage["message_subject"].'</td>
	                                            <td>'.$receivedMessage["message_sender_accounId"].'</td>
	                                            <td>'.$receivedMessage["added"].'</td>
	                                            <td>
	                                              <button class="btn-primary clientReadMessage" id="'.$receivedMessage["message_id"].'"><i class="fa fa-eye"> Read</i></button> 
	                                              <button class="btn-danger inbox_del_data" id="'.$receivedMessage["message_id"].'"><i class="fa fa-trash"></i></button>
	                                            </td>
	                                          </tr>
	                                        ';
		                              	}
		                              	else{
		                              		echo '
	                                          <tr>
	                                            <td>'.$receivedMessage["message_subject"].'</td>
	                                            <td>'.$receivedMessage["message_sender_accounId"].'</td>
	                                            <td>'.$receivedMessage["added"].'</td>
	                                            <td>
	                                              <button class="btn-primary clientReadMessage" id="'.$receivedMessage["message_id"].'"><i class="fa fa-eye"> Read</i></button> 
	                                              <button class="btn-danger inbox_del_data" id="'.$receivedMessage["message_id"].'"><i class="fa fa-trash"></i></button>
	                                            </td>
	                                          </tr>
	                                        ';
		                              	}    
	                              }
	                             ?>
		                    </tbody>
	                	</table>
	              </div>
		    </div>
		    <div id="sent" class="tab-pane fade">
			      <div class="table-responsive">
	                	<table class="table table-hover tableList"> 
		                    <thead>
		                        <tr>
		                            <th>Subject</th>
		                            <th>Date Sent</th>
		                            <th></th>
		                        </tr>
		                    </thead>
		                   	<tbody id="sentDisplay">
		                        <?php
	                              $sentMessages = $objMessages->get_sent_messages(); 
	                              foreach ($sentMessages as $sentMessage) {
	                                      echo '
	                                          <tr>
	                                            <td>'.$sentMessage["message_subject"].'</td>
	                                            <td>'.$sentMessage["added"].'</td>
	                                            <td>
	                                              <button class="btn-primary view_sent_message" id="'.$sentMessage["message_id"].'"><i class="fa fa-eye"></i></button> 
	                                              <button class="btn-danger view_sent_message_del_data" id="'.$sentMessage["message_id"].'"><i class="fa fa-trash"></i></button>
	                                            </td>
	                                          </tr>
	                                        ';
	                                  }
	                             ?>
		                    </tbody>
	                	</table>
	              </div>
		    </div>
	      </div>
	    </div>

	    <div class="clearfix"></div>

	  </div>
	</div>
</div>



<!-- for modal -->
<div class="modal fade" id="clientMessagesModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header" id="bg">
         <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true" style="color: red;font-size: 25px;" class="btn-default">&times; </span></button>
        <h4 class="modal-title">New Message</h4>
      </div>
      <div class="modal-body" id="bg">
          <form id="client_messages_form" class="f1">
          		<fieldset>
    		    <div class="row">
	                <div class="col-md-12">
	                	<div class="row">
			     			<div class="col-md-2">
			     				<label for="title" class="col-form-label">Case <span class="asterick"> *</span></label>
			     			</div>
			     			<div class="col-md-10">
			     				<div class="form-group">
			                       <select class="form-control clientMessageSelect2" style="width: 100%;" id="message_courtCaseId" name="message_courtCaseId">
			                        	<option selected="selected" disabled>Select Case</option>
			                        	<?php
			                              $objCourtCase = new CourtCase;
			                              $cases = $objCourtCase->get_contact_cases();
			                              foreach ($cases as $case) {
		                              		echo "<option value='".$case["case_id"]."'>".$case["case_name"].' ( '.$case["case_number"].")</option>";
			                              }
		                             	?>
			                        </select>
			                    </div>
			     			</div>
			     		</div>
			     		<hr>
	                    <!-- 1 -->
	                    <div class="row">
	                        <div class="col-md-2">
	                            <label for="title" class="col-form-label">Select Receipients</label>
	                        </div>
	                        <div class="col-md-10">
                              <ul class="nav nav-tabs nav-justified">
							    <li class="active"><a data-toggle="tab" href="#contact"><b>Contacts</b></a></li>
							    <li><a data-toggle="tab" href="#staff"><b>Employees</b></a></li>
							  </ul>

							  <div class="tab-content">
							    <div id="contact" class="tab-pane fade in active">
							      <div class="table-responsive">
			                            <table class="table table-hover tableList"> 
			                                <thead>
			                                    <tr>
			                                    	<th><input type="checkbox" id="select_all"/></th>
			                                        <th>First Name</th>
			                                        <th>Last Name</th>
			                                        <th>Phone Number</th>
			                                    </tr>
			                                </thead>
			                                <tbody id="contactResultsDisplay">
			                                    <?php
			                                      $contactobj= new Contact;
			                                      $contacts = $contactobj->get_contacts(); 
			                                      foreach ($contacts as $contact) {
			                                              echo '
			                                                  <tr>
			                                                  	<td><input type="checkbox" class="input-md messageContactIds" name="messageContactIds[]" value="'.trim($contact["contact_id"]).'"></td>
			                                                    <td>'.$contact["contact_First_name"].'</td>
			                                                    <td>'.$contact["contact_last_name"].'</td>
			                                                    <td>'.$contact["contact_cell"].'</td>
			                                                  </tr>
			                                                ';
			                                          }
			                                     ?>
			                                </tbody>
			                            </table>
			                      </div>
							    </div>
							    <!-- staff -->
							    <div id="staff" class="tab-pane fade">
							      <div class="table-responsive"><br>
							          	<table class="table table-hover tableList">
							                <thead>
							                    <tr>
			                                    	<th><input type="checkbox" id="select_all_staff"/></th>
							                        <th>First Name</th>
							                        <th>Last Name</th>
							                        <th>Phone Number</th>
							                        <th>Type</th>
							                    </tr>
							                </thead>
							               	<tbody id="staffResultsDisplay">
							                    <?php
							                        $objStaff = new Staff;
							                        $staffs = $objStaff->get_staff(); 
							                        foreach ($staffs as $staff) {
						                                echo '
						                                    <tr>
						                                      <td><input type="checkbox" class="input-md" name="messageStaffIds[]" id="messageStaffIds" value="'.trim($staff["staff_id"]).'"></td>
						                                      <td>'.$staff["staff_firstname"].'</td>
						                                      <td>'.$staff["staff_lastname"].'</td>
						                                      <td>'.$staff["staff_tel"].'</td>
						                                      <td>'.$staff["staff_type"].'</td>
						                                    </tr>
						                                  ';
						                            }
							                       ?>
							                </tbody>
							          	</table>
							       	</div>
							    </div>
							  </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
                <div class="f1-buttons">
                	<button type="button" class="btn btn-danger" data-dismiss="modal">Close <i class="fa fa-times"></i></button>
                    <button type="button" class="btn btn-next btn-primary">Compose Message <i class="fa fa-pencil"></i></button>
                </div>
            </fieldset>

            <fieldset>
            	
                <div class="row">
                    <div class="col-md-2">
                        <label for="title" class="col-form-label">Message Subject</label>
                    </div>
                    <div class="col-md-10">
                        <div class="form-group">
                           <input type="text" name="messageSubject" id="messageSubject" class="form-control" autocomplete="off" required placeholder="Subject of Message &hellip;" required>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                	<div class="col-md-12">
                		<textarea class="summernote" id="messageContent" name="messageContent">
							
						</textarea >
                	</div>
                </div>
	     		<!-- for inserting the page id -->
                <input type="hidden" name="data_id" id="messages_data_id" value="">
                <!-- for insert query -->
                <input type="hidden" name="mode" id="messagesMode" value="insert">
                <div class="f1-buttons">
                    <button type="button" class="btn btn-default btn-previous">Go Back </button>
                    <button type="submit" class="btn btn-primary" id="client_messages_btn">Send Message <i class="fa fa-send"></i></button>
                </div>
            </fieldset>
          </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- modal to read message -->
<div class="modal fade" id="clientReadMessageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header" id="bg">
         <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true" style="color: red;font-size: 25px;" class="btn-default">&times; </span></button>
        <h4 class="modal-title" id="ViewMessagesubject">View Message</h4>
      </div>
      <div class="modal-body" id="bg">
          <form id="read message_form">
            <div class="row">
                <div class="col-md-12">
                    <!-- 1 -->
                    <div class="row">
                        <div class="col-md-2">
                            <label for="title" class="col-form-label">Sender</label>
                        </div>
                        <div class="col-md-10">
                            <div class="form-group">
                               <input type="text" name="readSender" id="readSender" class="form-control" readonly>
                            </div>
                        </div>
                    </div>
                    <!-- 2 -->
                    <div class="row">
                        <div class="col-md-2">
                            <label for="title" class="col-form-label">Subject</label>
                        </div>
                        <div class="col-md-10">
                            <div class="form-group">
                               <input type="text" name="readSubject" id="readSubject" class="form-control" placeholder="Page url &hellip;" autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <!-- 3 -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                               <textarea class="form-control readSummernote" rows="10" id="readContent"></textarea>
                            </div>
                        </div>
                    </div>
                   <div class="modal-footer">
                      <button type="button" class="btn btn-danger" data-dismiss="modal">Close <i class="fa fa-times"></i></button>
                   </div>
                </div>
            </div>
          </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<?php require_once('../includes/footer.php');?>

<script src="js/pageScript/multistep.js"></script>
<script src="js/pageScript/client_message.js"></script>
