<?php
  ob_start(); 
  include_once('../includes/header.php');
  // check if user is a staff for not
   if ($_SESSION['account_name'] != 'staff') {
      die();
    } 
  $objCourtCase = new CourtCase; 
  $objEvents = new Events;
?>
<!-- welcome -->
<div class="row">
   <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_content">
        <h3 class="text-center">Welcome to EL AGBEMAVA Online Law Office , <strong><?php echo $accountName; ?></strong></h3>
      </div>
    </div>
   </div>
</div>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="row top_tiles">
        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
          <a href="court_cases_list.php">
            <div class="tile-stats">
              <div class="icon"><i class="fa fa-folder-open-o"></i></div>
              <div class="count"><?php echo $objCourtCase->count_cases(); ?></div>
              <h3>Open Cases</h3>
            </div>
          </a>
        </div>
        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
          <a href="contacts_list.php">
            <div class="tile-stats">
              <div class="icon"><i class="fa fa-users"></i></div>
              <div class="count"><?php $objContact = new Contact; echo $objContact->count_contacts() ?></div>
              <h3>Active Contacts</h3>
            </div>
          </a>
        </div>
        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
          <a href="leads_list.php">
            <div class="tile-stats">
              <div class="icon"><i class="fa fa-user-secret"></i></div>
              <div class="count"><?php $objleads = new Leads; echo $objleads->count_leads() ?></div>
              <h3>Active Leads</h3>
            </div>
          </a>
        </div>
        <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
          <a href="staff_list.php">
            <div class="tile-stats">
              <div class="icon"><i class="fa fa-users"></i></div>
              <div class="count"><?php echo $objStaff->count_staff(); ?></div>
              <h3>Staff</h3>
            </div>
          </a>
        </div>
      </div>
    </div>
  </div>
</div>



<!-- short cut menus -->

<div class="row">
  <!-- left part -->
  <div class="col-md-7">
    <!-- short cut menu -->
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_content">
            <?php 
               if (in_array("events.php", $_SESSION['pagesAllowed'])) {
                 echo '<a class="btn btn-app" data-toggle="modal" data-target="#eventModal">
                        <i class="fa fa-edit"></i> Events
                      </a>';
               }
               if (in_array("documents.php", $_SESSION['pagesAllowed'])) {
                  echo '<a class="btn btn-app" data-toggle="modal" data-target="#addDocumentModal">
                          <i class="fa fa-file"></i> Documents
                        </a>';
               }
               if (in_array("tasks.php", $_SESSION['pagesAllowed'])) {
                echo '<a class="btn btn-app" data-toggle="modal" data-target="#addTaskModal">
                        <i class="fa fa-tasks"></i> Task
                      </a>';
               }
               if (in_array("admin_lead.php", $_SESSION['pagesAllowed'])) {
                 echo '<a class="btn btn-app" data-toggle="modal" data-target="#addLeadModal">
                        <i class="fa fa-road"></i> Lead
                      </a>';
               }
               if (in_array("contacts.php", $_SESSION['pagesAllowed'])) {
                 echo '<a class="btn btn-app" data-toggle="modal" data-target="#addContactModal">
                        <i class="fa fa-user"></i> Contact
                      </a>';
               }
               if (in_array("court_cases.php", $_SESSION['pagesAllowed'])) {
                echo '<a class="btn btn-app" data-toggle="modal" data-target="#addCaseModal">
                        <i class="fa fa-briefcase"></i> Case
                      </a>';
               }
               if (in_array("bills.php", $_SESSION['pagesAllowed'])) {
                echo '<a class="btn btn-app" data-toggle="modal" data-target="#timeEnteriesModal">
                        <i class="fa fa-clock-o"></i> Time Entry
                      </a>';
               }
               if (in_array("bills.php", $_SESSION['pagesAllowed'])) {
                echo '<a class="btn btn-app" data-toggle="modal" data-target="#expensesModal">
                        <i class="fa fa-credit-card"></i> Expenses
                      </a>';
               }
               if (in_array("messages.php", $_SESSION['pagesAllowed'])) {
                echo '<a class="btn btn-app" href="messages.php">
                        <span class="badge bg-red">12</span>
                        <i class="fa fa-envelope"></i> Inbox
                      </a>';
               }
               if (in_array("notification.php", $_SESSION['pagesAllowed'])) {
                echo '<a class="btn btn-app" href="notification.php">
                        <span class="badge bg-green">12</span>
                        <i class="fa fa-bullhorn"></i> Notification
                      </a>';
               }

            ?>

            
          </div>
        </div>
      </div>
    </div>
    <!-- Recent activities-->
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <h3>Recent Activities</h3>
              <!-- <div class="clearfix"></div> -->
            </div>
            <div class="x_content">
              <!--  -->

              <ul class="list-unstyled timeline">
                    <?php 
                      $objActivity = new Activity;
                      $activities = $objActivity->get_user_activity_limit_10();
                      foreach ($activities as $activity) {
                        echo '
                          <li><div class="block">
                            <div class="tags"><a href="" class="tag"><span>'.$activity["date_done"].'</span></a></div>
                            <div class="block_content"><p class="excerpt">'.$activity["activity_name"].' <b> by </b>'.$activity["user_id"].'</p></div>
                          </div></li>';
                      }
                    ?>
              </ul>

              <!--  -->
            </div>
          </div>
      </div>
    </div>


  </div>
  <!-- right part -->
  <div class="col-md-5">
    <!-- Upcoming events -->
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
              <h3>Upcoming Events</h3>
            </div>
            <div class="x_content">
              <!--  -->
              <?php 
                $staffEvents = $objEvents->dashboard_upcoming_event();
                foreach ($staffEvents as $staffEvent) {
                  echo '<h4>
                          <i class="fa fa-calendar-check-o fa-2x">
                          <span> </i> '. $staffEvent["event_start_date"] .' <b>To</b> '.$staffEvent["event_end_Date"] .'</span> 
                          <span class="dashboardTaskName"> ['.$staffEvent["event_name"].' ]</span> 
                          <span><i class="fa fa-users"> '.sizeof(json_decode($staffEvent["event_staff_id"]),true).'</i></span>
                        </h4>
                        <hr>';
                }
               ?>

              
              <!--  -->
            </div>
          </div>
      </div>
    </div>
    <!-- Upcoming task -->
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <h3>Upcoming Task</h3>
            </div>
            <div class="x_content">

              <div class="">
                <ul class="to_do">
                  <?php
                      $objTasks = new Tasks;
                      $tasks = $objTasks->dashboard_upcoming_task($_SESSION['account_id']);
                      foreach ($tasks as $task) {
                        if($objTasks->check_date_month($task["task_due_date"])){
                          switch ($task["task_priority"]) {
                            case 'High':
                              echo '
                                <li style="background-color:#FFDCDC;margin-bottom:5px;">
                                  <p>
                                    <span class="title">'.$task["task_name"].'</span>
                                    <span class="short-description">'.$objCourtCase->get_case_name($task["court_case_id"]).'</span>
                                    <span class="date">'.$task["task_due_date"].'</span>
                                  </p>
                                </li>';
                                break;
                                case 'Medium':
                                echo '
                                <li style="background-color:#FFFACD;">
                                  <p>
                                    <span class="title">'.$task["task_name"].'</span>
                                    <span class="short-description">'.$objCourtCase->get_case_name($task["court_case_id"]).'</span>
                                    <span class="date">'.$task["task_due_date"].'</span>
                                  </p>
                                </li>';
                                break;
                                case 'Low':
                                echo '
                                <li style="background-color:#E0FFFF">
                                  <p>
                                    <span class="title">'.$task["task_name"].'</span>
                                    <span class="short-description">'.$objCourtCase->get_case_name($task["court_case_id"]).'</span>
                                    <span class="date">'.$task["task_due_date"].'</span>
                                  </p>
                                </li>';
                                break;
                                case 'No Priority':
                                echo '
                                <li style="background-color:">
                                  <p>
                                    <span class="title">'.$task["task_name"].'</span>
                                    <span class="short-description">'.$objCourtCase->get_case_name($task["court_case_id"]).'</span>
                                    <span class="date">'.$task["task_due_date"].'</span>
                                  </p>
                                </li>';
                              break;  
                            default:
                              # code...
                              break;
                          }
                        }
                      }
               ?>
                </ul>
              </div>
            </div>
          </div>
      </div>
    </div>
  </div>

  </div>
  <!-- right part -->

<?php require_once('../includes/footer.php'); ?>
<!-- events include -->
<?php include_once('events_include.php'); ?>
<!-- documents include -->
<?php include_once('documents_include.php'); ?>
<!-- task include -->
<?php include_once('tasks_include.php'); ?>
<!-- admin lead include -->
<?php include_once('admin_lead_include.php'); ?>
<!-- contacts include -->
<?php include_once('contacts_include.php'); ?>
<!-- court case include -->
<?php include_once('court_cases_include.php'); ?>
<!-- time entry include -->
<?php include_once('billTimeEntryInclude.php'); ?>
<!-- bill expences include -->
<?php include_once('billExpensesInclude.php'); ?>
