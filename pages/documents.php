<?php
	ob_start(); 
	require_once('../includes/header.php');
	// refer back to dashboard if page is not in group
	$retrievedFileName = basename($_SERVER['PHP_SELF']);
	if (!in_array($retrievedFileName, $_SESSION['pagesAllowed'])) {
	  header('Location: ../pages/dashboard.php');
	}
	// check if user is a staff for not
	 if ($_SESSION['account_name'] != 'staff') {
	  	die();
	  }  
?>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-folder"> Documents Page</i></h2>
        <!-- add new button -->
        <div class="pull-right">
            <button data-toggle="modal" data-target="#addDocumentModal" class="btn btn-danger"><span class="fa fa-plus"></span> Add Case Document</button>
        </div>
        <!-- end new button -->
        <div class="clearfix"></div>

      </div>
      <div class="x_content">
        <div class="table-responsive">
          <table class="table table-hover tableList"> 
            <thead>
                <tr>
                    <th>Case</th>
                    <th>Folder Name</th>
                    <th>Total</th>
                    <th>Assigned Date</th>
                    <th>Last Updated</th>
              		<th></th>
                </tr>
            </thead>
           	<tbody id="documentDisplay">
            <?php
              $objDocument = new Document;
              $documents = $objDocument->get_case_documents(); 
              if (!empty($documents)) {
                print_r($documents);
              }
             ?>
            </tbody>
          </table>
          </div>
      </div>
    </div>
  </div>
</div>


<!-- ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- Documents reader -->
<div class="modal fade" id="documentListModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" id="bg">
         <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true" class="btn-default asterick">&times; </span></button>
        <h4 class="modal-title">Documents Uploaded</h4>
      </div>
      <div class="modal-body" id="bg">
      	<div class="row">
      		<div class="col-md-12">
      			<table class="table table-condensed">
              <thead>
                <th width="55%">File Name</th>
                <th width="25%"></th>
                <th width="20%"></th>
              </thead>
              <tbody id="documentDisplayDiv"></tbody>   
            </table>
      		</div>
      	</div>
      </div>
       <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close <i class="fa fa-close"></i></button>
       </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->


<!-- ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- for firm document -->
<?php include('../includes/footer.php'); ?>
<?php include_once('documents_include.php'); ?>
<?php include_once('viewer_pdfImage.php'); ?>



