<?php
    ob_start();  
    include_once('../includes/header.php');
    // refer back to dashboard if page is not in group
    $retrievedFileName = basename($_SERVER['PHP_SELF']);
    if (!in_array($retrievedFileName, $_SESSION['pagesAllowed'])) {
      header('Location: ../pages/dashboard.php');
    }
    // check if user is a staff for not
   if ($_SESSION['account_name'] != 'staff') {
      die();
    } 
?>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-users"> Staff Setup </i></h2>
        <!-- add new button -->
        <div class="pull-right">
          <button data-toggle="modal" data-target="#addStaffModal" class="btn btn-danger input-sm"><span class="glyphicon glyphicon-user"></span> Add Staff</button>
        </div>
        <!-- end new button -->
        <div class="clearfix"></div>

      </div>
      <div class="x_content">
          <div class="table-responsive">
              <table class="table table-hover tableList">
                <thead>
                    <tr>
                        <th width="16%">First Name</th>
                        <th width="16%">Last Name</th>
                        <th width="16%">Phone Number</th>
                        <th width="16%">Type</th>
                        <th width="16%">Date Employed</th>
                        <th width="16%"></th>
                    </tr>
                </thead>
                <tbody id="resultsDisplay">
                    <?php
                        $objStaff = new Staff;
                        $staffs = $objStaff->get_staff(); 
                        foreach ($staffs as $staff) {
                                echo '
                                    <tr>
                                      <td>'.$staff["staff_firstname"].'</td>
                                      <td>'.$staff["staff_lastname"].'</td>
                                      <td>'.$staff["staff_tel"].'</td>
                                      <td>'.$staff["staff_type"].'</td>
                                      <td>'.$staff["staff_employdate"].'</td>
                                      <td>
                                        <button class="btn-primary update_data" id="'.$staff["staff_id"].'"><i class="fa fa-pencil"></i></button> <button class="btn-danger del_data" id="'.$staff["staff_id"].'"><i class="fa fa-trash"></i></button>
                                      </td>
                                    </tr>
                                  ';
                            }
                       ?>
                </tbody>
            </table>
          </div>
      </div>
    </div>
  </div>
</div>

<!-- modal -->
<div class="modal fade" id="addStaffModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header" id="bg">
         <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true" style="color: red;font-size: 25px;" class="btn-default">&times; </span></button>
        <h4 class="modal-title">Add Staff</h4>
      </div>
      <div class="modal-body" id="bg form-box">
        <form id="insert_form" method="post" class="f1" enctype="multipart/form-data">
        <fieldset>
          <h4><b>Personal Details:</b></h4>
          <div class="form-group">
            <!-- 1 -->
              <div class="row">
                <div class="col-md-2">
                  <label for="title" class="col-form-label">Name</label>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <input type="text" class="form-control input-sm" id="staffFirstName" name="staffFirstName" placeholder="First Name" autocomplete="off" required>
                </div>
                </div>
                <div class="col-md-2">
                  <div class="form-group">
                              <input type="text" class="form-control" id="staffMidName" name="staffMidName" placeholder="Middle" autocomplete="off" >
                          </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                      <input type="text" class="form-control" id="staffLastName" name="staffLastName" placeholder="Last Name" autocomplete="off" required>
                  </div>
                </div>
              </div>
              <!-- 2 -->
              <div class="row">
                <div class="col-md-2">
                  <label for="title" class="col-form-label">Phone Number</label>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                      <input type="tel" name="stafftel" id="stafftel" class="form-control" autocomplete="off" placeholder="xxx xxx xxxx">
                  </div>
                </div>
              </div>
              <!-- 3 -->
              <div class="row">
                <div class="col-md-2">
                  <label for="title" class="col-form-label">Emergency Contact</label>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                      <input type="tel" name="staffemergencytel" id="staffemergencytel" class="form-control" autocomplete="off" placeholder="For Emergency &hellip;">
                  </div>
                </div>
              </div>
              <!-- 5 -->
              <div class="row">
                <div class="col-md-2">
                  <label for="title" class="col-form-label">Email</label>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                      <input type="email" name="staffEmail" id="staffEmail" class="form-control" autocomplete="off" placeholder="example@example.com">
                  </div>
                </div>
              </div>
              <!-- 4 -->
              <div class="row">
                  <div class="col-md-2">
                    <label for="title" class="col-form-label">Date Of Birth</label>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group input-group">
                       <input type="tel" class="form-control" id="staffDob" name="staffDob" data-toggle="datepicker" readonly>
                       <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    </div>
                  </div>
                </div>
              <!-- 5 -->
              <div class="row">
                <div class="col-md-2">
                  <label for="title" class="col-form-label">Postal Address</label>
                </div>
                <div class="col-md-10">
                  <div class="form-group">
                     <textarea class="form-control" id="staffPostal" name="staffPostal" rows="2" placeholder="P.O.BOX 0000"></textarea>
                  </div>
                </div>
              </div>
              <!-- 6 -->
              <div class="row">
                <div class="col-md-2">
                  <label for="title" class="col-form-label">House Number</label>
                </div>
                <div class="col-md-10">
                  <div class="form-group">
                      <input type="tel" name="staffHouseNum" id="staffHouseNum" class="form-control" autocomplete="off">
                  </div>
                </div>
              </div>
              <!-- 7 -->
              <div class="row">
                <div class="col-md-2">
                  <label for="title" class="col-form-label">Location</label>
                </div>
                <div class="col-md-10">
                  <div class="form-group">
                     <textarea class="form-control" id="staffHouseLoc" name="staffHouseLoc" rows="6"></textarea>
                  </div>
                </div>
              </div>
              <hr>
              <div class="f1-buttons">
                  <button type="button" class="btn btn-next btn-primary">Continue to Professional Details</button>
              </div>
            </fieldset>

            <fieldset>
                <h4><b>Professional Details</b></h4>
                <!-- 1 -->
                <div class="row">
                  <div class="col-md-3">
                    <label for="title" class="col-form-label">Type</label>
                  </div>
                  <div class="col-md-9">
                    <select class="form-control" id="staffType" name="staffType">
                      <option value="None">None</option>
                      <option value="Attorney">Attorney</option>
                      <option value="Paralegal">Paralegal</option>
                      <option value="Staff">Staff</option>
                    </select>
                  </div>
                </div><br>
                <!-- 2 -->
                <div class="row">
                  <div class="col-md-3">
                    <label for="title" class="col-form-label">Professional Number</label>
                  </div>
                  <div class="col-md-9">
                    <div class="form-group">
                        <input type="text" name="staffProfNum" id="staffProfNum" class="form-control">
                    </div>
                  </div>
                </div>
                <!-- 4 -->
                <div class="row">
                  <div class="col-md-3">
                    <label for="title" class="col-form-label">Date Employed</label>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group input-group">
                       <input type="tel" class="form-control" id="staffEmployDate" name="staffEmployDate" data-toggle="datepicker" readonly>
                       <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    </div>
                  </div>
                </div>
                <hr> 
                <div class="f1-buttons">
                    <button type="button" class="btn btn-danger btn-previous">Go Back <i class="fa fa-times"></i></button>
                    <button type="button" class="btn btn-next btn-primary">Additional Details</button>
                </div>
                </fieldset>

                <fieldset>
                <!-- 1 -->
                <div class="row">
                  <div class="col-md-3">
                    <label for="title" class="col-form-label">Add Picture</label>
                  </div>
                  <div class="col-md-9">
                    <div class="form-group" id="staffPicDiv">
                        <img src="../img/avatar.png">
                    </div>
                  </div>
                  <div class="col-md-2"></div>
                  <div class="col-md-5">
                    <div class="form-group">
                        <input type="file" name="staffPic" id="staffPic" class="form-control">
                    </div>
                  </div>
                  
                </div>
                <!-- 2 -->
                <div class="row">
                  <div class="col-md-2">
                    <label for="title" class="col-form-label">Private Note</label>
                  </div>
                  <div class="col-md-10">
                    <div class="form-group">
                       <textarea class="form-control" id="staffNote" name="staffNote" rows="6"></textarea>
                    </div>
                  </div>
                </div>
                <!-- for inserting the page id -->
                    <input type="hidden" name="data_id" id="staffdata_id" value="">
                    <!-- for insert query -->
                    <input type="hidden" name="mode" id="staffmode" value="insert"> 

                <div class="f1-buttons modal-footer">
                    <button type="button" class="btn btn-danger btn-previous">Go Back <i class="fa fa-times"></i></button>
                    <button type="submit" class="btn btn-submit btn-primary" id="staffsave_btn">Save & Finish <i class="fa fa-floppy-o"></i></button>
                </div>
            </fieldset>
      
      </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<?php include('../includes/footer.php'); ?>
<script type="text/javascript" src="js/pageScript/multistep.js"></script>
<!-- scripts -->
<script src="js/pageScript/staff.js"></script>
