
<!-- for modal -->
<div class="modal fade" id="addLeadModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header" id="bg">
         <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true" style="color: red;font-size: 25px;" class="btn-default">&times; </span></button>
        <h3 class="modal-title leadSubject" >Add lead</h3>
      </div>
      <div class="modal-body">
          <form id="lead_form" method="POST">
            <div class="row">
                <div class="col-md-12">
                  <h3 class="defaultColor">Potential New Client</h3><br>
                    <!--  -->
                    <div class="row">
                      <div class="col-md-2">
                        <label for="title" class="col-form-label">Lead Name <span class="asterick">*</span></label>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                            <input type="text" class="form-control input-sm" id="leadFirstName" name="leadFirstName" placeholder="First Name" autocomplete="off" required>
                        </div>
                      </div>
                      <div class="col-md-2">
                        <div class="form-group">
                            <input type="text" class="form-control" id="leadMidName" name="leadMidName" placeholder="Middle" autocomplete="off">
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                            <input type="text" class="form-control" id="leadLastName" name="leadLastName" placeholder="Last Name" autocomplete="off" required>
                        </div>
                      </div>
                    </div>
                    <!--  -->
                    <div class="row">
                        <div class="col-md-2">
                            <label for="title" class="col-form-label">Email <span class="asterick">*</span></label>
                        </div>
                        <div class="col-md-10">
                            <div class="form-group">
                               <input type="email" name="leadEmail" id="leadEmail" class="form-control" placeholder="example@email.com" autocomplete="off" required>
                            </div>
                        </div>
                    </div>
                    <!--  -->
                    <div class="row">
                      <div class="col-md-2">
                        <label for="title" class="col-form-label">Cell Phone <span class="asterick">*</span></label>
                      </div>
                      <div class="col-md-10">
                        <div class="form-group">
                            <input type="number" class="form-control" id="leadTel" name="leadTel" placeholder="xxx xxx xxxx" autocomplete="off" required>
                        </div>
                      </div>
                    </div>
                    <!--  -->
                    <div class="row">
                      <div class="col-md-2">
                        <label for="title" class="col-form-label">Office Location</label>
                      </div>
                      <div class="col-md-10">
                        <div class="form-group">
                            <textarea class="form-control" id="leadOfficeLocation" name="leadOfficeLocation" placeholder="Address of lead" autocomplete="off"></textarea>
                        </div>
                      </div>
                    </div>
                    <!--  -->
                    <div class="row">
                      <div class="col-md-2">
                        <label for="title" class="col-form-label">Town, Region <span class="asterick">*</span></label>
                      </div>
                      <div class="col-md-5">
                        <div class="form-group">
                            <input type="text" class="form-control" id="leadTown" name="leadTown" placeholder="Contact City" autocomplete="off" >
                        </div>
                      </div>
                      <div class="col-md-5">
                        <select name="leadRegion" id="leadRegion" class="form-control" required>
                          <option disabled selected>Please Select</option>
                          <?php
                            include_once('../includes/regions.html'); 
                          ?>
                         </select>
                      </div>
                    </div>
                  </div>
                </div>
                  <hr>
                  <!-- for Potential case -->
                  <div class="row">
                    <div class="col-md-12">
                      <h3 class="defaultColor">Potential New Case</h3><br>
                      <!--  -->
                      <div class="row">
                        <div class="col-md-2">
                          <label for="title" class="col-form-label">Matter (Case) <span class="asterick">*</span></label>
                        </div>
                        <div class="col-md-10">
                          <div class="form-group">
                              <input type="text" class="form-control" id="leadCaseMatter" name="leadCaseMatter" placeholder="Name of Case" autocomplete="off" required>
                          </div>
                        </div>
                      </div>
                      <!--  -->
                      <div class="row">
                        <div class="col-md-2">
                          <label for="title" class="col-form-label">Case Description</label>
                        </div>
                        <div class="col-md-10">
                          <div class="form-group">
                            <textarea rows="4" class="form-control" id="leadCaseDescription" name="leadCaseDescription" placeholder="Add notes about Potential mew case" autocomplete="off"></textarea>
                          </div>
                        </div>
                      </div>
                      <!--  -->
                      <div class="row">
                        <div class="col-md-2">
                          <label for="title" class="col-form-label">Status <span class="asterick">*</span></label>
                        </div>
                        <div class="col-md-10">
                          <div class="form-group">
                            <select class="form-control" id="leadCaseStatus" name="leadCaseStatus" required>
                              <option selected disabled>Please Select</option>
                              <option value="New">New</option>
                              <option value="Contacted">Contacted</option>
                              <option value="Consulting">Consulting Scheduled</option>
                              <option value="Pending">Pending</option>
                            </select>
                          </div>
                        </div>
                      </div>
                      <!--  -->
                      <div class="row">
                        <div class="col-md-2">
                          <label for="title" class="col-form-label">Practice Area <span class="asterick">*</span></label>
                        </div>
                        <div class="col-md-5">
                          <div class="form-group">
                            <select class="form-control" style="width:100%;" name="leadCasePracticeArea" id="leadCasePracticeArea" required> 
                                <option value="" selected="selected" disabled>Select Practice Area</option>
                                <?php
                                    $objPracticeArea = new PracticeArea;
                                    $details = $objPracticeArea->get_practice_areas_options(); 
                                    if (!empty($details)) {
                                      print_r($details);
                                    }
                                    
                                ?>
                             </select>
                          </div>
                        </div>
                      </div>
                      <!--  -->
                      <div class="row">
                        <div class="col-md-2">
                          <label for="title" class="col-form-label">Assigned To <span class="asterick">*</span></label>
                        </div>
                        <div class="col-md-10">
                          <div class="form-group">
                            <select class="form-control" style="width: 100%;" id="leadCaseAssignedTo" name="leadCaseAssignedTo" required>
                              <option value="" selected="selected" disabled>Select Attorney</option>
                              <?php
                                  $objStaff = new Staff;
                                  $staffs = $objStaff->get_staff_by_type("Attorney"); 
                                  foreach ($staffs as $staff) {
                                          echo '<option value='.$staff["staff_id"].'>'.$staff["staff_firstname"]." ".$staff["staff_lastname"].'</option>';
                                      }
                              ?>
                             </select>
                          </div>
                        </div>
                      </div>
                      <!--  -->
                      <div class="row">
                        <div class="col-md-2">
                          <label for="title" class="col-form-label">Due Date <span class="asterick">*</span></label>
                        </div>
                        <div class="col-md-5">
                          <div class="form-group input-group">
                            <input type="text" class="form-control" id="leadCaseCompletionDate" name="leadCaseCompletionDate" data-toggle="datepicker" readonly required>
                          <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                          </div>
                        </div>
                      </div>

                      <!--  -->
                      <h4 class="defaultColor">Fee Arrangement</h4><br>
                      <!--  -->
                      <div class="row">
                        <div class="col-md-2">
                          <label for="title" class="col-form-label">Hourly Rate <span class="asterick">*</span></label>
                        </div>
                        <div class="col-md-5">
                          <div class="form-group">
                            <input type="number" class="form-control" id="leadCaseHourlyRate" name="leadCaseHourlyRate" autocomplete="off" required>
                          </div>
                        </div>
                      </div>
                      <!--  -->
                      <div class="row">
                        <div class="col-md-2">
                          <label for="title" class="col-form-label">Monthly Retainer <span class="asterick">*</span></label>
                        </div>
                        <div class="col-md-5">
                          <div class="form-group">
                            <input type="number" class="form-control" id="leadCaseMonthlyRetainer" name="leadCaseMonthlyRetainer" placeholder="Name of Case" autocomplete="off" required>
                          </div>
                        </div>
                      </div>
                      <!--  -->
                      <h4 class="defaultColor">Fee Arrangement</h4><br>
                      <!--  -->
                      <div class="row">
                        <div class="col-md-2">
                          <label for="title" class="col-form-label">Frequency <span class="asterick">*</span></label>
                        </div>
                        <div class="col-md-5">
                          <div class="form-group">
                            <select class="form-control" id="leadCaseFeeFrequency" name="leadCaseFeeFrequency" required>
                              <option selected disabled>Please Select</option>
                              <option value="Monthly">Monthly</option>
                              <option value="Quarterly">Quarterly</option>
                              <option value="Completion">Completion</option>
                              <option value="Others">Others</option>
                            </select>
                          </div>
                        </div>
                      </div>
                      <!--  -->
                      <div class="row">
                        <div class="col-md-2">
                          <label for="title" class="col-form-label">Special Arrangement</label>
                        </div>
                        <div class="col-md-10">
                          <div class="form-group">
                            <input type="text" class="form-control" id="leadCaseSpecialArrangement" name="leadCaseSpecialArrangement" placeholder="" autocomplete="off" required>
                          </div>
                        </div>
                      </div>
                      <!--  -->
                      <h4 class="defaultColor">Conflict information</h4><br>
                      <!--  -->
                      <div class="row">
                        <div class="col-md-2">
                          <label for="title" class="col-form-label">Associated Clients & Parties <span class="asterick">*</span></label>
                        </div>
                        <div class="col-md-10">
                          <div class="form-group">
                              <textarea rows="4" class="form-control" id="leadCaseAssociatedClients" name="leadCaseAssociatedClients" placeholder="Client and other parties associated with client" autocomplete="off" required></textarea>
                          </div>
                        </div>
                      </div>
                      <!--  -->
                      <div class="row">
                        <div class="col-md-2">
                          <label for="title" class="col-form-label">Adverse Parties</label>
                        </div>
                        <div class="col-md-10">
                          <div class="form-group">
                              <textarea rows="4" class="form-control" id="leadCaseAdverseParties" name="leadCaseAdverseParties" autocomplete="off"></textarea>
                          </div>
                        </div>
                      </div>
                      <!--  -->
                       <div class="row">
                        <div class="col-md-2">
                          <label for="title" class="col-form-label">Associated Files</label>
                        </div>
                        <div class="col-md-10">
                          <div class="form-group">
                              <textarea rows="4" class="form-control" id="leadCaseAssociatedFiles" name="leadCaseAssociatedFiles" placeholder="Names associated with other files for this client" autocomplete="off"></textarea>
                          </div>
                        </div>
                      </div>
                      <!--  -->
                    </div>
                  </div>

                  <!-- for inserting the page id -->
                  <input type="hidden" name="data_id" id="lead_data_id" value="">
                  <!-- for insert query -->
                  <input type="hidden" name="mode" id="leadMode" value="insert">

                 <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close <i class="fa fa-times"></i></button>
                    <button type="submit" class="btn btn-primary" id="leadSave_btn">Save Lead <i class="fa fa-save"></i></button>
                 </div>
          </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- scripts -->
<script src="js/pageScript/lead.js"></script>