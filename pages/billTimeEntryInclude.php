<div class="modal fade" id="timeEnteriesModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header" id="bg">
         <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true" style="color: red;font-size: 25px;" class="btn-default">&times; </span></button>
        <h4 class="modal-title" id="timerSubject">Add Time Entry</h4>
      </div>
      <div class="modal-body" id="bg">
          <form id="timeEnteries_form">
            <div class="row">
                <div class="col-md-12">
                    <!-- 1 -->
                    <div class="row">
                        <div class="col-md-2">
                            <label for="title" class="col-form-label">Case Link <span class="asterick">*</span></label>
                        </div>
                        <div class="col-md-10">
                            <div class="form-group">
                              <select class="form-control timeEntryselect2" style="width: 100%;" id="timeEntriesCourtCaseId" name="timeEntriesCourtCaseId" required>
                                <option value="" selected="selected">Select Case</option>
                                <?php
                                    $cases = $objCourtCase->get_cases(); 
                                    if (!empty($cases)) {
                                        print_r($cases);
                                    }
                                ?>
                              </select>
                          </div>
                        </div>
                    </div>
                    <!-- grab staff id to select staff -->
                                <input type="hidden" id="accountID" value="<?php echo $_SESSION['account_id'];?>">
                    <!-- 2 -->
                    <div class="row">
                        <div class="col-md-2">
                            <label for="title" class="col-form-label">User</label>
                        </div>
                        <div class="col-md-10">
                            <div class="form-group">
                               <select class="form-control" id="timeEntryStaff" name="timeEntryStaff">
                                <!-- activate for update -->
                                <?php
                                  foreach ($objStaff->get_staff() as $staff) {
                                     echo '<option value="'.$staff['staff_id'].'">'.$staff['staff_firstname'].' '.$staff['staff_lastname'].'</option>';
                                  }
                                ?>
                                 
                               </select>
                            </div>
                        </div>
                    </div>
                    <!-- 3 -->
                    <div class="row">
                        <div class="col-md-2">
                            <label for="title" class="col-form-label">Activity</label>
                        </div>
                        <div class="col-md-10">
                            <div class="form-group">
                               <input type="text" name="timeEntryActivity" id="timeEntryActivity" class="form-control" placeholder="Enter Activity &hellip;" autocomplete="off" required>
                            </div>
                        </div>
                    </div>
                    <!--  -->
                    <div class="row">
                        <div class="col-md-2">
                          <label for="title" class="col-form-label">Time Billable</label>
                        </div>
                        <div class="col-md-5">
                            <label class="switch">
                              <input type="checkbox" checked id="timeEntryBillable" name="timeEntryBillable" data-width="150">
                              <input type="hidden" name="timeEntryBillable_log" id="timeEntryBillable_log" value="YES" />

                            </label>
                            <!-- <input type="checkbox" checked data-toggle="toggle" data-on="YES" data-off="NO" data-onstyle="success" data-offstyle="danger" data-size="normal" data-width="120" id="timeEntryBillable" name="timeEntryBillable"> -->
                        </div>
                    </div>
                    <br>
                    <!-- 4 -->
                    <div class="row">
                        <div class="col-md-2">
                            <label for="title" class="col-form-label">Description</label>
                        </div>
                        <div class="col-md-10">
                            <div class="form-group">
                              <textarea class="form-control" name="timeEntryDescribe" id="timeEntryDescribe" placeholder=""></textarea>
                            </div>
                        </div>
                    </div>
                    <!-- 5 -->
                    <div class="row well">
                      <div class="col-md-3">
                        <label for="title"><b>Date</b></label>
                        <div class="form-group input-group">
                          <input type="text" class="form-control" id="timeEntryDate" name="timeEntryDate" data-toggle="datepicker" readonly="" placeholder="select" required>
                          <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <label for="title"><b>Rate</b></label>
                        <div class="form-group input-group">
                          <span class="input-group-addon">₵</span>
                          <input type="number" class="form-control" id="timeEntryRate" name="timeEntryRate" required>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <label for="title"><b>Type</b></label>
                        <div class="form-group ">
                          <select class="form-control" id="timeEntryRateType" name="timeEntryRateType">
                            <option value="hr">/hr</option>
                            <option value="flat">flat</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-3">
                         <label for="title"><b>Duration</b></label>
                         <div class="form-group">
                          <input type="text" class="form-control" id="timeEntryDuration" name="timeEntryDuration" required>
                        </div>
                        <p><b>1.0 = 1 hour</b></p>
                      </div>
                    </div>
                    <!-- invoiced -->
                    <input type="hidden" name="timeEntryInvoiced" id="timeEntryInvoiced" value="">
                    <!-- for inserting the page id -->
                    <input type="hidden" name="data_id" id="timeEntry_data_id" value="">
                    <!-- for insert query -->
                    <input type="hidden" name="mode" id="timeEntryMode" value="insert">

                   <div class="modal-footer">
                      <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                      <button type="submit" class="btn btn-primary" id="timeEntry_btn">Add <i class="fa fa-floppy-o"></i></button>
                   </div>
                </div>
            </div>
          </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal-->

<script type="text/javascript">
  $(document).ready(function(){
    $('#timeEnteries_form').parsley();
  });
  
</script>