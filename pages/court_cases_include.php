<div class="modal fade" id="addCaseModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header" id="bg">
         <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true" style="color: red;font-size: 25px;" class="btn-default">&times; </span></button>
        <h4 class="modal-title casetTitle">Add Case</h4>
      </div>
      <div class="modal-body" class="form-box">
      	<form id="courtCase_form" method="post" class="f1">
    		<div class="f1-steps">
    			<div class="f1-progress">
    			    <div class="f1-progress-line" data-now-value="16.66" data-number-of-steps="4" style="width: 15.66%;"></div>
    			</div>
    			<div class="f1-step active" id="firstStep">
    				<div class="f1-step-icon"><h4><i class="fa fa-user"></i></h4></div>
    				<p>Contacts</p>
    			</div>
    			<div class="f1-step">
    				<div class="f1-step-icon"><h4><i class="fa fa-file"></i></h4></div>
    				<p>Details</p>
    			</div>
    			<div class="f1-step">
    				<div class="f1-step-icon"><h4><i class="fa fa-money"></i></h4></div>
    				<p>Billing</p>
    			</div>
    		    <div class="f1-step">
    				<div class="f1-step-icon"><h4><i class="fa fa-user"></i></h4></div>
    				<p>Staff</p>
    			</div>
    		</div>
    		
    		<fieldset>
    		     <h4>Add Contacts:</h4><hr>
    		     <div class="row">
    		     	<div class="col-md-2">
    		     		<label for="title" class="col-form-label"><b>Select Contacts <span class="asterick">*</span></b></label>
    		     	</div>
    		     	<div class="col-md-10">
    		     		<div class="form-group">
	    		     		<select class="form-control contactSelect2" style="width: 100%;" id="caseContacts" name="caseContacts[]">
		                    	<option value="" selected="selected" disabled>Select Contact</option>
		                    	<?php
		                          $objContact = new Contact;
		                          $contacts = $objContact->get_contacts(); 
		                          foreach ($contacts as $contact) {
		                              echo '<option value='.$contact["contact_id"].'>'.$contact["contact_First_name"]." ".$contact["contact_mid_name"]." ".$contact["contact_last_name"].'</option>';
		                              }
		                         ?>
		                    </select>
		                </div>
    		     	</div>
    		     </div>

                <div class="row">
			     	<div class="col-md-2">
			     		<label for="title" class="col-form-label">Contacts Added</label>
			     	</div>
			     	<div class="col-md-10" id="contactsAddedDiv">
			     		
			     	</div>
			    </div>
			    <hr>
                <div class="f1-buttons">
                	<button type="button" class="btn bg-red" data-dismiss="modal">Close <i class="fa fa-times"></i></button>
                    <button type="button" class="btn btn-next btn-primary">Continue to Case Details <i class="fa fa-arrow-right"></i></button>
                </div>
            </fieldset>

            <fieldset>
                <h4>Case Details:</h4><hr>
                <!-- 1 -->
                <div class="row">
	     			<div class="col-md-2">
	     				<label for="title" class="col-form-label">Case Name <span class="asterick">*</span></label>
	     			</div>
	     			<div class="col-md-10">
	     				<div class="form-group">
		                    <input type="text" name="caseName" id="caseName" placeholder="E.g Jane Doe - Divorce" class="form-control" required>
		                </div>
	     			</div>
	     		</div>
	     		<!--  -->
	     		<div class="row">
	     			<div class="col-md-2">
	     				<label for="title" class="col-form-label">Case Suit No <span class="asterick">*</span></label>
	     			</div>
	     			<div class="col-md-10">
	     				<div class="form-group">
		                    <input type="text" name="caseSuitNumber" id="caseSuitNumber" placeholder="Enter Case suit number" class="form-control" required>
		                </div>
	     			</div>
	     		</div>
	     		<!-- 2 -->
	     		<div class="row">
	     			<div class="col-md-2">
	     				<label for="title" class="col-form-label">Case Code </label>
	     			</div>
	     			<div class="col-md-10">
	     				<div class="form-group">
		                    <input type="text" name="caseNumber" id="caseNumber" class="form-control" value="<?php echo mt_rand(100,500).'-'.substr(str_shuffle('abcdefghijklmnopqrstuvwxyABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'),0,4);?>" readonly>
		                </div>
	     			</div>
	     		</div>
	     		<!-- 3 -->
	     		<div class="row">
	     			<div class="col-md-2">
	     				<label for="title" class="col-form-label">Practice Area <span class="asterick">*</span></label>
	     			</div>
	     			<div class="col-md-10">
	     				<div class="form-group">
		                   <select class="form-control practiceAreaSelect2" style="width:100%;" name="casePracticeArea" id="casePracticeArea" required>
		                   	  <option value="" selected="selected" disabled>Select Practice Area</option>
		                   	  <?php
	                                $objPracticeArea = new PracticeArea;
	                                $details = $objPracticeArea->get_practice_areas_options(); 
	                                if (!empty($details)) {
	                                  print_r($details);
	                                }
	                                
	                            ?>
		                   </select>
		                </div>
	     			</div>
	     		</div>
	     		<!-- 4 -->
	     		<div class="row">
	     			<div class="col-md-2">
	     				<label for="title" class="col-form-label">Case Stage <span class="asterick">*</span></label>
	     			</div>
	     			<div class="col-md-4">
	     				<div class="form-group">
		                   <select class="form-control caseStageselect2" style="width: 100%;" name="caseStage" id="caseStage" required>
		                   		<option value="" selected="selected" disabled>Select Case Stage</option> 
		                   		<option value="Discovery">Discovery</option>
		                   		<option value="In Trial">In Trial</option>
		                   		<option value="On Hold">On Hold</option>
		                   		<option value="Pleading">Pleading</option>
		                   		<option value="Summon For Direction">Summon For Direction</option>
		                   		<option value="Case Managment Conference">Case Managment Conference</option>
		                   		<option value="Witness Statement">Witness Statement</option>
		                   		<option value="Examination Witness">Examination Witness</option>
		                   		<option value="Address">Address</option>
		                   		<option value="Judgement">Judgement</option>
		                   		<option value="interlocutory injunction">interlocutory injunction</option>
		                   </select>
		                </div>
	     			</div>
	     		</div>
	     		<!-- 5 -->
	     		<div class="row">
	     			<div class="col-md-2">
	     				<label for="title" class="col-form-label">Date Opened <span class="asterick">*</span></label>
	     			</div>
	     			<div class="col-md-4">
	     				<div class="form-group input-group">
		                   <input type="text" class="form-control" id="caseDateOpened" name="caseDateOpened" data-toggle="datepicker" readonly required value="<?php echo date('d-m-Y');?>">
		                   <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
		                </div>
	     			</div>
	     		</div>

	     		<hr>
	     		<!-- current court stage -->
	     		<div class="row">
	     			<div class="col-md-2">
	     				<label for="title" class="col-form-label">Current Court <span class="asterick">*</span></label>
	     			</div>
	     			<div class="col-md-4">
	     				<div class="form-group">
		                   <select class="form-control currentCourtSelect2" style="width:100%;" name="currentCourtMain" id="currentCourtMain" required>
		                   	  <option value="" selected="selected" disabled>Select Current Court</option>
		                   	  <?php
	                              $objCourtAdd = new CourtAdd;
	                              $details = $objCourtAdd->get_main_courts_options(); 
	                              if (!empty($details)) {
	                              	print_r($details);
	                              }
                             ?>
		                   </select>
		                </div>
	     			</div>
	     			<!-- list division -->
	     			<div class="col-md-4">
	     				<select class="form-control currentCourtSubSelect2" style="width:100%;" name="currentCourtSub" id="currentCourtSub" >
	     					<option value="" selected="selected" disabled>Select Current Court Division</option>
	     				</select>
	     			</div>
	     			<!-- enter Number -->
	     			<div class="col-md-2">
	     				<div class="form-group">
		                   <input type="number" name="currentCourtNum" id="currentCourtNum" class="form-control" value="1">
		                </div>
	     			</div>
	     			<!--  -->
	     		</div>
	     		<!--next court stage  -->
	     		<div class="row">
	     			<div class="col-md-2">
	     				<label for="title" class="col-form-label">Next Court <span class="asterick">*</span></label>
	     			</div>
	     			<div class="col-md-4">
	     				<div class="form-group">
		                   <select class="form-control nextCourtSelect2" style="width:100%;" name="nextCourtMain" id="nextCourtMain" required>
		                   	  <option value="" selected="selected" disabled>Select Current Court</option>
		                   	  <?php
	                              $objCourtAdd = new CourtAdd;
	                              $details = $objCourtAdd->get_main_courts_options(); 
	                              if (!empty($details)) {
	                              	print_r($details);
	                              }
                             ?>
		                   </select>
		                </div>
	     			</div>
	     			<!-- list division -->
	     			<div class="col-md-4">
	     				<select class="form-control nextCourtSubSelect2" style="width:100%;" name="nextCourtSub" id="nextCourtSub" >
	     					<option value="" selected="selected" disabled>Select Next Court Division</option>
	     				</select>
	     			</div>
	     			<!-- enter Number -->
	     			<div class="col-md-2">
	     				<div class="form-group">
		                   <input type="number" name="nextCourtNum" id="nextCourtNum" class="form-control" value="1">
		                </div>
	     			</div>
	     			<!--  -->
	     		</div>
	     		<!-- next court date -->
	     		<div class="row">
	     			<div class="col-md-2">
	     				<label for="title" class="col-form-label">Next Court Date <span class="asterick">*</span></label>
	     			</div>
	     			<div class="col-md-4">
	     				<div class="form-group input-group">
		                   <input type="text" class="form-control" id="nextCourtDate" name="nextCourtDate" data-toggle="datepicker" readonly required >
		                   <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
		                </div>
	     			</div>
	     		</div>
	     		<!--  -->
	     		<hr>
	     		<!-- 6 -->
                <div class="row">
	     			<div class="col-md-2">
	     				<label for="title" class="col-form-label">Description</label>
	     			</div>
	     			<div class="col-md-10">
	     				<div class="form-group ">
		                   <textarea class="form-control" rows="5" name="caseDescription" id="caseDescription" placeholder="Enter description of case &hellip;"></textarea>
		                </div>
	     			</div>
	     		</div>
	     		<!-- 7 -->

	     		<hr>
                <div class="f1-buttons">
                    <button type="button" class="btn btn-previous bg-red"><i class="fa fa-arrow-left"></i> Go Back </button>
                    <button type="button" class="btn btn-next btn-primary">Continue to Billing <i class="fa fa-arrow-right"></i></button>
                </div>
            </fieldset>

            <fieldset>
            	<h4>Case Billing Setup:</h4><hr>
	     		<div class="row">
	     			<div class="col-md-2">
	     				<label for="title" class="col-form-label">Billing Contact <span class="asterick">*</span></label>
	     			</div>
	     			<div class="col-md-6">
	     				<div class="form-group">
	    		     		<select class="form-control billingContactselect2" style="width: 100%;" id="caseBillingContact" name="caseBillingContact" required>
		                    	<option value="" selected="selected" disabled>Select Contact</option>
		                    	<?php
		                          $objContact = new Contact;
		                          $contacts = $objContact->get_contacts(); 
		                          foreach ($contacts as $contact) {
		                              echo '<option value='.$contact["contact_id"].'>'.$contact["contact_First_name"]." ".$contact["contact_mid_name"]." ".$contact["contact_last_name"].'</option>';
		                              }
		                         ?>
		                    </select>
		                </div>
	     			</div>
	     			<div class="col-md-4"></div>
	     		</div>
	     		<div class="row">
	     			<div class="col-md-2">
	     				<label for="title" class="col-form-label">Billing Method</label>
	     			</div>
	     			<div class="col-md-6">
	     				<div class="form-group">
		                   <select class="form-control" name="caseBillingMethod" id="caseBillingMethod">
		                   		<option value="Hourly">Hourly</option> 
		                   		<option value="Flat Fee">Flat Fee</option>
		                   		<option value="Pro Bono">Pro Bono</option>
		                   		<option value="None">None</option>
		                   </select>
		                </div>
	     			</div>
	     		</div>
	     		<hr>
            	<div class="f1-buttons">
                    <button type="button" class="btn btn-previous bg-red"><i class="fa fa-arrow-left"></i> Go Back</button>
                    <button type="button" class="btn btn-next btn-primary">Continue to staff <i class="fa fa-arrow-right"></i></button>
                </div>
            </fieldset>

            <fieldset>
            	<h4>Add Staff:</h4><hr>
                <!-- 1 -->
                <div class="row">
	     			<div class="col-md-8">
	     				<div class="form-group">
	     				   <label for="title" class="col-form-label"><b>Lead Attorney <span class="asterick">*</span></b></label>
		                   <select class="form-control leadAttorselect2" style="width: 100%;" id="caseLeadAttorney" name="caseLeadAttorney" required>
		                   	<option value="" selected="selected" disabled>Select Lead Attorney</option>
		                   	<?php
		                        $objStaff = new Staff;
		                        $staffs = $objStaff->get_staff_by_type("Attorney"); 
		                        foreach ($staffs as $staff) {
		                                echo '<option value='.$staff["staff_id"].'>'.$staff["staff_firstname"]." ".$staff["staff_middlename"]." ".$staff["staff_lastname"].'</option>';
		                            }
		                    ?>
		                   </select>
		                </div>
	     			</div>
	     		</div>
	     		<hr>
	     		<!-- 2 -->
	     		<div class="row">
	     			<div class="col-md-12">
	     				<div class="form-group">
	     				   <h4><b>Who should have access to this case</b></h4>
		                   <div class="table-responsive">
		                   	<table class="table tableList">
		                   		<thead>
		                   			<th><input type="checkbox" id="select_all"/></th>
		                   			<th>Name</th>
		                   			<th>User Type</th>
		                   		</thead>
		                   		<tbody id="caseStaff">
		                   			<?php
				                        $objStaff = new Staff;
				                        $staffs = $objStaff->get_staff(); 
				                        foreach ($staffs as $staff) {
				                                echo '
		                                          <tr>
		                                            <td><input type="checkbox" class="input-md caseStaffIds" name="caseStaffIds[]" value="'.trim($staff["staff_id"]).'"></td>
		                                            <td>'.$staff["staff_firstname"]." ".$staff["staff_middlename"]." ".$staff["staff_lastname"].'</td>
		                                            <td>'.$staff["staff_type"].'</td>
		                                          </tr>';
				                            }
				                    ?>
		                   		</tbody>
		                   	</table>
		                   </div>
		                </div>
	     			</div>
	     		</div>
	     		<!-- for inserting the page id -->
                <input type="hidden" name="data_id" id="courtCase_data_id" value="">
                <!-- for insert query -->
                <input type="hidden" name="mode" id="courtCaseMode" value="insert">
                <hr>
                <div class="f1-buttons">
                    <button type="button" class="btn btn-previous bg-red"><i class="fa fa-arrow-left"></i> Go Back</button>
                    <button type="submit" class="btn btn-submit btn-success" id="courtCaseBtn">Save & Finish <i class="fa fa-floppy-o"></i></button>
                </div>
            </fieldset>
    	
    	</form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript" src="js/pageScript/multistep.js"></script>
<script src="js/pageScript/court_case.js"></script>