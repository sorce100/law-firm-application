<?php $objEventType = new EventType; ?>
<!-- modal -->
<div class="modal fade" id="eventModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header" id="bg">
         <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true" class="btn-default asterick">&times; </span></button>
        <h4 class="modal-title eventTitle">Add Event</h4>
      </div>
      <div class="modal-body" id="bg">
	      <form id="event_form" method="POST">
	     	<div class="row">
	     		<div class="col-md-8" style="border-right:1px solid #bec4be; ">
	     			<!-- 1 -->
		     		<div class="row">
		     			<div class="col-md-2">
		     				<label for="title" class="col-form-label">Case <span class="asterick"> *</span></label>
		     			</div>
		     			<div class="col-md-10">
		     				<div class="form-group">
		                       <select class="form-control eventSelect2" style="width: 100%;" id="eventCourtCaseId" name="eventCourtCaseId" required>
		                        	<option selected="selected" disabled>Select Case</option>
		                        	<?php
	                                    $cases = $objCourtCase->get_cases(); 
	                                    if (!empty($cases)) {
	                                        print_r($cases);
	                                    }
	                                ?>
		                        </select>
		                    </div>
		     			</div>
		     		</div>
		     	<!-- 2 -->
		     		<div class="row">
		     			<div class="col-md-2">
		     				<label for="title" class="col-form-label">Event Name <span class="asterick"> *</span></label>
		     			</div>
		     			<div class="col-md-7">
		     				<div class="form-group">
		                        <input type="text" name="eventName" id="eventName" class="form-control" required autocomplete="off">
		                    </div>
		     			</div>
		     			<!-- select event color -->
		     			<div class="col-md-3">
		     				<select class="form-control" name="eventType" id="eventType" required>
		     					<option selected disabled>Please Select</option>
		     					<?php
			     					$events = $objEventType->get_event_types(); 
				                    foreach ($events as $event) {
				                     echo '<option style="color:#fff;background:'.$event["event_type_color"].'" value="'.$event["event_type_id"].'">'.$event["event_type_name"].'</option>';
				                    } 
		     					?>
		     				</select>
		     			</div>
		     		</div>
		     		

		     	<!-- 3 -->
		     		<div class="row">
		     			<div class="col-md-2">
		     				<label for="title" class="col-form-label">Start <span class="asterick"> *</span></label>
		     			</div>
		     			<div class="col-md-6">
		     				<div class="form-group input-group">
		                        <input type="text" class="form-control" id="eventStartDate" name="eventStartDate" data-toggle="calendardatepicker" readonly required>
				                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
		                    </div>
		     			</div>
		     		</div>
		     	<!-- 4 -->
		     	<div class="row">
		     			<div class="col-md-2">
		     				<label for="title" class="col-form-label">End <span class="asterick"> *</span></label>
		     			</div>
		     			<div class="col-md-6">
		     				<div class="form-group input-group">
		                        <input type="text" class="form-control" id="eventEndDate" name="eventEndDate" data-toggle="calendardatepicker" readonly required>
				                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
		                    </div>
		     			</div>
		     		</div>
		     	<!-- 5 -->
		     		<div class="row">
		     			<div class="col-md-2">
		     				<label for="title" class="col-form-label">Location</label>
		     			</div>
		     			<div class="col-md-10">
		     				<div class="form-group">
		                         <input type="text" class="form-control" id="eventLocation" name="eventLocation" placeholder="Set Location &hellip;" autocomplete="off">
		                    </div>
		     			</div>
		     		</div>
		     	<!-- 6 -->
		     		<div class="row">
			     			<div class="col-md-2">
			     				<label for="title" class="col-form-label">Description</label>
			     			</div>
			     			<div class="col-md-10">
			     				<div class="form-group ">
				                   <textarea class="form-control" rows="6" name="eventDescription" id="eventDescription" placeholder="Enter description of task &hellip;" autocomplete="off"></textarea>
				                </div>
			     			</div>
			     		</div>
	     		</div>
	     		<div class="col-md-4" >
	     			<div class="table-responsive">
	     				<table class="table">
	     					<thead>
	     						<th width="80%">Case Contacts</th>
	     						<th width="20%">Share</th>
	     					</thead>
	     					<tbody id="caseContactsDisplay">
	     						<tr><td>Select Case</td></tr>
	     					</tbody>
	     				</table>
	     				<table class="table">
	     					<thead>
	     						<th width="80%">Staff</th>
	     						<th width="20%">Share</th>
	     					</thead>
	     					<tbody id="caseStaffsDisplay">
	     						<tr><td>Select Case</td></tr>
	     					</tbody>
	     				</table>
	     			</div>
	     		</div>
	     	</div>
	     	<!-- insert staff and contacts ids -->
	     	<input type="hidden" name="staffIds" id="staffIds" value=''>
	     	<input type="hidden" name="contactIds" id="contactIds" value=''>
	     	<!-- for inserting the page id -->
            <input type="hidden" name="data_id" id="event_data_id" value="">
            <!-- for insert query -->
            <input type="hidden" name="mode" id="eventMode" value="insert">
	     	<div class="modal-footer">
		        <button type="button" class="btn btn-danger" data-dismiss="modal">Close <i class="fa fa-times"></i></button>
		        <button type="submit" class="btn btn-primary" id="eventBtn">Save Event <i class="fa fa-floppy-o"></i></button>
		     </div>
		  </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script src="js/pageScript/events.js"></script>
<script src="js/pageScript/eventsFullcalendar.js"></script>