<?php
	ob_start(); 
	include_once('../includes/header.php');
	// refer back to dashboard if page is not in group
	$retrievedFileName = basename($_SERVER['PHP_SELF']);
	if ((!in_array($retrievedFileName, $_SESSION['pagesAllowed'])) || ($_SESSION['account_name'] != 'staff')) {
	  header('Location: ../pages/dashboard.php');
	}
?>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
         <h2><i class="fa fa-calendar"></i> Events</h2>
         <!-- add new button -->
        <div class="pull-right">
            <button data-toggle="modal" data-target="#eventModal" class="btn btn-danger"><span class="glyphicon glyphicon-plus"></span> Add Event </button>
        </div>
        <!-- end new button -->
        <div class="clearfix"></div>
      </div>
      <div class="x_content">

      	<ul class="nav nav-tabs">
		  	<li class="active"><a data-toggle="tab" href="#eventCalendar">Calender <i class="fa fa-calendar"></i></a></li>
            <li><a data-toggle="tab" href="#eventDetails">Event Details <i class="fa fa-list"></i></a></li>
		</ul><br>

		<div class="tab-content">
		  <div id="eventCalendar" class="tab-pane fade in active">
		    <div class="row">
				<div class="col-md-2">
					<!-- for listing of events colorization -->
					<div class="table-responsive">
			          <table class="table table-hover"> 
			          	<thead>
			              	<tr>
			              		<th>Event Type</th>
			              		<th>Color</th>
			              	</tr>
			          	</thead>
			            <tbody>
			              <?php
			             	$objEventType = new EventType;
			                $events = $objEventType->get_event_types(); 
			                foreach ($events as $event) {
			                  echo '
			                  <tr>
			                    <td><b>'.$event["event_type_name"].'</b></td>
			                    <td><div class="smallBox" style="background:'.$event["event_type_color"].';"></div></td>
			                  </tr>';
			                }
			               ?>
			            </tbody>
			          </table>
		      		</div>
				</div>
				<div class="col-md-10" style="border-left:1px solid black">
					<!-- calendar display -->
					<div id='calendarDisplay' ></div>
				</div>
			</div>
		  </div>
		  <div id="eventDetails" class="tab-pane fade">
		    <div class="table-responsive">
            	<table class="table table-hover tableList"> 
                    <thead>
                        <tr>
                            <th>Event Title</th>
                            <th>Event Start</th>
                            <th>Event End</th>
                            <th>Added</th>
                      <th></th>
                        </tr>
                    </thead>
                   	<tbody id="eventResultsDisplay">
                    <?php
                      $objEvents = new Events;
                      $details = $objEvents->get_events_list(); 
                      if (!empty($details)) {
                      	print_r($details);
                      }
                     ?>
                    </tbody>
            	</table>
         	</div>
		  </div>
		</div>

	   </div>
	</div>
  </div>
</div>

<?php include('../includes/footer.php'); ?>
<?php include('events_include.php'); ?>