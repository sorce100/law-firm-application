<?php 
  ob_start();
  include_once('../includes/header.php'); 
?>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Reports Generation</h2>
        <!-- end new button -->
        <div class="clearfix"></div>

      </div>
      <div class="x_content">
        <ul class="nav nav-pills">
          <li class="active"><a data-toggle="tab" href="#reports">Reports</a></li>
          <li><a data-toggle="tab" href="#savedReports">Saved Reports</a></li>
        </ul><hr>
        <div class="tab-content">
          
          <!-- /////////////////////////////////reports///////////////////////////////// -->
          <div id="reports" class="tab-pane fade in active">
            <!--  -->
              <div class="row">
                <!-- left part -->
                <div class="col-md-2 col-sm-12 col-xs-12 borderRight">
                  <!-- financial Report -->
                  <h3><u>Finanacial Reports</u></h3>
                  <ul class="nav nav-tabs tabs-left">
                    <li class="active"><a href="#caseRevenueReport" data-toggle="tab">Case Revenue</a></li>
                    <li><a href="#payedInvoicesReport" data-toggle="tab">Payed Invoices</a></li>
                    <li><a href="#unpayedInvoicesReport" data-toggle="tab">Unpayed Invoices</a></li>
                  </ul>
                  <!-- Case Report -->
                  <h3><u>Case Reports</u></h3>
                  <ul class="nav nav-tabs tabs-left">
                    <li><a href="#caseListReport" data-toggle="tab">Case List Report</a></li>
                  </ul>

                  <br>
                  <!-- Productivity Report -->
                  <h3><u>Productivity Reports</u></h3>
                  <ul class="nav nav-tabs tabs-left">
                    <li><a href="#userTimeExpensesReport" data-toggle="tab">User Time & Expenses</a></li>
                    <li><a href="#firmTimeExpensesReport" data-toggle="tab">Firm Time & Expenses</a></li>
                    <li><a href="#caseTimeExpensesReport" data-toggle="tab">Case Time & Expenses</a></li>
                  </ul>
                </div>


                <!-- Right part -->
                <div class="col-md-10 col-sm-12 col-xs-12">
                  
                  <div class="tab-content">
                    <!-- financial Report -->
                    <div class="tab-pane active" id="caseRevenueReport">
                      <p class="lead">Case Revenue Report</p>
                      
                    </div>
                    <div class="tab-pane" id="payedInvoicesReport">
                      <p class="lead">Payed Invoices</p>
                    </div>
                    <div class="tab-pane" id="unpayedInvoicesReport">
                      <p class="lead">Unpayed Invoices</p>
                    </div>
                  
                  <!-- case Report -->
                    <div class="tab-pane" id="caseListReport">
                      <p class="lead">Case List Report</p>
                      
                    </div>

                  <!-- prouctivity Report -->
                    <div class="tab-pane" id="userTimeExpensesReport">
                      <p class="lead">User Time And Expenses</p>
                      
                    </div>
                    <div class="tab-pane" id="firmTimeExpensesReport">
                      <p class="lead">Firm Time And Expenses</p>
                    </div>
                    <div class="tab-pane" id="caseTimeExpensesReport">
                      <p class="lead">Case Time And Expenses</p>
                   </div>

                  <!-- tab content end -->
                  </div>
                </div>
                <!-- end of right part -->
              </div>
            <!-- end of row -->
          </div>
          <!-- end of reports tabs -->
          <!-- ///////////////////////////Saved reports/////////////////////////// -->
          <div id="savedReports" class="tab-pane fade">

          </div>
          <!-- end of saved reports tabs -->
        </div>
        <!-- end of main tab content -->
      </div>
      <!-- end of xcontent -->
    </div>
  </div>
</div>


<?php require_once('../includes/footer.php'); ?>

