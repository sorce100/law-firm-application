<?php 
  ob_start();
  include_once('../includes/header.php'); 
  include_once('../Classes/EventType.php'); 
  // retrive page file name
  $retrievedFileName = basename($_SERVER['PHP_SELF']);
  if (!in_array($retrievedFileName, $_SESSION['pagesAllowed'])) {
      header('Location: dashboard.php');
  }
?>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Events Type</h2>
        <!-- add new button -->
        <div class="pull-right">
            <button data-toggle="modal" data-target="#eventTypeModal" class="btn btn-danger"><span class="fa fa-plus"></span> Add New</button>
        </div>
        <!-- end new button -->
        <div class="clearfix"></div>

      </div>
      <div class="x_content">
          <div class="table-responsive">
              <table class="table table-hover tableList"> 
                <thead>
                  <tr>
                    <th width="25%">Event Type</th>
                    <th width="30%">Event Color</th>
                    <th width="25%">Added</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody id="resultsDisplay">
                  <?php
                    $objEventType = new EventType;
                    $events = $objEventType->get_event_types(); 
                    foreach ($events as $event) {
                      echo '
                          <tr>
                            <td>'.$event["event_type_name"].'</td>
                            <td><div class="smallBox" style="background:'.$event["event_type_color"].';"></div></td>
                            <td>'.$event["added"].'</td>
                            <td>
                              <button class="btn-primary update_data" id="'.$event["event_type_id"].'"><i class="fa fa-pencil"></i></button> <button class="btn-danger del_data" id="'.$event["event_type_id"].'"><i class="fa fa-trash"></i></button>
                            </td>
                          </tr>';
                    }
                   ?>
                </tbody>
              </table>
          </div>
      </div>
    </div>
  </div>
</div>

<!-- for modal -->
<div class="modal fade" id="eventTypeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" id="bg">
         <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true" style="color: red;font-size: 25px;" class="btn-default">&times; </span></button>
        <h4 class="modal-title" id="subject">Add Event Type</h4>
      </div>
      <div class="modal-body" id="bg">
          <form id="eventType_form">
            <div class="row">
                <div class="col-md-12">
                    <!-- 1 -->
                    <div class="row">
                        <div class="col-md-3">
                            <label for="title" class="col-form-label">Event Type <span class="asterick">*</span></label>
                        </div>
                        <div class="col-md-9">
                            <div class="form-group">
                               <input type="text" name="eventType" id="eventType" class="form-control" placeholder="Eg.Client Meeting" autocomplete="off" required>
                            </div>
                        </div>
                    </div>

                    <!-- page file name -->
                    <div class="row">
                        <div class="col-md-3">
                            <label for="title" class="col-form-label">Select Color <span class="asterick">*</span></label>
                        </div>

                        <div class="col-md-9">
                          <div class="input-group demo2">
                            <input type="text" value="#e01ab5" name="eventColorPicker" id="eventColorPicker" class="form-control" readonly required />
                            <span class="input-group-addon"><i></i></span>
                          </div>
                        </div>
                    </div>
                    <!-- for inserting the page id -->
                    <input type="hidden" name="data_id" id="eventType_data_id" value="">
                    <!-- for insert query -->
                    <input type="hidden" name="mode" id="eventTypeMode" value="insert">

                   <div class="modal-footer">
                      <button type="button" class="btn btn-danger" data-dismiss="modal">Close <i class="fa fa-times"></i></button>
                      <button type="submit" class="btn btn-primary" id="eventTypeSave_btn">Add New <i class="fa fa-save"></i></button>
                   </div>
                </div>
            </div>
          </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<?php require_once('../includes/footer.php'); ?>
<!-- scripts -->
<script src="js/pageScript/event_type.js"></script>

