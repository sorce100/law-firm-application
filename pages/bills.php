<?php
  ob_start(); 
  require_once('../includes/header.php');
 
 if ($_SESSION['account_name'] != 'staff') {
    die();
  }  
  $objBillTimeEntry = new BillTimeEntry;
  $objBillExpenses = new BillExpenses; 
  $objBillInvoice = new BillInvoice;
?>


<div class="col-md-12 col-sm-12 col-xs-12">
  <div class="x_panel">
    <div class="x_title">
      <h2><i class="fa fa-credit-card"></i> Billing & Invoice</h2>
      
      <div class="clearfix"></div>
    </div>
    <div class="x_content">


      <div class="" role="tabpanel" data-example-id="togglable-tabs">
        <ul id="myTab" class="nav nav-tabs" role="tablist">
          <li role="presentation" class="active"><a href="#mainDashboard" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Dashboard <i class="fa fa-home"></i></a></li>
          <li role="presentation"><a href="#timeEnteries" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Time Entry <i class="fa fa-clock-o"></i></a></li>
          <li role="presentation" class=""><a href="#expenses" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Expenses <i class="fa fa-money"></i></a></li>
          <li role="presentation" class=""><a href="#invocies" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Invocies <i class="fa fa-file-text"></i></a></li>
        </ul><br>
        <div id="myTabContent" class="tab-content">
          <!-- ////////////////////////////////////////////////////MAIN DASHBOARD///////////////////////////////// -->
          <div role="tabpanel" class="tab-pane fade active in" id="mainDashboard" aria-labelledby="home-tab">
            <div class="row">
              <!-- first row -->
              <div class="col-md-6 col-sm-6 col-xs-12">
                <!-- part 1 -->
                <div class="x_panel">
                    <div class="x_title">
                      <h3>Billing Actions</h3>
                    </div>
                    <div class="x_content">
                      <a class="btn btn-app-big" data-toggle="modal" data-target="#invoiceModal">
                        <i class="fa fa-file-text-o"></i> Add Invoice
                      </a>
                      <a class="btn btn-app-big" data-toggle="modal" data-target="#timeEnteriesModal">
                        <i class="fa fa-clock-o"></i> Add Time Entry
                      </a>
                      <a class="btn btn-app-big" data-toggle="modal" data-target="#expensesModal">
                        <i class="fa fa-credit-card"></i> Add Expenses
                      </a>
                      <a class="btn btn-app-big" data-toggle="modal" data-target="#recordPaymentModal">
                        <i class="fa fa-archive"></i> Record Payment
                      </a>
                    </div>
                  </div>
                  <br>
                  <br>
                  <!-- part 2 -->
                <div class="x_panel">
                    <div class="x_title">
                      <h3>Recent Activities</h3>
                    </div>
                    <div class="x_content">
                       <div class="table-responsive">
                          <table class="table table-hover tableList"> 
                            <thead>
                              <tr>
                                <th>Activity</th>
                                <th>Date</th>
                                <th>Account</th>
                              </tr>
                            </thead>
                            <tbody id="resultsDisplay">
                              <?php
                                $obActivity = new Activity;
                                $details = $obActivity->get_table_activity("bill_time_entry"); 
                                foreach ($details as $detail) {
                                  echo '
                                      <tr>
                                        <td>'.$detail["activity_name"].'</td>
                                        <td>'.$detail["date_done"].'</td>
                                        <td>'.$detail["fullName"].'</td>
                                      </tr>';
                                }
                               ?>
                            </tbody>
                          </table>
                      </div>
                    </div>
                  </div>
              </div>
              <!-- second row -->
              <div class="col-md-6 col-sm-6 col-xs-12">
                <!-- part 1 -->
                <div class="x_panel">
                    <div class="x_title">
                      <h3>Time Entry Sheet</h3>
                    </div>
                    <div class="x_content">
                       <div class="table-responsive">
                          <table class="table table-hover"> 
                            <thead>
                              <tr>
                                <th></th>
                                <th>Billable</th>
                                <th>Non Billable</th>
                                <th>Total </th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td>Today</td>
                                <td>0.0</td>
                                <td>0.0</td>
                                <td>0.0</td>
                              </tr>
                              <tr>
                                <td>This Week</td>
                                <td>0.0</td>
                                <td>0.0</td>
                                <td>0.0</td>
                              </tr>
                              <tr>
                                <td>This Month</td>
                                <td>0.0</td>
                                <td>0.0</td>
                                <td><?php $objBillTimeEntry->get_time_period_total_month(); ?></td>
                              </tr>
                              <tr>
                                <td>This Year</td>
                                <td>0.0</td>
                                <td>0.0</td>
                                <td>0.0</td>
                              </tr>
                            </tbody>
                          </table>
                      </div>
                    </div>
                  </div>
                  <br>
                  <br>
                  <!-- 2 -->
                  <div class="x_panel">
                    <div class="x_title">
                      <h3>Expenses Sheet</h3>
                    </div>
                    <div class="x_content">
                       <div class="table-responsive">
                          <table class="table table-hover"> 
                            <thead>
                              <tr>
                                <th></th>
                                <th>Billable</th>
                                <th>Non Billable</th>
                                <th>Total </th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td>Today</td>
                                <td>0.0</td>
                                <td>0.0</td>
                                <td>0.0</td>
                              </tr>
                              <tr>
                                <td>This Week</td>
                                <td>0.0</td>
                                <td>0.0</td>
                                <td>0.0</td>
                              </tr>
                              <tr>
                                <td>This Month</td>
                                <td>0.0</td>
                                <td>0.0</td>
                                <td><?php $objBillTimeEntry->get_time_period_total_month(); ?></td>
                              </tr>
                              <tr>
                                <td>This Year</td>
                                <td>0.0</td>
                                <td>0.0</td>
                                <td>0.0</td>
                              </tr>
                            </tbody>
                          </table>
                      </div>
                    </div>
                  </div>
                  <!--  -->
                  <div class="x_panel">
                    <div class="x_title">
                      <h3>Invoice Summary</h3>
                    </div>
                    <div class="x_content">
                       <div class="row">
                          <!-- total -->
                          <div class="col-md-3 invoicePayStatus bg-green">TOTAL : </div>
                          <!-- sent -->
                          <div class="col-md-2 invoicePayStatus bg-orange">SENT :</div>
                          <!-- unsent -->
                          <div class="col-md-2 invoicePayStatus bg-purple">UNSENT :</div>
                          <!-- payed -->
                          <div class="col-md-2 invoicePayStatus bg-el">PAYED :</div>
                          <!-- pending -->
                          <div class="col-md-3 invoicePayStatus bg-red">PENDING :</div>
                        </div>
                    </div>
                  </div>
                </div>
                  <!--  -->
              </div>
            </div>

          <!-- ////////////////////////////////////////////////////END DASHBOARD///////////////////////////////// -->
          <!-- 1 -->
          <div role="tabpanel" class="tab-pane fade" id="timeEnteries" aria-labelledby="home-tab">

            <!-- /////////////////////////////////////TIME ENTRY//////////////////////////////////////////////////// -->
            <ul class="nav nav-pills">
              <li class="active"><a data-toggle="pill" href="#timeEntryOpen">Open</a></li>
              <li><a data-toggle="pill" href="#timeEntryInvoiced">Invoiced</a></li>
              <li><a data-toggle="pill" href="#timeEntryDetails">All Time Entries</a></li>
              <li class="pull-right">
                <button data-toggle="modal" data-target="#timeEnteriesModal" class="btn btn-danger"><span class="glyphicon glyphicon-plus"></span> Add Time Entry</button>
              </li>
            </ul><hr>

            <div class="tab-content">
              <!--  -->
              <div id="timeEntryOpen" class="tab-pane fade in active">
                
              </div>
              <!--  -->
              <div id="timeEntryInvoiced" class="tab-pane fade">
                
              </div>
              <!--  -->
              <div id="timeEntryDetails" class="tab-pane fade">
                <div class="col-md-12 table-responsive">
                  <table class="table table-hover tableList"> 
                      <thead>
                          <tr>
                              <th>Case</th>
                              <th>Date</th>
                              <th>Activity</th>
                              <th>Duration</th>
                              <th>Rate(₵)</th>
                              <th>Total(₵)</th>
                              <th>Invoiced</th>
                              <th>User</th>
                              <th></th>
                          </tr>
                      </thead>
                      <tbody id="timeEntriesDisplay">
                          <?php
                              $enteries = $objBillTimeEntry->get_time_entries(); 
                              foreach ($enteries as $entery) {
                                  echo '
                                      <tr>
                                        <td>'.$entery["caseName"].'</td>
                                        <td>'.$entery["time_entry_date"].'</td>
                                        <td>'.$entery["time_entry_activity"].'</td>
                                        <td>'.$entery["time_entry_duration"].'</td>
                                        <td>'.$entery["time_entry_rate"].'</td>
                                        <td>'.$entery["time_entry_total"].'</td>
                                        <td>'.$entery["time_entry_invoiced"].'</td>
                                        <td>'.$entery["time_entry_staff_id"].'</td>
                                        <td>
                                          <button class="btn-default timeEntryview" id="'.$entery["time_entry_id"].'"><i class="fa fa-eye"></i></button>
                                          <button class="btn-primary timeEntryupdate_data" id="'.$entery["time_entry_id"].'"><i class="fa fa-pencil"></i></button>
                                          <button class="btn-danger del_data" id="'.$entery["time_entry_id"].'"><i class="fa fa-trash"></i></button>
                                        </td>
                                      </tr>
                                    ';
                              }
                         ?>
                      </tbody>
                  </table>
                </div>
              </div>
            </div>
            <!-- /////////////////////////////////////////EXPENSES//////////////////////////////////////////////// -->
          </div>
          <!-- 2 -->
          <div role="tabpanel" class="tab-pane fade" id="expenses" aria-labelledby="profile-tab">
            <!-- ///////////////////////////////////////////////////////////////////////////////////////// -->
            <ul class="nav nav-pills">
              <li class="active"><a data-toggle="pill" href="#expensesOpen">Open</a></li>
              <li><a data-toggle="pill" href="#expensesInvoiced">Invoiced</a></li>
              <li><a data-toggle="pill" href="#expensesDetails">All Expenses Details</a></li>
              <li class="pull-right">
                <button data-toggle="modal" data-target="#expensesModal" class="btn btn-danger"><span class="glyphicon glyphicon-plus"></span> Add Expenses</button>
              </li>
            </ul><hr>

            <div class="tab-content">
              <!--  -->
              <div id="expensesOpen" class="tab-pane fade in active">
                
              </div>
              <!--  -->
              <div id="expensesInvoiced" class="tab-pane fade">
                
              </div>
              <!--  -->
              <div id="expensesDetails" class="tab-pane fade">
                <div class="col-md-12 table-responsive">
                  <table class="table table-hover tableList"> 
                      <thead>
                          <tr>
                              <th>Case</th>
                              <th>Date</th>
                              <th>Activity</th>
                              <th>Quantity</th>
                              <th>Cost(₵)</th>
                              <th>Total(₵)</th>
                              <th>Invoiced</th>
                              <th>User</th>
                              <th></th>
                          </tr>
                      </thead>
                      <tbody id="expensesDisplay">
                         <?php
                              $expenses = $objBillExpenses->get_expenses(); 
                              foreach ($expenses as $expense) {
                                echo '
                                    <tr>
                                      <td>'.$expense["caseName"].'</td>
                                      <td>'.$expense["bill_expenses_date"].'</td>
                                      <td>'.$expense["bill_expenses_activity"].'</td>
                                      <td>'.$expense["bill_expenses_quantity"].'</td>
                                      <td>'.$expense["bill_expenses_cost"].'</td>
                                      <td>'.$expense["bill_expenses_total"].'</td>
                                      <td>'.$expense["bill_expenses_invoiced"].'</td>
                                      <td>'.$expense["bill_expenses_staff_id"].'</td>
                                      <td>
                                        <button class="btn-default expenses_view" id="'.$expense["bill_expenses_id"].'"><i class="fa fa-eye"></i></button>
                                        <button class="btn-primary expenses_update_data" id="'.$expense["bill_expenses_id"].'"><i class="fa fa-pencil"></i></button> 
                                        <button class="btn-danger expenses_del_data" id="'.$expense["bill_expenses_id"].'"><i class="fa fa-trash"></i></button>
                                      </td>
                                    </tr>
                                  ';
                            }
                       ?>
                      </tbody>
                  </table>
                </div>
              </div>
            </div>
            <!-- ///////////////////////////////////////////////////////////////////////////////////////// -->
          </div>
          <!-- 3 -->
          <div role="tabpanel" class="tab-pane fade" id="invocies" aria-labelledby="profile-tab">
            <!-- /////////////////////////////////INVOICE//////////////////////////////////////////////////////// -->
            <ul class="nav nav-pills">
              <li class="active"><a data-toggle="pill" href="#invoiceDashboard">Invoices Dashboard</a></li>
              <li><a data-toggle="pill" href="#invoiceDetails">Invoices Details</a></li>
              <li class="pull-right">
                <button data-toggle="modal" data-target="#invoiceModal" class="btn btn-danger"><span class="glyphicon glyphicon-plus"></span> Add Invoice</button>
              </li>
            </ul><hr>

            <div class="tab-content">
              <div id="invoiceDashboard" class="tab-pane fade in active">
                <!-- invoice bar -->
                <div class="row">
                  <!-- total -->
                  <div class="col-md-3 invoicePayStatus bg-green">TOTAL ( GHC ) : </div>
                  <!-- sent -->
                  <div class="col-md-2 invoicePayStatus bg-orange">SENT ( GHC ) :</div>
                  <!-- unsent -->
                  <div class="col-md-2 invoicePayStatus bg-purple">UNSENT ( GHC ) :</div>
                  <!-- payed -->
                  <div class="col-md-2 invoicePayStatus bg-el">PAYED ( GHC ) :</div>
                  <!-- pending -->
                  <div class="col-md-3 invoicePayStatus bg-red">PENDING ( GHC ) :</div>
                </div>
                
              </div>
              <div id="invoiceDetails" class="tab-pane fade">
                <div class="col-md-12 table-responsive">
                  <table class="table table-hover tableList"> 
                      <thead>
                          <tr>
                              <th>Invoice Number</th>
                              <th>Case</th>
                              <th>Invoice Date</th>
                              <th>Invoice Contact</th>
                              <th>Invoice Amount(₵)</th>
                              <th>Employee</th>
                              <th>Added</th>
                              <th></th>
                              <th></th>
                          </tr>
                      </thead> 
                      <tbody id="invoiceDisplay">
                        <?php
                          $invoices = $objBillInvoice->get_invoices(); 
                          foreach ($invoices as $invoice) {
                            echo '
                                <tr>
                                  <td>'.$invoice["bill_invoice_number"].'</td>
                                  <td>'.$invoice["bill_invoice_case_id"].'</td>
                                  <td>'.$invoice["bill_invoice_date"].'</td>
                                  <td>'.$invoice["bill_invoice_contact_id"].'</td>
                                  <td>'.$invoice["bill_invoice_main_total"].'</td>
                                  <td>'.$invoice["bill_invoice_staff"].'</td>
                                  <td>'.$invoice["added"].'</td>
                                  <td>
                                    <a href="../pdfGenerators/invoice.php" class="btn btn-default generatePdf" id="'.$invoice["bill_invoice_id"].'"><i class="fa fa-print"></i> </a>
                                  </td>
                                  <td>
                                    <button class="btn-primary invoice_update_data" id="'.$invoice["bill_invoice_id"].'"><i class="fa fa-pencil"></i></button> 
                                    <button class="btn-danger expenses_del_data" id="'.$invoice["bill_invoice_id"].'"><i class="fa fa-trash"></i></button>
                                  </td>
                                </tr>';
                          }
                       ?>
                      </tbody>
                  </table>
                </div>
              </div>
            </div>
            <!-- ///////////////////////////////////////////////////////////////////////////////////////// -->
          </div>
          <!--  -->
        </div>
      </div>

    </div>
  </div>
</div>


<!-- buttons -->

<input type="hidden" name="accountId" id="accountId" value="<?php echo $_SESSION['account_id']?>">

<?php include('../includes/footer.php'); ?>

<?php include_once('billTimeEntryInclude.php'); ?>
<?php include_once('billExpensesInclude.php'); ?>
<?php include_once('billInvoiceInclude.php'); ?>
<?php include_once('billRecordPaymentInclude.php'); ?>





