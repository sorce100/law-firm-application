<!-- Modal  -->
<div class="modal fade" id="addTaskModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header" id="bg">
         <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true" style="color: red;font-size: 25px;" class="btn-default">&times; </span></button>
        <h4 class="modal-title taskTitle">Add Task</h4>
      </div>
      <div class="modal-body" id="bg">
	      <form id="task_form" method="POST">
	     	<div class="row">
	     		<div class="col-md-8" style="border-right:1px solid #bec4be; ">
	     			<!-- 1 -->
		     		<div class="row">
		     			<div class="col-md-2">
		     				<label for="title" class="col-form-label">Case <span class="asterick">*</span></label>
		     			</div>
		     			<div class="col-md-10">
		     				<div class="form-group">
		                       <select class="form-control taskSelect2" style="width:100%;" name="taskCourtCase" id="taskCourtCase" required> 
			                   	  <option value="" selected="selected" disabled>Select Case</option>
			                   	  <?php
	                                    $cases = $objCourtCase->get_cases(); 
	                                    if (!empty($cases)) {
	                                        print_r($cases);
	                                    }
	                                ?>
			                   </select>
		                    </div>
		     			</div>
		     		</div>
		     	<!-- 2 -->
		     		<div class="row">
		     			<div class="col-md-2">
		     				<label for="title" class="col-form-label">Task Name <span class="asterick">*</span></label>
		     			</div>
		     			<div class="col-md-10">
		     				<div class="form-group">
		                        <input type="text" name="taskName" id="taskName" class="form-control" autocomplete="off">
		                    </div>
		     			</div>
		     		</div>
		     	<!-- 3 -->
		     		<div class="row">
		     			<div class="col-md-2">
		     				<label for="title" class="col-form-label">Due Date <span class="asterick">*</span></label>
		     			</div>
		     			<div class="col-md-4">
		     				<div class="form-group input-group">
		                        <input type="text" class="form-control" id="taskDueDate" name="taskDueDate" data-toggle="datepicker" readonly>
				                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
		                    </div>
		     			</div>
		     		</div>
		     	<!-- 4 -->
		     		<div class="row">
		     			<div class="col-md-2">
		     				<label for="title" class="col-form-label">Checklist</label>
		     			</div>
		     			<div class="col-md-10 checklistDiv">
		     				<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-tasks"></i></span>
								<input class="form-control" type="text" id="taskCheckList" name="taskCheckList[]" autocomplete="off">
								<span class="input-group-addon "><i class="fa fa-trash"></i></span>
							</div>
		     			</div>
		     			<div class="col-md-2"></div>
		     			<div class="col-md-10">
		     				<br>
							<span name="addChecklist" class="btn-link" id="addChecklist"><b>Add checklist item</b></span>
		     			</div>
		     		</div><br>
		     	<!-- 5 -->
		     		<div class="row">
		     			<div class="col-md-2">
		     				<label for="title" class="col-form-label">Priority</label>
		     			</div>
		     			<div class="col-md-4">
		     				<div class="form-group">
		                        <select class="form-control" id="taskPriority" name="taskPriority">
		                        	<option value="No Priority">No Priority</option>
		                        	<option value="Low">Low</option>
		                        	<option value="Medium">Medium</option>
		                        	<option value="High">High</option>
		                        </select>
		                    </div>
		     			</div>
		     		</div>
		     	<!-- 6 -->
		     		<div class="row">
			     			<div class="col-md-2">
			     				<label for="title" class="col-form-label">Description</label>
			     			</div>
			     			<div class="col-md-10">
			     				<div class="form-group ">
				                   <textarea class="form-control" rows="6" name="taskDescription" id="taskDescription" placeholder="Enter description of task &hellip;"></textarea>
				                </div>
			     			</div>
			     		</div>
	     		</div>
	     		<div class="col-md-4" >
	     			<div class="table-responsive">
	     				<table class="table">
	     					<thead>
	     						<th>Staff</th>
	     						<th>Assign</th>
	     					</thead>
	     					<tbody>
	     						<?php
			                        $objStaff = new Staff;
			                        $staffs = $objStaff->get_staff(); 
			                        foreach ($staffs as $staff) {
		                                echo '
		                                    <tr>
		                                      <td>'.$staff["staff_firstname"].' '.$staff["staff_lastname"].'</td>
		                                      <td><input type="checkbox" value="'.$staff["staff_id"].'" class="taskAssignStaff" name="taskAssignStaff[]" required></td>
		                                    </tr>
		                                  ';
			                            }
			                       ?>
	     					</tbody>
	     				</table>
	     			</div>
	     		</div>
	     	</div>
	     	 <!-- for inserting the page id -->
            <input type="hidden" name="data_id" id="task_data_id" value="">
            <!-- for insert query -->
            <input type="hidden" name="mode" id="taskMode" value="insert">

	     	<div class="modal-footer">
		        <button type="button" class="btn btn-danger" data-dismiss="modal">Close <i class="fa fa-times"></i></button>
		        <button type="submit" class="btn btn-primary" id="taskBtn">Save Task <i class="fa fa-floppy-o"></i></button>
		     </div>
		  </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script src="js/pageScript/tasks.js"></script>