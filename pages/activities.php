<?php
ob_start();
include_once('../assets/includes/header.php');

 // check if user is a staff for not
 if ($_SESSION['account_name'] != 'staff') {
  	die();
  } 

 include_once('../Classes/Activity.php');
 $obActivity = new Activity;
 

?>
<div class="row">
	<div class="panel">
		<!-- TODO LIST -->
		<div>
			<div class="panel-heading">
				<h3 class="panel-title">User Activities</h3>
			</div>
			<div class="panel-body">
				  <ul class="nav nav-tabs nav-justified" style="background-color: #f4f4f4;border-bottom: 1px solid #99a1aa;">
				    <li class="active"><a data-toggle="tab" href="#user"><b>user (You) <i class="fa fa-user"></i></b></a></li>
				    <li><a data-toggle="tab" href="#all"><b>General <i class="fa fa-users"></i></b></a></li>
				  </ul>

				  <div class="tab-content">
				    <div id="user" class="tab-pane fade in active">
				      <h3>User Activity</h3>
				     		<div class="table-responsive">
			                	<table class="table table-hover tableList"> 
				                    <thead>
				                        <tr>
				                            <th>Activity</th>
				                            <th>User</th>
				                            <th>Account Type</th>
				                            <th>Date  Time</th>
				                        </tr>
				                    </thead>
				                   	<tbody id="userActivity">
				                        <?php
				                            $activities = $obActivity->get_user_activity(); 
				                            foreach ($activities as $activity) {
				                                    echo '
				                                        <tr>
				                                          <td>'.$activity["activity_name"].'</td>
				                                          <td>'.$activity["user_id"].'</td>
				                                          <td>'.$activity["account_name"].'</td>
				                                          <td>'.$activity["date_done"].'</td>
				                                        </tr>
				                                      ';
				                                }
				                           ?>
				                    </tbody>
			                	</table>
			              </div>
				    </div>
				    <div id="all" class="tab-pane fade">
				      <h3>All Activities</h3>
				      <div class="table-responsive">
			                	<table class="table table-hover tableList"> 
				                    <thead>
				                        <tr>
				                            <th>Activity</th>
				                            <th>User</th>
				                            <th>Account Type</th>
				                            <th>Date / Time</th>
				                        </tr>
				                    </thead>
				                   	<tbody id="userActivity">
				                        <?php
				                            $activities = $obActivity->get_user_activity_all(); 
				                            foreach ($activities as $activity) {
				                                    echo '
				                                        <tr>
				                                          <td>'.$activity["activity_name"].'</td>
				                                          <td>'.$activity["user_id"].'</td>
				                                          <td>'.$activity["account_name"].'</td>
				                                          <td>'.$activity["date_done"].'</td>
				                                        </tr>
				                                      ';
				                                }
				                           ?>
				                    </tbody>
			                	</table>
			              </div>
				    </div>
				  </div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php include('../assets/includes/footer.php'); ?>
