</div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            © <?php echo date('Y');?> All Rights Reserved. <a href="#" target="_blank">EL AGBEMAVA LAW OFFICE</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.js"></script>
    <!-- add summernote js -->
    <script src="../vendors/summernote/summernote.min.js"></script>
    <!-- bootstrap checkbox toggle -->
    <script src="../vendors/bootstrap-toggle/bootstrap2-toggle.min.js"></script>
    <!-- include datatable -->
    <script src="../vendors/datables.net/js/dataTables.min.js"></script>
    <!-- incude select2 -->
    <script src="../vendors/select2/select2.min.js"></script>
    <!-- add parsley -->
    <script src="../vendors/parsley/parsley.min.js"></script>
    <!-- toastr -->
    <script src="../vendors/toastr/toastr.min.js"></script>
    <!-- date /time picker -->
    <script src='../vendors/datetimepicker/datetimepicker.min.js'></script>
    <!-- for jquery smart wizard -->
    <script src="../vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js"></script>
    <!-- icheck -->
    <script src='../vendors/icheck/icheck.min.js'></script>
    <!-- color picker -->
    <script src='../vendors/colorpicker/dist/js/bootstrap-colorpicker.min.js'></script>

    <!-- fullcalendar -->
    <script src="../vendors/fullcalendar/lib/moment.min.js"></script>
    <script src="../vendors/fullcalendar/fullcalendar.min.js"></script>
    
    <!-- dashboard footer -->
    <!-- jQuery Sparklines -->
    <!-- <script src="../vendors/jquery-sparkline/dist/jquery.sparkline.min.js"></script> -->
    <!-- bootstrap-daterangepicker -->
    <script src="../vendors/moment/min/moment.min.js"></script>
    <script src="../vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
  </body>
</html>

<script>
    $(document).ready(function(){


        $('.tableList').dataTable({ ordering: false});
        $('.dataTables_filter input[type="search"]').prop('placeholder','....').css({'width':'350px','height':'40px','display':'inline-block'});
        // date
        // for date time picker
        $(".timerCountSelect2").select2({
          dropdownParent: $("#timerModal")
        });
        
        // for date time picker
        $(function() {
          $('[data-toggle="datepicker"]').datetimepicker({
              format: 'dd-mm-yyyy',
              autoclose: true,
              minView: 2
          });
        });

         // time picker
        $(function() {
          $('[data-toggle="calendardatepicker"]').datetimepicker({format: 'yyyy-mm-dd hh:ii ',autoclose: true});
        });

        // clear warning when added
        $('.form-control').on('focus', function() {
          $(this).css({"border-color":"#808080"});
        });

       $('#select_all,#select_all_staff').change(function() {
            var checkboxes = $(this).closest('form').find(':checkbox');
            checkboxes.prop('checked', $(this).is(':checked'));
        });
  ////////////////////////////////////////////////////////////////////////////////////////////////
      

      var timer = new Timer();

      var timeTotal = $('.timerCount'),
      timeKey = 'time_stored_seconds',
      timeStored = localStorage.getItem(timeKey);

      timer.addEventListener('secondsUpdated', function (e) {
        var newValue = parseInt(localStorage.getItem(timeKey) | 0)+1
          localStorage.setItem(timeKey, newValue);
          $(timeTotal).html(timer.getTimeValues().toString());
      });

      timer.addEventListener('started', function (e) {
          $(timeTotal).html(timer.getTimeValues().toString());
      });
      timer.addEventListener('reset', function (e) {
          $(timeTotal).html(timer.getTimeValues().toString());
      });
      $('.startButton').click(function () {timer.start();});
      $('.pauseButton').click(function () {timer.pause();});
      $('.stopButton').click(function () {timer.stop();localStorage.clear();});
      // $('#caseTimer .resetButton').click(function () {timer.reset();});

      /* When page loads
      ********************************/
      $(document).ready(function() {
        if (timeStored) {

            timeTotal.text(timeStored);
        }else{
            localStorage.setItem(timeKey, 0);
            timeStored = 0
        }

        timer.start({ precision: 'seconds', startValues: {seconds: parseInt(timeStored)}});
        // localStorage.clear();

      }); 

    // <!-- changing password script -->
 
      $('#change_password_form').parsley();
      // for reset modal when close
      $('#changePassModal').on('hidden.bs.modal', function () {
          $('#change_password_form').parsley().reset();
          $("#change_password_form")[0].reset();

          $('#changePass_btn').text("Change Password").prop('disabled',false);  
      });

      // changing password
      $("#change_password_form").on("submit",function(e){
          e.preventDefault();
          $.ajax({
          url:"../controllers/usersController.php",
          method:"POST",
          data:$("#change_password_form").serialize(),
          beforeSend:function(){  
              $('#changePass_btn').text("Please wait ...").prop('disabled',true);  
           },
          success:function(data){  
            // console.log(data);
            $("#changePassModal").modal("hide");
             $("#change_password_form")[0].reset();
             if (data == "success") {
              toastr.success('Password Changed Successfully');
              // $.ajax({
              //     url:"Script/log_out.php",
              // });
             window.location.href = "../controllers/logOut.php";
             }
             else if(data == "error"){
              toastr.error('Sorry, There was a problem changing your password!');
             }
          } 

          });  
      });
  });

// auto refresh page after inactivity
var time = new Date().getTime();
$(document.body).bind("mousemove keypress", function(e) {
   time = new Date().getTime();
});

function refresh() {
   if(new Date().getTime() - time >= 60000) 
       window.location.reload(true);
   else 
       setTimeout(refresh, 1200000);
}

setTimeout(refresh, 1200000);

</script>
<!-- //////////////////////////////////////////////////////////////// -->
<script src="../pages/js/pageScript/bills_time_entry.js"></script>