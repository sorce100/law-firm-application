<?php 
  include_once("Check.php");
  $objCheck = new Check;

  require_once("../Classes/Users.php");

  switch ($_SESSION['account_name']) {
    case 'staff':
      $retrievedFileName = basename($_SERVER['PHP_SELF']);
      
      require_once("../Classes/Staff.php");
      $objStaff = new Staff;
      require_once('../Classes/Contact.php');
      require_once('../Classes/CourtCase.php');
      $objCourtCase = new CourtCase; 
      require_once('../Classes/Events.php');
      include_once('../Classes/EventType.php'); 
      require_once('../Classes/Document.php');
      require_once('../Classes/Tasks.php');
      include_once('../Classes/PracticeArea.php'); 
      include_once('../Classes/Leads.php');
      require_once('../Classes/ContactGroups.php');
      require_once('../Classes/ContactCompanys.php');
      require_once('../Classes/BillTimeEntry.php');
      require_once('../Classes/BillExpenses.php');
      require_once('../Classes/BillInvoice.php');
      require_once('../Classes/Activity.php');
      include_once('../Classes/CourtAdd.php');
      include_once('../Classes/Sms.php');

    break;
    case 'contact':
      require_once('../Classes/Contact.php');
      require_once('../Classes/CourtCase.php'); 
      require_once("../Classes/Staff.php");



    break;
    
    default:
      # code...
      break;
  }
  

  
  $usersObject = new Users;
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>E.L. AGBEMAVA LAW OFFICE</title>
    <link rel="icon" type="image/jpg" sizes="96x96" href="../pages/images/favicon.png">
    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- add summer note -->
    <link href="../vendors/summernote/summernote.css" rel="stylesheet">
    <!-- add select2  -->
    <link href="../vendors/select2/select2.min.css" rel="stylesheet">
    <!-- add parsley --> 
    <link href="../vendors/parsley/parsley.css" rel="stylesheet">
    <!-- add bootstrap checkbox toggle --> 
    <link href="../vendors/bootstrap-toggle/bootstrap-toggle.min.css" rel="stylesheet">
     <!-- toastr -->
    <link href="../vendors/toastr/toastr.min.css" rel="stylesheet">
    <!-- date/time picker -->
    <link rel="stylesheet" href="../vendors/datetimepicker/datetimepicker.min.css">
    <!-- full calendar -->
    <link href="../vendors/fullcalendar/fullcalendar.min.css" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="../build/css/custom.css" rel="stylesheet">
    <!-- color picker -->
    <link href="../vendors/colorpicker/dist/css/bootstrap-colorpicker.min.css" rel="stylesheet">
    <!-- ckeditor -->
    <script src="../vendors/ckeditor/ckeditor.js"></script>
    <!-- for easy timer -->
    <script src='../vendors/easytimer/easytimer.min.js'></script>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <!-- <a href="index.html" class="site_title"><i class="fa fa-globe"></i> <span>LISAG</span></a> -->
              <a href="dashboard.php"><img src="../pages/images/original.jpg" class="img img-responsive header-logo" width="100%" height="30%"></a>
            </div>
            <!-- <div class="clearfix"></div> -->
            <br><br>
            <!-- <hr/> -->

            <!-- /menu profile quick info -->
            
            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <!-- <h3>General</h3> -->
                <hr>
                <ul class="nav side-menu">
                  <!-- menu links -->
                  <li><a href="#" data-toggle="modal" data-target="#gettingStartedModal"><i class="fa fa-play"></i> Getting Started </a></li>
                  <!-- workin on pages -->
                  
                  <!-- ////////////////////////////////////////////////////////////////////////////////////////////////// -->
                    <?php 
                      switch ($_SESSION['account_name']) {
                        case 'staff':
                          echo '<li class="menuLink"><a href="../pages/dashboard.php"><i class="fa fa-home"></i> <span>Dashboard</span></a></li>';
                          // <!-- get Pages per the group given -->
                          // getting the group id from session then getting the pages id from the database
                            require_once("../Classes/PagesGroup.php");
                            $objPagesGroup = new PagesGroup;
                            $pagesId = $objPagesGroup->menu_pages_id($_SESSION['group_id']);
                            if (!empty($pagesId)) {
                                // passing the pages id to  get the pages url
                                require_once("../Classes/Pages.php");
                              $objPages = new Pages;
                              foreach ($pagesId as $page_id) {
                                 $objPages->get_menu_pages($page_id);
                                }
                            }
                          break;
                        case 'contact':
                          // list pages for clients
                          echo '
                            <li><a href="../pages/client_home.php"><i class="fa fa-home"></i> <span>Dashboard</span></a></li>
                            <li><a href="../pages/client_messages.php"><i class="fa fa-envelope-o"></i> <span>Messages</span></a></li>
                            <li><a href="../pages/client_documents.php"><i class="fa fa-book"></i> <span>Documents</span></a></li>
                            <li><a href="../pages/client_events.php"><i class="fa fa-calendar"></i> <span>Event</span></a></li>
                            <li><a href="../pages/client_billing.php"><i class="fa fa-credit-card"></i> <span>Billing</span></a></li>
                            <li><a href="../pages/client_service_request.php"><i class="fa fa-bullhorn"></i> <span>New Service Request</span></a></li>

                          ';
                          break;
                        default:
                          session_destroy();
                          header("Location: ../../index.php");
                          break;
                      }
                    ?>
                  <!-- ////////////////////////////////////////////////////////////////////////////////////////////////// -->

                  
                  <!-- end menu links -->
                </ul>
              </div>
            </div>
            <!-- /sidebar menu -->
            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Profile">
                <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Days Calculator">
                <span class="glyphicon glyphicon-calendar" aria-hidden="true"></span>
              </a>
              <a >
                <span class="glyphicon glyphicon-option-vertical" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="../controllers/logOut.php">
                <span class="asterick glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->

          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"  data-toggle="tooltip" data-placement="down" title="Show/Hide Menu"><i class="fa fa-bars"></i></a>
              </div>
              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <!-- <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> -->
                    <?php 
                      switch ($_SESSION['account_name']) {
                        case 'staff':
                            $objStaff = new Staff;
                            $staffDetails = explode("|",$objStaff->get_staffName($_SESSION["account_id"]));
                            $accountName =  strtoupper($staffDetails[0]);
                            if ($staffDetails[1] == "NONE") {
                              $accountImg = '<img src="../pages/images/user.png" class="img-circle" alt="Avatar">';
                            }
                            else {
                              $accountImg = '<img src="../uploads/staffpic/'.$staffDetails[1].'" width="24px" height="24px" class="img-circle" alt="Avatar">';
                            }
                          break;
                        case 'contact':
                          $contactobj= new Contact;
                          $accountName =strtoupper($contactobj->get_contactName($_SESSION["account_id"]));
                          $accountImg = '<img src="../pages/images/user.png" class="img-circle" alt="Avatar">';
                          break;
                        default:
                          # code...
                          break;
                      }
                     ?>

                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <?php echo $accountImg;?>
                    <span class=" fa fa-angle-down"></span>
                  </a>

                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="javascript:;">User Profile</a></li>
                    <li><a data-toggle="modal" data-target="#changePassModal">Change Password <i class="fa fa-cog pull-right"></i></a></li>
                    <li><a href="../controllers/logOut.php"><i class="fa fa-sign-out pull-right asterick"></i> Log Out</a></li>
                  </ul>
                </li>

                <!-- start timer button -->
                <li>
                  <a class="info-number">
                    <!-- <i class="fa fa-envelope-o"></i> -->
                    <?php 
                      switch ($_SESSION['account_name']) {
                        case 'staff':
                          if (in_array("bills.php", $_SESSION['pagesAllowed'])) {
                            echo '<button class="btn bg-el" data-toggle="modal" data-target="#timerModal">Start Timer <i class="fa fa-clock-o"></i></button>';
                          }
                          else{
                            // echo $accountName;
                          }
                          break;
                        case 'contact':
                            echo $accountName;
                          break;
                        default:
                          # code...
                          break;
                      }
                     ?>
                    
                  </a>
                </li>
                <!-- end  -->
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
        <!-- ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->

          
    <!-- modal for changing password -->
     <div class="modal fade" id="changePassModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog ">
            <div class="modal-content">
              <div class="modal-header" id="bg">
                 <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true" class="btn-default btnClose">&times;</span></button>
                <h4 class="modal-title"><b id="subject">Change Password</b></h4>
              </div>
              <div class="modal-body" id="bg">
                <form id="change_password_form" method="POST"> 
                    <!--  -->
                  <div class="row">
                    <div class="col-md-4"><label>New Password <span class="asterick"> *</span></label></div>
                    <div class="col-md-8">
                      <input type="password" class="form-control" id="newPasswd" name="newPasswd" minlength="4" autocomplete="off" placeholder="Enter New Password &hellip;" required>
                    </div>
                  </div>

                  <br>
                  <!--  -->
                  <div class="row">
                    <div class="col-md-4"><label>Retype Password <span class="asterick"> *</span></label></div>
                    <div class="col-md-8">
                      <input type="password" class="form-control" id="retypeNewPasswd" name="retypeNewPasswd" minlength="4" autocomplete="off" placeholder="Retype Password &hellip;" required>
                    </div>
                  </div>

                    <br>
                    <input type="hidden" name="mode" value="userChangePassword">

                    <div class=" modal-footer" id="bg">
                      <button type="button" class="btn btn-danger" data-dismiss="modal">Close <i class="fa fa-times"></i></button>
                      <button type="submit" class="btn btn-primary" id="changePass_btn">Change Password <i class="fa fa-exchange"></i></button>
                    </div>        
                </form>
              </div>
            </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

<!-- ////////////////////////////////////////////////////////// Getting started modal ////////////////////////////////////////////////////////////////////// -->
        
        <div id="gettingStartedModal" class="modal fade" role="dialog">
          <div class="modal-dialog modal-xl">
            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header gettingStarted" style="">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center">WELCOME TO EL AGBEMAVA LAW OFFICE , <strong><?php echo $accountName; ?></strong></h4>
              </div>
              <div class="modal-body">
                <p>Some text in the modal.</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close <i class="fa fa-times"></i></button>
              </div>
            </div>

          </div>
        </div>

<!-- timer modal -->
      <!-- for modal for timer-->
    <div class="modal fade" id="timerModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-md">
        <div class="modal-content">
          <div class="modal-body">
            <div id="caseTimer">
                  <!-- 1 -->
                <!-- <div class="col-md-12"> -->
                  <div class="table-responsive" >
                    <table class="table ">
                      <tr>
                        <td><button class="startButton btn btn-success"><i class="fa fa-play"></i></button></td>
                        <td><div class="timerCount" style="font-size: 25px;" >00:00:00</div></td>
                        <td><button class="pauseButton btn btn-primary" ><i class="fa fa-pause"></i></button></td>
                        <td><button class="stopButton btn  btn-danger"><i class="fa fa-stop"></i></button></td>
                      </tr>
                      <tr>
                        <td><b>Case <span class="asterick"> *</span></b></td>
                        <td colspan="4">
                          <select class="form-control timerCountSelect2" style="width: 100%;" id="timerCase" name="timerCase">
                            <option value="" selected="selected">Select Case</option>
                            <?php
                                $cases = $objCourtCase->get_cases(); 
                                if (!empty($cases)) {
                                    print_r($cases);
                                }
                            ?>
                          </select>
                        </td>
                      </tr>
                      <tr>
                        <td><b>Note</b></td>
                        <td colspan="4"><textarea name="timerNote" id="timerNote" rows="5" class="form-control" placeholder="Description"></textarea></td>
                      </tr>
                    </table>
                  </div>
                </div>
               <div class="modal-footer">
                  <button type="button" class="btn btn-danger" data-dismiss="modal">Close <i class="fa fa-times"></i></button>
                  <button type="button" class="btn btn-primary" id="timerAdd_btn">Save <i class="fa fa-floppy-o"></i></button>
               </div>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->