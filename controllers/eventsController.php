<?php
	require_once("../Classes/Events.php"); 
	session_start();
	class eventsController{
		function __construct(){
			// print_r($_POST);
					switch (trim($_POST["mode"])) {
						// for insert
						case 'insert':

							if (!empty($_POST["eventCourtCaseId"]) || !empty($_POST["eventName"]) || !empty($_POST["eventStartDate"]) || !empty($_POST["eventEndDate"]) || !empty($_POST["eventType"])) {
								try {
									$objEvents = new Events;
									$objEvents->set_courtCaseId($objEvents->CleanData($_POST["eventCourtCaseId"]));
									$objEvents->set_eventName($objEvents->CleanData($_POST["eventName"]));
									$objEvents->set_eventStartDate($objEvents->CleanData($_POST["eventStartDate"]));
									$objEvents->set_eventEndDate($objEvents->CleanData($_POST["eventEndDate"]));
									$objEvents->set_eventLocation($objEvents->CleanData($_POST["eventLocation"]));
									$objEvents->set_eventDescription($objEvents->CleanData($_POST["eventDescription"]));
									$objEvents->set_eventType($objEvents->CleanData($_POST["eventType"]));
									$objEvents->set_eventContactId($_POST["contactIds"]);
									$objEvents->set_eventContactShare(json_encode($_POST["eventContactShare"]));
									$objEvents->set_eventStaffid($_POST["staffIds"]);
									$objEvents->set_eventStaffShare(json_encode($_POST["eventStaffShare"]));
									if ($objEvents->insert()) {
										echo "success";
									}
									else{
										echo "error";
									}
								} catch (PDOException $e) {echo $e;}
									
							}
							else{
								echo "error";
							}
							
						break;
					// for update
						case 'update':
							if (!empty($_POST["eventCourtCaseId"]) || !empty($_POST["eventName"]) || !empty($_POST["eventStartDate"]) || !empty($_POST["eventEndDate"]) || !empty($_POST["eventType"])) {
								try {
									$objEvents = new Events;
									$objEvents->set_courtCaseId($objEvents->CleanData($_POST["eventCourtCaseId"]));
									$objEvents->set_eventName($objEvents->CleanData($_POST["eventName"]));
									$objEvents->set_eventStartDate($objEvents->CleanData($_POST["eventStartDate"]));
									$objEvents->set_eventEndDate($objEvents->CleanData($_POST["eventEndDate"]));
									$objEvents->set_eventLocation($objEvents->CleanData($_POST["eventLocation"]));
									$objEvents->set_eventDescription($objEvents->CleanData($_POST["eventDescription"]));
									$objEvents->set_eventType($objEvents->CleanData($_POST["eventType"]));
									$objEvents->set_eventContactId($_POST["contactIds"]);
									$objEvents->set_eventContactShare(json_encode($_POST["eventContactShare"]));
									$objEvents->set_eventStaffid($_POST["staffIds"]);
									$objEvents->set_eventStaffShare(json_encode($_POST["eventStaffShare"]));
									$objEvents->set_id($objEvents->CleanData($_POST["data_id"]));
									if ($objEvents->update()) {
										echo "success";
									}
									else{
										echo "error";
									}
								} catch (PDOException $e) {echo $e;}
							}
							else{
								echo "error";
							}
						break;
					// for delete
						case 'delete':
							if(isset($_POST["data_id"])){
								try{
								  $objEvents = new Events;
								  $objCourtCase->set_recordHide("YES");
							      $objEvents->set_id($objEvents->CleanData($_POST["data_id"]));
							      if ($objEvents->delete()) {
							      	echo "success";
							      }
							      else{
							      	echo "error";
							      }
							    } catch (PDOException $e) {echo $e;}
									
							}else{echo "error";}
						break;
						// geting details of a member with id
						case 'updateModal':
							if(!empty($_POST["data_id"])){
									try{
									  $objEvents = new Events;  
								      $objEvents->set_id($objEvents->CleanData($_POST["data_id"]));
								      $details = $objEvents->get_events_by_id();
								      print_r($details);
								    } catch (PDOException $e) {echo $e;}
										
								 }else{

								 	echo "error";
								 }
						break;
						// event drop updated
						case 'event_drop_update':
							if(!empty($_POST["data_id"])){
								try{
								  $objEvents = new Events;
								  $objEvents->set_eventStartDate($objEvents->CleanData($_POST["eventStartDate"]));
								  $objEvents->set_eventEndDate($objEvents->CleanData($_POST["eventEndDate"]));
								  $objEvents->set_id($objEvents->CleanData($_POST["data_id"]));
							      if ($objEvents->eventDrop()) {
							      	echo "success";
							      }
							      else{
							      	echo "error";
							      }
							    } catch (PDOException $e) {echo $e;}
									
							 }else{echo "error";}
						break;
						// get all
						case 'getAll':
							$objEvents = new Events;
							print_r(get_events_list());

						break;
						
						default:
							die();
							break;
					}

				}
			}

	$objeventsController = new eventsController;
 ?>