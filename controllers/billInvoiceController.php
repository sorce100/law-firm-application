<?php
	require_once("../Classes/BillInvoice.php"); 
	session_start();
	class billInvoiceController{
		function __construct(){
			// print_r($_POST);
					switch (trim($_POST["mode"])) {
						// for insert
						case 'insert':
							if (!empty($_POST["invoiceDate"]) || !empty($_POST["invoiceCourtCaseId"]) || !empty($_POST["invoiceContact"]) || !empty($_POST["timeEntryTotal"]) || !empty($_POST["expensesTotal"]) || !empty($_POST["mainTotal"])) {
								try{
									$objBillInvoice = new BillInvoice;
									$objBillInvoice->set_invoiceNum($objBillInvoice->CleanData($_POST["invoiceNum"]));
									$objBillInvoice->set_invoiceDate($objBillInvoice->CleanData($_POST["invoiceDate"]));
									$objBillInvoice->set_invoiceCourtCaseId($objBillInvoice->CleanData($_POST["invoiceCourtCaseId"]));
									$objBillInvoice->set_invoiceContact($objBillInvoice->CleanData($_POST["invoiceContact"]));
									$objBillInvoice->set_invoiceAddress($objBillInvoice->CleanData($_POST["invoiceAddress"]));
									// for adding billable
									if (empty($_POST["invoiceBillable"])) {
										$billAble = "off";
									}
									elseif (!empty($_POST["invoiceBillable"])) {
										$billAble = $_POST["invoiceBillable"];
									}
									$objBillInvoice->set_invoiceBillable($objBillInvoice->CleanData($billAble));
									$objBillInvoice->set_timeEnteryInvoiceAdd(json_encode($_POST["timeEnteryInvoiceAdd"]));
									$objBillInvoice->set_timeEntryTotal($objBillInvoice->CleanData($_POST["timeEntryTotal"]));
									$objBillInvoice->set_expensesInvoiceAdd(json_encode($_POST["expensesInvoiceAdd"]));
									$objBillInvoice->set_expensesTotal($objBillInvoice->CleanData($_POST["expensesTotal"]));
									$objBillInvoice->set_mainTotal($objBillInvoice->CleanData($_POST["mainTotal"]));
									if ($objBillInvoice->insert()) {
										echo "success";
									}
									else{
										echo "error";
									}
								} catch (Exception $e) {
									echo "error";
								}
							}
							else{
								echo "error";
							}
							
						break;
					// for update
						case 'update':
							if (!empty($_POST["pageName"]) || !empty($_POST["pageName"]) || !empty($_POST["pageName"])) {
								try{
									$objBillInvoice = new Pages;
									$objBillInvoice->set_pageName($objBillInvoice->CleanData($_POST["pageName"]));
									$objBillInvoice->set_pageUrl(trim($_POST["pageUrl"]));
									$objBillInvoice->set_id($objBillInvoice->CleanData($_POST["data_id"]));
									if ($objBillInvoice->update()) {
										echo "success";
									}
									else{
										echo "error";
									}
								} catch (Exception $e) {
									echo "error";
								}
							}
							else{
								echo "error";
							}
							
						break;
					// for delete
						case 'delete':
							if(isset($_POST["data_id"])){
								try{
								  $objBillInvoice = new Pages;
								  $objBillInvoice->set_recordHide("YES");
							      $objBillInvoice->set_id($objBillInvoice->CleanData($_POST["data_id"]));
							      if ($objBillInvoice->delete()) {
							      	echo "success";
							      }
							      else{
							      	echo "error";
							      }
								} catch (Exception $e) {
									echo "error";
								}     
							}else{die();}
						break;
						// geting details of a member with id
						case 'updateModal':
							if(!empty($_POST["data_id"])){
								try{
								  $objBillInvoice = new Pages;  
							      $objBillInvoice->set_id($objBillInvoice->CleanData($_POST["data_id"]));
							      $pages_details = $objBillInvoice->get_page_by_id();
							      print_r($pages_details);
							    } catch (Exception $e) {
									echo "error";
								}  
							 }else{

							 	echo "error";
							 }
						break;
						// get all
						case 'getAll':
							$objBillInvoice = new Pages;
							print_r(json_encode($objBillInvoice->get_pages(),true));

						break;
						default:
							die();
							break;
					}

				}
			}

	$objbillInvoiceController = new billInvoiceController;
 ?>