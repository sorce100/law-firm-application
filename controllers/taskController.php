<?php
	require_once("../Classes/Tasks.php"); 
	session_start();
	class tasksController{
		function __construct(){
			// print_r($_POST);
					switch (trim($_POST["mode"])) {
						// for insert
						case 'insert':
							if (!empty($_POST["taskCourtCase"]) || !empty($_POST["taskName"]) || !empty($_POST["taskDueDate"])) {
								try{
									$objTasks = new Tasks;
									$objTasks->set_taskCourtCase($objTasks->CleanData($_POST["taskCourtCase"]));
									$objTasks->set_taskName($objTasks->CleanData($_POST["taskName"]));
									$objTasks->set_taskDueDate($objTasks->CleanData($_POST["taskDueDate"]));
									$objTasks->set_taskCheckList(json_encode($_POST["taskCheckList"]));
									$objTasks->set_taskPriority($objTasks->CleanData($_POST["taskPriority"]));
									$objTasks->set_taskDescription($objTasks->CleanData($_POST["taskDescription"]));
									$objTasks->set_taskAssignStaff(json_encode($_POST["taskAssignStaff"]));
									$objTasks->set_taskStatus("INCOMPLETE");
									if ($objTasks->insert()) {
										echo "success";
									}
									else{
										echo "error";
									}
								} catch (PDOException $e) {echo $e;}
							}
							else{
								echo "error";
							}
							
						break;
					// for update
						case 'update':
							if (!empty($_POST["taskCourtCase"]) || !empty($_POST["taskName"]) || !empty($_POST["taskDueDate"])) {
								try{
									$objTasks = new Tasks;
									$objTasks->set_taskCourtCase($objTasks->CleanData($_POST["taskCourtCase"]));
									$objTasks->set_taskName($objTasks->CleanData($_POST["taskName"]));
									$objTasks->set_taskDueDate($objTasks->CleanData($_POST["taskDueDate"]));
									$objTasks->set_taskCheckList(json_encode($_POST["taskCheckList"]));
									$objTasks->set_taskPriority($objTasks->CleanData($_POST["taskPriority"]));
									$objTasks->set_taskDescription($objTasks->CleanData($_POST["taskDescription"]));
									$objTasks->set_taskAssignStaff(json_encode($_POST["taskAssignStaff"]));
									$objTasks->set_id($objTasks->CleanData($_POST["data_id"]));
									if ($objTasks->update()) {
										echo "success";
									}
									else{
										echo "error";
									}
								} catch (PDOException $e) {echo $e;}
							}
							else{
								echo "error";
							}
						break;
					// for delete
						case 'delete':
							if(!empty($_POST["data_id"])){
								try{
								  $objTasks = new Tasks;
								  $objTasks->set_recordHide("YES");
							      $objTasks->set_id($objTasks->CleanData($_POST["data_id"]));
							      if ($objTasks->delete()) {
							      	echo "success";
							      }
							      else{
							      	echo "error";
							      }
							    } catch (PDOException $e) {echo $e;}
							 }else{echo "error";}
						break;
						// geting details of a member with id
						case 'updateModal':
							if(!empty($_POST["data_id"])){
								try{
								  $objTasks = new Tasks;  
							      $objTasks->set_id($objTasks->CleanData($_POST["data_id"]));
							      $details = $objTasks->get_tasks_by_id();
							      print_r($details);  
							    } catch (PDOException $e) {echo $e;}
							 }else{
							 	echo "error";
							 }
						break;
						// mark task completed or not
						case 'taskComplete':
							if(!empty($_POST["data_id"])){
								try{
								  $objTasks = new Tasks;
								  $objTasks->set_taskStatus("COMPLETE");
							      $objTasks->set_id($objTasks->CleanData($_POST["data_id"]));
							      if ($objTasks->task_completed()) {
							      	echo "success";
							      }
							      else{
							      	echo "error";
							      }
							    } catch (PDOException $e) {echo $e;}
							     
							 }else{echo "error";}
						break;
						// get all
						case 'getAll':
							$objTasks = new Tasks;
							$details =$objTasks->get_tasks_list();
							if (!empty($details)) {
								print_r($details);
							}

						break;
						case 'get_case_tasks':
							if(!empty($_POST["caseId"])){
								try{
								  $objTasks = new Tasks;  
							      $objTasks->set_taskCourtCase($objTasks->CleanData($_POST["caseId"]));
							      $details = $objTasks->get_case_tasks();
							      print_r($details);
							    } catch (PDOException $e) {echo $e;}  
							 }else{

							 	echo "error";
							 }
						break;
						// task filter
						case 'filterTasks';
						// print_r($_POST);
						// exit();
							try{
							  $objTasks = new Tasks;  
						      $objTasks->set_taskCourtCase($objTasks->CleanData($_POST["taskFilterCaseId"]));
						      $objTasks->set_taskPriority($objTasks->CleanData($_POST["taskFilterPirority"]));
						      $objTasks->set_taskAssignStaff($objTasks->CleanData($_POST["taskFilterAssignedTo"]));
						      $details = $objTasks->task_filter();
						      if (!empty($details)) {
						      	print_r($details);
						      }
						    } catch (PDOException $e) {echo $e;}  
						break;
						default:
							die();
							break;
					}

				}
			}

	$objtasksController = new tasksController;
 ?>