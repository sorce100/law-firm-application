<?php
	require_once("../Classes/Users.php");
	require_once("../Classes/PagesGroup.php");
	require_once("../Classes/SessionLogs.php");
	session_start();
	class usersController{
		private $userpassword;
		private $resetpassword;
		private $accStatus;
		function __construct(){
			switch (trim($_POST["mode"])) {
						// for login
						case 'login':
							if ((!empty($_POST["userName"])) || (!empty($_POST["userPassword"]))) {
								try{
									$objUsers = new Users();
									$objUsers->set_userName(strtolower($objUsers->CleanData($_POST["userName"])));
									$objUsers->set_accStatus($objUsers->CleanData("on"));
									$users = $objUsers->login();

									foreach ($users as $user) {
										$this->userpassword = $objUsers->CleanData($user["user_password"]);
										$this->resetpassword = $objUsers->CleanData($user["password_reset"]);
									}
									if (password_verify (strtolower($objUsers->CleanData($_POST["userPassword"])) ,  $this->userpassword)) {
										// check if password change is required
										if ($this->resetpassword == "on") {
											// if password reset, return username and password entered
											echo ($_POST["userName"]."-".$_POST["userPassword"]);
											
										}elseif ($this->resetpassword == "off") {
											$_SESSION['user_id'] = $objUsers->CleanData($user['users_id']); // Initializing Session
											$_SESSION['user_name'] = $objUsers->CleanData($user['user_name']);
											$_SESSION['account_id'] = $objUsers->CleanData($user['account_id']);
											$_SESSION['account_name'] = $objUsers->CleanData($user['account_name']);
											$_SESSION['group_id'] = $objUsers->CleanData($user['group_id']);

											// if login successfull then update user online
											$objUsers->session_status_update('ONLINE');
											// save in session log table
											$objSessionLogs = new SessionLogs();
											$objSessionLogs->session_log_start();
											// check if session is staff or client and return
											switch ($_SESSION['account_name']) {
												case 'staff':
													echo "successStaff";
												break;
												case 'contact':
													echo "successContact";
												break;
												default:
													echo "error";
												break;
											}
											
										}

									}
									else{echo "error";}

								}catch(PDOException $e){echo $e;}
							}
							else{echo "error";}
						break;
						// for insert
						case 'insert':
							
							if (!empty($_POST["accountSelect"]) || !empty($_POST["userName"]) || !empty($_POST["accGroup"])) {
								try{
									// check if password reset or account status has being set
									if (!isset($_POST["accPasswdReset"])) {
										$this->resetpassword = "off";
									}
									if (!isset($_POST["accStatus"])) {
										$this->accStatus = "off";
									}
									elseif (isset($_POST["accStatus"])) {
										$this->accStatus = $_POST["accStatus"];
									}
									$objUsers = new Users();
									$this->userpassword = strtolower($objUsers->CleanData($_POST["userPassword"]));

									$objUsers->set_accountSelect($objUsers->CleanData($_POST["accountSelect"]));
									$objUsers->set_accountName("staff");
									$objUsers->set_userName($objUsers->CleanData($_POST["userName"]));
									$objUsers->set_userPassword(password_hash($this->userpassword, PASSWORD_DEFAULT));
									$objUsers->set_accPasswdReset($objUsers->CleanData($this->resetpassword));
									$objUsers->set_accGroup($objUsers->CleanData($_POST["accGroup"]));
									$objUsers->set_accStatus($objUsers->CleanData($this->accStatus));
									$Id = $objUsers->insert();
									if ($Id) {
										echo "success";
									}
									else{
										echo "error";
									}
								}catch(PDOException $e){echo $e;}
							}
							else{
								echo "error";
							}

								

						break;
						// for update
						case 'update':
							if (!empty($_POST["accountSelect"]) || !empty($_POST["userName"]) || !empty($_POST["accGroup"])) {
								try{
									$objUsers = new Users();
									$objUsers->set_id($objUsers->CleanData($_POST["data_id"]));
									// check if password reset or account status has being set
									if (!isset($_POST["accPasswdReset"])) {
										$this->resetpassword = "off";
									}
									elseif (isset($_POST["accPasswdReset"])) {
										$this->resetpassword = $_POST["accPasswdReset"];
									}
									if (!isset($_POST["accStatus"])) {
										$this->accStatus = "off";
									}
									elseif (isset($_POST["accStatus"])) {
										$this->accStatus = $_POST["accStatus"];
									}

									if (!empty($_POST["userPassword"])) {
										$this->userpassword = strtolower($objUsers->CleanData($_POST["userPassword"]));
										$objUsers->set_userPassword($objUsers->CleanData(password_hash($this->userpassword, PASSWORD_DEFAULT)));
									}
									elseif (empty($_POST["userPassword"])) {
										$objUsers->set_userPassword($objUsers->get_password());
									}
									
									$objUsers->set_userName($objUsers->CleanData($_POST["userName"]));
									$objUsers->set_accPasswdReset($objUsers->CleanData($this->resetpassword));
									$objUsers->set_accGroup($objUsers->CleanData($_POST["accGroup"]));
									$objUsers->set_accStatus($objUsers->CleanData($this->accStatus));

									if ($objUsers->update()) {
										echo "success";
									}
									else{
										echo "error";
									}

								}catch(PDOException $e){echo $e;}
							}
							else{
								echo "error";
							}		
								
						break;
						// for delete
						case 'delete':
							if(isset($_POST["data_id"])){
								try{
								  $objUsers = new Users();    
							      $objUsers->set_id($objUsers->CleanData($_POST["data_id"]));
							      $objUsers->set_recordHide("YES");
							      $users_details = $objUsers->delete();
							      echo "success";
							    }catch(PDOException $e){echo $e;}
							 }else{die();}
						break;
						// for update modal
						case 'updateModal':
							if(isset($_POST["data_id"])){
								try{
								  $objUsers = new Users();    
							      $objUsers->set_id($objUsers->CleanData($_POST["data_id"]));
							      $userdetails = $objUsers->get_user_by_id();
							      echo $userdetails;
							    }catch(PDOException $e){echo $e;}  
							 }else{die();}
						break;

						case 'changePass':
							$passwdReset="off";
							$chPassUserName = $_POST["chPassUserName"];
							$chPassUserPassword = $_POST["chPassUserPassword"];
							$newPassword = $_POST["newPassword"];
							$newRetypePassword = $_POST["newRetypePassword"];

							if((!empty($chPassUserName)) || (!empty($chPassUserPassword)) || (!empty($newPassword)) || (!empty($newRetypePassword))){
								try{
									// check if passwords typed is the same
									if (($newPassword === $newRetypePassword) && ($chPassUserPassword != $newPassword)) {
										// check for string length, password should not be less than four characters
										if (strlen($newPassword) >= 4) {
											$objUsers = new Users();
											$this->userpassword = strtolower($objUsers->CleanData($_POST["newRetypePassword"]));
											$objUsers->set_userPassword(password_hash($this->userpassword, PASSWORD_DEFAULT));
											$objUsers->set_accPasswdReset($passwdReset);
											$objUsers->set_userName($objUsers->CleanData($_POST["chPassUserName"]));
											if ($objUsers->change_password()) {
												echo "success";
											}
											else{
												echo "error";
											}
										}
										else{
											// if password is less than 4 characters
											echo "error";
										}
									}
									else{
										// if passwords not the same and the old password is same as new password
										echo "error";
									}
								}catch(PDOException $e){echo $e;}
							 }else{
							 	// if its empty
							 	echo "error";
							 }

						break;
						// get all
						case 'getAll':
							$objUsers = new Users;
							print_r(json_encode($objUsers->get_users_staff(),true));

						break;
						default:
							echo "There was a problem";
							break;
					
				}
			}
		}
	$objusersController = new usersController();
 ?>