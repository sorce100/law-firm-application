<?php
	require_once("../Classes/EventType.php"); 
	session_start();
	class EventTypeController{
		function __construct(){
			// print_r($_POST);
			switch (trim($_POST["mode"])) {
				// for insert
				case 'insert':
					if ((!empty($_POST["eventType"])) || (!empty($_POST["eventColorPicker"]))) {
						try{
							$objEventType = new EventType;
							$objEventType->set_eventType($objEventType->CleanData($_POST["eventType"]));
							$objEventType->set_eventColorPicker($objEventType->CleanData($_POST["eventColorPicker"]));

							if ($objEventType->insert()) {
									echo "success";
							}
							else{
								echo "error";
							}
						}catch(PDOException $e){echo $e;}
					}
					else{
						echo "error";
					}
					
				break;
			// for update
				case 'update':
					if ((!empty($_POST["eventType"])) || (!empty($_POST["eventColorPicker"])) || (!empty($_POST["data_id"]))) {
						try{
							$objEventType = new EventType;
							$objEventType->set_eventType($objEventType->CleanData($_POST["eventType"]));
							$objEventType->set_eventColorPicker($objEventType->CleanData($_POST["eventColorPicker"]));
							$objEventType->set_id($objEventType->CleanData($_POST["data_id"]));

							if ($objEventType->update()) {
									echo "success";
							}
							else{
								echo "error";
							}
						}catch(PDOException $e){echo $e;}
					}
					else{
						echo "error";
					}
				break;
			// for delete
				case 'delete':
					if(isset($_POST["data_id"])){
							 $objEventType = new EventType;
							  $objEventType->set_recordHide("YES");
						      $objEventType->set_id($objEventType->CleanData($_POST["data_id"]));
						      if ($objEventType->delete()) {
						      	echo "success";
						      }
						      else{
						      	echo "error";
						      }
						     
						 }else{die();}
				break;
				// geting details of a member with id
				case 'updateModal':
					if(!empty($_POST["data_id"])){
							 $objEventType = new EventType;  
						      $objEventType->set_id($objEventType->CleanData($_POST["data_id"]));
						      $details = $objEventType->get_event_type_by_id();
						      print_r($details);  
						 }else{

						 	echo "error";
						 }
				break;
				// get all
				case 'getAll':
					$objEventType = new EventType;
					print_r(json_encode($objEventType->get_event_types(),true));

				break;
				default:
					die();
					break;
					}

				}
			}

	$objEventType = new EventTypeController;
 ?>