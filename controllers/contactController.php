<?php
	require_once("../Classes/Contact.php"); 
	require_once("../Classes/Users.php");
	require_once("../Classes/Sms.php");

	session_start();
	class contactController{
		function __construct(){
			// print_r($_POST);
					switch (trim($_POST["contactMode"])) {
						// for insert
						case 'insert':
							if ((!empty($_POST["contactFirstName"])) || (!empty($_POST["contactLastName"])) || (!empty($_POST["contactCell"]))) {
								try{
									$objContact = new Contact;
									$objContact->set_contactFirstName($objContact->CleanData($_POST["contactFirstName"]));
									$objContact->set_contactMidName($objContact->CleanData($_POST["contactMidName"]));
									$objContact->set_contactLastName($objContact->CleanData($_POST["contactLastName"]));
									$objContact->set_contactEmail($objContact->CleanData($_POST["contactEmail"]));
									$objContact->set_contactCompany($objContact->CleanData($_POST["contactCompany"]));
									$objContact->set_contactGroup($objContact->CleanData($_POST["contactGroup"]));
									if (empty($_POST["contactPortal"])) {
										$objContact->set_contactPortal("off");
										$objContact->set_contactCell($objContact->CleanData($_POST["contactCell"]));
										$objContact->set_contactWrkPhone($objContact->CleanData($_POST["contactWrkPhone"]));
										$objContact->set_contactAddress($objContact->CleanData($_POST["contactAddress"]));
										$objContact->set_contactCity($objContact->CleanData($_POST["contactCity"]));
										$objContact->set_contactRegion($objContact->CleanData($_POST["contactRegion"]));
										$objContact->set_contactCountry($objContact->CleanData($_POST["contactCountry"]));
										if ($objContact->insert()) {
											echo "success";
										}
										else{
											echo "error";
										}
									}
								} catch (Exception $e) { echo $e;}
									
							}
							else{
								try{
									// if switched on then stup account
									$username = strtolower(substr(trim($_POST["contactFirstName"]), 0,1).".".trim($_POST["contactLastName"]));
									$defaultPassword = strtolower(substr(str_shuffle('abcdefghijklmnopqrstuvwxyz0123456789'),0,6));

									$objContact->set_contactPortal($objContact->CleanData($_POST["contactPortal"]));
									$objContact->set_contactCell($objContact->CleanData($_POST["contactCell"]));
									$objContact->set_contactWrkPhone($objContact->CleanData($_POST["contactWrkPhone"]));
									$objContact->set_contactAddress($objContact->CleanData($_POST["contactAddress"]));
									$objContact->set_contactCity($objContact->CleanData($_POST["contactCity"]));
									$objContact->set_contactRegion($objContact->CleanData($_POST["contactRegion"]));
									$objContact->set_contactCountry($objContact->CleanData($_POST["contactCountry"]));
									$returnedContactId = $objContact->insert();
										if (!empty($returnedContactId)) {
											// if contact is saved with enable portal then you click on add account
											$objUsers = new Users();
											$objUsers->set_accountSelect($returnedContactId);
											$objUsers->set_accountName("contact");
											$objUsers->set_userName($username);
											$objUsers->set_userPassword(password_hash($defaultPassword, PASSWORD_DEFAULT));
											$objUsers->set_accPasswdReset("on");
											$objUsers->set_accGroup("3");
											$objUsers->set_accStatus("on");
											if ($objUsers->insert()) {
												// if inserted successfully, then send sms
												$obSms = new Sms();
												$obSms->send_sms(trim($_POST["contactCell"]),"Welcome to E.L AGBEMAVA LAW OFFICE.Please log on https://elagbemava.com with credentials, username: $username and password : $defaultPassword");
												echo "success";
											}
											else{
												echo "error";
											}
										}
								} catch (Exception $e) { echo $e;}
							}
							
						break;
					// for update
						case 'update':
							if ((!empty($_POST["contactFirstName"])) || (!empty($_POST["contactLastName"])) || (!empty($_POST["contactCell"]))) {
								try{
									$objContact = new Contact;
									$objContact->set_contactFirstName($objContact->CleanData($_POST["contactFirstName"]));
									$objContact->set_contactMidName($objContact->CleanData($_POST["contactMidName"]));
									$objContact->set_contactLastName($objContact->CleanData($_POST["contactLastName"]));
									$objContact->set_contactEmail($objContact->CleanData($_POST["contactEmail"]));
									$objContact->set_contactCompany($objContact->CleanData($_POST["contactCompany"]));
									$objContact->set_contactGroup($objContact->CleanData($_POST["contactGroup"]));
									if (empty($_POST["contactPortal"])) {
										$objContact->set_contactPortal("off");
									}
									else{
										$objContact->set_contactPortal($objContact->CleanData($_POST["contactPortal"]));
									}
									$objContact->set_contactCell($objContact->CleanData($_POST["contactCell"]));
									$objContact->set_contactWrkPhone($objContact->CleanData($_POST["contactWrkPhone"]));
									$objContact->set_contactAddress($objContact->CleanData($_POST["contactAddress"]));
									$objContact->set_contactCity($objContact->CleanData($_POST["contactCity"]));
									$objContact->set_contactRegion($objContact->CleanData($_POST["contactRegion"]));
									$objContact->set_contactCountry($objContact->CleanData($_POST["contactCountry"]));
									$objContact->set_id($objContact->CleanData($_POST["contactData_id"]));
									if ($objContact->update()) {
										echo "success";
									}
									else{
										echo "error";
									}
									
								} catch (Exception $e) {
									
								}
							}
							else{
								echo "error";
							}
						break;
					// for delete
						case 'delete':
							if(!empty($_POST["contactData_id"])){
								try {
								  $objContact = new Contact;
								  $objContact->set_recordHide("YES");
							      $objContact->set_id($objContact->CleanData($_POST["contactData_id"]));
							      if ($objContact->delete()) {
							      	echo "success";
							      }
							      else{
							      	echo "error";
							      }
							    } catch (Exception $e) {
									
								}
							 }else{die();}
						break;
						// geting details of a member with id
						case 'updateModal':
							if(!empty($_POST["contactData_id"] )){
								  $objContact = new Contact;  
							      $objContact->set_id($objContact->CleanData($_POST["contactData_id"]));
							      $contactDetails = $objContact->get_contact_by_id();
							      print_r($contactDetails);  
							 }else{
							 	echo "error";
							 }
						break;
						// get all
						case 'getAll':
							$objContact = new Contact;
							print_r(json_encode($objContact->get_contacts(),true));
						break;
						case 'getContactName':
							if(!empty($_POST["contactData_id"] )){
								 $objContact = new Contact;  
							      $contactFullName = $objContact->get_contactName(trim($_POST["contactData_id"]));
							      print_r($contactFullName);  
							 }else{
							 	echo "error";
							 }
						break;
						// get contacts checkbox select
						case 'getContactData':
							if(!empty($_POST["caseContactIds"] )){
								$returnRecords = '';
								$caseContactIdsArray = json_decode($_POST["caseContactIds"],true);
								$objContact = new Contact; 
								  // loop through all the contacts array
								foreach ($caseContactIdsArray as $caseContact) {
								  	$objContact->set_id($objContact->CleanData($caseContact));
							      	$details = $objContact->get_contact_by_id();
							      	if (!empty($details)) {
							      		$returnRecords .='<tr>
								      						<td>'.$details["contact_First_name"].' '.$details["contact_last_name"].'</td>
								      						<td><input type="checkbox" name="eventContactShare[]" class="eventContactShare" value="'.$details["contact_id"].'"></td>
								      					</tr>';
							      	}
								}
							    print_r($returnRecords);  
						 	}else{
						 		echo "error";
						 	}

						default:
							die();
							break;
					}

				}
			}

	$objcontactController = new contactController;
 ?>