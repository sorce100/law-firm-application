<?php
	require_once("../Classes/CourtCaseNote.php"); 
	session_start();
	class courtCaseNoteController{
		function __construct(){
			// print_r($_POST);
					switch (trim($_POST["mode"])) {
						// for insert
						case 'insert':
						// print_r($_POST);
						// die();
							if ((!empty($_POST["viewNoteContent"])) || (!empty($_POST["view_note_caseId"]))) {
								try{
									$objCourtCaseNote = new CourtCaseNote;
									$objCourtCaseNote->set_courtCasId($objCourtCaseNote->CleanData($_POST["view_note_caseId"]));
									$objCourtCaseNote->set_viewNoteContent($objCourtCaseNote->CleanData($_POST["viewNoteContent"]));
									if ($objCourtCaseNote->insert()) {
										echo "success";
									}
									else{
										echo "error";
									}
								} catch (PDOException $e) {
									
								}
							}
							else{
								echo "error";
							}
							
						break;
					// for update
						case 'update':
							if (!empty($_POST["viewNoteContent"])) {
								try{
									$objCourtCaseNote = new CourtCaseNote;
									$objCourtCaseNote->set_courtCasId($objCourtCaseNote->CleanData($_POST["view_note_caseId"]));
									$objCourtCaseNote->set_viewNoteContent($objCourtCaseNote->CleanData($_POST["viewNoteContent"]));
									$objCourtCaseNote->set_id($objCourtCaseNote->CleanData($_POST["data_id"]));
									if ($objCourtCaseNote->update()) {
										echo "success";
									}
									else{
										echo "error";
									}
								} catch (PDOException $e) {
									
								}
							}
							else{
								echo "error";
							}
						break;
					// for delete
						case 'delete':
							if(!empty($_POST["data_id"])){
								try{
								  $objCourtCaseNote = new CourtCaseNote;
								  $objCourtCaseNote->set_recordHide("YES");
							      $objCourtCaseNote->set_id($objCourtCaseNote->CleanData($_POST["data_id"]));
							      if ($objCourtCaseNote->delete()) {
							      	echo "success";
							      }
							      else{
							      	echo "error";
							      }
							    } catch (PDOException $e) {
									
								}
							 }else{die();}
						break;
						// geting details of a member with id
						case 'updateModal':
							if(!empty($_POST["data_id"] )){
								try{
								  $objCourtCaseNote = new CourtCaseNote;  
							      $objCourtCaseNote->set_id($objCourtCaseNote->CleanData($_POST["data_id"]));
							      $details = $objCourtCaseNote->get_court_case_by_id();
							      print_r($details);
							    } catch (PDOException $e) {
									
								}  
							 }else{
							 	echo "error";
							 }
						break;
						// get all
						case 'getAll':
							$objCourtCaseNote = new CourtCaseNote;
							$objCourtCaseNote->set_courtCasId($objCourtCaseNote->CleanData($_POST["caseId"]));
							print_r($objCourtCaseNote->get_view_case_notes());
						break;
						// close case
						case 'get_case_messages':
							$objCourtCaseNote = new CourtCaseNote;
							$objCourtCaseNote->set_message_courtCaseId($objCourtCaseNote->CleanData($_POST["caseId"]));
							print_r(json_encode($objCourtCaseNote->get_court_case_messages(),true));
							
						break;
						default:
							die();
							break;
					}

				}
			}

	$objcourtCaseNoteController = new courtCaseNoteController;
 ?>