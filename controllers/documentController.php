<?php
	require_once("../Classes/Document.php");
	session_start();
	class DocumentControl{
		function __construct(){
			$objDocument = new Document; 
			$FolderPath = "../uploads/casedocuments/";
			$filesUploaded = [];
					switch (trim($_POST["mode"])) {
						// for insert
						case 'insert':
							// print_r($_POST);
							// exit();
							if (empty($_POST["documentCourtCaseId"]) || empty($_POST["docFolderName"]) || empty($_POST["docAssignedDate"])) {
								echo "error";
								die();
							}
							else{
								if ($_FILES['file_array']['error'][0] != "4") {
									try {
										$foldername=trim($_POST["docFolderName"]);
										$foldercreate=$FolderPath.$foldername."/";
										// create folder in the library folder
										if(!is_dir($foldercreate)) {
											mkdir($foldercreate,0777,true);
										}
										// make file upload
										if (!empty($_FILES['file_array']['name'][0])) {
											// declaring a new variable for the gloabl 
											$files=$_FILES['file_array'];
											$allowed=array('pdf','png','jpeg','jpg');

											foreach ($files['name'] as $position => $file_name) {
												$file_tmp =$files['tmp_name'][$position];
												$file_size =$files['size'][$position];
												$file_error =$files['error'][$position];
												$file_ext=explode('.',$file_name);
												$filename= current($file_ext);
												$file_ext = strtolower(end($file_ext));

												if (in_array($file_ext,$allowed)) {
													// check if ther is any errors from the uploaded file
													if ($file_error === 0) {
														// check if file uploaded has ther righ size
														if ($file_size < 1048576) {
															// creating unique ids for uploads instead of names of files, number is in microsecond
															$filenewname=$filename.".".$file_ext;
															// $structure="/uploads/agent_files/".$folder."/";				
															$filedestination = $foldercreate.$filenewname;
															// moving file from temporal location to permanent storage
															$fileup=move_uploaded_file($file_tmp,$filedestination);
															// add files to array to save
															$filesUploaded[]=$filenewname;
															
														}
															// end of if to check file size
													}
													// end of file error
													else{
														echo "error";
														die();
													}
												}
													// end of if file is allowed
												
											}
										// if file is uploaded successfully then save details in database
											if ($fileup) {
												
												$objDocument->set_courtCaseId($objDocument->CleanData($_POST["documentCourtCaseId"]));
												$objDocument->set_docFolderName($objDocument->CleanData($_POST["docFolderName"]));
												$objDocument->set_docFiles(json_encode($filesUploaded));
												$objDocument->set_docAssignedDate($objDocument->CleanData($_POST["docAssignedDate"]));
												$objDocument->set_docDescription($objDocument->CleanData($_POST["docDescription"]));
												if ($objDocument->insert()) {
													echo "success";
												}
												else{
													// if details could not save into database then delete all documents
													for ($i=0; $i <sizeof($filesUploaded) ; $i++) { 
														unlink($foldercreate.$filesUploaded[$i]);
													}
													echo "error";
													die();
												}
											}
										}
									} catch (PDOException $e) {$objDocument->log_error($e);}
								// if no image is uploaded
								}
								else if ($_FILES['file_array']['error'][0] == "4") {
									echo "error";
									die();
							}	}
							
						break;
					// for update
						case 'update':
							if (empty($_FILES)) {
								try{
									$objDocument->set_docFiles($_POST['documentsArray']);
									$objDocument->set_docAssignedDate($objDocument->CleanData($_POST["docAssignedDate"]));
									$objDocument->set_docDescription($objDocument->CleanData($_POST["docDescription"]));
									$objDocument->set_id($objDocument->CleanData($_POST["data_id"]));
									if ($objDocument->update()) {
										echo "success";
									}
									else{
										echo "error";
									}
								} catch (PDOException $e) {$objDocument->log_error($e);}
									
							}else if (!empty($_FILES)) {
								try {
									// check if files were deleted then save that
									$returnedDocsArray = json_decode($_POST['documentsArray']); 
									
									$foldercreate;
									$foldername;
									// check if foldername is empty

									if (!empty($_POST["folderName"])) {
										$foldername=trim($_POST["folderName"]);
										$foldercreate=$FolderPath.$foldername."/";

										// create folder in the library folder
										if(!is_dir($foldercreate)) {
											mkdir($foldercreate,0777,true);
										}
									}
									else{
										$foldername = trim($_POST["folderName"]);
										$foldercreate = $FolderPath.$foldername."/";
									}

									// //////////////////////////delete all files in the folder/////////////////////////////////
									// $getFolder = new RecursiveDirectoryIterator($foldercreate, FilesystemIterator::SKIP_DOTS);
									// $contents = new RecursiveIteratorIterator($getFolder, RecursiveIteratorIterator::CHILD_FIRST);
									// foreach ( $contents as $content ) {
									//     $content->isDir() ?  rmdir($content) : unlink($content);
									// }

									// make file upload
									if (!empty($_FILES['file_array']['name'][0])) {
										// declaring a new variable for the gloabl 
										$files=$_FILES['file_array'];
										$allowed=array('pdf','png','jpeg','jpg');

										foreach ($files['name'] as $position => $file_name) {
											$file_tmp =$files['tmp_name'][$position];
											$file_size =$files['size'][$position];
											$file_error =$files['error'][$position];
											$file_ext=explode('.',$file_name);
											$filename= current($file_ext);
											$file_ext = strtolower(end($file_ext));

											if ($file_name != "null") {
												if (in_array($file_ext,$allowed)) {
													// check if ther is any errors from the uploaded file
													if ($file_error === 0) {
														// check if file uploaded has ther righ size
														if ($file_size < 1048576) {
															// creating unique ids for uploads instead of names of files, number is in microsecond
															$filenewname=$filename.".".$file_ext;
															// $structure="/uploads/agent_files/".$folder."/";				
															$filedestination = $foldercreate.$filenewname;
															// moving file from temporal location to permanent storage
															$fileup=move_uploaded_file($file_tmp,$filedestination);
														}
															// end of if to check file size
														$returnedDocsArray[]=$filenewname;
													}
													// end of file erroe
													else{
														echo "error";
														die();
													}
												}
												// end of if file is allowed
												else{
													echo "error";
													die();
												}
											}
										}
										
									// if file is uploaded successfully then save details in database
										if ($fileup) {
											
											$objDocument->set_docFiles(json_encode($returnedDocsArray));
											$objDocument->set_docAssignedDate($objDocument->CleanData($_POST["docAssignedDate"]));
											$objDocument->set_docDescription($objDocument->CleanData($_POST["docDescription"]));
											$objDocument->set_id($objDocument->CleanData($_POST["data_id"]));
											if ($objDocument->update()) {
												echo "success";
											}
											else{
												echo "error";
											}
										}
									}
								} catch (PDOException $e) {$objDocument->log_error($e);}
							}
						break;
					// for delete
						case 'delete':
							if(!empty($_POST["data_id"])){
								try{
								  
								  $objDocument->set_recordHide("YES");
							      $objDocument->set_id($objDocument->CleanData($_POST["data_id"]));
							      if ($objDocument->delete()) {
										echo "success";
									}
									else{
										echo "error";
									}
								} catch (PDOException $e) {$objDocument->log_error($e);}
							}else{echo "error";}
						break;
						// geting details of a member with id
						case 'updateModal':
							if(!empty($_POST["data_id"])){
								    
							      $objDocument->set_id($objDocument->CleanData($_POST["data_id"]));
							      $details = $objDocument->get_case_doc_by_id();
							      print_r($details);  
							 }else{echo "error";}
						break;
						// document list
						case 'documentList':
							if(!empty($_POST["data_id"])){
								try{    
							      $objDocument->set_id($objDocument->CleanData($_POST["data_id"]));
							      $details = $objDocument->get_document_list();
							      print_r($details); 
							    } catch (PDOException $e) {$objDocument->log_error($e);} 
							 }else{echo "error";}
						break;
						// get all
						case 'getAll':
							
							$details = $objDocument->get_case_documents();
							if (!empty($details)) {
								try{
									print_r($details);
								} catch (PDOException $e) {$objDocument->log_error($e);}
							}

						break;
						case 'get_case_documents';
							if(isset($_POST["caseId"])){
								try{    
							      $objDocument->set_courtCaseId($objDocument->CleanData($_POST["caseId"]));
							      $details = $objDocument->get_case_document_list();
							      print_r($details); 

							    } catch (PDOException $e) {$objDocument->log_error($e);} 
							 }else{echo "error";}
						break;
						// remove file
						case 'removeFile';
							if((!empty($_POST["data_id"])) || (!empty($_POST["folderName"])) || (!empty($_POST["documentsArray"])) || (!empty($_POST["fileName"]))){
								try{
									// see if document is in arrary then remove file from folder
									$folderName= $_POST['folderName'];
									$documentsArray= json_decode($_POST['documentsArray']);
									$fileNameDel= $_POST['fileName'];
									if (in_array($fileNameDel, $documentsArray)) {
										// delete file from folder
										$fileDelPath = $FolderPath.$folderName."/".$fileNameDel;
										// check if file exits
										if (file_exists($fileDelPath)) {
											if (unlink($fileDelPath)) { 
												// if file deleted successfully then  remove file from array and update documentsArray
												if (($key = array_search($fileNameDel, $documentsArray)) !== false) { unset($documentsArray[$key]);}
												
											}
										}
										else{
											// if file does not exit 
											if (($key = array_search($fileNameDel, $documentsArray)) !== false) { unset($documentsArray[$key]);}
										}
										// save results
										
										$objDocument->set_docFiles(json_encode($documentsArray));
										$objDocument->set_id($objDocument->CleanData($_POST["data_id"]));
										if ($objDocument->update_case_docs()) {
											echo "success";
										}
										else{
											echo "error";
										}
										
									}
									else{
										// if file is not in the arry
										echo "error";
									}
								} catch (PDOException $e) {$objDocument->log_error($e);}
							 }else{echo "error";}
						break;
						default:
							echo "error";
							die();
							break;
					}

				}
			}

	$objDocumentControl = new DocumentControl;
 ?>