<?php
	require_once("../Classes/BillTimeEntry.php"); 
	session_start();
	class billTimeEntryController{
		function __construct(){
			$billAble;
			// print_r($_POST);
					switch (trim($_POST["mode"])) {
						
						// for insert
						case 'insert':
							if (!empty($_POST["timeEntriesCourtCaseId"]) || !empty($_POST["timeEntryStaff"]) || !empty($_POST["timeEntryDate"]) || !empty($_POST["timeEntryRate"]) || !empty($_POST["timeEntryDuration"])) {
								$objBillTimeEntry = new BillTimeEntry;
								$objBillTimeEntry->set_timeEntriesCourtCaseId($objBillTimeEntry->CleanData($_POST["timeEntriesCourtCaseId"]));
								$objBillTimeEntry->set_timeEntryStaff($objBillTimeEntry->CleanData($_POST["timeEntryStaff"]));
								$objBillTimeEntry->set_timeEntryActivity($objBillTimeEntry->CleanData($_POST["timeEntryActivity"]));
								// for adding invoice
								if (empty($_POST["timeEntryBillable"])) {
									$billAble = "off";
								}
								elseif (!empty($_POST["timeEntryBillable"])) {
									$billAble = $_POST["timeEntryBillable"];
								}
								$objBillTimeEntry->set_timeEntryBillable($objBillTimeEntry->CleanData($billAble));
								$objBillTimeEntry->set_timeEntryDescribe($objBillTimeEntry->CleanData($_POST["timeEntryDescribe"]));
								$objBillTimeEntry->set_timeEntryDate($objBillTimeEntry->CleanData($_POST["timeEntryDate"]));
								$objBillTimeEntry->set_timeEntryRate($objBillTimeEntry->CleanData($_POST["timeEntryRate"]));
								$objBillTimeEntry->set_timeEntryRateType($objBillTimeEntry->CleanData($_POST["timeEntryRateType"]));
								$objBillTimeEntry->set_timeEntryDuration($objBillTimeEntry->CleanData($_POST["timeEntryDuration"]));
								// calculating the total from the rate and time in cedis
								$total = $_POST["timeEntryRate"] * $_POST["timeEntryDuration"];
								$objBillTimeEntry->set_timeEntryTotal($objBillTimeEntry->CleanData($total));
								$objBillTimeEntry->set_timeEntryInvoiced("NO");
								
								if ($objBillTimeEntry->insert()) {
										echo "success";
									}
									else{
										echo "error";
									}
							}
							else{
								echo "error";
							}
							
						break;
					// for update
						case 'update':
							if (!empty($_POST["timeEntriesCourtCaseId"]) || !empty($_POST["timeEntryStaff"]) || !empty($_POST["timeEntryDate"]) || !empty($_POST["timeEntryRate"]) || !empty($_POST["timeEntryDuration"])) {
								$objBillTimeEntry = new BillTimeEntry;
								$objBillTimeEntry->set_timeEntriesCourtCaseId($objBillTimeEntry->CleanData($_POST["timeEntriesCourtCaseId"]));
								$objBillTimeEntry->set_timeEntryActivity($objBillTimeEntry->CleanData($_POST["timeEntryActivity"]));
								// for adding invoice
								if (empty($_POST["timeEntryBillable"])) {
									$billAble = "off";
								}
								elseif (!empty($_POST["timeEntryBillable"])) {
									$billAble = $_POST["timeEntryBillable"];
								}
								$objBillTimeEntry->set_timeEntryBillable($objBillTimeEntry->CleanData($billAble));
								$objBillTimeEntry->set_timeEntryDescribe($objBillTimeEntry->CleanData($_POST["timeEntryDescribe"]));
								$objBillTimeEntry->set_timeEntryDate($objBillTimeEntry->CleanData($_POST["timeEntryDate"]));
								$objBillTimeEntry->set_timeEntryRate($objBillTimeEntry->CleanData($_POST["timeEntryRate"]));
								$objBillTimeEntry->set_timeEntryRateType($objBillTimeEntry->CleanData($_POST["timeEntryRateType"]));
								$objBillTimeEntry->set_timeEntryDuration($objBillTimeEntry->CleanData($_POST["timeEntryDuration"]));
								// calculating the total from the rate and time in cedis
								$total = $_POST["timeEntryRate"] * $_POST["timeEntryDuration"];
								$objBillTimeEntry->set_timeEntryTotal($objBillTimeEntry->CleanData($total));
								$objBillTimeEntry->set_timeEntryInvoiced($objBillTimeEntry->CleanData($_POST["timeEntryInvoiced"]));
								$objBillTimeEntry->set_id($objBillTimeEntry->CleanData($_POST["data_id"]));
								
								if ($objBillTimeEntry->update()) {
										echo "success";
									}
									else{
										echo "error";
									}
							}
							else{
								echo "error";
							}
						break;
					// for delete
						case 'delete':
							if(isset($_POST["data_id"])){
									 $objBillTimeEntry = new BillTimeEntry;
									  $objBillTimeEntry->set_recordHide("YES");
								      $objBillTimeEntry->set_id($objBillTimeEntry->CleanData($_POST["data_id"]));
								      if ($objBillTimeEntry->delete()) {
								      	echo "success";
								      }
								      else{
								      	echo "error";
								      }
								     
								 }else{die();}
						break;
						// geting details of a member with id
						case 'updateModal':
							if(!empty($_POST["data_id"])){
									 $objBillTimeEntry = new BillTimeEntry;  
								      $objBillTimeEntry->set_id($objBillTimeEntry->CleanData($_POST["data_id"]));
								      $details = $objBillTimeEntry->get_time_entry_by_id();
								      print_r($details);  
								 }else{

								 	echo "error";
								 }
						break;
						case 'getTimeEntries_by_caseId':
							if(!empty($_POST["caseId"])){
									 $objBillTimeEntry = new BillTimeEntry;  
								      $objBillTimeEntry->set_timeEntriesCourtCaseId($objBillTimeEntry->CleanData($_POST["caseId"]));
								      $objBillTimeEntry->set_timeEntryInvoiced("NO");
								      $objBillTimeEntry->set_timeEntryBillable("on");
								      $details = $objBillTimeEntry->get_billable_time_entry_by_case();
								      print_r($details);  
								 }else{

								 	echo "error";
								 }
						break;
						// get all
						case 'getAll':
							$objBillTimeEntry = new BillTimeEntry;
							print_r(json_encode($objBillTimeEntry->get_time_entries(),true));

						break;
						default:
							die();
							break;
					}

				}
			}

	$objbillTimeEntryController = new billTimeEntryController;
 ?>