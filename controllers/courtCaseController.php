<?php
	require_once("../Classes/CourtCase.php"); 
	session_start();
	class courtCaseController{
		function __construct(){
			// print_r($_POST);
					switch (trim($_POST["mode"])) {
						// for insert
						case 'insert':
						// print_r($_POST);
						// die();
							if ((!empty($_POST["caseLeadAttorney"])) || (!empty($_POST["caseName"])) || (!empty($_POST["caseNumber"])) || (!empty($_POST["caseBillingContact"]))) {
								try {
									$objCourtCase = new CourtCase;
									$objCourtCase->set_caseContactIds(json_encode($_POST["caseContactIds"]));
									$objCourtCase->set_caseName($objCourtCase->CleanData($_POST["caseName"]));
									$objCourtCase->set_caseSuitNumber($objCourtCase->CleanData($_POST["caseSuitNumber"]));
									$objCourtCase->set_caseNumber($objCourtCase->CleanData($_POST["caseNumber"]));
									$objCourtCase->set_casePracticeArea($objCourtCase->CleanData($_POST["casePracticeArea"]));
									$objCourtCase->set_caseStage($objCourtCase->CleanData($_POST["caseStage"]));
									$objCourtCase->set_caseDateOpened($objCourtCase->CleanData($_POST["caseDateOpened"]));
									$objCourtCase->set_caseDescription($objCourtCase->CleanData($_POST["caseDescription"]));
									$objCourtCase->set_caseBillingContact($objCourtCase->CleanData($_POST["caseBillingContact"]));
									$objCourtCase->set_caseBillingMethod($objCourtCase->CleanData($_POST["caseBillingMethod"]));
									$objCourtCase->set_caseLeadAttorney($objCourtCase->CleanData($_POST["caseLeadAttorney"]));
									$objCourtCase->set_caseStaffIds(json_encode($_POST["caseStaffIds"]));
									$objCourtCase->set_currentCourtMain($objCourtCase->CleanData($_POST['currentCourtMain']));
									$objCourtCase->set_currentCourtSub($objCourtCase->CleanData($_POST['currentCourtSub']));
									$objCourtCase->set_currentCourtNum($objCourtCase->CleanData($_POST['currentCourtNum']));
									$objCourtCase->set_nextCourtMain($objCourtCase->CleanData($_POST['nextCourtMain']));
									$objCourtCase->set_nextCourtSub($objCourtCase->CleanData($_POST['nextCourtSub']));
									$objCourtCase->set_nextCourtNum($objCourtCase->CleanData($_POST['nextCourtNum']));
									$objCourtCase->set_nextCourtDate($objCourtCase->CleanData($_POST['nextCourtDate']));
									if ($objCourtCase->insert()) {
										echo "success";
									}
									else{
										echo "error";
									}
								} catch (PDOException $e) {
									echo $e;echo $e;
								}
							}
							else{
								echo "error";
							}
							
						break;
					// for update
						case 'update':
							if ((!empty($_POST["caseLeadAttorney"])) || (!empty($_POST["caseName"])) || (!empty($_POST["caseNumber"])) || (!empty($_POST["caseBillingContact"]))) {
								try{
									$objCourtCase = new CourtCase;
									$objCourtCase->set_caseContactIds(json_encode($_POST["caseContactIds"]));
									$objCourtCase->set_caseName($objCourtCase->CleanData($_POST["caseName"]));
									$objCourtCase->set_caseSuitNumber($objCourtCase->CleanData($_POST["caseSuitNumber"]));
									$objCourtCase->set_caseNumber($objCourtCase->CleanData($_POST["caseNumber"]));
									$objCourtCase->set_casePracticeArea($objCourtCase->CleanData($_POST["casePracticeArea"]));
									$objCourtCase->set_caseStage($objCourtCase->CleanData($_POST["caseStage"]));
									$objCourtCase->set_caseDateOpened($objCourtCase->CleanData($_POST["caseDateOpened"]));
									$objCourtCase->set_caseDescription($objCourtCase->CleanData($_POST["caseDescription"]));
									$objCourtCase->set_caseBillingContact($objCourtCase->CleanData($_POST["caseBillingContact"]));
									$objCourtCase->set_caseBillingMethod($objCourtCase->CleanData($_POST["caseBillingMethod"]));
									$objCourtCase->set_caseLeadAttorney($objCourtCase->CleanData($_POST["caseLeadAttorney"]));
									$objCourtCase->set_caseStaffIds(json_encode($_POST["caseStaffIds"]));
									$objCourtCase->set_currentCourtMain($objCourtCase->CleanData($_POST['currentCourtMain']));
									$objCourtCase->set_currentCourtSub($objCourtCase->CleanData($_POST['currentCourtSub']));
									$objCourtCase->set_currentCourtNum($objCourtCase->CleanData($_POST['currentCourtNum']));
									$objCourtCase->set_nextCourtMain($objCourtCase->CleanData($_POST['nextCourtMain']));
									$objCourtCase->set_nextCourtSub($objCourtCase->CleanData($_POST['nextCourtSub']));
									$objCourtCase->set_nextCourtNum($objCourtCase->CleanData($_POST['nextCourtNum']));
									$objCourtCase->set_nextCourtDate($objCourtCase->CleanData($_POST['nextCourtDate']));

									$objCourtCase->set_id($objCourtCase->CleanData($_POST["data_id"]));
									if ($objCourtCase->update()) {
										echo "success";
									}
									else{
										echo "error";
									}
								} catch (PDOException $e) {
									echo $e;echo $e;
								}
							}
							else{
								echo "error";
							}
						break;
					// for delete
						case 'delete':
							if(!empty($_POST["data_id"])){
								try{
								  $objCourtCase = new CourtCase;
								  $objCourtCase->set_recordHide("YES");
							      $objCourtCase->set_id($objCourtCase->CleanData($_POST["data_id"]));
							      if ($objCourtCase->delete()) {
							      	echo "success";
							      }
							      else{
							      	echo "error";
							      }
							    } catch (PDOException $e) {
									echo $e;
								}
							     
							 }else{die();}
						break;
						// geting details of a member with id
						case 'updateModal':
							if(!empty($_POST["data_id"] )){
								try{
								  $objCourtCase = new CourtCase;  
							      $objCourtCase->set_id($objCourtCase->CleanData($_POST["data_id"]));
							      $courtCasesDetails = $objCourtCase->get_case_by_id();
							      print_r($courtCasesDetails); 
							    } catch (PDOException $e) {
									echo $e;
								} 
							 }else{
							 	echo "error";
							 }
						break;
						// get all
						case 'getAll':
							$objCourtCase = new CourtCase;
							print_r($objCourtCase->get_cases_all());
						break;
						// get contacts and staff of case
						case 'case_staffContacts_id':
							if(!empty($_POST["caseId"] )){
								try{
								  $objCourtCase = new CourtCase;  
							      $objCourtCase->set_id($objCourtCase->CleanData($_POST["caseId"]));
							      $details = $objCourtCase->get_case_contacts_and_staff();
							      print_r(json_encode($details));  
							    } catch (PDOException $e) {
									echo $e;
								}
							 }else{
							 	echo "error";
							 }
						break;
						// get all details of court case for view
						case 'case_view_details':
							if(!empty($_POST["caseId"] )){
								try{
								  $objCourtCase = new CourtCase;  
							      $objCourtCase->set_id($objCourtCase->CleanData($_POST["caseId"]));
							      $courtCasesDetails = $objCourtCase->get_case_view_details_by_id();
							      print_r($courtCasesDetails); 
							    } catch (PDOException $e) {
									echo $e;
								} 
							 }else{
							 	echo "error";
							 }
						break;
						// close case
						case 'close_court_case':
							$objCourtCase = new CourtCase;
							$objCourtCase->set_id($objCourtCase->CleanData($_POST["data_id"]));
							$objCourtCase->set_caseStatus('CLOSED');
							if ($objCourtCase->close_court_case()) {
						      	echo "success";
						      }
						      else{
						      	echo "error";
						      }
							
						break;
						// filter search
						case 'filter_court_case':
							$objCourtCase = new CourtCase;
							$objCourtCase->set_casePracticeArea($objCourtCase->CleanData($_POST["filterPracticeArea"]));
							$objCourtCase->set_caseLeadAttorney($objCourtCase->CleanData($_POST["filterLeadAttorney"]));
							$objCourtCase->set_caseStage($objCourtCase->CleanData($_POST["filtercaseStage"]));
							print_r($objCourtCase->court_case_filter());
						break;
						default:
							die();
							break;
					}

				}
			}

	$objcourtCaseController = new courtCaseController;
 ?>