<?php
	require_once("../Classes/PracticeArea.php"); 
	session_start();
	class PracticeAreaController{
		function __construct(){
			// print_r($_POST);
					switch (trim($_POST["mode"])) {
						// for insert
						case 'insert':
							if (!empty($_POST["practiceArea"])) {
								try{
									$objPracticeArea = new PracticeArea;
									$objPracticeArea->set_practiceArea($objPracticeArea->CleanData($_POST["practiceArea"]));
									
									if ($objPracticeArea->insert()) {
										echo "success";
									}
									else{
										echo "error";
									}
								} catch (Exception $e) {
									
								}
							}
							else{
								echo "error";
							}
							
						break;
					// for update
						case 'update':
							if (!empty($_POST["practiceArea"])) {
								try{
									$objPracticeArea = new PracticeArea;
									$objPracticeArea->set_practiceArea($objPracticeArea->CleanData($_POST["practiceArea"]));
									$objPracticeArea->set_id($objPracticeArea->CleanData($_POST["data_id"]));
									if ($objPracticeArea->update()) {
										echo "success";
									}
									else{
										echo "error";
									}
								} catch (Exception $e) {
										
								}
							}
							else{
								echo "error";
							}
						break;
					// for delete
						case 'delete':
							if(isset($_POST["data_id"])){
								try{
								  $objPracticeArea = new PracticeArea;
								  $objPracticeArea->set_recordHide("YES");
							      $objPracticeArea->set_id($objPracticeArea->CleanData($_POST["data_id"]));
							      if ($objPracticeArea->delete()) {
							      	echo "success";
							      }
							      else{
							      	echo "error";
							      }
							    } catch (Exception $e) {
									
								}
							 }else{die();}
						break;
						// geting details of a member with id
						case 'updateModal':
							if(!empty($_POST["data_id"])){
								try{
								  $objPracticeArea = new PracticeArea;  
							      $objPracticeArea->set_id($objPracticeArea->CleanData($_POST["data_id"]));
							      $pages_details = $objPracticeArea->get_area_by_id();
							      print_r($pages_details);
							    } catch (Exception $e) {
									
								}  
							 }else{
							 	echo "error";
							 }
						break;
						// get all
						case 'getAll':
							$objPracticeArea = new PracticeArea;
							$details = $objPracticeArea->get_practice_areas_list();
							if (!empty($details)) {
								print_r($details);
							}

						break;
						case 'get_practice_name':
							if(!empty($_POST["data_id"])){
								  $objPracticeArea = new PracticeArea;  
							      $objPracticeArea->set_id($objPracticeArea->CleanData($_POST["data_id"]));
							      $details = $objPracticeArea->get_practiceArea_name();
							      print_r($details);  
								 }else{
								 	echo "error";
								 }
						break;
						default:
							die();
							break;
					}

				}
			}

	$objPracticeAreaController = new PracticeAreaController;
 ?>