<?php
	require_once("../Classes/ContactCompanys.php"); 
	session_start();
	class ContactCompanysController{
		function __construct(){
			// print_r($_POST);
					switch (trim($_POST["comMode"])) {
						// for insert
						case 'insert':
							if ((!empty($_POST["ConCompanyName"])) || (!empty($_POST["ConCompanyMainPhone"])) || (!empty($_POST["ConCompanyCountry"]))) {
								try{
									$objContactCompanys = new ContactCompanys;
									$objContactCompanys->set_ConCompanyName($objContactCompanys->CleanData($_POST["ConCompanyName"]));
									$objContactCompanys->set_ConCompanyEmail($objContactCompanys->CleanData($_POST["ConCompanyEmail"]));
									$objContactCompanys->set_ConCompanyWebsite($objContactCompanys->CleanData($_POST["ConCompanyWebsite"]));
									$objContactCompanys->set_ConCompanyMainPhone($objContactCompanys->CleanData($_POST["ConCompanyMainPhone"]));
									$objContactCompanys->set_ConCompanyAddress($objContactCompanys->CleanData($_POST["ConCompanyAddress"]));
									$objContactCompanys->set_ConCompanyCity($objContactCompanys->CleanData($_POST["ConCompanyCity"]));
									$objContactCompanys->set_ConCompanyRegion($objContactCompanys->CleanData($_POST["ConCompanyRegion"]));
									$objContactCompanys->set_ConCompanyCountry($objContactCompanys->CleanData($_POST["ConCompanyCountry"]));
									$objContactCompanys->set_ConCompanyPrivateNote($objContactCompanys->CleanData($_POST["ConCompanyPrivateNote"]));
									if ($objContactCompanys->insert()) {
										echo "success";
									}
									else{
										echo "error";
									}
								} catch (Exception $e) {
									echo "error";
								}
							}
							else{
								echo "error";
							}
							
						break;
					// for update
						case 'update':
							if ((!empty($_POST["ConCompanyName"])) || (!empty($_POST["ConCompanyMainPhone"])) || (!empty($_POST["ConCompanyCountry"]))) {
								try{
									$objContactCompanys = new ContactCompanys;
									$objContactCompanys->set_ConCompanyName($objContactCompanys->CleanData($_POST["ConCompanyName"]));
									$objContactCompanys->set_ConCompanyEmail($objContactCompanys->CleanData($_POST["ConCompanyEmail"]));
									$objContactCompanys->set_ConCompanyWebsite($objContactCompanys->CleanData($_POST["ConCompanyWebsite"]));
									$objContactCompanys->set_ConCompanyMainPhone($objContactCompanys->CleanData($_POST["ConCompanyMainPhone"]));
									$objContactCompanys->set_ConCompanyAddress($objContactCompanys->CleanData($_POST["ConCompanyAddress"]));
									$objContactCompanys->set_ConCompanyCity($objContactCompanys->CleanData($_POST["ConCompanyCity"]));
									$objContactCompanys->set_ConCompanyRegion($objContactCompanys->CleanData($_POST["ConCompanyRegion"]));
									$objContactCompanys->set_ConCompanyCountry($objContactCompanys->CleanData($_POST["ConCompanyCountry"]));
									$objContactCompanys->set_ConCompanyPrivateNote($objContactCompanys->CleanData($_POST["ConCompanyPrivateNote"]));
									$objContactCompanys->set_id($objContactCompanys->CleanData($_POST["comData_id"]));
									if ($objContactCompanys->update()) {
										echo "success";
									}
									else{
										echo "error";
									}
								} catch (Exception $e) {
									echo "error";
								}
							}
							else{
								echo "error";
							}
						break;
					// for delete
						case 'delete':
							if(!empty($_POST["comData_id"])){
								try{
								  $objContactCompanys = new ContactCompanys;
								  $objContactCompanys->set_recordHide("YES");
							      $objContactCompanys->set_id($objContactCompanys->CleanData($_POST["comData_id"]));
							      if ($objContactCompanys->delete()) {
							      	echo "success";
							      }
							      else{
							      	echo "error";
							      }
							    } catch (Exception $e) {
									echo "error";
								}
							 }else{die();}
						break;
						// geting details of a member with id
						case 'updateModal':
							if(!empty($_POST["comData_id"] )){
									 $objContactCompanys = new ContactCompanys;  
								      $objContactCompanys->set_id($objContactCompanys->CleanData($_POST["comData_id"]));
								      $contactCompanyDetails = $objContactCompanys->get_conCompany_by_id();
								      print_r($contactCompanyDetails);  
								 }else{
								 	echo "error";
								 }
						break;
						// get all
						case 'getAll':
							$objContactCompanys = new ContactCompanys;
							print_r(json_encode($objContactCompanys->get_contactCompany(),true));
						break;
						default:
							die();
							break;
					}

				}
			}

	$objContactCompanysController = new ContactCompanysController;
 ?>