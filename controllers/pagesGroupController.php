<?php
	require_once("../Classes/PagesGroup.php"); 
	session_start();
	class PagesGroupController{
		function __construct(){
			// print_r($_POST);
					switch (trim($_POST["mode"])) {
						// for insert
						case 'insert':
							if (!empty($_POST["pagesGroupName"])) {
								try{
									$objPagesGroup = new PagesGroup;
									$objPagesGroup->set_pagesGroupName($objPagesGroup->CleanData($_POST["pagesGroupName"]));
									$objPagesGroup->set_pagesId(json_encode($_POST["pagesId"]));
									if ($objPagesGroup->insert()) {
										echo "success";
									}
									else{
										echo "error";
									}
								} catch (Exception $e) {
									
								}
							}
							else{
								echo "error";
							}
							
						break;
					// for update
						case 'update':
							if (!empty($_POST["pagesGroupName"])) {
								try{
									$objPagesGroup = new PagesGroup;
									$objPagesGroup->set_pagesGroupName($objPagesGroup->CleanData($_POST["pagesGroupName"]));
									$objPagesGroup->set_pagesId(json_encode($_POST["pagesId"]));
									$objPagesGroup->set_id($objPagesGroup->CleanData($_POST["data_id"]));
									if ($objPagesGroup->update()) {
										echo "success";
									}
									else{
										echo "error";
									}
								} catch (Exception $e) {
									
								}
							}
							else{
								echo "error";
							}
						break;
					// for delete
						case 'delete':
							if(isset($_POST["data_id"])){
								try{
								  $objPagesGroup = new PagesGroup;
								  $objPagesGroup->set_recordHide("YES");
							      $objPagesGroup->set_id($objPagesGroup->CleanData($_POST["data_id"]));
							      if ($objPagesGroup->delete()) {
							      	echo "success";
							      }
							      else{
							      	echo "error";
							      }
							    } catch (Exception $e) {
									
								}
							 }else{die();}
						break;
						// geting details of a member with id
						case 'updateModal':
							if(!empty($_POST["data_id"])){
								try{
								  $objPagesGroup = new PagesGroup;  
							      $objPagesGroup->set_id($objPagesGroup->CleanData($_POST["data_id"]));
							      $pages_details = $objPagesGroup->get_group_by_id();
							      print_r($pages_details);
							    } catch (Exception $e) {
									
								}  
							 }else{
							 	echo "error";
							 }
						break;
						// get all
						case 'getAll':
							$objPagesGroup = new PagesGroup;
							$details = $objPagesGroup->get_pages_groups_list();
							if (!empty($details)) {
								print_r($details);
							}

						break;
						default:
							die();
							break;
					}

				}
			}

	$objPagesGroupController = new PagesGroupController;
 ?>