<?php
	require_once("../Classes/Pages.php"); 
	session_start();
	class PagesController{
		function __construct(){
			// print_r($_POST);
					switch (trim($_POST["mode"])) {
						// for insert
						case 'insert':
							if (!empty($_POST["pageName"])) {
								try{
									$objPages = new Pages;
									$objPages->set_pageName($objPages->CleanData($_POST["pageName"]));
									$objPages->set_pageUrl(trim($_POST["pageUrl"]));
									$objPages->set_pageFileName($objPages->CleanData($_POST["pageFileName"]));
									if ($objPages->insert()) {
										echo "success";
									}
									else{
										echo "error";
									}
								} catch (Exception $e) {
									
								}
							}
							else{
								echo "error";
							}
							
						break;
					// for update
						case 'update':
							if (!empty($_POST["pageName"])) {
								try{
									$objPages = new Pages;
									$objPages->set_pageName($objPages->CleanData($_POST["pageName"]));
									$objPages->set_pageUrl(trim($_POST["pageUrl"]));
									$objPages->set_pageFileName($objPages->CleanData($_POST["pageFileName"]));
									$objPages->set_id($objPages->CleanData($_POST["data_id"]));
									if ($objPages->update()) {
										echo "success";
									}
									else{
										echo "error";
									}
								} catch (Exception $e) {
										
								}
							}
							else{
								echo "error";
							}
						break;
					// for delete
						case 'delete':
							if(!empty($_POST["data_id"])){
								try{
								  $objPages = new Pages;
								  $objPages->set_recordHide("YES");
							      $objPages->set_id($objPages->CleanData($_POST["data_id"]));
							      if ($objPages->delete()) {
							      	echo "success";
							      }
							      else{
							      	echo "error";
							      }
							    } catch (Exception $e) {
									
								}
							 }else{die();}
						break;
						// geting details of a member with id
						case 'updateModal':
							if(!empty($_POST["data_id"])){
								try{
								  $objPages = new Pages;  
							      $objPages->set_id($objPages->CleanData($_POST["data_id"]));
							      $pages_details = $objPages->get_page_by_id();
							      print_r($pages_details); 
							    } catch (Exception $e) {
									
								} 
							 }else{
							 	echo "error";
							 }
						break;
						// get all
						case 'getAll':
							$objPages = new Pages;
							$details = $objPages->get_pages_list();
							if (!empty($details)) {
								print_r($details);
							}

						break;
						default:
							die();
							break;
					}

				}
			}

	$objPagesController = new PagesController;
 ?>