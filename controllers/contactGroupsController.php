<?php
	require_once("../Classes/ContactGroups.php"); 
	session_start();
	class ContactGroupsController{
		function __construct(){
			// print_r($_POST);
					switch (trim($_POST["grpMode"])) {
						// for insert
						case 'insert':
							if (!empty($_POST["contactGroupName"])) {
								try {
									$objContactGroups = new ContactGroups;
									$objContactGroups->set_contactGroupName($objContactGroups->CleanData($_POST["contactGroupName"]));
									if ($objContactGroups->insert()) {
										echo "success";
									}
									else{
										echo "error";
									}
								} catch (Exception $e) {
									
								}
							}
							else{
								echo "error";
							}
							
						break;
					// for update
						case 'update':
							if (!empty($_POST["contactGroupName"])) {
								try{
									$objContactGroups = new ContactGroups;
									$objContactGroups->set_contactGroupName($objContactGroups->CleanData($_POST["contactGroupName"]));
									$objContactGroups->set_id($objContactGroups->CleanData($_POST["grpData_id"]));
									if ($objContactGroups->update()) {
										echo "success";
									}
									else{
										echo "error";
									}
								} catch (Exception $e) {
									
								}
							}
							else{
								echo "error";
							}
						break;
					// for delete
						case 'delete':
							if(!empty($_POST["grpData_id"])){
								try{
								  $objContactGroups = new ContactGroups;
								  $objContactGroups->set_recordHide("YES");
							      $objContactGroups->set_id($objContactGroups->CleanData($_POST["grpData_id"]));
							      if ($objContactGroups->delete()) {
							      	echo "success";
							      }
							      else{
							      	echo "error";
							      }
							    } catch (Exception $e) {
									
								}
							 }else{die();}
						break;
						// geting details of a member with id
						case 'updateModal':
							if(!empty($_POST["grpData_id"] )){
								try{
								  $objContactGroups = new ContactGroups;  
							      $objContactGroups->set_id($objContactGroups->CleanData($_POST["grpData_id"]));
							      $contactGroupDetails = $objContactGroups->get_conGrp_by_id();
							      print_r($contactGroupDetails);
								} catch (Exception $e) {
									
								}  
							 }else{
							 	echo "error";
							 }
						break;
						// get all
						case 'getAll':
							$objContactGroups = new ContactGroups;
							print_r(json_encode($objContactGroups->get_contactGroups(),true));
						break;
						default:
							die();
						break;
					}

				}
			}

	$objContactGroupsController = new ContactGroupsController;
 ?>