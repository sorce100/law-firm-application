<?php
	require_once("../Classes/Leads.php"); 
	session_start();
	class LeadsController{
		function __construct(){
			// print_r($_POST);
			switch (trim($_POST["mode"])) {
				// for insert
				case 'insert':
					if ((!empty($_POST["leadFirstName"])) || (!empty($_POST["leadLastName"])) || (!empty($_POST["leadTel"])) || (!empty($_POST["leadCaseMatter"])) || (!empty($_POST["leadCaseStatus"])) || (!empty($_POST["leadCasePracticeArea"])) || (!empty($_POST["leadCaseAssignedTo"])) || (!empty($_POST["leadCaseFeeFrequency"]))) {
						try{
							$objLeads = new Leads;
							$objLeads->set_leadFirstName($objLeads->CleanData($_POST["leadFirstName"]));
							$objLeads->set_leadMidName($objLeads->CleanData($_POST["leadMidName"]));
							$objLeads->set_leadLastName($objLeads->CleanData($_POST["leadLastName"]));
							$objLeads->set_leadEmail($objLeads->CleanData($_POST["leadEmail"]));
							$objLeads->set_leadTel($objLeads->CleanData($_POST["leadTel"]));
							$objLeads->set_leadOfficeLocation($objLeads->CleanData($_POST["leadOfficeLocation"]));
							$objLeads->set_leadTown($objLeads->CleanData($_POST["leadTown"]));
							$objLeads->set_leadRegion($objLeads->CleanData($_POST["leadRegion"]));
							$objLeads->set_leadCaseMatter($objLeads->CleanData($_POST["leadCaseMatter"]));
							$objLeads->set_leadCaseDescription($objLeads->CleanData($_POST["leadCaseDescription"]));
							$objLeads->set_leadCaseStatus($objLeads->CleanData($_POST["leadCaseStatus"]));
							$objLeads->set_leadCasePracticeArea($objLeads->CleanData($_POST["leadCasePracticeArea"]));
							$objLeads->set_leadCaseAssignedTo($objLeads->CleanData($_POST["leadCaseAssignedTo"]));
							$objLeads->set_leadCaseCompletionDate($objLeads->CleanData($_POST["leadCaseCompletionDate"]));
							$objLeads->set_leadCaseHourlyRate($objLeads->CleanData($_POST["leadCaseHourlyRate"]));
							$objLeads->set_leadCaseMonthlyRetainer($objLeads->CleanData($_POST["leadCaseMonthlyRetainer"]));
							$objLeads->set_leadCaseFeeFrequency($objLeads->CleanData($_POST["leadCaseFeeFrequency"]));
							$objLeads->set_leadCaseSpecialArrangement($objLeads->CleanData($_POST["leadCaseSpecialArrangement"]));
							$objLeads->set_leadCaseAssociatedClients($objLeads->CleanData($_POST["leadCaseAssociatedClients"]));
							$objLeads->set_leadCaseAdverseParties($objLeads->CleanData($_POST["leadCaseAdverseParties"]));
							$objLeads->set_leadCaseAssociatedFiles($objLeads->CleanData($_POST["leadCaseAssociatedFiles"]));
							if ($objLeads->insert()) {
								echo "success";
							}
							else{
								echo "error";
							}
						} catch (Exception $e) {
							echo $e;
						}
					}
					else{
						echo "error";
					}
					
				break;
			// for update
				case 'update':
					if ((!empty($_POST["leadFirstName"])) || (!empty($_POST["leadLastName"])) || (!empty($_POST["leadTel"])) || (!empty($_POST["leadCaseMatter"])) || (!empty($_POST["leadCaseStatus"])) || (!empty($_POST["leadCasePracticeArea"])) || (!empty($_POST["leadCaseAssignedTo"])) || (!empty($_POST["leadCaseFeeFrequency"])) || (!empty($_POST["data_id"]))) {
						try{
							$objLeads = new Leads;
							$objLeads->set_leadFirstName($objLeads->CleanData($_POST["leadFirstName"]));
							$objLeads->set_leadMidName($objLeads->CleanData($_POST["leadMidName"]));
							$objLeads->set_leadLastName($objLeads->CleanData($_POST["leadLastName"]));
							$objLeads->set_leadEmail($objLeads->CleanData($_POST["leadEmail"]));
							$objLeads->set_leadTel($objLeads->CleanData($_POST["leadTel"]));
							$objLeads->set_leadOfficeLocation($objLeads->CleanData($_POST["leadOfficeLocation"]));
							$objLeads->set_leadTown($objLeads->CleanData($_POST["leadTown"]));
							$objLeads->set_leadRegion($objLeads->CleanData($_POST["leadRegion"]));
							$objLeads->set_leadCaseMatter($objLeads->CleanData($_POST["leadCaseMatter"]));
							$objLeads->set_leadCaseDescription($objLeads->CleanData($_POST["leadCaseDescription"]));
							$objLeads->set_leadCaseStatus($objLeads->CleanData($_POST["leadCaseStatus"]));
							$objLeads->set_leadCasePracticeArea($objLeads->CleanData($_POST["leadCasePracticeArea"]));
							$objLeads->set_leadCaseAssignedTo($objLeads->CleanData($_POST["leadCaseAssignedTo"]));
							$objLeads->set_leadCaseCompletionDate($objLeads->CleanData($_POST["leadCaseCompletionDate"]));
							$objLeads->set_leadCaseHourlyRate($objLeads->CleanData($_POST["leadCaseHourlyRate"]));
							$objLeads->set_leadCaseMonthlyRetainer($objLeads->CleanData($_POST["leadCaseMonthlyRetainer"]));
							$objLeads->set_leadCaseFeeFrequency($objLeads->CleanData($_POST["leadCaseFeeFrequency"]));
							$objLeads->set_leadCaseSpecialArrangement($objLeads->CleanData($_POST["leadCaseSpecialArrangement"]));
							$objLeads->set_leadCaseAssociatedClients($objLeads->CleanData($_POST["leadCaseAssociatedClients"]));
							$objLeads->set_leadCaseAdverseParties($objLeads->CleanData($_POST["leadCaseAdverseParties"]));
							$objLeads->set_leadCaseAssociatedFiles($objLeads->CleanData($_POST["leadCaseAssociatedFiles"]));
							$objLeads->set_id($objLeads->CleanData($_POST["data_id"]));
							if ($objLeads->update()) {
								echo "success";
							}
							else{
								echo "error";
							}
						} catch (Exception $e) {
							echo $e;	
						}
					}
					else{
						echo "error";
					}
				break;
			// for delete
				case 'delete':
					if(!empty($_POST["data_id"])){
						try{
						  $objLeads = new Leads;
						  $objLeads->set_recordHide("YES");
					      $objLeads->set_id($objLeads->CleanData($_POST["data_id"]));
					      if ($objLeads->delete()) {
					      	echo "success";
					      }
					      else{
					      	echo "error";
					      }
					    } catch (Exception $e) {
							echo $e;
						}
					 }else{die();}
				break;
				// geting details of a member with id
				case 'updateModal':
					if(!empty($_POST["data_id"])){
						try{
						  $objLeads = new Leads;  
					      $objLeads->set_id($objLeads->CleanData($_POST["data_id"]));
					      $details = $objLeads->get_lead_by_id();
					      print_r($details); 
					    } catch (Exception $e) {
							echo $e;
						} 
					 }else{
					 	echo "error";
					 }
				break;
				// get all
				case 'getAll':
					$objLeads = new Leads;
					print_r(json_encode($objLeads->get_pages(),true));

				break;
				default:
					die();
					break;
			}

		}
	}

	$objLeadsController = new LeadsController;
 ?>