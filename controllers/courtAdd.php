<?php
	require_once("../Classes/CourtAdd.php"); 
	session_start();
	class CourtAddController{
		function __construct(){
			// print_r($_POST);
					switch (trim($_POST["mode"])) {
						// for main court insert
						case 'mainCourtInsert':
							if (!empty($_POST["mainCourtName"])) {
								try{
									$objCourtAdd = new CourtAdd;
									$objCourtAdd->set_mainCourtName($objCourtAdd->CleanData($_POST["mainCourtName"]));
									if ($objCourtAdd->main_court_insert()) {
										echo "success";
									}
									else{
										echo "error";
									}
								} catch (PDOException $e) {
									echo $e;
								}
							}
							else{
								echo "error";
							}
							
						break;
					// for sub court insert
						case 'subCourtInsert':
							if ((!empty($_POST["mainCourtSelect"])) || (!empty($_POST["subCourtName"])) || (!empty($_POST["subCourtRegion"]))) {
								try{
									$objCourtAdd = new CourtAdd;
									$objCourtAdd->set_mainCourtSelect($objCourtAdd->CleanData($_POST["mainCourtSelect"]));
									$objCourtAdd->set_subCourtName($objCourtAdd->CleanData($_POST["subCourtName"]));
									$objCourtAdd->set_subCourtRegion($objCourtAdd->CleanData($_POST["subCourtRegion"]));
									if ($objCourtAdd->sub_court_insert()) {
										echo "success";
									}
									else{
										echo "error";
									}
								} catch (PDOException $e) {
									echo $e;
								}
							}
							else{
								echo "error";
							}
							
						break;
					// for main court update
						case 'mainCourtupdate':
							if (!empty($_POST["mainCourtName"])) {
								try{
									$objCourtAdd = new CourtAdd;
									$objCourtAdd->set_mainCourtName($objCourtAdd->CleanData($_POST["mainCourtName"]));
									$objCourtAdd->set_mainCourtId($objCourtAdd->CleanData($_POST["data_id"]));
									if ($objCourtAdd->main_court_update()) {
										echo "success";
									}
									else{
										echo "error";
									}
								} catch (PDOException $e) {
									echo $e;	
								}
							}
							else{
								echo "error";
							}
						break;
					// for sub court update
						case 'subCourtupdate':
						
							if ((!empty($_POST["mainCourtSelect"])) || (!empty($_POST["subCourtName"])) || (!empty($_POST["subCourtRegion"]))) {
								try{
									$objCourtAdd = new CourtAdd;
									$objCourtAdd->set_mainCourtSelect($objCourtAdd->CleanData($_POST["mainCourtSelect"]));
									$objCourtAdd->set_subCourtName($objCourtAdd->CleanData($_POST["subCourtName"]));
									$objCourtAdd->set_subCourtRegion($objCourtAdd->CleanData($_POST["subCourtRegion"]));
									$objCourtAdd->set_subCourtId($objCourtAdd->CleanData($_POST["data_id"]));
									if ($objCourtAdd->sub_court_update()) {
										echo "success";
									}
									else{
										echo "error";
									}
								} catch (PDOException $e) {
									echo $e;	
								}
							}
							else{
								echo "error";
							}
						break;
					// for main court delete
						case 'mainCourtdelete':
							if(!empty($_POST["data_id"])){
								try{
								  $objCourtAdd = new CourtAdd;
								  $objCourtAdd->set_recordHide("YES");
							      $objCourtAdd->set_mainCourtId($objCourtAdd->CleanData($_POST["data_id"]));
							      if ($objCourtAdd->mainCourtdelete()) {
							      	echo "success";
							      }
							      else{
							      	echo "error";
							      }
							    } catch (PDOException $e) {
									echo $e;
								}
							 }else{die();}
						break;
					// for sub court delete
						case 'subCourtdelete':
							if(!empty($_POST["data_id"])){
								try{
								  $objCourtAdd = new CourtAdd;
								  $objCourtAdd->set_recordHide("YES");
							      $objCourtAdd->set_subCourtId($objCourtAdd->CleanData($_POST["data_id"]));
							      if ($objCourtAdd->subCourtDelete()) {
							      	echo "success";
							      }
							      else{
							      	echo "error";
							      }
							    } catch (PDOException $e) {
									echo $e;
								}
							 }else{die();}
						break;
						// geting details for main court
						case 'mainUpdateModal':
							if(!empty($_POST["data_id"])){
								try{
								  $objCourtAdd = new CourtAdd;  
							      $objCourtAdd->set_mainCourtId($objCourtAdd->CleanData($_POST["data_id"]));
							      $details = $objCourtAdd->get_main_court_by_id();
							      print_r($details); 
							    } catch (PDOException $e) {
									echo $e;
								} 
							 }else{
							 	echo "error";
							 }
						break;
						// geting details for sub court
						case 'subUpdateModal':
							if(!empty($_POST["data_id"])){
								try{
								  $objCourtAdd = new CourtAdd;  
							      $objCourtAdd->set_subCourtId($objCourtAdd->CleanData($_POST["data_id"]));
							      $details = $objCourtAdd->get_sub_court_by_id();
							      print_r($details); 
							    } catch (PDOException $e) {
									echo $e;
								} 
							 }else{
							 	echo "error";
							 }
						break;
						// get all
						case 'getAll':
							$objCourtAdd = new CourtAdd;
							print_r(json_encode($objCourtAdd->get_pages(),true));

						break;
						// grab all sub divisions by main court id
						case 'get_sub_division':
							if(!empty($_POST["mainCourtId"])){
								try{
								  $objCourtAdd = new CourtAdd;  
							      $objCourtAdd->set_mainCourtSelect($objCourtAdd->CleanData($_POST["mainCourtId"]));
							      $details = $objCourtAdd->get_sub_division_by_main_id();
							      print_r($details); 
							    } catch (PDOException $e) {
									echo $e;
								} 
							 }else{
							 	echo "error";
							 }
						break;
						default:
							die();
							break;
					}

				}
			}

	$objCourtAddController = new CourtAddController;
 ?>