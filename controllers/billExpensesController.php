<?php
	require_once("../Classes/BillExpenses.php"); 
	session_start();
	class billExpensesController{
		function __construct(){
			// print_r($_POST);
					switch (trim($_POST["mode"])) {
						// for insert
						case 'insert':
							if (!empty($_POST["expenseCourtCaseId"]) || !empty($_POST["expensesStaff"]) || !empty($_POST["expensesDate"]) || !empty($_POST["expensesCost"]) || !empty($_POST["expensesQuantity"])) {
								try{
									$objBillExpenses = new BillExpenses;
									$objBillExpenses->set_expenseCourtCaseId($objBillExpenses->CleanData($_POST["expenseCourtCaseId"]));
									$objBillExpenses->set_expensesStaff($objBillExpenses->CleanData($_POST["expensesStaff"]));
									$objBillExpenses->set_expensesActivity($objBillExpenses->CleanData($_POST["expensesActivity"]));
									// for adding invoice
									if (empty($_POST["expensesBillable"])) {
										$billAble = "off";
									}
									elseif (!empty($_POST["expensesBillable"])) {
										$billAble = $_POST["expensesBillable"];
									}
									$objBillExpenses->set_expensesBillable($objBillExpenses->CleanData($billAble));
									$objBillExpenses->set_expensesDescribe($objBillExpenses->CleanData($_POST["expensesDescribe"]));
									$objBillExpenses->set_expensesDate($objBillExpenses->CleanData($_POST["expensesDate"]));
									$objBillExpenses->set_expensesCost($objBillExpenses->CleanData($_POST["expensesCost"]));
									$objBillExpenses->set_expensesQuantity($objBillExpenses->CleanData($_POST["expensesQuantity"]));
									// calculating the total from the rate and time in cedis
									$total = $_POST["expensesCost"] * $_POST["expensesQuantity"];
									$objBillExpenses->set_expensesTotal($objBillExpenses->CleanData($total));
									$objBillExpenses->set_expensesInvoiced("NO");
									
									if ($objBillExpenses->insert()) {
										echo "success";
									}
									else{
										echo "error";
									}
								} catch (Exception $e) {
									echo "error";
								}
							}
							else{
								echo "error";
							}
							
						break;
					// for update
						case 'update':
							if (!empty($_POST["expenseCourtCaseId"]) || !empty($_POST["expensesDate"]) || !empty($_POST["expensesCost"]) || !empty($_POST["expensesQuantity"])) {
								try{
									$objBillExpenses = new BillExpenses;
									$objBillExpenses->set_expenseCourtCaseId($objBillExpenses->CleanData($_POST["expenseCourtCaseId"]));
									$objBillExpenses->set_expensesActivity($objBillExpenses->CleanData($_POST["expensesActivity"]));
									// for adding invoice
									if (empty($_POST["expensesBillable"])) {
										$billAble = "off";
									}
									elseif (!empty($_POST["expensesBillable"])) {
										$billAble = $_POST["expensesBillable"];
									}
									$objBillExpenses->set_expensesBillable($objBillExpenses->CleanData($billAble));
									$objBillExpenses->set_expensesDescribe($objBillExpenses->CleanData($_POST["expensesDescribe"]));
									$objBillExpenses->set_expensesDate($objBillExpenses->CleanData($_POST["expensesDate"]));
									$objBillExpenses->set_expensesCost($objBillExpenses->CleanData($_POST["expensesCost"]));
									$objBillExpenses->set_expensesQuantity($objBillExpenses->CleanData($_POST["expensesQuantity"]));
									// calculating the total from the rate and time in cedis
									$total = $_POST["expensesCost"] * $_POST["expensesQuantity"];
									$objBillExpenses->set_expensesTotal($objBillExpenses->CleanData($total));
									$objBillExpenses->set_id($objBillExpenses->CleanData($_POST["data_id"]));
									
									if ($objBillExpenses->update()) {
										echo "success";
									}
									else{
										echo "error";
									}
								} catch (Exception $e) {
									echo "error";
								}
							}
							else{
								echo "error";
							}
						break;
					// for delete
						case 'delete':
							if(isset($_POST["data_id"])){
								try{
								  $objBillExpenses = new BillExpenses;
								  $objBillExpenses->set_recordHide("YES");
							      $objBillExpenses->set_id($objBillExpenses->CleanData($_POST["data_id"]));
							      if ($objBillExpenses->delete()) {
							      	echo "success";
							      }
							      else{
							      	echo "error";
							      }
							    } catch (Exception $e) {
									echo "error";
								}
							 }else{die();}
						break;
						// geting details of a member with id
						case 'updateModal':
							if(!empty($_POST["data_id"])){
								try{
								  $objBillExpenses = new BillExpenses;  
							      $objBillExpenses->set_id($objBillExpenses->CleanData($_POST["data_id"]));
							      $pages_details = $objBillExpenses->get_expenses_by_id();
							      print_r($pages_details);
							    } catch (Exception $e) {
									echo "error";
								}  
							 }else{

							 	echo "error";
							 }
						break;
						case 'getExpenses_by_caseId':
							if(!empty($_POST["caseId"])){
								try{
								  $objBillExpenses = new BillExpenses;  
							      $objBillExpenses->set_expenseCourtCaseId($objBillExpenses->CleanData($_POST["caseId"]));
							      $objBillExpenses->set_expensesInvoiced("NO");
							      $objBillExpenses->set_expensesBillable("on");
							      $details = $objBillExpenses->get_billable_expenses_by_case();
							      print_r($details); 
							    } catch (Exception $e) {
									echo "error";
								} 
							}else{

							 	echo "error";
							}
						break;
						// get all
						case 'getAll':
							$objBillExpenses = new BillExpenses;
							print_r(json_encode($objBillExpenses->get_expenses(),true));

						break;
						default:
							die();
							break;
					}

				}
			}

	$objbillExpensesController = new billExpensesController;
 ?>