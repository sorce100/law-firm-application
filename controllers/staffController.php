<?php
	require_once("../Classes/Staff.php"); 
	session_start();
	class StaffController{
		function __construct(){
			$folderPath = "../uploads/staffpic/";
			$returnedStaffpic="";
					switch (trim($_POST["mode"])) {
						// for insert
						case 'insert':
							if ($_POST["staffFirstName"] != "" || $_POST["staffLastName"] != "" || $_POST["stafftel"] != "") {

								if ($_FILES['staffPic']['size'] != 0) {
										// declaring a new variable forthe gloabl 
										$files=$_FILES['staffPic'];
										$allowed=array('png','jpeg','jpg');

										$filename = $_FILES['staffPic']['name'];
										$file_tmp =$_FILES['staffPic']['tmp_name'];
										$file_size =$_FILES['staffPic']['size'];
										$file_error = $_FILES['staffPic']['error'];
										$file_ext=explode('.',$filename);
										$filename= current($file_ext);
										$file_ext = strtolower(end($file_ext));

										if (!in_array($file_ext,$allowed)) {
											// for delete folders and its content when one file is uploaded

											if (is_dir($folderPath)) {
												// deleting the files and the folder 
												$files = scandir($folderPath);
												foreach ($files as $file) {
													if (($file === '.')||  ($file === '...') || ($file === '..')) {
														continue;
													}else{
														// die($file);
														unlink($folderPath.$file);
													}
												}
												// now delete folder
												if (rmdir($folderPath)) {
													echo "WRONG FILES UPLOADED";
													die();
												}
											}
										}
										else if (in_array($file_ext,$allowed)) {
											// check if ther is any errors from the uploaded file
											if ($file_error === 0) {
												// check if file uploaded has ther righ size
												if ($file_size < 100000000000000000) {
													// creating unique ids for uploads instead of names of files, number is in microsecond
													$filenewname=$filename.".".$file_ext;
													// $structure="/uploads/agent_files/".$folder."/";				
													$filedestination = $folderPath.$filenewname;
													// moving file from temporal location to permanent storage
													$fileup=move_uploaded_file($file_tmp,$filedestination);
												}
													// end of if to check file size
											}
													// end of file erroe
													else{
														echo "error";
														die();
													}
										}
												// end of if file is allowed
									// if file is uploaded successfully then save details in database
										if ($fileup) {
											$objStaff = new Staff;
											$objStaff->set_staffFirstName($objStaff->CleanData($_POST["staffFirstName"]));
											$objStaff->set_staffMidName($objStaff->CleanData($_POST["staffMidName"]));
											$objStaff->set_staffLastName($objStaff->CleanData($_POST["staffLastName"]));
											$objStaff->set_stafftel($objStaff->CleanData($_POST["stafftel"]));
											$objStaff->set_staffemergencytel($objStaff->CleanData($_POST["staffemergencytel"]));
											$objStaff->set_staffEmail($objStaff->CleanData($_POST["staffEmail"]));
											$objStaff->set_staffDob($objStaff->CleanData($_POST["staffDob"]));
											$objStaff->set_staffPostal($objStaff->CleanData($_POST["staffPostal"]));
											$objStaff->set_staffHouseNum($objStaff->CleanData($_POST["staffHouseNum"]));
											$objStaff->set_staffHouseLoc($objStaff->CleanData($_POST["staffHouseLoc"]));
											$objStaff->set_staffType($objStaff->CleanData($_POST["staffType"]));
											$objStaff->set_staffProfNum($objStaff->CleanData($_POST["staffProfNum"]));
											$objStaff->set_staffEmployDate($objStaff->CleanData($_POST["staffEmployDate"]));
											$objStaff->set_staffNote($objStaff->CleanData($_POST["staffNote"]));
											$objStaff->set_staffPic($filenewname);
											$Id = $objStaff->insert();
											if ($Id) {
												echo "success";
											}
											else{
												echo "error";
											}
										}
									}
									else{
										$staffPic = "NONE";
										// save when no image is uploaded
										$objStaff = new Staff;
										$objStaff->set_staffFirstName($objStaff->CleanData($_POST["staffFirstName"]));
										$objStaff->set_staffMidName($objStaff->CleanData($_POST["staffMidName"]));
										$objStaff->set_staffLastName($objStaff->CleanData($_POST["staffLastName"]));
										$objStaff->set_stafftel($objStaff->CleanData($_POST["stafftel"]));
										$objStaff->set_staffemergencytel($objStaff->CleanData($_POST["staffemergencytel"]));
										$objStaff->set_staffEmail($objStaff->CleanData($_POST["staffEmail"]));
										$objStaff->set_staffDob($objStaff->CleanData($_POST["staffDob"]));
										$objStaff->set_staffPostal($objStaff->CleanData($_POST["staffPostal"]));
										$objStaff->set_staffHouseNum($objStaff->CleanData($_POST["staffHouseNum"]));
										$objStaff->set_staffHouseLoc($objStaff->CleanData($_POST["staffHouseLoc"]));
										$objStaff->set_staffType($objStaff->CleanData($_POST["staffType"]));
										$objStaff->set_staffProfNum($objStaff->CleanData($_POST["staffProfNum"]));
										$objStaff->set_staffEmployDate($objStaff->CleanData($_POST["staffEmployDate"]));
										$objStaff->set_staffNote($objStaff->CleanData($_POST["staffNote"]));
										$objStaff->set_staffPic($staffPic);
										$Id = $objStaff->insert();
										if ($Id) {
												echo "success";
											}
											else{
												echo "error";
											}
									}
								}
								else{
									echo "error";
								}


							
						break;
					// for update
						// getting the daved staffpic
						$objStaff = new Staff;
						$objStaff->set_id($objStaff->CleanData($_POST["data_id"]));
						$returnedStaffpic =  $objStaff->staffCheck_id();

						case 'update':
							if ($_POST["staffFirstName"] != "" || $_POST["staffLastName"] != "" || $_POST["stafftel"] != "") {

								if ($_FILES['staffPic']['size'] != 0) {
										// declaring a new variable forthe gloabl 
										$files=$_FILES['staffPic'];
										$allowed=array('png','jpeg','jpg');

										$filename = $_FILES['staffPic']['name'];
										$file_tmp =$_FILES['staffPic']['tmp_name'];
										$file_size =$_FILES['staffPic']['size'];
										$file_error = $_FILES['staffPic']['error'];
										$file_ext=explode('.',$filename);
										$filename= current($file_ext);
										$file_ext = strtolower(end($file_ext));

										if (!in_array($file_ext,$allowed)) {
											// for delete folders and its content when one file is uploaded

											if (is_dir($folderPath)) {
												// deleting the files and the folder 
												$files = scandir($folderPath);
												foreach ($files as $file) {
													if (($file === '.')||  ($file === '...') || ($file === '..')) {
														continue;
													}else{
														// die($file);
														unlink($folderPath.$file);
													}
												}
												// now delete folder
												if (rmdir($folderPath)) {
													echo "WRONG FILES UPLOADED";
													die();
												}
											}
										}
										else if (in_array($file_ext,$allowed)) {
											// check if ther is any errors from the uploaded file
											if ($file_error === 0) {
												// check if file uploaded has ther righ size
												if ($file_size < 100000000000000000) {
													// creating unique ids for uploads instead of names of files, number is in microsecond
													$filenewname=$filename.".".$file_ext;
													// $structure="/uploads/agent_files/".$folder."/";				
													$filedestination = $folderPath.$filenewname;
													// moving file from temporal location to permanent storage
													$fileup=move_uploaded_file($file_tmp,$filedestination);
												}
													// end of if to check file size
											}
											// end of file erroe
											else{
												echo "error";
												die();
											}
										}
												// end of if file is allowed
										// delete old image from uploads folder
											if ($returnedStaffpic != "NONE") {
												$path = $folderPath.$returnedStaffpic;
												chmod($path, 0666);
												unlink($path);
											}

									// if file is uploaded successfully then save details in database
										if ($fileup) {
											$objStaff = new Staff;
											$objStaff->set_staffFirstName($objStaff->CleanData($_POST["staffFirstName"]));
											$objStaff->set_staffMidName($objStaff->CleanData($_POST["staffMidName"]));
											$objStaff->set_staffLastName($objStaff->CleanData($_POST["staffLastName"]));
											$objStaff->set_stafftel($objStaff->CleanData($_POST["stafftel"]));
											$objStaff->set_staffemergencytel($objStaff->CleanData($_POST["staffemergencytel"]));
											$objStaff->set_staffEmail($objStaff->CleanData($_POST["staffEmail"]));
											$objStaff->set_staffDob($objStaff->CleanData($_POST["staffDob"]));
											$objStaff->set_staffPostal($objStaff->CleanData($_POST["staffPostal"]));
											$objStaff->set_staffHouseNum($objStaff->CleanData($_POST["staffHouseNum"]));
											$objStaff->set_staffHouseLoc($objStaff->CleanData($_POST["staffHouseLoc"]));
											$objStaff->set_staffType($objStaff->CleanData($_POST["staffType"]));
											$objStaff->set_staffProfNum($objStaff->CleanData($_POST["staffProfNum"]));
											$objStaff->set_staffEmployDate($objStaff->CleanData($_POST["staffEmployDate"]));
											$objStaff->set_staffNote($objStaff->CleanData($_POST["staffNote"]));
											$objStaff->set_staffPic($filenewname);
											$objStaff->set_id($objStaff->CleanData($_POST["data_id"]));
											if ($objStaff->update()) {
												echo "success";
											}
											else{
												echo "error";
											}
										}
									}
									else{
										// save when no image is uploaded
										$objStaff = new Staff;
										$objStaff->set_staffFirstName($objStaff->CleanData($_POST["staffFirstName"]));
										$objStaff->set_staffMidName($objStaff->CleanData($_POST["staffMidName"]));
										$objStaff->set_staffLastName($objStaff->CleanData($_POST["staffLastName"]));
										$objStaff->set_stafftel($objStaff->CleanData($_POST["stafftel"]));
										$objStaff->set_staffemergencytel($objStaff->CleanData($_POST["staffemergencytel"]));
										$objStaff->set_staffEmail($objStaff->CleanData($_POST["staffEmail"]));
										$objStaff->set_staffDob($objStaff->CleanData($_POST["staffDob"]));
										$objStaff->set_staffPostal($objStaff->CleanData($_POST["staffPostal"]));
										$objStaff->set_staffHouseNum($objStaff->CleanData($_POST["staffHouseNum"]));
										$objStaff->set_staffHouseLoc($objStaff->CleanData($_POST["staffHouseLoc"]));
										$objStaff->set_staffType($objStaff->CleanData($_POST["staffType"]));
										$objStaff->set_staffProfNum($objStaff->CleanData($_POST["staffProfNum"]));
										$objStaff->set_staffEmployDate($objStaff->CleanData($_POST["staffEmployDate"]));
										$objStaff->set_staffNote($objStaff->CleanData($_POST["staffNote"]));
										$objStaff->set_staffPic($returnedStaffpic);
										$objStaff->set_id($objStaff->CleanData($_POST["data_id"]));
										if ($objStaff->update()) {
											echo "success";
										}
										else{
											echo "error";
										}
									}
								}
								else{
									echo "error";
								}
						break;
					// for delete
						case 'delete':
							if(isset($_POST["data_id"])){
								 $objStaff = new Staff;
								  $objStaff->set_recordHide("YES");
							      $objStaff->set_id($objStaff->CleanData($_POST["data_id"]));
							      if ($objStaff->delete()) {
							      	echo "success";
							      }
							      else{
							      	echo "error";
							      }
							     
							 }else{die();}
						break;
						// geting details of a member with id
						case 'updateModal':
							if($_POST["data_id"] != ""){
									 $objStaff = new Staff;  
								      $objStaff->set_id($objStaff->CleanData($_POST["data_id"]));
								      $staff_details = $objStaff->get_staff_by_id();
								      print_r($staff_details);  
								 }else{
								 	echo "error";
								 }
						break;
						// get all
						case 'getAll':
							$objStaff = new Staff;
							print_r(json_encode($objStaff->get_staff(),true));

						break;
						// get staff name only
						case 'staff_name_only':
							$objStaff = new Staff;
							print_r($objStaff->get_staffName_only($objStaff->CleanData($_POST["data_id"])));

						break;
						// get staff checkbox select
						case 'getStaffData':
							if(!empty($_POST["caseStaffIds"] )){
								  $returnRecords = '';
								  $caseStaffIdsArray = json_decode($_POST["caseStaffIds"],true);
								  $objStaff = new Staff;
							      foreach ($caseStaffIdsArray as $caseStaff) {
							      	  $objStaff->set_id($objStaff->CleanData($caseStaff));
								      $details = $objStaff->get_staff_by_id();
								      if (!empty($details)) {
								      	$returnRecords .='<tr>
								      						<td>'.$details["staff_firstname"].' '.$details["staff_lastname"].'</td>
								      						<td>
								      							<input type="checkbox" name="eventStaffShare[]" class="eventStaffShare" value="'.$details["staff_id"].'">
								      						</td>
										                  </tr>';
								      }
							      }
							      print_r($returnRecords);  
							 }else{
							 	echo "error";
							 }
						default:
							die();
							break;
					}

				}
			}

	$objStaffController = new StaffController;
 ?>