<?php
	require_once("../Classes/Messages.php"); 
	session_start();
	class messagesController{
		function __construct(){
			// print_r($_POST);
					switch (trim($_POST["mode"])) {
						// for insert
						case 'insert':
						// print_r($_POST);
						// die();
							if (!empty($_POST["messageSubject"])) {
								try{
									$objMessages = new Messages;
									$objMessages->set_message_courtCaseId($objMessages->CleanData($_POST["message_courtCaseId"]));
									$objMessages->set_messageContactIds(json_encode($_POST["messageContactIds"]));
									$objMessages->set_messageStaffIds(json_encode($_POST["messageStaffIds"]));
									$objMessages->set_messageSubject($objMessages->CleanData($_POST["messageSubject"]));
									$objMessages->set_messageContent($_POST["messageContent"]);
									$objMessages->set_messageStatus("UNREAD");
									if ($objMessages->insert()) {
										echo "success";
									}
									else{
										echo "error";
									}
								} catch (Exception $e) {
									
								}
							}
							else{
								echo "error";
							}
							
						break;
					// for update
						case 'update':
							if (!empty($_POST["messageSubject"])) {
								try{
									$objMessages = new Messages;
									$objMessages->set_message_courtCaseId($objMessages->CleanData($_POST["message_courtCaseId"]));
									$objMessages->set_messageContactIds(json_encode($_POST["messageContactIds"]));
									$objMessages->set_messageStaffIds(json_encode($_POST["messageStaffIds"]));
									$objMessages->set_messageSubject($objMessages->CleanData($_POST["messageSubject"]));
									$objMessages->set_messageContent($_POST["messageContent"]);
									$objMessages->set_id($objMessages->CleanData($_POST["data_id"]));
									if ($objMessages->update()) {
										echo "success";
									}
									else{
										echo "error";
									}
								} catch (Exception $e) {
									
								}
							}
							else{
								echo "error";
							}
						break;
					// for delete
						case 'receiverDelete':
							if(isset($_POST["data_id"])){
								try{
								  $objMessages = new Messages;
								  $objMessages->set_receiverRecordHide("YES");
							      $objMessages->set_id($objMessages->CleanData($_POST["data_id"]));
							      if ($objMessages->receive_delete()) {
							      	echo "success";
							      }
							      else{
							      	echo "error";
							      }
							    } catch (Exception $e) {
									
								}
							 }else{die();}
						break;
						case 'senderDelete':
							if(isset($_POST["data_id"])){
								try{
								  $objMessages = new Messages;
								  $objMessages->set_senderRecordHide("YES");
							      $objMessages->set_id($objMessages->CleanData($_POST["data_id"]));
							      if ($objMessages->sender_delete()) {
							      	echo "success";
							      }
							      else{
							      	echo "error";
							      }
							    } catch (Exception $e) {
									
								}
							 }else{die();}
						break;
						// geting details of a member with id
						case 'updateModal':
							if(isset($_POST["data_id"] )){
								try{
								  $objMessages = new Messages;  
							      $objMessages->set_id($objMessages->CleanData($_POST["data_id"]));
							      $details = $objMessages->get_messages_by_id();
							      print_r($details);
							    } catch (Exception $e) {
									
								}  
							 }else{
							 	echo "error";
							 }
						break;
						// get all
						case 'getAll_sent':
							$objMessages = new Messages;
							print_r(json_encode($objMessages->get_sent_messages(),true));
						break;
						case 'getAll_received':
							$objMessages = new Messages;
							print_r(json_encode($objMessages->get_staff_received_messages(),true));
						break;
						// close case
						case 'get_case_messages':
							$objMessages = new Messages;
							$objMessages->set_message_courtCaseId($objMessages->CleanData($_POST["caseId"]));
							print_r(json_encode($objMessages->get_court_case_messages(),true));
							
						break;
						default:
							die();
							break;
					}

				}
			}

	$objmessagesController = new messagesController;
 ?>