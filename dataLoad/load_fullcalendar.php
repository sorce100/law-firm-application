<?php
$connect = new PDO('mysql:host=localhost;dbname=lawoffice', 'root', '');
$data = array();
$query = "SELECT e.event_id,e.event_name,e.event_start_date,e.event_end_Date,t.event_type_id,t.event_type_color  
FROM events AS e 
LEFT JOIN event_type AS t
ON e.event_typeId = t.event_type_id";

$statement = $connect->prepare($query);
$statement->execute();
$result = $statement->fetchAll();
foreach($result as $row){
 $data[] = array(
  'id'   => $row["event_id"],
  'title'   => $row["event_name"],
  'start'   => $row["event_start_date"],
  'end'   => $row["event_end_Date"],
  'backgroundColor'   => $row["event_type_color"]

 );
}

echo json_encode($data);

?>
