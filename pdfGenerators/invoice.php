<?php
require('../vendors/fpdf/fpdf.php');

ob_end_clean(); //    the buffer and never prints or returns anything.
ob_start(); // it starts buffering

$pdf = new FPDF('P','mm','A4');
$pdf->AddPage();
////////////////////////////////////////////////////////////////////////
// logo and invoice
////////////////////////////////////////////////////////////////////////
// margin-left,heght,image-width,image-height
$pdf->Image("../pages/images/original.jpg",10,10,50,20);
$pdf->Cell(145,10,'',0,0);
// writing invoice
$pdf->SetFont('Arial','',30);
$pdf->SetTextColor(80,80,80);
$pdf->Cell(50,10,'INVOICE',0,1);
$pdf->Ln(12);
////////////////////////////////////////////////////////////////////////
// for address and tel No
$pdf->SetFont('Times','',12);
$pdf->MultiCell(100,6,"2nd Floor, Nalag House\nNext to GS Plaza Gulf House Avenue\nOff Tetteh Quarshie Interchange\n\n+233 26 315 8606 ", '', 'L');
$pdf->Ln(3);
////////////////////////////////////////////////////////////////////////
// for invoice date and number
$pdf->SetLeftMargin(125);

$width_cell=array(40,35,25,35);
$pdf->SetFillColor(241,241,241); // Background color of header 
// Header starts /// 
$pdf->Cell(40,8,'Invoice Date',1,0,'C',true); // First header column 
$pdf->Cell(35,8,'Invoice Number',1,1,'C',true); // Second header column

$pdf->Cell(40,8,date('d-m-Y'),1,0,'C',false); // First column of row 1 
$pdf->Cell(35,8,'12314132',1,0,'C',false); // Second column

$pdf->SetLeftMargin(10);
$pdf->Ln(17);

////////////////////////////////////////////////////////////////////////
// for project time Entery
$pdf->SetFillColor(255,255,255); // Background color of header 
$pdf->SetFont('Times','',12);
$pdf->Cell(190,8,'In Reference To : CASE AGAINST MERLIN (1x-354) - Labor',1,1,'L',true);
$pdf->SetFillColor(241,241,241); // Background color of header 
// header
$pdf->SetFont('Times','B',12);
$width_cell=array(30,75,25,30,30);
$pdf->Cell($width_cell[0],8,'Date',1,0,'L',true); // First header column 
$pdf->Cell($width_cell[1],8,'Services',1,0,'L',true); // Second header column
$pdf->Cell($width_cell[2],8,'Hours',1,0,'R',true); // third header column
$pdf->Cell($width_cell[3],8,'Rates',1,0,'R',true); // fourth header column
$pdf->Cell($width_cell[4],8,'Amount',1,1,'R',true); // fifth header column
// header
// body
// tr
$pdf->SetFont('Times','',12);
$pdf->Cell($width_cell[0],8,date('d-m-Y'),1,0,'L',false); // 1 
// $pdf->MultiCell($width_cell[1],8,'Met with client to discuss several items relating to this project','LRTB',1,0, 'L');
 
$pdf->Cell($width_cell[1],8,'Met with client to discuss several Issues',1,0,'L',false); // 2
$pdf->Cell($width_cell[2],8,'4.0',1,0,'R',false); // 2
$pdf->Cell($width_cell[3],8,'20.00/hr',1,0,'R',false); // 3 
$pdf->Cell($width_cell[4],8,'80',1,1,'R',false); // 4
// tr
$pdf->Cell($width_cell[0],8,date('d-m-Y'),1,0,'L',false); // 1 
$pdf->Cell($width_cell[1],8,'Researched Several possible for case',1,0,'L',false); // First column of 
$pdf->Cell($width_cell[2],8,'6.0',1,0,'R',false); // 2
$pdf->Cell($width_cell[3],8,'10.00/hr',1,0,'R',false); // 3 
$pdf->Cell($width_cell[4],8,'60',1,1,'R',false); // 4
$pdf->Ln(7);
// body
//////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////
// for project time Entery
$pdf->SetFillColor(255,255,255); // Background color of header 
$pdf->SetFont('Times','',12);
$pdf->Cell(190,8,'In Reference To : CASE AGAINST MERLIN (1x-354) - Expenses',1,1,'L',true);
$pdf->SetFillColor(241,241,241); // Background color of header 
// header
$pdf->SetFont('Times','B',12);
$width_cell=array(30,130,30);
$pdf->Cell($width_cell[0],8,'Date',1,0,'L',true); // First header column 
$pdf->Cell($width_cell[1],8,'Expenses',1,0,'L',true); // Second header column
$pdf->Cell($width_cell[2],8,'Amount',1,1,'R',true); // fifth header column
// header
// body
// tr
$pdf->SetFont('Times','',12);
$pdf->Cell($width_cell[0],8,date('d-m-Y'),1,0,'L',false); // 1 
$pdf->Cell($width_cell[1],8,'Launch with Client',1,0,'L',false); // First column of 3 
$pdf->Cell($width_cell[2],8,'300',1,1,'R',false); // 4
// tr
$pdf->Cell($width_cell[0],8,date('d-m-Y'),1,0,'L',false); // 1 
$pdf->Cell($width_cell[1],8,'Filing Motion',1,0,'L',false);  
$pdf->Cell($width_cell[2],8,'600',1,1,'R',false); // 4
$pdf->Ln(10);
// body
//////////////////////////////////////////////////////////////////////////////////////////////////
//for footer
////////////////////////////////////////////////////////////////////////////////////////////////

$pdf->SetLeftMargin(110);
$width_cell=array(60,30);
// name 1
$pdf->SetFillColor(241,241,241); // Background color of header 
$pdf->SetFont('Times','B',12);
$pdf->Cell($width_cell[0],8,'Total Hours',1,0,'L',true);
// value 1
$pdf->SetFont('Times','',12);
$pdf->SetFillColor(255,255,255); // Background color of header 
$pdf->Cell($width_cell[1],8,'10.00 hrs',1,1,'R',true);
// name 2
$pdf->SetFillColor(241,241,241); // Background color of header 
$pdf->SetFont('Times','B',12);
$pdf->Cell($width_cell[0],8,'Total Expenses',1,0,'L',true);
// value 2
$pdf->SetFont('Times','',12);
$pdf->SetFillColor(255,255,255); // Background color of header 
$pdf->Cell($width_cell[1],8,'300.00',1,1,'R',true);// name
// nam 3
$pdf->SetFillColor(241,241,241); // Background color of header 
$pdf->SetFont('Times','B',12);
$pdf->Cell($width_cell[0],8,'Total Invoice Amount',1,0,'L',true);
// value 3
$pdf->SetFont('Times','',12);
$pdf->SetFillColor(255,255,255); // Background color of header 
$pdf->Cell($width_cell[1],8,'100.00',1,1,'R',true);// name
// name 4
$pdf->SetFillColor(241,241,241); // Background color of header 
$pdf->SetFont('Times','B',12);
$pdf->Cell($width_cell[0],8,'Balance (Amount Due)',1,0,'L',true);
// value 4
$pdf->SetFont('Times','',12);
$pdf->SetFillColor(255,255,255); // Background color of header 
$pdf->Cell($width_cell[1],8,'100.00',1,1,'R',true);



$pdf->Output('D','Law Office Invoice.pdf');
ob_end_flush();
?>