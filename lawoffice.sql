-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 20, 2019 at 03:22 AM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lawoffice`
--

-- --------------------------------------------------------

--
-- Table structure for table `activities`
--

CREATE TABLE `activities` (
  `activity_id` int(11) NOT NULL,
  `table_name` varchar(50) NOT NULL,
  `table_id` int(11) NOT NULL,
  `activity_name` varchar(200) NOT NULL,
  `account_id` int(11) NOT NULL,
  `account_name` varchar(10) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date_done` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `activities`
--

INSERT INTO `activities` (`activity_id`, `table_name`, `table_id`, `activity_name`, `account_id`, `account_name`, `user_id`, `date_done`) VALUES
(288, 'users', 6, 'Loged In', 9, 'staff', 6, '2019-11-05 21:51:59'),
(289, 'case_document', 1, 'Updated Case Document', 9, 'staff', 6, '2019-11-05 22:58:41'),
(290, 'case_document', 8, 'Updated Case Document', 9, 'staff', 6, '2019-11-05 22:59:45'),
(291, 'case_document', 8, 'Updated Case Document', 9, 'staff', 6, '2019-11-05 23:04:46'),
(292, 'tasks', 1, 'Updated Tasks', 9, 'staff', 6, '2019-11-05 23:50:20'),
(293, 'tasks', 1, 'Updated Tasks', 9, 'staff', 6, '2019-11-05 23:50:26'),
(294, 'tasks', 1, 'Updated Tasks', 9, 'staff', 6, '2019-11-05 23:50:31'),
(295, 'tasks', 4, 'Updated Tasks', 9, 'staff', 6, '2019-11-05 23:56:50'),
(296, 'tasks', 5, 'Added New Tasks', 9, 'staff', 6, '2019-11-05 23:58:22'),
(297, 'tasks', 5, 'Updated Tasks', 9, 'staff', 6, '2019-11-06 00:08:30'),
(298, 'tasks', 5, 'Tasks Completed', 9, 'staff', 6, '2019-11-06 00:09:44'),
(299, 'tasks', 5, 'Tasks Completed', 9, 'staff', 6, '2019-11-06 00:10:35'),
(300, 'tasks', 5, 'Tasks Completed', 9, 'staff', 6, '2019-11-06 00:12:06'),
(301, 'users', 6, 'Loged In', 9, 'staff', 6, '2019-11-06 09:24:35'),
(302, 'users', 6, 'Loged In', 9, 'staff', 6, '2019-11-06 11:04:51'),
(303, 'users', 6, 'Loged In', 9, 'staff', 6, '2019-11-06 12:00:16'),
(304, 'users', 6, 'Loged In', 9, 'staff', 6, '2019-11-06 13:40:21'),
(305, 'users', 6, 'Loged In', 9, 'staff', 6, '2019-11-06 14:15:01'),
(306, 'users', 6, 'Loged In', 9, 'staff', 6, '2019-11-06 18:42:57'),
(307, 'users', 6, 'Loged In', 9, 'staff', 6, '2019-11-06 19:22:49'),
(308, 'events', 23, 'Added New Event', 9, 'staff', 6, '2019-11-06 20:41:29'),
(309, 'events', 24, 'Added New Event', 9, 'staff', 6, '2019-11-07 00:10:18'),
(310, 'events', 25, 'Added New Event', 9, 'staff', 6, '2019-11-07 00:43:10'),
(311, 'events', 26, 'Added New Event', 9, 'staff', 6, '2019-11-07 01:03:22'),
(312, 'users', 6, 'Loged In', 9, 'staff', 6, '2019-11-07 10:35:33'),
(313, 'court_case', 6, 'Added New Court Case', 9, 'staff', 6, '2019-11-07 14:27:24'),
(314, 'court_case', 6, 'Updated Court Case', 9, 'staff', 6, '2019-11-07 14:32:06'),
(315, 'bill_time_entry', 6, 'Deleted Time Entry', 9, 'staff', 6, '2019-11-07 14:36:12'),
(316, 'court_case', 6, 'Deleted Court Case', 9, 'staff', 6, '2019-11-07 14:36:13'),
(317, 'bill_time_entry', 6, 'Deleted Time Entry', 9, 'staff', 6, '2019-11-07 14:37:17'),
(318, 'bill_time_entry', 6, 'Deleted Time Entry', 9, 'staff', 6, '2019-11-07 14:39:40'),
(319, 'court_case', 6, 'Deleted Court Case', 9, 'staff', 6, '2019-11-07 14:39:40'),
(320, 'court_case', 6, 'Deleted Court Case', 9, 'staff', 6, '2019-11-07 14:40:21'),
(321, 'court_case', 6, 'Deleted Court Case', 9, 'staff', 6, '2019-11-07 14:40:49'),
(322, 'court_case', 6, 'Deleted Court Case', 9, 'staff', 6, '2019-11-07 14:42:56'),
(323, 'court_case', 6, 'Deleted Court Case', 9, 'staff', 6, '2019-11-07 14:43:32'),
(324, 'users', 6, 'Loged In', 9, 'staff', 6, '2019-11-07 23:08:52'),
(325, 'users', 6, 'Loged In', 9, 'staff', 6, '2019-11-08 07:21:28'),
(326, 'users', 6, 'Loged In', 9, 'staff', 6, '2019-11-08 12:44:00'),
(327, 'users', 6, 'Loged In', 9, 'staff', 6, '2019-11-08 12:44:07'),
(328, 'users', 6, 'Loged In', 9, 'staff', 6, '2019-11-08 12:53:15'),
(329, 'users', 6, 'Loged In', 9, 'staff', 6, '2019-11-08 13:28:58'),
(330, 'users', 6, 'Loged In', 9, 'staff', 6, '2019-11-08 15:32:48'),
(331, 'users', 6, 'Loged In', 9, 'staff', 6, '2019-11-08 17:56:54'),
(332, 'users', 6, 'Loged In', 9, 'staff', 6, '2019-11-08 21:25:54'),
(333, 'users', 6, 'Loged In', 9, 'staff', 6, '2019-11-09 17:56:46'),
(334, 'users', 6, 'Loged In', 9, 'staff', 6, '2019-11-09 21:03:00'),
(335, 'users', 19, 'Loged In', 3, 'contact', 19, '2019-11-09 21:06:34'),
(336, 'users', 6, 'Loged In', 9, 'staff', 6, '2019-11-09 21:07:03'),
(337, 'court_case_notes', 7, 'Added New Case Note', 9, 'staff', 6, '2019-11-09 23:08:49'),
(338, 'court_case_notes', 0, 'Updated Case Note', 9, 'staff', 6, '2019-11-09 23:11:01'),
(339, 'court_case_notes', 0, 'Updated Case Note', 9, 'staff', 6, '2019-11-09 23:11:38'),
(340, 'court_case_notes', 0, 'Updated Case Note', 9, 'staff', 6, '2019-11-09 23:12:13'),
(341, 'court_case_notes', 0, 'Updated Case Note', 9, 'staff', 6, '2019-11-09 23:12:36'),
(342, 'court_case_notes', 0, 'Updated Case Note', 9, 'staff', 6, '2019-11-09 23:12:54'),
(343, 'court_case_notes', 7, 'Updated Case Note', 9, 'staff', 6, '2019-11-09 23:13:55'),
(344, 'court_case_notes', 7, 'Updated Case Note', 9, 'staff', 6, '2019-11-09 23:19:18'),
(345, 'court_case_notes', 7, 'Deleted Case Note', 9, 'staff', 6, '2019-11-09 23:25:53'),
(346, 'court_case_notes', 5, 'Updated Case Note', 9, 'staff', 6, '2019-11-09 23:28:54'),
(347, 'court_case_notes', 5, 'Updated Case Note', 9, 'staff', 6, '2019-11-09 23:29:27'),
(348, 'court_case_notes', 3, 'Updated Case Note', 9, 'staff', 6, '2019-11-09 23:29:44'),
(349, 'court_case_notes', 8, 'Added New Case Note', 9, 'staff', 6, '2019-11-09 23:34:14'),
(350, 'court_case_notes', 8, 'Updated Case Note', 9, 'staff', 6, '2019-11-09 23:34:29'),
(351, 'court_case_notes', 8, 'Deleted Case Note', 9, 'staff', 6, '2019-11-09 23:34:35'),
(352, 'court_case_notes', 9, 'Added New Case Note', 9, 'staff', 6, '2019-11-09 23:35:24'),
(353, 'users', 6, 'Loged In', 9, 'staff', 6, '2019-11-10 10:28:30'),
(354, 'bill_time_entry', 12, 'Added New Time Entry', 9, 'staff', 6, '2019-11-10 10:28:46'),
(355, 'users', 6, 'Loged In', 9, 'staff', 6, '2019-11-10 11:11:56'),
(356, 'events', 27, 'Added New Event', 9, 'staff', 6, '2019-11-10 11:35:41'),
(357, 'users', 6, 'Loged In', 9, 'staff', 6, '2019-11-10 12:17:08'),
(358, 'users', 6, 'Loged In', 9, 'staff', 6, '2019-11-10 13:57:51'),
(359, 'users', 6, 'Loged In', 9, 'staff', 6, '2019-11-10 13:57:58'),
(360, 'users', 6, 'Loged In', 9, 'staff', 6, '2019-11-14 07:13:24'),
(361, 'users', 6, 'Loged In', 9, 'staff', 6, '2019-11-14 07:17:17'),
(362, 'users', 6, 'Loged In', 9, 'staff', 6, '2019-11-15 15:50:38');

-- --------------------------------------------------------

--
-- Table structure for table `bill_expenses`
--

CREATE TABLE `bill_expenses` (
  `bill_expenses_id` int(11) NOT NULL,
  `bill_expenses_case_id` int(11) NOT NULL,
  `bill_expenses_staff_id` int(11) NOT NULL,
  `bill_expenses_activity` varchar(255) NOT NULL,
  `bill_expenses_billable` varchar(5) NOT NULL,
  `bill_expenses_describe` text NOT NULL,
  `bill_expenses_date` varchar(50) NOT NULL,
  `bill_expenses_cost` varchar(10) NOT NULL,
  `bill_expenses_quantity` varchar(10) NOT NULL,
  `bill_expenses_total` varchar(10) NOT NULL,
  `bill_expenses_invoiced` varchar(5) NOT NULL,
  `bill_expenses_invoice_num` varchar(10) NOT NULL,
  `added` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL,
  `record_hide` varchar(5) NOT NULL,
  `last_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bill_expenses`
--

INSERT INTO `bill_expenses` (`bill_expenses_id`, `bill_expenses_case_id`, `bill_expenses_staff_id`, `bill_expenses_activity`, `bill_expenses_billable`, `bill_expenses_describe`, `bill_expenses_date`, `bill_expenses_cost`, `bill_expenses_quantity`, `bill_expenses_total`, `bill_expenses_invoiced`, `bill_expenses_invoice_num`, `added`, `user_id`, `record_hide`, `last_updated`) VALUES
(1, 3, 9, 'asdf', 'on', 'asdf', '25-06-2019', '5', '32', '160', 'YES', '019063', '16-06-2019', 6, 'NO', '2019-06-16 17:40:20'),
(2, 3, 9, 'Transport', 'on', 'Transportation for 5 members of the legal team', '16-06-2019', '20', '5', '100', 'YES', '019063', '16-06-2019', 6, 'NO', '2019-06-16 17:56:50'),
(3, 3, 9, 'afssasdf@123', 'on', 'asdfs123@2010', '12-06-2019', '5', '12', '60', 'YES', '019063', '22-06-2019', 6, 'NO', '2019-06-22 22:53:15'),
(4, 3, 9, 'New Expense', 'on', 'testing activity', '26-06-2019', '20', '5', '100', 'NO', '', '26-06-2019', 6, 'YES', '2019-06-26 14:58:01'),
(5, 3, 9, 'afssasdf@123', 'on', 'asdfs123@2010', '12-06-2019', '5', '12', '60', 'NO', '', '16-08-2019', 6, 'NO', '2019-08-16 17:01:51');

-- --------------------------------------------------------

--
-- Table structure for table `bill_invoice`
--

CREATE TABLE `bill_invoice` (
  `bill_invoice_id` int(11) NOT NULL,
  `bill_invoice_staff` int(11) NOT NULL,
  `bill_invoice_number` varchar(10) NOT NULL,
  `bill_invoice_date` varchar(20) NOT NULL,
  `bill_invoice_case_id` int(11) NOT NULL,
  `bill_invoice_contact_id` int(11) NOT NULL,
  `bill_invoice_address` text NOT NULL,
  `bill_invoice_billable` varchar(5) NOT NULL,
  `bill_invoice_timeEntry_ids` text NOT NULL,
  `bill_invoice_timeEntry_total` float NOT NULL,
  `bill_invoice_expenses_ids` text NOT NULL,
  `bill_invoice_expenses_total` float NOT NULL,
  `bill_invoice_main_total` float NOT NULL,
  `added` varchar(20) NOT NULL,
  `user_id` int(11) NOT NULL,
  `record_hide` varchar(5) NOT NULL,
  `last_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bill_invoice`
--

INSERT INTO `bill_invoice` (`bill_invoice_id`, `bill_invoice_staff`, `bill_invoice_number`, `bill_invoice_date`, `bill_invoice_case_id`, `bill_invoice_contact_id`, `bill_invoice_address`, `bill_invoice_billable`, `bill_invoice_timeEntry_ids`, `bill_invoice_timeEntry_total`, `bill_invoice_expenses_ids`, `bill_invoice_expenses_total`, `bill_invoice_main_total`, `added`, `user_id`, `record_hide`, `last_updated`) VALUES
(1, 9, '0190641', '23-06-2019', 3, 2, 'P.O.BOX 2295, ACCRA MADINA', 'on', '[\"2\",\"4\"]', 2289, '[\"1\",\"2\",\"3\"]', 320, 2609, '23-06-2019', 6, 'NO', '2019-06-23 22:23:43'),
(2, 9, '0190613', '23-06-2019', 3, 2, 'P.O.BOX 2295, ACCRA MADINA', 'on', '[\"2\",\"4\"]', 2289, '[\"1\",\"2\",\"3\"]', 320, 2609, '23-06-2019', 6, 'NO', '2019-06-23 22:45:59'),
(3, 9, '019063', '23-06-2019', 3, 2, 'P.O.BOX 2295, ACCRA MADINA', 'on', '[\"2\",\"4\"]', 2289, '[\"1\",\"2\",\"3\"]', 320, 2609, '23-06-2019', 6, 'NO', '2019-06-23 22:46:38'),
(4, 9, '019063', '23-06-2019', 3, 2, 'P.O.BOX 2295, ACCRA MADINA', 'on', 'null', 0, '[\"1\",\"2\",\"3\"]', 320, 320, '23-06-2019', 6, 'NO', '2019-06-23 22:49:22');

-- --------------------------------------------------------

--
-- Table structure for table `bill_time_entry`
--

CREATE TABLE `bill_time_entry` (
  `time_entry_id` int(11) NOT NULL,
  `time_entry_case_id` int(11) NOT NULL,
  `time_entry_staff_id` int(11) NOT NULL,
  `time_entry_activity` varchar(255) NOT NULL,
  `time_entry_billable` varchar(5) NOT NULL,
  `time_entry_describe` text NOT NULL,
  `time_entry_date` varchar(50) NOT NULL,
  `time_entry_rate` varchar(10) NOT NULL,
  `time_entry_rate_type` varchar(10) NOT NULL,
  `time_entry_duration` varchar(10) NOT NULL,
  `time_entry_total` varchar(10) NOT NULL,
  `time_entry_invoiced` varchar(5) NOT NULL,
  `time_entry_invoice_num` varchar(10) NOT NULL,
  `user_id` int(11) NOT NULL,
  `added` varchar(50) NOT NULL,
  `record_hide` varchar(5) NOT NULL,
  `last_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bill_time_entry`
--

INSERT INTO `bill_time_entry` (`time_entry_id`, `time_entry_case_id`, `time_entry_staff_id`, `time_entry_activity`, `time_entry_billable`, `time_entry_describe`, `time_entry_date`, `time_entry_rate`, `time_entry_rate_type`, `time_entry_duration`, `time_entry_total`, `time_entry_invoiced`, `time_entry_invoice_num`, `user_id`, `added`, `record_hide`, `last_updated`) VALUES
(1, 3, 11, 'Uploading Documents', 'off', 'Uploading Documents@123', '13-06-2019', '200', 'hr', '4.3', '860', 'NO', '', 6, '16-06-2019', 'NO', '2019-06-16 09:31:51'),
(2, 3, 9, 'Merlin', 'on', 'merlin123', '16-06-2019', '502', 'flat', '4.5', '2259', 'YES', '019063', 6, '16-06-2019', 'NO', '2019-06-16 09:53:44'),
(3, 2, 11, 'Court', 'on', '', '30-06-2019', '51', 'hr', '0.9', '45.9', 'NO', '', 6, '20-06-2019', 'NO', '2019-06-20 23:18:46'),
(4, 3, 9, 'New Evidence', 'on', '', '22-06-2019', '15', 'hr', '2.0', '30', 'YES', '019063', 6, '22-06-2019', 'YES', '2019-06-22 22:11:27'),
(5, 3, 9, 'Saving Documents', 'on', 'Saving documents into case', '20-06-2019', '5', 'hr', '1.6', '8', 'NO', '', 6, '27-06-2019', 'NO', '2019-06-27 22:40:19'),
(6, 3, 9, 'uio', 'on', 'asdfghjkertyuioxcm,ghjk 890', '17-07-2019', '5', 'hr', '3.0', '15', 'NO', '', 6, '1-07-2019', 'YES', '2019-07-01 18:28:28'),
(7, 0, 9, '', 'on', '', '', '', 'hr', '', '0', 'NO', '', 6, '24-10-2019', 'NO', '2019-10-24 01:17:28'),
(8, 0, 9, '', 'on', '', '', '', 'hr', '', '0', 'NO', '', 6, '24-10-2019', 'YES', '2019-10-24 01:17:47'),
(9, 5, 9, 'asdf', 'on', 'asdf', '24-10-2019', '232', 'hr', '2.0', '464', 'NO', '', 6, '24-10-2019', 'YES', '2019-10-24 22:55:11'),
(10, 0, 9, '', 'on', '', '', '', 'hr', '1.8', '0', 'NO', '', 6, '25-10-2019', 'NO', '2019-10-25 01:04:02'),
(11, 0, 9, '', 'on', '', '', '', 'hr', '', '0', 'NO', '', 6, '5-11-2019', 'NO', '2019-11-05 13:57:50'),
(12, 0, 9, '', 'on', '', '', '', 'hr', '', '0', 'NO', '', 6, '10-11-2019', 'NO', '2019-11-10 10:28:46');

-- --------------------------------------------------------

--
-- Table structure for table `case_document`
--

CREATE TABLE `case_document` (
  `case_doc_id` int(11) NOT NULL,
  `doc_court_case_id` int(11) NOT NULL,
  `doc_folder_name` varchar(50) NOT NULL,
  `doc_files_uploaded` text NOT NULL,
  `doc_assigned_date` varchar(100) NOT NULL,
  `doc_description` text NOT NULL,
  `added` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL,
  `account_type` varchar(20) NOT NULL,
  `record_hide` varchar(10) NOT NULL,
  `date_done` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `case_document`
--

INSERT INTO `case_document` (`case_doc_id`, `doc_court_case_id`, `doc_folder_name`, `doc_files_uploaded`, `doc_assigned_date`, `doc_description`, `added`, `user_id`, `account_type`, `record_hide`, `date_done`) VALUES
(1, 3, '393-rAlh', '[\"GhIS LSD.pdf\"]', '20-06-2019', '', '22-06-2019', 6, 'staff', 'NO', '2019-06-22 13:27:51'),
(2, 2, '381-iQfL', '[\"work_order_1.pdf\",\"FakeInvoice.pdf\",\"The Art of Work- A Proven Path to Discovering What You Were Meant to Do ( PDFDrive.pdf\",\"Ego-Is-the-Enemy.pdf\"]', '12-06-2019', 'kjghfds', '22-06-2019', 6, 'staff', 'NO', '2019-06-22 17:09:02'),
(3, 3, '393-rAlh', '[\"PDF Vieweer.pdf\",\"sop_guide2.pdf\"]', '20-06-2019', 'testing', '22-06-2019', 6, 'staff', 'NO', '2019-06-22 18:53:50'),
(4, 1, '498-2o8L', '[\"GhIS LSD.pdf\",\"GhIS LSD.pdf\"]', '30-05-2019', 'asdftest', '22-06-2019', 6, 'staff', 'NO', '2019-06-22 18:58:36'),
(5, 2, '381-iQfL', '[\"Chrysanthemum.jpg\",\"doc.pdf\"]', '27-06-2019', '', '27-06-2019', 6, 'staff', 'NO', '2019-06-27 22:37:57'),
(6, 3, '393-rAlh', '[\"sophos-exposed-cyberattacks-on-cloud-honeypots-wp.pdf\"]', '16-07-2019', 'sopos', '1-07-2019', 6, 'staff', 'NO', '2019-07-01 18:23:36'),
(7, 3, '393-rAlh', '[\"CV HERBERT OTCHERE ADJARE.pdf\",\"67447479_227962018178335_7871170435746121632_n.jpg\"]', '13-07-2019', 'images', '13-07-2019', 6, 'staff', 'NO', '2019-07-13 09:43:13'),
(8, 3, '393-rAlh', '[\"GhISLSD_api.pdf\"]', '27-09-2019', 'Best Description Change', '28-09-2019', 6, 'staff', 'NO', '2019-09-28 18:24:52'),
(9, 5, '357-Kl6W', '[\"GhISLSD_api.pdf\",\"manhood.jpg\"]', '05-11-2019', 'New Addition', '5-11-2019', 6, 'staff', 'YES', '2019-11-05 21:15:20');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `contact_id` int(11) NOT NULL,
  `contact_First_name` varchar(100) NOT NULL,
  `contact_mid_name` varchar(100) NOT NULL,
  `contact_last_name` varchar(100) NOT NULL,
  `contact_email` varchar(100) NOT NULL,
  `contact_company` int(11) NOT NULL,
  `contact_group_id` int(11) NOT NULL,
  `contact_portal` varchar(10) NOT NULL,
  `contact_cell` varchar(20) NOT NULL,
  `contact_wrk_phone` varchar(20) NOT NULL,
  `contact_address` varchar(100) NOT NULL,
  `contact_city` varchar(100) NOT NULL,
  `contact_region` varchar(100) NOT NULL,
  `contact_country` varchar(100) NOT NULL,
  `user_id` int(11) NOT NULL,
  `added` varchar(100) NOT NULL,
  `record_hide` varchar(100) NOT NULL,
  `last_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`contact_id`, `contact_First_name`, `contact_mid_name`, `contact_last_name`, `contact_email`, `contact_company`, `contact_group_id`, `contact_portal`, `contact_cell`, `contact_wrk_phone`, `contact_address`, `contact_city`, `contact_region`, `contact_country`, `user_id`, `added`, `record_hide`, `last_updated`) VALUES
(1, 'sorce', 'a', 'kwarteng', 'a@gmail.com', 6, 1, 'off', '123123123', '', '', '', '', 'Aruba', 6, '2nd June 2019 / 10:48:25 PM', 'NO', '2019-06-21 14:17:32'),
(2, 'Merlin', '', 'Joe', 'a@gmail.com', 2, 1, 'on', '0209969656', '2295', 'P.O.BOX 2295, ACCRA MADINA', '', '', 'Ghana', 6, '2nd June 2019 / 11:29:15 PM', 'NO', '2019-06-21 14:17:25'),
(3, 'merlin', 'a', 'Kabosh', 'merlin@gmail.com', 8, 1, 'on', '0209969656', '2295', '', '', '', 'Ghana', 6, '16th June 2019 / 11:54:40 PM', 'NO', '2019-06-26 19:42:22'),
(4, 'asdf', '', 'asdf', '', 6, 1, 'on', '3212', '', '', '', '', 'Australia', 6, '21st June 2019 / 02:15:10 PM', 'NO', '2019-06-26 19:43:33'),
(5, 'asd', '', 'asdf', '', 7, 1, 'on', '324', '', '', '', '', 'Armenia', 6, '21st June 2019 / 02:15:53 PM', 'NO', '2019-06-26 19:42:46'),
(6, 'Herbert', '', 'Adjare', 'ad@gmail.com', 8, 1, 'on', '0206677883', '', 'tf', 'accra', 'gr', 'Ghana', 6, '16th August 2019 / 05:32:47 PM', 'YES', '2019-08-20 23:36:09'),
(7, 'kk', '', 'mjku', 'gghted/@gmail.com', 6, 4, 'on', '0209969656', '', '', '', '', 'Ghana', 6, '16th August 2019 / 05:49:49 PM', 'NO', '2019-11-09 22:40:03'),
(8, 'sorce', '', 'merlin', 'sorce100@gmail.com', 7, 4, 'on', '0209969656', '', '', '', '', 'Ghana', 6, '20th August 2019 / 11:37:45 PM', 'NO', '2019-08-20 23:37:45'),
(9, 'aa', '', 'aa', '', 0, 4, 'on', '0209969656', '', '', '', '', 'American Samoa', 6, '20th August 2019 / 11:39:33 PM', 'NO', '2019-11-09 22:40:06'),
(10, 'contact', '', 'test', '', 0, 4, 'on', '0209969656', '', '', '', '', 'Aruba', 6, '20th August 2019 / 11:41:29 PM', 'NO', '2019-11-09 22:40:09'),
(11, 'aa', '', 'aa', '', 0, 4, 'on', '0209969656', '', '', '', '', 'Australia', 6, '20th August 2019 / 11:43:35 PM', 'NO', '2019-11-09 22:40:12'),
(12, 'Richard', '', 'Nunekpeku', '', 7, 4, 'on', '0500000767', '', '', '', '', 'United States', 6, '21st August 2019 / 11:35:26 AM', 'NO', '2019-11-09 22:40:15');

-- --------------------------------------------------------

--
-- Table structure for table `contacts_company`
--

CREATE TABLE `contacts_company` (
  `con_company_id` int(11) NOT NULL,
  `con_company_name` varchar(100) NOT NULL,
  `con_company_email` varchar(100) NOT NULL,
  `con_company_website` varchar(100) NOT NULL,
  `con_company_mainphone` varchar(20) NOT NULL,
  `con_company_address` varchar(200) NOT NULL,
  `con_company_city` varchar(100) NOT NULL,
  `con_company_region` varchar(100) NOT NULL,
  `con_company_country` varchar(100) NOT NULL,
  `con_company_privatenote` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `record_hide` varchar(10) NOT NULL,
  `added` varchar(50) NOT NULL,
  `last_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contacts_company`
--

INSERT INTO `contacts_company` (`con_company_id`, `con_company_name`, `con_company_email`, `con_company_website`, `con_company_mainphone`, `con_company_address`, `con_company_city`, `con_company_region`, `con_company_country`, `con_company_privatenote`, `user_id`, `record_hide`, `added`, `last_updated`) VALUES
(1, 'sorce123', 'a@gmail.com', '', '', '', '', '', 'France', '', 6, 'NO', '26th May 2019 / 08:25:00 AM', '2019-05-26 08:25:00'),
(2, 'sorce kwarteng', 'sorce100@gmail.com', 'www.google.com', '0209969656', 'abc', 'abc', 'abc', 'Cuba', 'private note', 6, 'NO', '26th May 2019 / 08:26:13 AM', '2019-05-26 08:26:13'),
(6, 'Merlin', 'merlin@gmail.com', '', '', '', '', '', 'Ghana', '', 6, 'NO', '2nd June 2019 / 07:06:15 PM', '2019-06-02 19:06:15'),
(7, 'New company', '', '', '', '', '', '', 'Ghana', 'asdf', 6, 'NO', '2nd June 2019 / 08:20:34 PM', '2019-06-02 20:20:34'),
(8, 'Other', 'other@gmail.com', 'other.com', '02000000000', 'other', 'other', 'other', 'Ghana', 'other', 6, 'NO', '21st June 2019 / 03:10:41 PM', '2019-06-21 15:10:41');

-- --------------------------------------------------------

--
-- Table structure for table `contacts_group`
--

CREATE TABLE `contacts_group` (
  `contact_group_id` int(11) NOT NULL,
  `contact_group_name` varchar(100) NOT NULL,
  `user_id` int(11) NOT NULL,
  `added` varchar(50) NOT NULL,
  `record_hide` varchar(10) NOT NULL,
  `last_update` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contacts_group`
--

INSERT INTO `contacts_group` (`contact_group_id`, `contact_group_name`, `user_id`, `added`, `record_hide`, `last_update`) VALUES
(1, 'Client', 6, '25th May 2019 / 10:33:58 PM', 'NO', '2019-05-25 22:33:58'),
(2, 'Co-counsel', 6, '25th May 2019 / 10:47:33 PM', 'NO', '2019-05-25 22:47:33'),
(3, 'Expert', 6, '25th May 2019 / 10:49:21 PM', 'NO', '2019-05-25 22:49:21'),
(4, 'Judge', 6, '25th May 2019 / 10:50:13 PM', 'NO', '2019-05-25 22:50:13'),
(5, 'Unassigned', 6, '25th May 2019 / 10:50:21 PM', 'NO', '2019-05-25 22:50:21'),
(12, 'Group1', 6, '2nd June 2019 / 08:20:50 PM', 'YES', '2019-06-02 20:20:50');

-- --------------------------------------------------------

--
-- Table structure for table `court_case`
--

CREATE TABLE `court_case` (
  `case_id` int(11) NOT NULL,
  `case_contactIds` text NOT NULL,
  `case_name` text NOT NULL,
  `case_suit_number` varchar(100) NOT NULL,
  `case_number` varchar(10) NOT NULL,
  `case_practice_area` int(11) NOT NULL,
  `case_stage` varchar(50) NOT NULL,
  `case_date_opened` varchar(50) NOT NULL,
  `current_court_main_id` int(11) NOT NULL,
  `current_court_sub_id` int(11) NOT NULL,
  `current_court_num` int(11) NOT NULL,
  `next_court_main_id` int(11) NOT NULL,
  `next_court_sub_id` int(11) NOT NULL,
  `next_court_num` int(11) NOT NULL,
  `next_court_date` varchar(150) NOT NULL,
  `case_description` text NOT NULL,
  `case_statue_limit` varchar(50) NOT NULL,
  `case_billing_contact` int(11) NOT NULL,
  `case_billing_method` varchar(50) NOT NULL,
  `case_lead_attorney` int(11) NOT NULL,
  `case_staffIds` text NOT NULL,
  `case_status` varchar(50) NOT NULL,
  `case_closed_user_id` int(11) NOT NULL,
  `case_closed_date` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL,
  `added` varchar(50) NOT NULL,
  `record_hide` varchar(10) NOT NULL,
  `last_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `court_case`
--

INSERT INTO `court_case` (`case_id`, `case_contactIds`, `case_name`, `case_suit_number`, `case_number`, `case_practice_area`, `case_stage`, `case_date_opened`, `current_court_main_id`, `current_court_sub_id`, `current_court_num`, `next_court_main_id`, `next_court_sub_id`, `next_court_num`, `next_court_date`, `case_description`, `case_statue_limit`, `case_billing_contact`, `case_billing_method`, `case_lead_attorney`, `case_staffIds`, `case_status`, `case_closed_user_id`, `case_closed_date`, `user_id`, `added`, `record_hide`, `last_updated`) VALUES
(1, '[\"1\",\"2\"]', 'asdf', '', '498-2o8L', 12, 'In Trial', '08-06-2019', 0, 0, 0, 0, 0, 0, '', 'asdf', '22-06-2019', 4, 'Flat Fee', 9, '[\"9\"]', 'OPEN', 0, '', 6, '3rd June 2019', 'NO', '2019-06-03 19:04:31'),
(2, '[\"1\",\"3\"]', 'Jane Doe', '', '381-iQfL', 12, 'Discovery', '14-06-2019', 0, 0, 0, 0, 0, 0, '', 'asdf1234', '28-06-2019', 2, 'Hourly', 9, '[\"11\",\"10\",\"9\"]', 'OPEN', 0, '', 6, '3rd June 2019 ', 'NO', '2019-06-03 19:06:20'),
(3, '[\"1\",\"2\",\"3\"]', 'merlinNew', '', '393-rAlh', 12, 'Summon For Direction', '08-06-2019', 0, 0, 0, 0, 0, 0, '', 'asdf123@123', '29-06-2019', 1, 'Hourly', 9, '[\"11\",\"10\",\"9\"]', 'CLOSED', 6, '25-10-2019 01:09:05am', 6, '03-06-2019', 'NO', '2019-06-03 19:32:52'),
(4, '[\"3\"]', 'Testing case', '', '102-aOQ8', 8, 'Discovery', '28-06-2019', 0, 0, 0, 0, 0, 0, '', 'This is a new case', '22-06-2019', 1, 'Hourly', 11, '[\"11\"]', 'OPEN', 0, '', 6, '22-06-2019', 'NO', '2019-06-22 20:30:44'),
(5, '[\"2\",\"1\",\"5\"]', 'er5tyui', '34f42', '357-Kl6W', 12, 'In Trial', '15-08-2019', 4, 15, 1, 2, 16, 1, '31-10-2019', 'THis is the description', '', 1, 'Hourly', 9, '[\"12\",\"9\"]', 'OPEN', 0, '', 6, '16-08-2019', 'NO', '2019-08-16 17:23:51'),
(6, '[\"11\",\"12\"]', 'kweku ntim vs state', 'case', '378-oEc8', 13, 'On Hold', '07-11-2019', 4, 15, 1, 1, 1, 1, '07-11-2019', 'Please comply and make the report', '', 12, 'Hourly', 11, '[\"11\",\"9\"]', 'OPEN', 0, '', 6, '07-11-2019', 'NO', '2019-11-07 14:27:23');

-- --------------------------------------------------------

--
-- Table structure for table `court_case_notes`
--

CREATE TABLE `court_case_notes` (
  `court_case_note_id` int(11) NOT NULL,
  `court_case_id` int(11) NOT NULL,
  `court_case_note_content` text NOT NULL,
  `account_id` int(11) NOT NULL,
  `added` varchar(50) NOT NULL,
  `record_hide` varchar(5) NOT NULL,
  `last_update` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `court_case_notes`
--

INSERT INTO `court_case_notes` (`court_case_note_id`, `court_case_id`, `court_case_note_content`, `account_id`, `added`, `record_hide`, `last_update`) VALUES
(1, 3, 'Hello Test', 9, '21st June 2019 / 10:55:43 AM', 'NO', '2019-06-21 10:55:43'),
(2, 3, 'merlin', 9, '21st June 2019 / 11:00:33 AM', 'NO', '2019-06-21 11:00:33'),
(3, 3, 'Case closed by the case lead', 9, '21st June 2019 / 11:05:50 AM', 'NO', '2019-06-21 11:05:50'),
(4, 2, 'New Case', 9, '21st June 2019 / 11:08:12 AM', 'NO', '2019-06-21 11:08:12'),
(5, 3, 'Files have being updated', 9, '22nd June 2019 / 09:10:45 PM', 'NO', '2019-06-22 21:10:45'),
(6, 5, 'This case has being adjourned', 9, '3rd September 2019 / 07:32:20 PM', 'NO', '2019-09-03 19:32:20'),
(7, 3, 'New ADD 123 456', 9, '9th November 2019 / 11:08:49 PM', 'YES', '2019-11-09 23:08:49'),
(8, 3, 'Merlin Text', 9, '9th November 2019 / 11:34:14 PM', 'YES', '2019-11-09 23:34:14'),
(9, 6, 'First text', 9, '9th November 2019 / 11:35:24 PM', 'NO', '2019-11-09 23:35:24');

-- --------------------------------------------------------

--
-- Table structure for table `error_logs`
--

CREATE TABLE `error_logs` (
  `error_log_id` int(11) NOT NULL,
  `error_message` text NOT NULL,
  `error_date` varchar(150) NOT NULL,
  `account_name` varchar(10) NOT NULL,
  `account_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `event_id` int(11) NOT NULL,
  `event_court_case_id` int(11) NOT NULL,
  `event_name` varchar(200) NOT NULL,
  `event_start_date` varchar(100) NOT NULL,
  `event_end_Date` varchar(100) NOT NULL,
  `event_typeId` int(11) NOT NULL,
  `event_location` varchar(300) NOT NULL,
  `event_description` text NOT NULL,
  `event_contact_id` text NOT NULL,
  `event_contact_share` text NOT NULL,
  `event_staff_id` text NOT NULL,
  `event_staff_share` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `added` varchar(50) NOT NULL,
  `record_hide` varchar(10) NOT NULL,
  `last_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`event_id`, `event_court_case_id`, `event_name`, `event_start_date`, `event_end_Date`, `event_typeId`, `event_location`, `event_description`, `event_contact_id`, `event_contact_share`, `event_staff_id`, `event_staff_share`, `user_id`, `added`, `record_hide`, `last_updated`) VALUES
(17, 2, 'Witness interview', '2019-05-26 16:25:00', '2019-05-27 15:50:00', 1, 'Office', 'Meeting to interview witness', '[\"2\",\"1\"]', '[\"2\",\"1\"]', '[\"11\",\"10\",\"9\"]', '[\"11\",\"10\",\"9\"]', 6, '13-06-2019', 'NO', '2019-06-13 15:24:39'),
(19, 3, 'Meeting', '2019-08-16 08:30', '2019-08-30 10:55', 2, 'Main Office', 'All should be present', '[\"1\",\"2\",\"3\"]', 'null', '[\"11\",\"10\",\"9\"]', 'null', 6, '21-07-2019', 'NO', '2019-07-21 15:34:24'),
(20, 3, 'meeting', '2019-09-22 15:50', '2019-09-30 19:50', 1, 'adenta', '', '[\"1\",\"2\",\"3\"]', '[\"1\"]', '[\"11\",\"10\",\"9\"]', '[\"11\"]', 6, '22-09-2019', 'NO', '2019-09-22 15:49:26'),
(21, 5, 'tosa', '2019-10-17 09:55', '2019-10-19 14:55', 3, 'asd', 'asdfsdf', '[\"2\",\"1\",\"5\"]', '[\"2\",\"5\",\"1\"]', '[\"9\"]', '[\"9\"]', 6, '17-10-2019', 'NO', '2019-10-17 09:58:54'),
(22, 3, 'sitting for case', '2019-10-19 09:30', '2019-10-26 11:55', 1, 'Adenta', 'asdfasdfasdf', '[\"1\",\"2\",\"3\"]', '[\"1\",\"2\"]', '[\"11\",\"10\",\"9\"]', 'null', 6, '19-10-2019', 'NO', '2019-10-19 09:58:14'),
(23, 3, 'This is it', '2019-11-13 11:50', '2019-11-21 13:55', 2, 'Added by me', 'Description', '[\"1\",\"2\",\"3\"]', '[\"1\",\"3\"]', '[\"11\",\"10\",\"9\"]', '[\"11\",\"10\"]', 6, '6-11-2019', 'NO', '2019-11-06 20:41:29'),
(24, 3, 'Tonight test', '2019-11-07 06:30', '2019-11-08 08:30', 1, 'Office', 'Everybody should be available', '[\"1\",\"2\",\"3\"]', '[\"1\",\"2\",\"3\"]', '[\"11\",\"10\",\"9\"]', '[\"11\",\"10\",\"9\"]', 6, '7-11-2019', 'NO', '2019-11-07 00:10:18'),
(25, 3, 'Please be there on time', '2019-11-07 06:30', '2019-11-22 06:50', 3, '', 'No Description', '[\"1\",\"2\",\"3\"]', '[\"1\",\"2\",\"3\"]', '[\"11\",\"10\",\"9\"]', '[\"11\",\"10\",\"9\"]', 6, '7-11-2019', 'NO', '2019-11-07 00:43:10'),
(26, 3, 'New Test', '2019-11-07 01:10', '2019-11-09 02:10', 1, 'Location', 'Please be there', '[\"1\",\"2\",\"3\"]', '[\"1\",\"2\",\"3\"]', '[\"11\",\"10\",\"9\"]', '[\"11\",\"10\",\"9\"]', 6, '7-11-2019', 'NO', '2019-11-07 01:03:22'),
(27, 2, 'This is the Jane Doe meeting', '2019-11-10 11:30', '2019-11-12 08:30', 1, 'Office', 'Office Description', '[\"1\",\"3\"]', '[\"1\",\"3\"]', '[\"11\",\"10\",\"9\"]', '[\"11\",\"10\",\"9\"]', 6, '10-11-2019', 'NO', '2019-11-10 11:35:41');

-- --------------------------------------------------------

--
-- Table structure for table `event_type`
--

CREATE TABLE `event_type` (
  `event_type_id` int(11) NOT NULL,
  `event_type_name` varchar(200) NOT NULL,
  `event_type_color` varchar(200) NOT NULL,
  `added` varchar(150) NOT NULL,
  `record_hide` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `event_type`
--

INSERT INTO `event_type` (`event_type_id`, `event_type_name`, `event_type_color`, `added`, `record_hide`) VALUES
(1, 'Client Meeting', '#372c7a', '19th October 2019 / 04:07:12 PM', 'NO'),
(2, 'Meeting', '#49123d', '19th October 2019 / 04:24:16 PM', 'NO'),
(3, 'Lead Meet', '#e01ab5', '19th October 2019 / 07:00:54 PM', 'NO');

-- --------------------------------------------------------

--
-- Table structure for table `leads`
--

CREATE TABLE `leads` (
  `lead_id` int(11) NOT NULL,
  `lead_first_name` varchar(200) NOT NULL,
  `lead_mid_name` varchar(200) NOT NULL,
  `lead_last_name` varchar(200) NOT NULL,
  `lead_email` varchar(150) NOT NULL,
  `lead_tel` varchar(20) NOT NULL,
  `lead_office_location` text NOT NULL,
  `lead_town` varchar(200) NOT NULL,
  `lead_region` varchar(100) NOT NULL,
  `lead_case_matter` text NOT NULL,
  `lead_case_description` text NOT NULL,
  `lead_case_status` varchar(50) NOT NULL,
  `lead_case_practice_area` int(11) NOT NULL,
  `lead_case_assignedTo` int(11) NOT NULL,
  `lead_case_completion_date` varchar(50) NOT NULL,
  `lead_case_hourly_rate` double NOT NULL,
  `lead_case_monthly_retainer` double NOT NULL,
  `lead_case_fee_frequency` varchar(20) NOT NULL,
  `lead_case_special_arrangement` varchar(200) NOT NULL,
  `lead_case_associated_clients` text NOT NULL,
  `lead_case_adverse_parties` text NOT NULL,
  `lead_case_associated_files` text NOT NULL,
  `added` varchar(150) NOT NULL,
  `record_hide` varchar(5) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `leads`
--

INSERT INTO `leads` (`lead_id`, `lead_first_name`, `lead_mid_name`, `lead_last_name`, `lead_email`, `lead_tel`, `lead_office_location`, `lead_town`, `lead_region`, `lead_case_matter`, `lead_case_description`, `lead_case_status`, `lead_case_practice_area`, `lead_case_assignedTo`, `lead_case_completion_date`, `lead_case_hourly_rate`, `lead_case_monthly_retainer`, `lead_case_fee_frequency`, `lead_case_special_arrangement`, `lead_case_associated_clients`, `lead_case_adverse_parties`, `lead_case_associated_files`, `added`, `record_hide`, `user_id`, `status`) VALUES
(1, 'sdf', 'middle name', 'asdf', 'asdfasd@gmai.com', '24323', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'Accra', 'Ashanti', '34234', 'asfasdf', 'Contacted', 10, 9, '22-10-2019', 23, 23, 'Monthly', 'asdfasd', 'asdfas', 'afsdfas', 'dfasdfasdf', '22nd October 2019 / 10:49:28 AM', 'NO', 6, 'NEW'),
(2, 'Ferdinand', 'Ogyam', 'Kwarteng', 'sorce100@gmail.com', '0209969656', 'Medi Moses Prostrate Center, Adenta New Site, Accra', 'Accra', 'Greater_Accra', 'the state vs merlin', 'This is a new case', 'Pending', 6, 9, '22-10-2019', 54, 200, 'Monthly', 'No', 'This is no parties invloved', 'None at the moment', 'None at the moment', '22nd October 2019 / 10:52:23 AM', 'NO', 6, 'NEW');

-- --------------------------------------------------------

--
-- Table structure for table `main_court`
--

CREATE TABLE `main_court` (
  `main_court_id` int(11) NOT NULL,
  `main_court_name` varchar(220) NOT NULL,
  `added` varchar(150) NOT NULL,
  `record_hide` varchar(5) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `main_court`
--

INSERT INTO `main_court` (`main_court_id`, `main_court_name`, `added`, `record_hide`, `user_id`) VALUES
(1, 'Supreme Court', '21st October 2019 / 03:55:30 PM', 'NO', 6),
(2, 'District Court', '22nd October 2019 / 02:22:16 AM', 'NO', 6),
(3, 'Court Of Appeal', '22nd October 2019 / 03:03:38 AM', 'NO', 6),
(4, 'High Court', '22nd October 2019 / 03:03:49 AM', 'NO', 6),
(5, 'District Court', '22nd October 2019 / 03:04:07 AM', 'YES', 6);

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `message_id` int(11) NOT NULL,
  `message_sender_accountName` varchar(50) NOT NULL,
  `message_sender_accounId` int(11) NOT NULL,
  `message_courtCaseId` int(11) NOT NULL,
  `message_receive_contacts` text NOT NULL,
  `message_receive_staff` text NOT NULL,
  `message_subject` varchar(255) NOT NULL,
  `message_content` text NOT NULL,
  `message_status` varchar(10) NOT NULL,
  `added` varchar(50) NOT NULL,
  `receiver_record_hide` varchar(5) NOT NULL,
  `sender_record_hide` varchar(5) NOT NULL,
  `last_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`message_id`, `message_sender_accountName`, `message_sender_accounId`, `message_courtCaseId`, `message_receive_contacts`, `message_receive_staff`, `message_subject`, `message_content`, `message_status`, `added`, `receiver_record_hide`, `sender_record_hide`, `last_updated`) VALUES
(1, 'staff', 9, 3, '[\"2\",\"1\"]', '[\"11\",\"10\",\"9\"]', 'asdf', '<p><b>asfasdfa</b></p><p><i>asd</i>fasdf,<u>asdfasdfasdf&nbsp;</u></p><p><u>sorrry </u><strike>GOD</strike></p>', 'UNREAD', '20th June 2019 / 05:12:56 PM', 'NO', 'NO', '2019-06-20 17:12:56'),
(2, 'staff', 9, 3, '[\"2\",\"1\"]', '[\"11\",\"10\",\"9\"]', 'asdasdasd', '<p><b><i><u>asfasdfasd</u></i></b></p><p><b><i><u><br></u></i></b></p><table class=\"table table-bordered\"><tbody><tr><td>asfd</td><td>asdf</td><td>asdf</td><td>12313</td><td>asf321</td></tr><tr><td>asdf</td><td>asdf</td><td>a</td><td>123</td><td>sfa34</td></tr><tr><td>asdf</td><td><br></td><td>asd</td><td>123</td><td>asd323</td></tr><tr><td>asdf</td><td>asdf</td><td>asdf</td><td>12</td><td>asd34</td></tr></tbody></table><p><b><i><u><br></u></i></b></p>', 'UNREAD', '20th June 2019 / 05:29:22 PM', 'NO', 'NO', '2019-06-20 17:29:22'),
(3, 'staff', 9, 1, '[\"2\",\"1\"]', '[\"9\"]', 'hee', '<p>asdfasdfasdfd</p><table class=\"table table-bordered\"><tbody><tr><td>asdfa</td><td><br></td><td><br></td><td><br></td></tr><tr><td>asdf</td><td>asd</td><td>esa</td><td><br></td></tr><tr><td>sdf</td><td><br></td><td><br></td><td>asdfasdsdfsdf<span style=\"background-color: rgb(255, 0, 255);\">asdfsdfsdfasdfasdf</span></td></tr></tbody></table><p>asdasdf</p>', 'UNREAD', '20th June 2019 / 11:51:33 PM', 'NO', 'YES', '2019-06-20 23:51:33'),
(4, 'staff', 9, 3, '[\"2\",\"1\"]', '[\"11\",\"10\"]', 'YOU WONTT', '<p>NOT USER CONTENR</p><p><br></p><table class=\"table table-bordered\"><tbody><tr><td>asfesdafsd</td><td><br></td></tr></tbody></table><p><br></p><table class=\"table table-bordered\"><tbody><tr><td>asdfasd</td><td>asdfasdf</td></tr><tr><td>fasdfsadf</td><td>asdfasdf</td></tr></tbody></table><p><br></p>', 'UNREAD', '21st June 2019 / 12:24:20 AM', 'NO', 'NO', '2019-06-21 00:24:20'),
(5, 'staff', 9, 3, '[\"2\",\"1\"]', '[\"11\",\"10\",\"9\"]', 'Genereal testing', 'Test this', 'UNREAD', '21st June 2019 / 04:04:15 PM', 'NO', 'NO', '2019-06-21 16:04:15'),
(6, 'staff', 9, 1, '[\"2\",\"1\"]', '[\"9\"]', 'asdf', 'asdf', 'UNREAD', '21st June 2019 / 04:05:53 PM', 'NO', 'NO', '2019-06-21 16:05:53'),
(7, 'staff', 9, 1, '[\"2\",\"1\"]', '[\"9\"]', 'meetig', '<p>Please be there</p>', 'UNREAD', '21st June 2019 / 04:06:45 PM', 'NO', 'NO', '2019-06-21 16:06:45'),
(8, 'staff', 9, 1, '[\"2\",\"1\"]', '[\"9\"]', 's', '<p><br></p><table class=\"table table-bordered\"><tbody><tr><td><br></td><td><br></td><td><br></td><td><br></td><td><br></td><td><br></td><td><br></td><td><br></td></tr><tr><td><br></td><td><br></td><td><br></td><td><br></td><td><br></td><td><br></td><td><br></td><td><br></td></tr><tr><td><br></td><td><br></td><td><br></td><td><br></td><td><br></td><td><br></td><td><br></td><td><br></td></tr></tbody></table><p>							\r\n						</p>', 'UNREAD', '21st June 2019 / 04:07:36 PM', 'YES', 'YES', '2019-06-21 16:07:36'),
(9, 'staff', 9, 3, '[\"5\",\"3\",\"2\",\"1\"]', '[\"11\",\"10\",\"9\"]', 'New Message', '<p>Hello world</p><p><br></p>', 'UNREAD', '13th July 2019 / 09:36:30 AM', 'NO', 'NO', '2019-07-13 09:36:30');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `pages_id` int(11) NOT NULL,
  `pages_name` varchar(100) NOT NULL,
  `pages_url` text NOT NULL,
  `page_file_name` varchar(200) NOT NULL,
  `added` varchar(50) NOT NULL,
  `record_hide` varchar(10) NOT NULL,
  `last_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`pages_id`, `pages_name`, `pages_url`, `page_file_name`, `added`, `record_hide`, `last_updated`) VALUES
(1, 'Staff Setup', '<li><a href=\"../pages/admin_staff.php\"><i class=\"fa fa-users\"></i> <span>Staff</span></a></li>', 'admin_staff.php', '25th May 2019 / 12:51:33 PM', 'NO', '2019-05-25 12:51:33'),
(2, 'Pages Setup', '<li><a href=\"../pages/admin_pages.php\"><i class=\"fa fa-link\"></i> <span>Pages</span></a></li>', 'admin_pages.php', '25th May 2019 / 12:52:24 PM', 'NO', '2019-05-25 12:52:24'),
(3, 'Pages Group Setup', '<li><a href=\"../pages/admin_pages_groups.php\"><i class=\"fa fa-users\"></i> <span>Groups</span></a></li>', 'admin_pages_groups.php', '25th May 2019 / 12:52:48 PM', 'NO', '2019-05-25 12:52:48'),
(4, 'Users Setup', '<li><a href=\"../pages/admin_users.php\"><i class=\"fa fa-users\"></i> <span>Users</span></a></li>', 'admin_users.php', '25th May 2019 / 12:54:07 PM', 'NO', '2019-05-25 12:54:07'),
(5, 'Messages', '<li><a href=\"../pages/messages.php\"><i class=\"fa fa-envelope\"></i> <span>Messages </span></a></li>', 'messages.php', '25th May 2019 / 12:54:45 PM', 'NO', '2019-05-25 12:54:45'),
(6, 'Calendar', '<li><a href=\"../pages/events.php\"><i class=\"fa fa-calendar\"></i> <span>Calendar</span></a></li>', 'events.php', '25th May 2019 / 12:55:12 PM', 'NO', '2019-05-25 12:55:12'),
(7, 'Tasks', '<li><a href=\"../pages/tasks.php\"><i class=\"fa fa-list\"></i> <span>Tasks</span></a></li>', 'tasks.php', '25th May 2019 / 12:55:30 PM', 'NO', '2019-05-25 12:55:30'),
(8, 'Contacts', '<li><a href=\"../pages/contacts.php\"><i class=\"fa fa-users\"></i> <span>Contacts</span></a></li>', 'contacts.php', '25th May 2019 / 12:55:49 PM', 'NO', '2019-05-25 12:55:49'),
(9, 'Cases', '<li><a href=\"../pages/court_cases.php\"><i class=\"fa fa-briefcase\"></i> <span>Cases</span></a></li>', 'court_cases.php', '25th May 2019 / 12:56:03 PM', 'NO', '2019-05-25 12:56:03'),
(10, 'Documents', '<li><a href=\"../pages/documents.php\"><i class=\"fa fa-file\"></i> <span>Documents</span></a></li>', 'documents.php', '25th May 2019 / 12:56:14 PM', 'NO', '2019-05-25 12:56:14'),
(11, 'Practice Area Setup', '<li><a href=\"../pages/admin_practice_area.php\" class=\"\"><i class=\"fa fa-globe\"></i> <span>Practice Area</span></a></li>', 'admin_practice_area.php', '25th May 2019 / 12:56:39 PM', 'NO', '2019-05-25 12:56:39'),
(15, 'sorc134123412341234', '<sorce>12345141234aaa', '', '26th May 2019 / 12:47:44 PM', 'YES', '2019-05-26 12:47:44'),
(16, 'sorce', '<sorce>aaaa</kwarteng>', '', '26th May 2019 / 12:53:01 PM', 'YES', '2019-05-26 12:53:01'),
(17, 'SMS Broadcast', '<li><a href=\"../pages/sms_broadcast.php\"><i class=\"fa fa-envelope\"></i> <span>SMS Broadcast</span></a></li>', 'sms_broadcast.php', '23rd June 2019 / 11:24:01 PM', 'NO', '2019-06-23 23:24:01'),
(18, 'Events Type Setup', '<li><a href=\"../pages/events_type.php\"><i class=\"fa fa-filter\"></i> <span>Events Type</span></a></li>', 'events_type.php', '19th October 2019 / 07:02:16 PM', 'NO', '2019-10-19 19:02:16'),
(19, 'Leads Setup', '<li><a href=\"../pages/admin_lead.php\"><i class=\"fa fa-road\"></i> <span>Lead</span></a></li>', 'admin_lead.php', '22nd October 2019 / 03:20:05 PM', 'NO', '2019-10-22 15:20:05'),
(20, 'Courts Setup', '<li><a href=\"../pages/admin_court_add.php\"><i class=\"fa fa-plus-square-o\"></i> <span>Courts SetUp</span></a></li>', 'admin_court_add.php', '22nd October 2019 / 03:21:06 PM', 'NO', '2019-10-22 15:21:06'),
(21, 'Bills And Invoices', '<li><a href=\"../pages/bills.php\"><i class=\"fa fa-credit-card-alt\"></i> <span>Billing</span></a></li>', 'bills.php', '22nd October 2019 / 09:08:40 PM', 'NO', '2019-10-22 21:08:40'),
(22, 'Adminstrator Reports Generations', '<li><a href=\"../pages/reporting.php\"><i class=\"fa fa-files-o\"></i> <span>Reports</span></a></li>', 'reporting.php', '24th October 2019 / 01:22:34 AM', 'NO', '2019-10-24 01:22:34');

-- --------------------------------------------------------

--
-- Table structure for table `pages_group`
--

CREATE TABLE `pages_group` (
  `pages_group_id` int(11) NOT NULL,
  `pages_group_name` varchar(100) NOT NULL,
  `pages_id` text NOT NULL,
  `added` varchar(50) NOT NULL,
  `record_hide` varchar(10) NOT NULL,
  `last_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pages_group`
--

INSERT INTO `pages_group` (`pages_group_id`, `pages_group_name`, `pages_id`, `added`, `record_hide`, `last_updated`) VALUES
(1, 'Administrator', '[\"22\",\"21\",\"20\",\"17\",\"11\",\"10\",\"9\",\"8\",\"7\",\"6\",\"5\",\"4\",\"3\",\"2\",\"1\"]', '23rd May 2019 / 12:30:51 AM', 'NO', '2019-05-23 00:30:51'),
(3, 'Clients', 'null', '26th May 2019 / 03:08:48 PM', 'NO', '2019-05-26 15:08:48'),
(4, 'new one test', '[\"17\",\"10\",\"7\",\"6\",\"5\",\"4\",\"3\",\"2\",\"1\"]', '24th June 2019 / 07:31:14 PM', 'NO', '2019-06-24 19:31:14');

-- --------------------------------------------------------

--
-- Table structure for table `practice_area`
--

CREATE TABLE `practice_area` (
  `practice_area_id` int(11) NOT NULL,
  `practice_area_name` varchar(100) NOT NULL,
  `added` varchar(50) NOT NULL,
  `record_hide` varchar(10) NOT NULL,
  `last_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `practice_area`
--

INSERT INTO `practice_area` (`practice_area_id`, `practice_area_name`, `added`, `record_hide`, `last_updated`) VALUES
(1, 'Bankruptcy', '25th May 2019 / 09:48:42 PM', 'NO', '2019-05-25 21:48:42'),
(2, 'Business', '25th May 2019 / 09:48:49 PM', 'NO', '2019-05-25 21:48:49'),
(3, 'Civil', '25th May 2019 / 09:48:54 PM', 'NO', '2019-05-25 21:48:54'),
(4, 'Criminal Defense', '25th May 2019 / 09:49:01 PM', 'NO', '2019-05-25 21:49:01'),
(5, 'Divorce/Separation', '25th May 2019 / 09:49:07 PM', 'NO', '2019-05-25 21:49:07'),
(6, 'Employment', '25th May 2019 / 09:49:14 PM', 'NO', '2019-05-25 21:49:14'),
(7, 'Estate Planning', '25th May 2019 / 09:49:21 PM', 'NO', '2019-05-25 21:49:21'),
(8, 'Family', '25th May 2019 / 09:49:28 PM', 'NO', '2019-05-25 21:49:28'),
(9, 'Foreclosure', '25th May 2019 / 09:49:35 PM', 'NO', '2019-05-25 21:49:35'),
(10, 'Immigration', '25th May 2019 / 09:49:40 PM', 'NO', '2019-05-25 21:49:40'),
(11, 'Landlord/Tenant', '25th May 2019 / 09:49:47 PM', 'NO', '2019-05-25 21:49:47'),
(12, 'Personal Injury', '25th May 2019 / 09:49:53 PM', 'NO', '2019-05-25 21:49:53'),
(13, 'Real Estate', '25th May 2019 / 09:50:02 PM', 'NO', '2019-05-25 21:50:02'),
(14, 'Real Estate done 123 567', '26th May 2019 / 09:39:39 AM', 'YES', '2019-05-26 09:39:39');

-- --------------------------------------------------------

--
-- Table structure for table `sms`
--

CREATE TABLE `sms` (
  `sms_id` int(11) NOT NULL,
  `sms_message` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `sms_destination` varchar(20) NOT NULL,
  `sms_status` text NOT NULL,
  `added` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sms`
--

INSERT INTO `sms` (`sms_id`, `sms_message`, `user_id`, `sms_destination`, `sms_status`, `added`) VALUES
(1, 'Welcome to E.L AGBEMAVA LAW OFFICE.Please log on https://elagbemava.com with credentials, username: h.adjare and password : klhn53', 6, '0209969656', 'Array', '2019-06-02 23:29:15'),
(2, 'Welcome to E.L AGBEMAVA LAW OFFICE.Please log on https://elagbemava.com with credentials, username: m.kabosh and password : 0m2owc', 6, '0209969656', '{\"code\":\"000\",\"desc\":\"Operation successful.\",\"data\":{\"new_record_id\":190768258}}', '2019-06-16 23:54:41'),
(3, 'Welcome to E.L AGBEMAVA LAW OFFICE.Please log on https://elagbemava.com with credentials, username: a.asdf and password : 4dti9r', 6, '3212', '{\"code\":\"000\",\"desc\":\"Operation successful.\",\"data\":{\"new_record_id\":193235375}}', '2019-06-21 14:15:12'),
(4, 'Welcome to E.L AGBEMAVA LAW OFFICE.Please log on https://elagbemava.com with credentials, username: a.asdf and password : qfdgpl', 6, '324', '{\"code\":\"000\",\"desc\":\"Operation successful.\",\"data\":{\"new_record_id\":193235499}}', '2019-06-21 14:15:54'),
(5, 'Welcome to E.L AGBEMAVA LAW OFFICE.Please log on https://elagbemava.com with credentials, username: a.aa and password : 9y12ch', 6, '0209969656', '{\"code\":\"000\",\"desc\":\"Operation successful.\",\"data\":{\"new_record_id\":234899826},\"configs\":{\"enableSenderIdRouting\":true,\"enableAdRiders\":false,\"enableAutomaticSurveys\":false,\"enableCountrySettings\":true,\"enableAlerts\":true,\"enableCostCenters\":false,\"enableTrustedOrgs\":false,\"enabledConfigs\":true,\"enableCampaigns\":false,\"copyright\":\"Meliora Technologies (K) Ltd.\",\"version\":\"Unified Communications Manager Version 1.4.7\"}}', '2019-08-20 23:43:35'),
(6, 'Welcome to E.L AGBEMAVA LAW OFFICE.Please log on https://elagbemava.com with credentials, username: r.nunekpeku and password : l4xcmy', 6, '0500000767', '{\"code\":\"000\",\"desc\":\"Operation successful.\",\"data\":{\"new_record_id\":235010914},\"configs\":{\"enableSenderIdRouting\":true,\"enableAdRiders\":false,\"enableAutomaticSurveys\":false,\"enableCountrySettings\":true,\"enableAlerts\":true,\"enableCostCenters\":false,\"enableTrustedOrgs\":false,\"enabledConfigs\":true,\"enableCampaigns\":false,\"copyright\":\"Meliora Technologies (K) Ltd.\",\"version\":\"Unified Communications Manager Version 1.4.7\"}}', '2019-08-21 11:35:27');

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE `staff` (
  `staff_id` int(11) NOT NULL,
  `staff_firstname` varchar(100) NOT NULL,
  `staff_middlename` varchar(100) NOT NULL,
  `staff_lastname` varchar(100) NOT NULL,
  `staff_tel` varchar(50) NOT NULL,
  `staff_emergency` varchar(50) NOT NULL,
  `staff_email` varchar(100) NOT NULL,
  `staff_dob` varchar(50) NOT NULL,
  `staff_postal` text NOT NULL,
  `staff_housenum` varchar(100) NOT NULL,
  `staff_houseloc` text NOT NULL,
  `staff_type` varchar(50) NOT NULL,
  `staff_profnum` varchar(100) NOT NULL,
  `staff_employdate` varchar(50) NOT NULL,
  `staff_note` text NOT NULL,
  `staff_pic` varchar(100) NOT NULL,
  `added` varchar(100) NOT NULL,
  `record_hide` varchar(10) NOT NULL,
  `user_id` int(11) NOT NULL,
  `last_updated` timestamp NOT NULL DEFAULT current_timestamp(),
  `staff_acc_user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`staff_id`, `staff_firstname`, `staff_middlename`, `staff_lastname`, `staff_tel`, `staff_emergency`, `staff_email`, `staff_dob`, `staff_postal`, `staff_housenum`, `staff_houseloc`, `staff_type`, `staff_profnum`, `staff_employdate`, `staff_note`, `staff_pic`, `added`, `record_hide`, `user_id`, `last_updated`, `staff_acc_user_id`) VALUES
(9, 'Ferdinand', 'p', 'kwarteng', '0209969656', '0209969656', '', '08-05-2019', 'Accra', '', 'Accra', 'Attorney', 't123', '23-05-2019', 'Best guy', 'Koala.jpg', '23rd May 2019 / 08:39:24 PM', 'NO', 1, '2019-05-23 20:39:24', 0),
(10, 'asdf', 'asdf', 'asdf', 'asdf', '', '', '', 'asdf', '', 'adsf', 'Paralegal', 'asdf', '', 'asdf', 'NONE', '26th May 2019 / 02:23:42 PM', 'NO', 1, '2019-05-26 14:23:42', 0),
(11, 'asdf', 'asdf', 'asdf', '', '', '', '', 'asdf', '', 'asdf', 'Attorney', 'asdf', '15-05-2019', 'asdf', 'Tulips.jpg', '26th May 2019 / 02:27:43 PM', 'NO', 1, '2019-05-26 14:27:43', 0),
(12, 'herbert', '', 'otchere', '', '', '', '', '', '', '', 'None', '', '', '', 'NONE', '7th August 2019 / 02:39:39 PM', 'NO', 6, '2019-08-07 14:39:39', 0);

-- --------------------------------------------------------

--
-- Table structure for table `sub_court`
--

CREATE TABLE `sub_court` (
  `sub_court_id` int(11) NOT NULL,
  `main_court_select_id` int(11) NOT NULL,
  `sub_court_name` varchar(200) NOT NULL,
  `sub_court_region` varchar(200) NOT NULL,
  `added` varchar(150) NOT NULL,
  `record_hide` varchar(5) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_court`
--

INSERT INTO `sub_court` (`sub_court_id`, `main_court_select_id`, `sub_court_name`, `sub_court_region`, `added`, `record_hide`, `user_id`) VALUES
(1, 1, 'Supreme Court', 'Greater_Accra', '22nd October 2019 / 02:41:47 AM', 'NO', 6),
(2, 3, 'Court Of Appeal Greater Accra', 'Greater_Accra', '22nd October 2019 / 03:48:27 AM', 'NO', 6),
(3, 3, 'Court Of Appeal Ashanti Region', 'Ashanti', '22nd October 2019 / 03:48:42 AM', 'NO', 6),
(4, 4, 'General Jurisdiction', 'Greater_Accra', '22nd October 2019 / 03:49:35 AM', 'NO', 6),
(5, 4, 'Commercial Court', 'Greater_Accra', '22nd October 2019 / 03:49:48 AM', 'NO', 6),
(6, 4, 'Financial Crime', 'Greater_Accra', '22nd October 2019 / 03:50:04 AM', 'NO', 6),
(7, 4, 'Criminal Court', 'Greater_Accra', '22nd October 2019 / 03:50:17 AM', 'NO', 6),
(8, 4, 'Financial Crime', 'Greater_Accra', '22nd October 2019 / 03:50:39 AM', 'NO', 6),
(9, 4, 'Criminal Court', 'Greater_Accra', '22nd October 2019 / 03:50:57 AM', 'NO', 6),
(10, 4, 'Land Court', 'Greater_Accra', '22nd October 2019 / 03:51:12 AM', 'NO', 6),
(11, 4, 'Probate', 'Greater_Accra', '22nd October 2019 / 03:51:32 AM', 'NO', 6),
(12, 4, 'Divorce And Matrimonial', 'Greater_Accra', '22nd October 2019 / 03:51:55 AM', 'NO', 6),
(14, 4, 'Human Rights', 'Greater_Accra', '22nd October 2019 / 03:52:30 AM', 'NO', 6),
(15, 4, 'Labour Court', 'Greater_Accra', '22nd October 2019 / 03:52:50 AM', 'NO', 6),
(16, 2, 'District Court', 'Greater_Accra', '26th October 2019 / 12:49:41 AM', 'NO', 6);

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

CREATE TABLE `tasks` (
  `task_id` int(11) NOT NULL,
  `court_case_id` int(11) NOT NULL,
  `task_name` varchar(200) NOT NULL,
  `task_due_date` varchar(50) NOT NULL,
  `task_checklist` text NOT NULL,
  `task_priority` varchar(50) NOT NULL,
  `task_description` text NOT NULL,
  `task_assign_staff` text NOT NULL,
  `task_status` varchar(50) NOT NULL,
  `task_complete_user` int(11) NOT NULL,
  `task_complete_date` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL,
  `added` varchar(50) NOT NULL,
  `record_hide` varchar(10) NOT NULL,
  `last_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tasks`
--

INSERT INTO `tasks` (`task_id`, `court_case_id`, `task_name`, `task_due_date`, `task_checklist`, `task_priority`, `task_description`, `task_assign_staff`, `task_status`, `task_complete_user`, `task_complete_date`, `user_id`, `added`, `record_hide`, `last_updated`) VALUES
(1, 3, 'Task Name', '12-06-2019', '[\"Hello\",\"World\",\"Test\"]', 'High', 'Decription Of Tasks', '[\"11\",\"10\",\"9\"]', 'COMPLETE', 6, '02-08-2019 | 09:13:55 AM', 6, '10-06-2019', 'NO', '2019-06-10 12:39:48'),
(2, 2, 'Jane Doe tasks', '13-07-2019', '[\"Print Docs\",\"12asdf123\"]', 'No Priority', 'Here 123', '[\"11\",\"10\",\"9\"]', 'COMPLETE', 6, '10-06-2019 | 11:47:32 PM', 6, '10-06-2019', 'NO', '2019-06-10 12:42:33'),
(3, 3, 'Create Docket', '30-06-2019', '[\"Sign Agreement1\",\"Send Client Sms\",\"Prepare Docket and meet me at 5pm on 1st July\"]', 'Medium', 'This should be done with your colleagues123', '[\"11\",\"10\"]', 'INCOMPLETE', 0, '', 6, '10-06-2019', 'NO', '2019-06-10 23:50:20'),
(5, 3, 'Access Client information and see what to do', '05-11-2019', '[\"asdf\",\"asdf\",\"ANOTHER ONE\"]', 'High', 'Please get this done by due date', '[\"12\",\"10\",\"9\"]', 'COMPLETE', 6, '06-11-2019 | 12:12:06 AM', 6, '05-11-2019', 'NO', '2019-11-05 23:58:22');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `users_id` int(11) NOT NULL,
  `account_name` varchar(50) NOT NULL,
  `account_id` int(11) NOT NULL,
  `user_name` varchar(100) NOT NULL,
  `user_password` varchar(100) NOT NULL,
  `password_reset` varchar(5) NOT NULL,
  `group_id` int(11) NOT NULL,
  `acc_status` varchar(10) NOT NULL,
  `acc_login_status` varchar(10) NOT NULL,
  `record_hide` varchar(10) NOT NULL,
  `added` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL,
  `last_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`users_id`, `account_name`, `account_id`, `user_name`, `user_password`, `password_reset`, `group_id`, `acc_status`, `acc_login_status`, `record_hide`, `added`, `user_id`, `last_updated`) VALUES
(6, 'staff', 9, 'f.kwarteng', '$2y$10$EYiUB2jJtUQdgVV6DtcxnOCsyfYQhb8vMjf8YPSUDxD1h28rxXle.', 'off', 1, 'on', 'ONLINE', 'NO', '23rd May 2019 / 11:32:06 PM', 1, '2019-05-23 23:32:06'),
(19, 'contact', 3, 's.kwarteng', '$2y$10$EYiUB2jJtUQdgVV6DtcxnOCsyfYQhb8vMjf8YPSUDxD1h28rxXle.', 'off', 1, 'on', 'OFFLINE', 'NO', '23rd May 2019 / 11:32:06 PM', 1, '2019-10-23 09:08:56');

-- --------------------------------------------------------

--
-- Table structure for table `users_session_log`
--

CREATE TABLE `users_session_log` (
  `users_session_log_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `session_start` varchar(100) NOT NULL,
  `session_end` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_session_log`
--

INSERT INTO `users_session_log` (`users_session_log_id`, `user_id`, `session_start`, `session_end`) VALUES
(54, 6, '28th May 2019 / 11:20:02 PM', ''),
(55, 6, '29th May 2019 / 01:56:04 PM', ''),
(56, 6, '31st May 2019 / 11:39:01 AM', ''),
(57, 6, '2nd June 2019 / 07:02:24 PM', ''),
(58, 10, '2nd June 2019 / 10:50:52 PM', 'Sunday 2nd of June 2019 / 11:09:18 PM'),
(59, 10, '2nd June 2019 / 11:09:27 PM', 'Sunday 2nd of June 2019 / 11:11:20 PM'),
(60, 10, '2nd June 2019 / 11:11:28 PM', 'Sunday 2nd of June 2019 / 11:11:58 PM'),
(61, 6, '2nd June 2019 / 11:12:06 PM', 'Sunday 2nd of June 2019 / 11:13:30 PM'),
(62, 6, '2nd June 2019 / 11:13:38 PM', 'Sunday 2nd of June 2019 / 11:13:41 PM'),
(63, 10, '2nd June 2019 / 11:13:51 PM', 'Sunday 2nd of June 2019 / 11:16:35 PM'),
(64, 6, '2nd June 2019 / 11:18:59 PM', 'Sunday 2nd of June 2019 / 11:30:30 PM'),
(65, 11, '2nd June 2019 / 11:31:14 PM', 'Sunday 2nd of June 2019 / 11:35:40 PM'),
(66, 11, '2nd June 2019 / 11:35:48 PM', 'Sunday 2nd of June 2019 / 11:36:31 PM'),
(67, 6, '2nd June 2019 / 11:36:44 PM', 'Monday 3rd of June 2019 / 10:16:14 AM'),
(68, 6, '3rd June 2019 / 10:16:20 AM', 'Monday 3rd of June 2019 / 02:44:38 PM'),
(69, 6, '3rd June 2019 / 03:16:13 PM', 'Monday 3rd of June 2019 / 04:42:46 PM'),
(70, 6, '3rd June 2019 / 04:42:56 PM', ''),
(71, 6, '4th June 2019 / 09:07:39 AM', 'Thursday 20th of June 2019 / 11:54:26 AM'),
(72, 6, '4th June 2019 / 09:32:28 AM', 'Thursday 20th of June 2019 / 06:32:44 PM'),
(73, 6, '4th June 2019 / 09:35:10 AM', ''),
(74, 6, '4th June 2019 / 09:36:15 AM', ''),
(75, 6, '5th June 2019 / 10:45:07 AM', ''),
(76, 6, '5th June 2019 / 04:28:01 PM', ''),
(77, 6, '5th June 2019 / 07:58:09 PM', 'Wednesday 5th of June 2019 / 07:58:09 PM'),
(78, 6, '5th June 2019 / 07:58:23 PM', ''),
(79, 6, '6th June 2019 / 12:54:14 PM', ''),
(80, 6, '6th June 2019 / 01:13:18 PM', ''),
(81, 6, '8th June 2019 / 02:57:52 PM', 'Saturday 8th of June 2019 / 02:57:52 PM'),
(82, 6, '8th June 2019 / 02:58:01 PM', 'Saturday 8th of June 2019 / 03:02:25 PM'),
(83, 6, '10th June 2019 / 10:05:19 AM', 'Monday 10th of June 2019 / 09:59:53 PM'),
(84, 6, '10th June 2019 / 10:00:26 PM', 'Tuesday 11th of June 2019 / 12:03:07 AM'),
(85, 6, '11th June 2019 / 10:20:32 AM', 'Tuesday 11th of June 2019 / 12:41:15 PM'),
(86, 6, '11th June 2019 / 12:41:36 PM', ''),
(87, 6, '11th June 2019 / 04:51:30 PM', ''),
(88, 6, '11th June 2019 / 05:40:20 PM', 'Tuesday 11th of June 2019 / 05:54:39 PM'),
(89, 6, '11th June 2019 / 09:59:27 PM', ''),
(90, 6, '11th June 2019 / 10:05:55 PM', ''),
(91, 6, '11th June 2019 / 10:23:59 PM', ''),
(92, 6, '11th June 2019 / 11:37:39 PM', 'Tuesday 11th of June 2019 / 11:37:54 PM'),
(93, 6, '12th June 2019 / 10:39:33 AM', ''),
(94, 6, '12th June 2019 / 11:45:32 AM', ''),
(95, 6, '12th June 2019 / 01:01:17 PM', 'Wednesday 12th of June 2019 / 03:20:27 PM'),
(96, 6, '12th June 2019 / 03:20:32 PM', 'Wednesday 12th of June 2019 / 08:17:26 PM'),
(97, 6, '12th June 2019 / 08:17:32 PM', 'Wednesday 12th of June 2019 / 11:15:15 PM'),
(98, 6, '12th June 2019 / 11:15:23 PM', 'Thursday 13th of June 2019 / 08:41:17 AM'),
(99, 6, '13th June 2019 / 08:52:43 AM', ''),
(100, 6, '13th June 2019 / 03:52:42 PM', 'Thursday 13th of June 2019 / 09:38:43 PM'),
(101, 6, '13th June 2019 / 08:01:26 PM', 'Thursday 13th of June 2019 / 11:44:55 PM'),
(102, 6, '13th June 2019 / 09:39:03 PM', ''),
(103, 6, '13th June 2019 / 10:00:32 PM', 'Thursday 13th of June 2019 / 10:01:06 PM'),
(104, 6, '13th June 2019 / 10:01:18 PM', 'Thursday 13th of June 2019 / 10:01:31 PM'),
(105, 6, '13th June 2019 / 10:01:46 PM', ''),
(106, 6, '13th June 2019 / 11:45:01 PM', 'Friday 14th of June 2019 / 09:34:21 AM'),
(107, 6, '14th June 2019 / 10:28:28 PM', 'Saturday 15th of June 2019 / 01:15:15 PM'),
(108, 6, '15th June 2019 / 04:08:00 PM', 'Saturday 15th of June 2019 / 05:53:25 PM'),
(109, 6, '15th June 2019 / 05:53:30 PM', 'Saturday 15th of June 2019 / 10:00:41 PM'),
(110, 6, '15th June 2019 / 11:25:55 PM', 'Sunday 16th of June 2019 / 09:29:00 AM'),
(111, 6, '16th June 2019 / 12:54:20 AM', ''),
(112, 6, '16th June 2019 / 09:29:06 AM', 'Sunday 16th of June 2019 / 10:28:40 AM'),
(113, 6, '16th June 2019 / 10:36:06 AM', 'Sunday 16th of June 2019 / 12:07:06 PM'),
(114, 6, '16th June 2019 / 12:07:12 PM', 'Sunday 16th of June 2019 / 03:28:01 PM'),
(115, 6, '16th June 2019 / 03:51:38 PM', 'Sunday 16th of June 2019 / 05:36:12 PM'),
(116, 6, '16th June 2019 / 05:36:18 PM', 'Sunday 16th of June 2019 / 07:25:12 PM'),
(117, 6, '16th June 2019 / 07:25:24 PM', ''),
(118, 6, '16th June 2019 / 07:33:16 PM', 'Sunday 16th of June 2019 / 11:55:04 PM'),
(119, 12, '16th June 2019 / 11:55:53 PM', 'Sunday 16th of June 2019 / 11:56:02 PM'),
(120, 6, '17th June 2019 / 12:00:33 AM', ''),
(121, 6, '17th June 2019 / 12:09:00 AM', ''),
(122, 6, '17th June 2019 / 12:13:00 AM', 'Monday 17th of June 2019 / 12:19:14 AM'),
(123, 6, '17th June 2019 / 07:47:14 AM', 'Monday 17th of June 2019 / 10:39:23 AM'),
(124, 6, '17th June 2019 / 11:10:58 AM', 'Monday 17th of June 2019 / 07:20:24 PM'),
(125, 6, '17th June 2019 / 07:20:31 PM', ''),
(126, 6, '18th June 2019 / 12:59:31 AM', 'Tuesday 18th of June 2019 / 12:59:31 AM'),
(127, 6, '18th June 2019 / 12:59:40 AM', ''),
(128, 6, '18th June 2019 / 03:23:02 PM', 'Tuesday 18th of June 2019 / 03:35:29 PM'),
(129, 6, '18th June 2019 / 08:39:05 PM', ''),
(130, 6, '18th June 2019 / 09:07:12 PM', ''),
(131, 6, '18th June 2019 / 10:12:33 PM', 'Tuesday 18th of June 2019 / 10:12:33 PM'),
(132, 6, '18th June 2019 / 10:12:40 PM', ''),
(133, 6, '19th June 2019 / 09:13:29 AM', ''),
(134, 6, '19th June 2019 / 11:23:30 AM', 'Wednesday 19th of June 2019 / 11:53:46 AM'),
(135, 6, '19th June 2019 / 01:41:34 PM', 'Wednesday 19th of June 2019 / 09:10:31 PM'),
(136, 6, '19th June 2019 / 11:36:45 PM', 'Thursday 20th of June 2019 / 12:58:33 AM'),
(137, 6, '20th June 2019 / 08:22:16 AM', ''),
(138, 6, '20th June 2019 / 11:54:42 AM', 'Thursday 20th of June 2019 / 03:33:50 PM'),
(139, 6, '20th June 2019 / 03:33:55 PM', ''),
(140, 6, '20th June 2019 / 05:08:30 PM', ''),
(141, 6, '20th June 2019 / 07:19:19 PM', 'Thursday 20th of June 2019 / 08:44:10 PM'),
(142, 6, '20th June 2019 / 08:44:21 PM', 'Thursday 20th of June 2019 / 10:51:08 PM'),
(143, 6, '20th June 2019 / 10:25:35 PM', 'Thursday 20th of June 2019 / 10:40:43 PM'),
(144, 6, '20th June 2019 / 10:51:17 PM', 'Friday 21st of June 2019 / 07:54:47 AM'),
(145, 6, '20th June 2019 / 10:51:49 PM', ''),
(146, 6, '21st June 2019 / 12:43:44 AM', ''),
(147, 6, '21st June 2019 / 07:54:53 AM', ''),
(148, 6, '21st June 2019 / 09:57:13 AM', 'Friday 21st of June 2019 / 12:01:00 PM'),
(149, 6, '21st June 2019 / 01:12:17 PM', 'Friday 21st of June 2019 / 10:42:10 PM'),
(150, 6, '21st June 2019 / 10:42:17 PM', 'Saturday 22nd of June 2019 / 09:10:59 AM'),
(151, 6, '22nd June 2019 / 09:11:09 AM', 'Saturday 22nd of June 2019 / 11:44:43 AM'),
(152, 6, '22nd June 2019 / 01:26:21 PM', ''),
(153, 6, '22nd June 2019 / 04:12:22 PM', 'Sunday 23rd of June 2019 / 09:55:08 AM'),
(154, 6, '23rd June 2019 / 09:55:17 AM', 'Sunday 23rd of June 2019 / 07:02:43 PM'),
(155, 6, '23rd June 2019 / 07:02:48 PM', 'Sunday 23rd of June 2019 / 09:32:19 PM'),
(156, 6, '23rd June 2019 / 09:32:32 PM', 'Monday 24th of June 2019 / 05:48:36 PM'),
(157, 6, '24th June 2019 / 05:09:30 PM', 'Monday 24th of June 2019 / 05:47:16 PM'),
(158, 6, '24th June 2019 / 07:15:36 PM', 'Monday 24th of June 2019 / 07:44:31 PM'),
(159, 6, '24th June 2019 / 07:44:41 PM', 'Monday 24th of June 2019 / 08:01:08 PM'),
(160, 10, '24th June 2019 / 08:01:48 PM', 'Monday 24th of June 2019 / 08:02:03 PM'),
(161, 6, '24th June 2019 / 08:02:11 PM', 'Monday 24th of June 2019 / 08:07:42 PM'),
(162, 6, '24th June 2019 / 08:08:08 PM', 'Monday 24th of June 2019 / 08:08:40 PM'),
(163, 10, '24th June 2019 / 08:08:55 PM', ''),
(164, 6, '24th June 2019 / 08:50:32 PM', 'Monday 24th of June 2019 / 08:50:36 PM'),
(165, 10, '24th June 2019 / 08:50:41 PM', ''),
(166, 6, '24th June 2019 / 08:54:09 PM', 'Monday 24th of June 2019 / 09:01:50 PM'),
(167, 10, '24th June 2019 / 09:02:02 PM', ''),
(168, 6, '24th June 2019 / 09:02:22 PM', 'Monday 24th of June 2019 / 09:03:02 PM'),
(169, 6, '24th June 2019 / 09:03:10 PM', 'Monday 24th of June 2019 / 09:03:20 PM'),
(170, 6, '24th June 2019 / 09:03:27 PM', ''),
(171, 10, '24th June 2019 / 09:53:38 PM', ''),
(172, 6, '24th June 2019 / 10:18:20 PM', 'Monday 24th of June 2019 / 10:18:23 PM'),
(173, 10, '24th June 2019 / 10:18:38 PM', ''),
(174, 10, '24th June 2019 / 10:26:53 PM', ''),
(175, 6, '24th June 2019 / 10:28:11 PM', 'Monday 24th of June 2019 / 10:28:17 PM'),
(176, 10, '24th June 2019 / 10:28:27 PM', 'Monday 24th of June 2019 / 10:36:33 PM'),
(177, 10, '24th June 2019 / 10:36:43 PM', 'Monday 24th of June 2019 / 10:47:47 PM'),
(178, 6, '24th June 2019 / 10:43:21 PM', ''),
(179, 6, '24th June 2019 / 10:47:57 PM', 'Monday 24th of June 2019 / 10:48:02 PM'),
(180, 10, '24th June 2019 / 10:48:16 PM', ''),
(181, 10, '24th June 2019 / 11:51:38 PM', 'Tuesday 25th of June 2019 / 09:56:57 AM'),
(182, 10, '25th June 2019 / 09:57:10 AM', ''),
(183, 6, '25th June 2019 / 10:46:19 AM', 'Tuesday 25th of June 2019 / 10:46:23 AM'),
(184, 10, '25th June 2019 / 10:46:30 AM', 'Tuesday 25th of June 2019 / 05:07:55 PM'),
(185, 10, '25th June 2019 / 04:30:16 PM', 'Tuesday 25th of June 2019 / 04:30:55 PM'),
(186, 6, '25th June 2019 / 04:31:16 PM', ''),
(187, 6, '25th June 2019 / 05:08:03 PM', 'Tuesday 25th of June 2019 / 05:27:45 PM'),
(188, 10, '25th June 2019 / 05:27:50 PM', 'Tuesday 25th of June 2019 / 10:46:45 PM'),
(189, 6, '25th June 2019 / 05:30:32 PM', 'Tuesday 25th of June 2019 / 05:31:44 PM'),
(190, 10, '25th June 2019 / 05:31:58 PM', ''),
(191, 10, '25th June 2019 / 07:48:44 PM', 'Tuesday 25th of June 2019 / 07:53:29 PM'),
(192, 6, '25th June 2019 / 07:53:39 PM', ''),
(193, 10, '25th June 2019 / 10:47:00 PM', 'Wednesday 26th of June 2019 / 10:37:41 AM'),
(194, 6, '26th June 2019 / 10:16:01 AM', 'Wednesday 26th of June 2019 / 10:22:24 AM'),
(195, 6, '26th June 2019 / 10:22:34 AM', ''),
(196, 6, '26th June 2019 / 02:20:52 PM', ''),
(197, 6, '26th June 2019 / 02:56:33 PM', '26th June 2019 / 05:16:58 PM'),
(198, 6, '26th June 2019 / 05:17:06 PM', ''),
(199, 6, '27th June 2019 / 05:34:54 PM', '27th June 2019 / 07:15:36 PM'),
(200, 6, '27th June 2019 / 07:15:45 PM', '27th June 2019 / 10:44:52 PM'),
(201, 6, '27th June 2019 / 09:07:13 PM', ''),
(202, 10, '27th June 2019 / 10:45:06 PM', '27th June 2019 / 10:57:26 PM'),
(203, 6, '27th June 2019 / 10:57:32 PM', '28th June 2019 / 12:31:33 AM'),
(204, 6, '28th June 2019 / 12:31:39 AM', '28th June 2019 / 01:50:04 AM'),
(205, 10, '28th June 2019 / 01:50:15 AM', '28th June 2019 / 01:51:30 AM'),
(206, 6, '28th June 2019 / 07:52:29 AM', '28th June 2019 / 08:33:46 AM'),
(207, 10, '28th June 2019 / 08:33:54 AM', '28th June 2019 / 08:34:30 AM'),
(208, 6, '28th June 2019 / 08:34:41 AM', ''),
(209, 6, '28th June 2019 / 09:49:49 AM', '28th June 2019 / 09:53:59 AM'),
(210, 10, '28th June 2019 / 09:54:15 AM', '28th June 2019 / 09:54:41 AM'),
(211, 10, '28th June 2019 / 03:43:18 PM', '28th June 2019 / 03:44:02 PM'),
(212, 6, '28th June 2019 / 03:44:09 PM', '29th June 2019 / 05:48:48 PM'),
(213, 6, '1st July 2019 / 06:17:41 PM', '1st July 2019 / 06:29:33 PM'),
(214, 10, '1st July 2019 / 06:29:48 PM', '1st July 2019 / 06:30:21 PM'),
(215, 6, '2nd July 2019 / 12:17:39 AM', '2nd July 2019 / 10:08:55 AM'),
(216, 6, '2nd July 2019 / 10:09:10 AM', '2nd July 2019 / 10:12:04 AM'),
(217, 10, '2nd July 2019 / 10:12:18 AM', '2nd July 2019 / 10:13:25 AM'),
(218, 6, '2nd July 2019 / 10:13:37 AM', '2nd July 2019 / 10:21:44 AM'),
(219, 6, '3rd July 2019 / 10:07:43 PM', ''),
(220, 6, '9th July 2019 / 09:47:26 AM', ''),
(221, 6, '9th July 2019 / 11:24:53 AM', '9th July 2019 / 11:42:03 AM'),
(222, 6, '12th July 2019 / 08:53:18 AM', '12th July 2019 / 08:53:18 AM'),
(223, 6, '12th July 2019 / 08:53:25 AM', '12th July 2019 / 11:58:03 AM'),
(224, 6, '13th July 2019 / 09:31:09 AM', '13th July 2019 / 09:44:16 AM'),
(225, 10, '13th July 2019 / 09:44:45 AM', '13th July 2019 / 09:47:00 AM'),
(226, 6, '13th July 2019 / 09:32:06 PM', ''),
(227, 6, '15th July 2019 / 06:56:36 PM', ''),
(228, 6, '17th July 2019 / 12:21:56 AM', ''),
(229, 6, '18th July 2019 / 12:09:49 PM', '18th July 2019 / 12:15:20 PM'),
(230, 6, '21st July 2019 / 09:16:22 AM', '21st July 2019 / 09:45:57 AM'),
(231, 6, '21st July 2019 / 09:46:14 AM', '21st July 2019 / 01:31:15 PM'),
(232, 6, '21st July 2019 / 01:31:21 PM', '21st July 2019 / 01:36:12 PM'),
(233, 6, '21st July 2019 / 01:36:19 PM', '21st July 2019 / 04:14:55 PM'),
(234, 6, '22nd July 2019 / 11:37:17 PM', ''),
(235, 6, '23rd July 2019 / 09:17:35 AM', '23rd July 2019 / 09:17:47 AM'),
(236, 6, '23rd July 2019 / 09:18:12 AM', ''),
(237, 6, '1st August 2019 / 10:56:10 PM', '1st August 2019 / 11:23:14 PM'),
(238, 6, '1st August 2019 / 11:33:07 PM', '1st August 2019 / 11:33:10 PM'),
(239, 6, '1st August 2019 / 11:33:25 PM', ''),
(240, 6, '1st August 2019 / 11:34:34 PM', '1st August 2019 / 11:37:23 PM'),
(241, 6, '1st August 2019 / 11:40:49 PM', '1st August 2019 / 11:41:21 PM'),
(242, 6, '1st August 2019 / 11:48:26 PM', '2nd August 2019 / 12:28:07 AM'),
(243, 6, '2nd August 2019 / 09:09:15 AM', '2nd August 2019 / 09:17:40 AM'),
(244, 6, '2nd August 2019 / 09:39:17 AM', ''),
(245, 6, '2nd August 2019 / 01:23:17 PM', '2nd August 2019 / 01:23:17 PM'),
(246, 6, '2nd August 2019 / 01:23:24 PM', ''),
(247, 6, '3rd August 2019 / 10:15:20 AM', '3rd August 2019 / 10:15:20 AM'),
(248, 6, '3rd August 2019 / 10:15:29 AM', '3rd August 2019 / 10:15:48 AM'),
(249, 6, '7th August 2019 / 02:32:50 PM', '7th August 2019 / 02:40:11 PM'),
(250, 16, '7th August 2019 / 02:40:19 PM', '7th August 2019 / 06:53:51 PM'),
(251, 6, '11th August 2019 / 03:38:31 PM', ''),
(252, 6, '14th August 2019 / 09:36:19 AM', ''),
(253, 6, '14th August 2019 / 01:15:40 PM', ''),
(254, 6, '16th August 2019 / 12:44:30 AM', ''),
(255, 6, '16th August 2019 / 04:53:31 PM', '16th August 2019 / 05:07:45 PM'),
(256, 10, '16th August 2019 / 05:07:57 PM', '16th August 2019 / 05:09:09 PM'),
(257, 6, '16th August 2019 / 05:09:22 PM', ''),
(258, 6, '16th August 2019 / 05:47:25 PM', ''),
(259, 6, '16th August 2019 / 11:15:13 PM', ''),
(260, 6, '20th August 2019 / 11:25:10 PM', '20th August 2019 / 11:44:21 PM'),
(261, 17, '20th August 2019 / 11:44:59 PM', '20th August 2019 / 11:45:11 PM'),
(262, 6, '21st August 2019 / 02:26:54 AM', '21st August 2019 / 02:30:03 AM'),
(263, 6, '21st August 2019 / 07:59:47 AM', '21st August 2019 / 08:00:06 AM'),
(264, 6, '21st August 2019 / 09:47:33 AM', ''),
(265, 6, '21st August 2019 / 10:17:29 AM', '21st August 2019 / 11:20:57 AM'),
(266, 10, '21st August 2019 / 11:21:08 AM', '21st August 2019 / 11:34:38 AM'),
(267, 6, '21st August 2019 / 11:34:42 AM', '21st August 2019 / 11:36:47 AM'),
(268, 6, '30th August 2019 / 03:18:47 PM', '30th August 2019 / 03:21:32 PM'),
(269, 6, '1st September 2019 / 12:03:43 PM', '1st September 2019 / 12:05:29 PM'),
(270, 6, '1st September 2019 / 12:05:46 PM', '1st September 2019 / 09:31:15 PM'),
(271, 6, '1st September 2019 / 09:31:22 PM', ''),
(272, 6, '2nd September 2019 / 05:13:46 PM', ''),
(273, 6, '3rd September 2019 / 06:34:00 PM', '3rd September 2019 / 07:55:46 PM'),
(274, 6, '6th September 2019 / 08:33:35 AM', '6th September 2019 / 11:56:33 AM'),
(275, 6, '6th September 2019 / 11:56:39 AM', '6th September 2019 / 11:56:56 AM'),
(276, 6, '13th September 2019 / 07:51:50 AM', '13th September 2019 / 07:56:33 AM'),
(277, 6, '13th September 2019 / 03:59:01 PM', '13th September 2019 / 03:59:01 PM'),
(278, 6, '13th September 2019 / 03:59:11 PM', '13th September 2019 / 04:07:49 PM'),
(279, 6, '13th September 2019 / 04:08:39 PM', '13th September 2019 / 04:09:31 PM'),
(280, 6, '13th September 2019 / 04:09:38 PM', ''),
(281, 6, '13th September 2019 / 04:09:46 PM', ''),
(282, 6, '13th September 2019 / 04:10:08 PM', '13th September 2019 / 04:11:48 PM'),
(283, 6, '13th September 2019 / 04:11:55 PM', '13th September 2019 / 04:27:28 PM'),
(284, 6, '19th September 2019 / 09:49:38 AM', '19th September 2019 / 09:49:38 AM'),
(285, 6, '19th September 2019 / 09:49:49 AM', '19th September 2019 / 10:22:27 AM'),
(286, 6, '19th September 2019 / 12:20:11 PM', ''),
(287, 6, '19th September 2019 / 01:22:10 PM', '19th September 2019 / 01:39:09 PM'),
(288, 6, '19th September 2019 / 11:28:12 PM', '19th September 2019 / 11:28:12 PM'),
(289, 6, '19th September 2019 / 11:28:22 PM', ''),
(290, 6, '20th September 2019 / 08:50:28 AM', '20th September 2019 / 08:50:28 AM'),
(291, 6, '20th September 2019 / 08:50:34 AM', ''),
(292, 6, '22nd September 2019 / 03:47:46 PM', '22nd September 2019 / 03:55:37 PM'),
(293, 6, '23rd September 2019 / 02:09:57 AM', ''),
(294, 6, '28th September 2019 / 06:20:03 PM', '28th September 2019 / 06:25:59 PM'),
(295, 6, '28th September 2019 / 06:30:59 PM', ''),
(296, 6, '3rd October 2019 / 12:23:41 AM', ''),
(297, 6, '9th October 2019 / 05:09:47 AM', ''),
(298, 6, '11th October 2019 / 12:35:13 PM', '11th October 2019 / 12:37:43 PM'),
(299, 6, '13th October 2019 / 04:43:47 PM', ''),
(300, 6, '13th October 2019 / 04:48:51 PM', ''),
(301, 6, '13th October 2019 / 04:52:28 PM', '13th October 2019 / 07:54:25 PM'),
(302, 6, '13th October 2019 / 07:54:31 PM', '13th October 2019 / 08:41:03 PM'),
(303, 6, '13th October 2019 / 08:41:10 PM', '13th October 2019 / 09:34:02 PM'),
(304, 6, '13th October 2019 / 09:34:09 PM', '13th October 2019 / 11:45:41 PM'),
(305, 6, '13th October 2019 / 11:45:50 PM', '14th October 2019 / 10:34:03 AM'),
(306, 6, '15th October 2019 / 01:27:28 PM', ''),
(307, 6, '16th October 2019 / 05:19:07 PM', '16th October 2019 / 05:33:24 PM'),
(308, 6, '16th October 2019 / 05:33:42 PM', '16th October 2019 / 05:33:54 PM'),
(309, 6, '16th October 2019 / 05:34:01 PM', '16th October 2019 / 07:58:40 PM'),
(310, 6, '16th October 2019 / 07:58:46 PM', '16th October 2019 / 09:17:55 PM'),
(311, 6, '16th October 2019 / 09:18:00 PM', ''),
(312, 6, '16th October 2019 / 10:34:54 PM', '17th October 2019 / 05:49:47 AM'),
(313, 6, '17th October 2019 / 05:50:03 AM', '17th October 2019 / 08:56:41 AM'),
(314, 6, '17th October 2019 / 08:56:47 AM', '17th October 2019 / 11:45:58 AM'),
(315, 6, '17th October 2019 / 10:06:13 AM', '17th October 2019 / 01:23:15 PM'),
(316, 6, '17th October 2019 / 11:46:14 AM', '17th October 2019 / 01:23:22 PM'),
(317, 6, '17th October 2019 / 01:23:29 PM', '17th October 2019 / 01:23:41 PM'),
(318, 6, '18th October 2019 / 06:13:34 AM', ''),
(319, 6, '18th October 2019 / 06:19:25 AM', '18th October 2019 / 08:08:05 AM'),
(320, 6, '18th October 2019 / 08:08:29 AM', '18th October 2019 / 09:30:21 AM'),
(321, 6, '18th October 2019 / 09:30:29 AM', '18th October 2019 / 11:08:43 AM'),
(322, 6, '18th October 2019 / 11:22:06 AM', '18th October 2019 / 01:53:30 PM'),
(323, 6, '18th October 2019 / 01:53:41 PM', '18th October 2019 / 08:52:56 PM'),
(324, 6, '18th October 2019 / 08:53:07 PM', '19th October 2019 / 08:54:59 AM'),
(325, 6, '19th October 2019 / 08:55:06 AM', '19th October 2019 / 09:33:47 AM'),
(326, 6, '19th October 2019 / 09:33:53 AM', '19th October 2019 / 12:10:14 PM'),
(327, 6, '19th October 2019 / 12:10:19 PM', '19th October 2019 / 01:01:27 PM'),
(328, 6, '19th October 2019 / 01:01:34 PM', '19th October 2019 / 02:43:23 PM'),
(329, 6, '19th October 2019 / 02:43:31 PM', '19th October 2019 / 03:49:12 PM'),
(330, 6, '19th October 2019 / 03:49:17 PM', '19th October 2019 / 06:37:39 PM'),
(331, 6, '19th October 2019 / 06:37:51 PM', '19th October 2019 / 11:12:56 PM'),
(332, 6, '19th October 2019 / 11:13:44 PM', ''),
(333, 6, '20th October 2019 / 01:21:21 PM', '20th October 2019 / 01:23:52 PM'),
(334, 6, '21st October 2019 / 10:17:12 AM', ''),
(335, 6, '21st October 2019 / 01:09:54 PM', '21st October 2019 / 03:44:54 PM'),
(336, 6, '21st October 2019 / 03:45:06 PM', '21st October 2019 / 05:59:05 PM'),
(337, 6, '22nd October 2019 / 02:14:04 AM', '22nd October 2019 / 03:39:43 AM'),
(338, 6, '22nd October 2019 / 03:39:49 AM', '22nd October 2019 / 08:55:58 AM'),
(339, 6, '22nd October 2019 / 08:56:14 AM', '22nd October 2019 / 10:36:08 AM'),
(340, 6, '22nd October 2019 / 10:36:14 AM', '22nd October 2019 / 01:15:24 PM'),
(341, 6, '22nd October 2019 / 01:15:29 PM', '22nd October 2019 / 01:55:00 PM'),
(342, 6, '22nd October 2019 / 01:55:10 PM', '22nd October 2019 / 06:58:03 PM'),
(343, 6, '22nd October 2019 / 06:58:08 PM', ''),
(344, 6, '22nd October 2019 / 08:06:42 PM', ''),
(345, 6, '23rd October 2019 / 07:21:38 AM', '23rd October 2019 / 09:09:08 AM'),
(346, 19, '23rd October 2019 / 09:09:15 AM', '23rd October 2019 / 11:58:38 AM'),
(347, 6, '23rd October 2019 / 11:36:41 PM', '24th October 2019 / 02:12:42 AM'),
(348, 6, '24th October 2019 / 02:13:49 AM', '24th October 2019 / 01:23:00 PM'),
(349, 6, '24th October 2019 / 01:23:07 PM', '24th October 2019 / 02:55:51 PM'),
(350, 6, '24th October 2019 / 02:55:57 PM', '24th October 2019 / 02:56:20 PM'),
(351, 19, '24th October 2019 / 02:56:25 PM', '24th October 2019 / 04:06:04 PM'),
(352, 6, '24th October 2019 / 04:46:46 PM', ''),
(353, 6, '24th October 2019 / 10:33:05 PM', ''),
(354, 6, '24th October 2019 / 10:33:38 PM', '24th October 2019 / 10:36:23 PM'),
(355, 19, '24th October 2019 / 10:36:28 PM', '24th October 2019 / 10:37:07 PM'),
(356, 6, '24th October 2019 / 10:37:12 PM', ''),
(357, 6, '25th October 2019 / 01:03:46 AM', '25th October 2019 / 01:11:46 AM'),
(358, 6, '25th October 2019 / 12:14:04 PM', '25th October 2019 / 12:14:42 PM'),
(359, 6, '25th October 2019 / 06:54:39 PM', '26th October 2019 / 01:30:34 PM'),
(360, 6, '26th October 2019 / 01:30:42 PM', '26th October 2019 / 04:42:51 PM'),
(361, 6, '28th October 2019 / 10:05:20 PM', '28th October 2019 / 10:07:19 PM'),
(362, 6, '30th October 2019 / 11:28:36 AM', ''),
(363, 6, '31st October 2019 / 01:30:48 PM', '31st October 2019 / 01:36:52 PM'),
(364, 6, '3rd November 2019 / 04:25:29 PM', '3rd November 2019 / 04:26:50 PM'),
(365, 6, '5th November 2019 / 01:56:24 PM', '5th November 2019 / 06:11:23 PM'),
(366, 6, '5th November 2019 / 06:12:42 PM', '5th November 2019 / 07:35:20 PM'),
(367, 6, '5th November 2019 / 08:25:37 PM', '5th November 2019 / 09:51:45 PM'),
(368, 6, '5th November 2019 / 09:51:59 PM', '6th November 2019 / 12:54:29 AM'),
(369, 6, '6th November 2019 / 09:24:35 AM', '6th November 2019 / 11:04:43 AM'),
(370, 6, '6th November 2019 / 11:04:51 AM', '6th November 2019 / 11:25:04 AM'),
(371, 6, '6th November 2019 / 12:00:16 PM', '6th November 2019 / 12:45:12 PM'),
(372, 6, '6th November 2019 / 01:40:21 PM', '6th November 2019 / 02:14:56 PM'),
(373, 6, '6th November 2019 / 02:15:01 PM', '6th November 2019 / 02:51:56 PM'),
(374, 6, '6th November 2019 / 06:42:57 PM', '6th November 2019 / 07:22:07 PM'),
(375, 6, '6th November 2019 / 07:22:49 PM', '7th November 2019 / 10:35:25 AM'),
(376, 6, '7th November 2019 / 10:35:33 AM', '7th November 2019 / 03:36:16 PM'),
(377, 6, '7th November 2019 / 11:08:52 PM', '7th November 2019 / 11:38:38 PM'),
(378, 6, '8th November 2019 / 07:21:28 AM', ''),
(379, 6, '8th November 2019 / 12:44:00 PM', '8th November 2019 / 12:44:00 PM'),
(380, 6, '8th November 2019 / 12:44:07 PM', '8th November 2019 / 12:46:25 PM'),
(381, 6, '8th November 2019 / 12:53:15 PM', ''),
(382, 6, '8th November 2019 / 01:28:58 PM', '8th November 2019 / 03:32:13 PM'),
(383, 6, '8th November 2019 / 03:32:48 PM', ''),
(384, 6, '8th November 2019 / 05:56:54 PM', '8th November 2019 / 06:33:30 PM'),
(385, 6, '8th November 2019 / 09:25:54 PM', ''),
(386, 6, '9th November 2019 / 05:56:46 PM', '9th November 2019 / 09:06:29 PM'),
(387, 6, '9th November 2019 / 09:03:00 PM', ''),
(388, 19, '9th November 2019 / 09:06:34 PM', '9th November 2019 / 09:06:55 PM'),
(389, 6, '9th November 2019 / 09:07:03 PM', '9th November 2019 / 11:57:15 PM'),
(390, 6, '10th November 2019 / 10:28:30 AM', '10th November 2019 / 11:11:47 AM'),
(391, 6, '10th November 2019 / 11:11:56 AM', ''),
(392, 6, '10th November 2019 / 12:17:08 PM', ''),
(393, 6, '10th November 2019 / 01:57:51 PM', '10th November 2019 / 01:57:51 PM'),
(394, 6, '10th November 2019 / 01:57:58 PM', '10th November 2019 / 03:07:50 PM'),
(395, 6, '14th November 2019 / 07:13:24 AM', ''),
(396, 6, '14th November 2019 / 07:17:17 AM', '14th November 2019 / 08:47:21 AM'),
(397, 6, '15th November 2019 / 03:50:38 PM', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activities`
--
ALTER TABLE `activities`
  ADD PRIMARY KEY (`activity_id`);

--
-- Indexes for table `bill_expenses`
--
ALTER TABLE `bill_expenses`
  ADD PRIMARY KEY (`bill_expenses_id`);

--
-- Indexes for table `bill_invoice`
--
ALTER TABLE `bill_invoice`
  ADD PRIMARY KEY (`bill_invoice_id`);

--
-- Indexes for table `bill_time_entry`
--
ALTER TABLE `bill_time_entry`
  ADD PRIMARY KEY (`time_entry_id`);

--
-- Indexes for table `case_document`
--
ALTER TABLE `case_document`
  ADD PRIMARY KEY (`case_doc_id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`contact_id`);

--
-- Indexes for table `contacts_company`
--
ALTER TABLE `contacts_company`
  ADD PRIMARY KEY (`con_company_id`);

--
-- Indexes for table `contacts_group`
--
ALTER TABLE `contacts_group`
  ADD PRIMARY KEY (`contact_group_id`);

--
-- Indexes for table `court_case`
--
ALTER TABLE `court_case`
  ADD PRIMARY KEY (`case_id`);

--
-- Indexes for table `court_case_notes`
--
ALTER TABLE `court_case_notes`
  ADD PRIMARY KEY (`court_case_note_id`);

--
-- Indexes for table `error_logs`
--
ALTER TABLE `error_logs`
  ADD PRIMARY KEY (`error_log_id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`event_id`);

--
-- Indexes for table `event_type`
--
ALTER TABLE `event_type`
  ADD PRIMARY KEY (`event_type_id`);

--
-- Indexes for table `leads`
--
ALTER TABLE `leads`
  ADD PRIMARY KEY (`lead_id`);

--
-- Indexes for table `main_court`
--
ALTER TABLE `main_court`
  ADD PRIMARY KEY (`main_court_id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`message_id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`pages_id`);

--
-- Indexes for table `pages_group`
--
ALTER TABLE `pages_group`
  ADD PRIMARY KEY (`pages_group_id`);

--
-- Indexes for table `practice_area`
--
ALTER TABLE `practice_area`
  ADD PRIMARY KEY (`practice_area_id`);

--
-- Indexes for table `sms`
--
ALTER TABLE `sms`
  ADD PRIMARY KEY (`sms_id`);

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`staff_id`);

--
-- Indexes for table `sub_court`
--
ALTER TABLE `sub_court`
  ADD PRIMARY KEY (`sub_court_id`);

--
-- Indexes for table `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`task_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`users_id`);

--
-- Indexes for table `users_session_log`
--
ALTER TABLE `users_session_log`
  ADD PRIMARY KEY (`users_session_log_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activities`
--
ALTER TABLE `activities`
  MODIFY `activity_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=363;

--
-- AUTO_INCREMENT for table `bill_expenses`
--
ALTER TABLE `bill_expenses`
  MODIFY `bill_expenses_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `bill_invoice`
--
ALTER TABLE `bill_invoice`
  MODIFY `bill_invoice_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `bill_time_entry`
--
ALTER TABLE `bill_time_entry`
  MODIFY `time_entry_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `case_document`
--
ALTER TABLE `case_document`
  MODIFY `case_doc_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `contact_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `contacts_company`
--
ALTER TABLE `contacts_company`
  MODIFY `con_company_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `contacts_group`
--
ALTER TABLE `contacts_group`
  MODIFY `contact_group_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `court_case`
--
ALTER TABLE `court_case`
  MODIFY `case_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `court_case_notes`
--
ALTER TABLE `court_case_notes`
  MODIFY `court_case_note_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `error_logs`
--
ALTER TABLE `error_logs`
  MODIFY `error_log_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `event_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `event_type`
--
ALTER TABLE `event_type`
  MODIFY `event_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `leads`
--
ALTER TABLE `leads`
  MODIFY `lead_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `main_court`
--
ALTER TABLE `main_court`
  MODIFY `main_court_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `message_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `pages_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `pages_group`
--
ALTER TABLE `pages_group`
  MODIFY `pages_group_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `practice_area`
--
ALTER TABLE `practice_area`
  MODIFY `practice_area_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `sms`
--
ALTER TABLE `sms`
  MODIFY `sms_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `staff`
--
ALTER TABLE `staff`
  MODIFY `staff_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `sub_court`
--
ALTER TABLE `sub_court`
  MODIFY `sub_court_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `tasks`
--
ALTER TABLE `tasks`
  MODIFY `task_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `users_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `users_session_log`
--
ALTER TABLE `users_session_log`
  MODIFY `users_session_log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=398;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
