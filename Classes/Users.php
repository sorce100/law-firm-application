<?php 
	date_default_timezone_set('Africa/Accra');
	class Users{
		// setting and getting variables
		private $id;
		private $accountSelect;
		private $accountName;
		private $userName;
		private $userPassword;
		private $accPasswdReset;
		private $accGroup;
		private $accStatus;
		private $loginStatus = "NEVER";
		private $staffAccUserId;
		private $dbConn;
		private $recordHide = "NO";
		private $table = "users";

		function set_id($id) { $this->id = $id; }
		function set_accountSelect($accountSelect) { $this->accountSelect = $accountSelect; }
		function set_accountName($accountName) { $this->accountName = $accountName; }
		function set_userName($userName) { $this->userName = $userName; }
		function set_userPassword($userPassword) { $this->userPassword = $userPassword; }
		function set_accPasswdReset($accPasswdReset) { $this->accPasswdReset = $accPasswdReset; }
		function set_accGroup($accGroup) { $this->accGroup = $accGroup; }
		function set_accStatus($accStatus) { $this->accStatus = $accStatus; }
		function set_staffAccUserId($staffAccUserId) { $this->staffAccUserId = $staffAccUserId; }
		function set_added($added) { $this->added = $added; }
		function set_recordHide($recordHide) { $this->recordHide = $recordHide; }

		public function __construct(){
			require_once("db/db.php");
			$db = new DbConnect();
			$this->dbConn = $db->connect();
		}

		// clean data for data input
		public function CleanData($data){
			$data = trim($data);
			$data=htmlentities($data,ENT_QUOTES, 'UTF-8');
			$data = filter_var($data,FILTER_SANITIZE_SPECIAL_CHARS);
			return $data;
		}

		// for login
		function login(){
			$sql="SELECT * FROM $this->table WHERE user_name = :userName AND acc_status = :accStatus";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":userName",$this->userName);
			$stmt->bindParam(":accStatus",$this->accStatus);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $results;
			}
			else{
				die();
				}

		}

		// insert pages
		function insert(){
			$date = date("jS F Y \/ h:i:s A");
			$sql = "INSERT INTO $this->table (account_id,account_name,user_name,user_password,password_reset,group_id,acc_status,acc_login_status,added,record_hide,user_id) VALUES (:accountId,:accountName,:userName,:userPassword,:passwordReset,:groupId,:accStatus,:loginStatus,:added,:recordHide,:userId)";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":accountId",$this->accountSelect);
			$stmt->bindParam(":accountName",$this->accountName);
			$stmt->bindParam(":userName",$this->userName);
			$stmt->bindParam(":userPassword",$this->userPassword);
			$stmt->bindParam(":passwordReset",$this->accPasswdReset);
			$stmt->bindParam(":groupId",$this->accGroup);
			$stmt->bindParam(":accStatus",$this->accStatus);
			$stmt->bindParam(":loginStatus",$this->loginStatus);
			$stmt->bindParam(":added",$date);
			$stmt->bindParam(":recordHide",$this->recordHide);
			$stmt->bindParam(":userId",$_SESSION['user_id']);
			if ($stmt->execute()) {
				$this->log_activity($this->table,$this->dbConn->lastInsertId(),"Added New User");
				return trim($this->dbConn->lastInsertId());
			}
			else{
				die();
				}
		}
		// for update
		function update(){
			$sql="UPDATE $this->table SET user_name=:userName,user_password=:userPassword,password_reset=:accPasswdReset,group_id=:accGroup,acc_status=:accStatus WHERE users_id=:Id";
				$stmt = $this->dbConn->prepare($sql);
				$stmt->bindParam(":userName",$this->userName);
				$stmt->bindParam(":userPassword",$this->userPassword);
				$stmt->bindParam(":accPasswdReset",$this->accPasswdReset);
				$stmt->bindParam(":accGroup",$this->accGroup);
				$stmt->bindParam(":accStatus",$this->accStatus);
				$stmt->bindParam(":Id",$this->id);
				if ($stmt->execute()) {
					$this->log_activity($this->table,$this->id,"Updated User");
					return true;
				}
				else{
					return false;
					}

		}
		// for delete
		function delete(){
			$sql="UPDATE $this->table SET record_hide=:recordHide WHERE users_id=:Id";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			$stmt->bindParam(":Id",$this->id);
			if ($stmt->execute()) {
				$this->log_activity($this->table,$this->id,"Deleted User");
				return true;
			}
			else{
				return false;
			}
		
		}

		// get users
		function get_users_staff(){
			$this->accountName="staff";
			$sql="SELECT * FROM $this->table WHERE record_hide=:recordHide AND account_name=:accountName ORDER BY users_id DESC";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			$stmt->bindParam(":accountName",$this->accountName);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $results;
			}
			else{
				die();
				}

		}
		// get clients
		function get_users_contact(){
			$this->accountName="contact";
			$sql="SELECT * FROM $this->table WHERE record_hide=:recordHide AND account_name=:accountName ORDER BY users_id DESC";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			$stmt->bindParam(":accountName",$this->accountName);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $results;
			}
			else{
				die();
				}

		}

		// get user by id
		function get_user_by_id(){
			$sql="SELECT * FROM $this->table WHERE users_id=:Id";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":Id",$this->id);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				return json_encode($results);
			}
			else{
				die();
				}
		}

	/// get password of the user
		function get_password(){
			$sql="SELECT user_password FROM $this->table WHERE users_id=:user_id";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":user_id",$this->id);
			if ($stmt->execute()) {
				$results= $stmt->fetch(PDO::FETCH_ASSOC);
				return trim($results['user_password']);
			}
			else{
				die();
				}
		}
	// change password
		function change_password(){
			$sql="UPDATE $this->table SET user_password = :userPassword,password_reset = :passwordReset WHERE user_name=:userName";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":userPassword",$this->userPassword);
			$stmt->bindParam(":passwordReset",$this->accPasswdReset);
			$stmt->bindParam(":userName",$this->userName);
			if ($stmt->execute()) {
				return true;
			}
			else{
				return false;
				}
		}
//////////////////////////////////////////////////////////////////////////
//FOR UPDATING SESSION DETAILS
/////////////////////////////////////////////////////////////////////////

			function session_status_update($loginStatus){
				$sql="UPDATE $this->table SET acc_login_status=:loginStatus WHERE users_id=:Id";
				$stmt = $this->dbConn->prepare($sql);
				$stmt->bindParam(":loginStatus",$loginStatus);
				$stmt->bindParam(":Id",$_SESSION['user_id']);
				if ($stmt->execute()) {
					// if online successful then grab the 
					return true;
				}
				else{
					return false;
					}
				}

			function get_userName($userId){
				$sql="SELECT user_name FROM $this->table WHERE users_id=:Id";
				$stmt = $this->dbConn->prepare($sql);
				$stmt->bindParam(":Id",$userId);
				if ($stmt->execute()) {
					$results = $stmt->fetch(PDO::FETCH_ASSOC);
					return $results["user_name"];
				}
				else{
					return false;
					}
			}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function log_activity($tableName,$tableId,$activityName){
		$sql = "INSERT INTO activities (table_name,table_id,activity_name,account_id,account_name,user_id) VALUES (:tableName,:tableId,:activityName,:accountId,:accountName,:userId)";
		$stmt = $this->dbConn->prepare($sql);
		$stmt->bindParam(":tableName",$tableName);
		$stmt->bindParam(":tableId",$tableId);
		$stmt->bindParam(":tableId",$tableId);
		$stmt->bindParam(":activityName",$activityName);
		$stmt->bindParam(":accountId",$_SESSION['account_id']);
		$stmt->bindParam(":accountName",$_SESSION['account_name']);
		$stmt->bindParam(":userId",$_SESSION['user_id']);
		$stmt->execute();
	}

}

?>