<?php 
	date_default_timezone_set('Africa/Accra');
	class PagesGroup{
		// setting and getting variables
		private $id;
		private $pagesGroupName;
		private $pagesId;
		private $added;
		private $dbConn;
		private $recordHide = "NO";
		private $table = "pages_group";

		function set_id($id) { $this->id = $id; }
		function set_pagesGroupName($pagesGroupName) { $this->pagesGroupName = $pagesGroupName; }
		function set_pagesId($pagesId) { $this->pagesId = $pagesId; }
		function set_recordHide($recordHide) { $this->recordHide = $recordHide; }

		public function __construct(){
			require_once("db/db.php");
			$db = new DbConnect();
			$this->dbConn = $db->connect();
		}

		// clean data for data input
		public function CleanData($data){
			$data = trim($data);
			$data=htmlentities($data,ENT_QUOTES, 'UTF-8');
			$data = filter_var($data,FILTER_SANITIZE_SPECIAL_CHARS);
			return $data;
		}

		// insert pages
		function insert(){
			$date = date("jS F Y \/ h:i:s A");
			$sql = "INSERT INTO $this->table (pages_group_name,pages_id,added,record_hide) VALUES (:pagesGroupName,:pagesId,:added,:recordHide)";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":pagesGroupName",$this->pagesGroupName);
			$stmt->bindParam(":pagesId",$this->pagesId);
			$stmt->bindParam(":added",$date);
			$stmt->bindParam(":recordHide",$this->recordHide);
			if ($stmt->execute()) {
				$this->log_activity($this->table,$this->dbConn->lastInsertId(),"Added New Page Group");
				return true;
			}
			else{
				die();
				}
		}
		// for update
		function update(){
			$sql="UPDATE $this->table SET pages_group_name=:pagesGroupName,pages_id=:pagesId WHERE pages_group_id=:Id";
				$stmt = $this->dbConn->prepare($sql);
				$stmt->bindParam(":pagesGroupName",$this->pagesGroupName);
				$stmt->bindParam(":pagesId",$this->pagesId);
				$stmt->bindParam(":Id",$this->id);
				
				if ($stmt->execute()) {
					$this->log_activity($this->table,$this->id,"Updated Page Group");
					return true;
				}
				else{
					return false;
					}

		}
		// for delete
		function delete(){
			$sql="UPDATE $this->table SET record_hide=:recordHide WHERE pages_group_id=:Id";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			$stmt->bindParam(":Id",$this->id);
			if ($stmt->execute()) {
				$this->log_activity($this->table,$this->id,"Deleted Page Group");
				return true;
			}
			else{
				return false;
			}
		}


	// get pages
		function get_pages_groups(){
			$sql="SELECT * FROM $this->table WHERE record_hide=:recordHide ORDER BY pages_group_id DESC";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $results;
			}
			else{
				die();
				}

		}

	// get pages list
		function get_pages_groups_list(){
			$returnRecords = '';
			$sql="SELECT * FROM $this->table WHERE record_hide=:recordHide ORDER BY pages_group_id DESC";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				if (!empty($results)) {
					foreach ($results as $result) {
						$returnRecords .='
		                                <tr>
		                                  <td>'.$result["pages_group_name"].'</td>
		                                  <td>'.$result["added"].'</td>
		                                  <td><button class="btn-primary update_group_data" id="'.$result["pages_group_id"].'"><i class="fa fa-pencil"></i></button>
		                                  <button class="btn-danger del_data" id="'.$result["pages_group_id"].'"><i class="fa fa-trash"></i></button></td>
		                                </tr>';
					}
				}
				return $returnRecords;
			}
			else{
				die();
			}

		}

	// get user
		function get_group_by_id(){
			$sql="SELECT * FROM $this->table WHERE pages_group_id=:Id";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":Id",$this->id);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				return json_encode($results);
			}
			else{
				die();
				}
			}

	// get group name by id
		function get_groupName_by_id(){
			$sql="SELECT group_id,group_name FROM $this->table WHERE pages_group_id=:groupId";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":groupId",$this->id);
			if ($stmt->execute()) {
				$results= $stmt->fetch(PDO::FETCH_ASSOC);
				return json_encode($results);
			}
			else{
				die();
				}
			}

	// getting all pages for user login dashboard based on group number

			function menu_pages_id($groupId){
				
				$sql="SELECT pages_id FROM $this->table WHERE pages_group_id=:Id";
				$stmt = $this->dbConn->prepare($sql);
				$stmt->bindParam(":Id",$groupId);
				if ($stmt->execute()) {
					$results = $stmt->fetch(PDO::FETCH_ASSOC);
					$pagesId = json_decode($results["pages_id"]);
					return $pagesId;
				}
				else{
					die();
					}

			}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function log_activity($tableName,$tableId,$activityName){
		$sql = "INSERT INTO activities (table_name,table_id,activity_name,account_id,account_name,user_id) VALUES (:tableName,:tableId,:activityName,:accountId,:accountName,:userId)";
		$stmt = $this->dbConn->prepare($sql);
		$stmt->bindParam(":tableName",$tableName);
		$stmt->bindParam(":tableId",$tableId);
		$stmt->bindParam(":tableId",$tableId);
		$stmt->bindParam(":activityName",$activityName);
		$stmt->bindParam(":accountId",$_SESSION['account_id']);
		$stmt->bindParam(":accountName",$_SESSION['account_name']);
		$stmt->bindParam(":userId",$_SESSION['user_id']);
		$stmt->execute();
	}


}

?>