<?php 
	date_default_timezone_set('Africa/Accra');
	require_once("Activity.php");
	$objActivity = new Activity;
	class ContactCompanys{
		// setting and getting variables
		private $id;
		private $ConCompanyName;
		private $ConCompanyEmail;
		private $ConCompanyWebsite;
		private $ConCompanyMainPhone;
		private $ConCompanyAddress;
		private $ConCompanyCity;
		private $ConCompanyRegion;
		private $ConCompanyCountry;
		private $ConCompanyPrivateNote;
		private $added;
		private $dbConn;
		private $recordHide = "NO";
		private $table = "contacts_company";

		function set_id($id) { $this->id = $id; }
		function set_ConCompanyName($ConCompanyName) { $this->ConCompanyName = $ConCompanyName; }
		function set_ConCompanyEmail($ConCompanyEmail) { $this->ConCompanyEmail = $ConCompanyEmail; }
		function set_ConCompanyWebsite($ConCompanyWebsite) { $this->ConCompanyWebsite = $ConCompanyWebsite; }
		function set_ConCompanyMainPhone($ConCompanyMainPhone) { $this->ConCompanyMainPhone = $ConCompanyMainPhone; }
		function set_ConCompanyAddress($ConCompanyAddress) { $this->ConCompanyAddress = $ConCompanyAddress; }
		function set_ConCompanyCity($ConCompanyCity) { $this->ConCompanyCity = $ConCompanyCity; }
		function set_ConCompanyRegion($ConCompanyRegion) { $this->ConCompanyRegion = $ConCompanyRegion; }
		function set_ConCompanyCountry($ConCompanyCountry) { $this->ConCompanyCountry = $ConCompanyCountry; }
		function set_ConCompanyPrivateNote($ConCompanyPrivateNote) { $this->ConCompanyPrivateNote = $ConCompanyPrivateNote; }
		function set_added($added) { $this->added = $added; }
		function set_recordHide($recordHide) { $this->recordHide = $recordHide; }

		public function __construct(){
			require_once("db/db.php");
			$db = new DbConnect();
			$this->dbConn = $db->connect();
		}

		// clean data for data input
		public function CleanData($data){
			$data = trim($data);
			$data=htmlentities($data,ENT_QUOTES, 'UTF-8');
			$data = filter_var($data,FILTER_SANITIZE_SPECIAL_CHARS);
			return $data;
		}

		// insert pages
		function insert(){
			$date = date("jS F Y \/ h:i:s A");
			$sql = "INSERT INTO $this->table (con_company_name,con_company_email,con_company_website,con_company_mainphone,con_company_address,con_company_city,con_company_region,con_company_country,con_company_privatenote,user_id,added,record_hide) VALUES (:ConCompanyName,:ConCompanyEmail,:ConCompanyWebsite,:ConCompanyMainPhone,:ConCompanyAddress,:ConCompanyCity,:ConCompanyRegion,:ConCompanyCountry,:ConCompanyPrivateNote,:userId,:added,:recordHide)";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":ConCompanyName",$this->ConCompanyName);
			$stmt->bindParam(":ConCompanyEmail",$this->ConCompanyEmail);
			$stmt->bindParam(":ConCompanyWebsite",$this->ConCompanyWebsite);
			$stmt->bindParam(":ConCompanyMainPhone",$this->ConCompanyMainPhone);
			$stmt->bindParam(":ConCompanyAddress",$this->ConCompanyAddress);
			$stmt->bindParam(":ConCompanyCity",$this->ConCompanyCity);
			$stmt->bindParam(":ConCompanyRegion",$this->ConCompanyRegion);
			$stmt->bindParam(":ConCompanyCountry",$this->ConCompanyCountry);
			$stmt->bindParam(":ConCompanyPrivateNote",$this->ConCompanyPrivateNote);
			$stmt->bindParam(":userId",$_SESSION['user_id']);
			$stmt->bindParam(":added",$date);
			$stmt->bindParam(":recordHide",$this->recordHide);
			if ($stmt->execute()) {
				$this->log_activity($this->table,$this->dbConn->lastInsertId(),"Added New Contact Company");

				return true;
			}
			else{
				die();
				}
		}
		// for update
		function update(){
			$sql="UPDATE $this->table SET con_company_name=:ConCompanyName,con_company_email=:ConCompanyEmail,con_company_website=:ConCompanyWebsite,con_company_mainphone=:ConCompanyMainPhone,con_company_address=:ConCompanyAddress,con_company_city=:ConCompanyCity,con_company_region=:ConCompanyRegion,con_company_country=:ConCompanyCountry,con_company_privatenote=:ConCompanyPrivateNote WHERE con_company_id=:Id";
				$stmt = $this->dbConn->prepare($sql);
				$stmt->bindParam(":ConCompanyName",$this->ConCompanyName);
				$stmt->bindParam(":ConCompanyEmail",$this->ConCompanyEmail);
				$stmt->bindParam(":ConCompanyWebsite",$this->ConCompanyWebsite);
				$stmt->bindParam(":ConCompanyMainPhone",$this->ConCompanyMainPhone);
				$stmt->bindParam(":ConCompanyAddress",$this->ConCompanyAddress);
				$stmt->bindParam(":ConCompanyCity",$this->ConCompanyCity);
				$stmt->bindParam(":ConCompanyRegion",$this->ConCompanyRegion);
				$stmt->bindParam(":ConCompanyCountry",$this->ConCompanyCountry);
				$stmt->bindParam(":ConCompanyPrivateNote",$this->ConCompanyPrivateNote);
				$stmt->bindParam(":Id",$this->id);
				if ($stmt->execute()) {
					$this->log_activity($this->table,$this->id,"Updated Contact Company");
					return true;
				}
				else{
					return false;
					}

		}
		// for delete
		function delete(){
			$sql="UPDATE $this->table SET record_hide=:recordHide WHERE con_company_id=:Id";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			$stmt->bindParam(":Id",$this->id);
			if ($stmt->execute()) {
				$this->log_activity($this->table,$this->id,"Deleted Contact Company");
				return true;
			}
			else{
				return false;
			}
		}
		
		
		// get users
		function get_contactCompany(){
			$sql="SELECT * FROM $this->table WHERE record_hide=:recordHide ORDER BY con_company_id DESC";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $results;
			}
			else{
				die();
				}

		}

	// get user
		function get_conCompany_by_id(){
			$sql="SELECT * FROM $this->table WHERE con_company_id=:Id";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":Id",$this->id);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				return json_encode($results);
			}
			else{
				die();
				}
		}

	function get_company_Name($companyId){
		$sql="SELECT con_company_name FROM $this->table WHERE con_company_id=:companyId";
		$stmt = $this->dbConn->prepare($sql);
		$stmt->bindParam(":companyId",$companyId);
		if ($stmt->execute()) {
			$results = $stmt->fetch(PDO::FETCH_ASSOC);
			return trim($results["con_company_name"]);
		}
		else{
			return false;
			}
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function log_activity($tableName,$tableId,$activityName){
		$sql = "INSERT INTO activities (table_name,table_id,activity_name,account_id,account_name,user_id) VALUES (:tableName,:tableId,:activityName,:accountId,:accountName,:userId)";
		$stmt = $this->dbConn->prepare($sql);
		$stmt->bindParam(":tableName",$tableName);
		$stmt->bindParam(":tableId",$tableId);
		$stmt->bindParam(":tableId",$tableId);
		$stmt->bindParam(":activityName",$activityName);
		$stmt->bindParam(":accountId",$_SESSION['account_id']);
		$stmt->bindParam(":accountName",$_SESSION['account_name']);
		$stmt->bindParam(":userId",$_SESSION['user_id']);
		$stmt->execute();
	}

}

?>