<?php 
	date_default_timezone_set('Africa/Accra');
	class Staff{
		// setting and getting variables
		private $id;
		private $staffFirstName;
		private $staffMidName;
		private $staffLastName;
		private $stafftel;
		private $staffemergencytel;
		private $staffEmail;
		private $staffDob;
		private $staffPostal;
		private $staffHouseNum;
		private $staffHouseLoc;
		private $staffType;
		private $staffProfNum;
		private $staffEmployDate;
		private $staffNote;
		private $staffPic;
		private $added;
		private $dbConn;
		private $recordHide = "NO";
		private $table = "staff";


		function set_id($id) { $this->id = $id; }
		function set_added($added) { $this->added = $added; }
		function set_recordHide($recordHide) { $this->recordHide = $recordHide; }
		function set_staffFirstName($staffFirstName) { $this->staffFirstName = $staffFirstName; }
		function set_staffMidName($staffMidName) { $this->staffMidName = $staffMidName; }
		function set_staffLastName($staffLastName) { $this->staffLastName = $staffLastName; }
		function set_stafftel($stafftel) { $this->stafftel = $stafftel; }
		function set_staffemergencytel($staffemergencytel) { $this->staffemergencytel = $staffemergencytel; }
		function set_staffEmail($staffEmail) { $this->staffEmail = $staffEmail; }
		function set_staffDob($staffDob) { $this->staffDob = $staffDob; }
		function set_staffPostal($staffPostal) { $this->staffPostal = $staffPostal; }
		function set_staffHouseNum($staffHouseNum) { $this->staffHouseNum = $staffHouseNum; }
		function set_staffHouseLoc($staffHouseLoc) { $this->staffHouseLoc = $staffHouseLoc; }
		function set_staffType($staffType) { $this->staffType = $staffType; }
		function set_staffProfNum($staffProfNum) { $this->staffProfNum = $staffProfNum; }
		function set_staffEmployDate($staffEmployDate) { $this->staffEmployDate = $staffEmployDate; }
		function set_staffNote($staffNote) { $this->staffNote = $staffNote; }
		function set_staffPic($staffPic) { $this->staffPic = $staffPic; }


		public function __construct(){
			require_once("db/db.php");
			$db = new DbConnect();
			$this->dbConn = $db->connect();
		}

		// clean data for data input
		public function CleanData($data){
			$data = trim($data);
			$data=htmlentities($data,ENT_QUOTES, 'UTF-8');
			$data = filter_var($data,FILTER_SANITIZE_SPECIAL_CHARS);
			return $data;
		}

		// insert pages
		function insert(){
			$date = date("jS F Y \/ h:i:s A");
			// delete this
			$sql = "INSERT INTO $this->table (staff_firstname,staff_middlename,staff_lastname,staff_tel,staff_emergency,staff_email,staff_dob,staff_postal,staff_housenum,staff_houseloc,staff_type,staff_profnum,staff_employdate,staff_note,staff_pic,added,record_hide,user_id) VALUES (:staffFirstname,:staffMiddlename,:staffLastname,:staffTel,:staffEmergency,:staffEmail,:staffDob,:staffPostal,:staffHousenum,:staffHouseloc,:staff_type,:staffProfnum,:staffEmploydate,:staffNote,:staffPic,:added,:recordHide)";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":staffFirstname",$this->staffFirstName);
			$stmt->bindParam(":staffMiddlename",$this->staffMidName);
			$stmt->bindParam(":staffLastname",$this->staffLastName);
			$stmt->bindParam(":staffTel",$this->stafftel);
			$stmt->bindParam(":staffEmergency",$this->staffemergencytel);
			$stmt->bindParam(":staffEmail",$this->staffEmail);
			$stmt->bindParam(":staffDob",$this->staffDob);
			$stmt->bindParam(":staffPostal",$this->staffPostal);
			$stmt->bindParam(":staffHousenum",$this->staffHouseNum);
			$stmt->bindParam(":staffHouseloc",$this->staffHouseLoc);
			$stmt->bindParam(":staff_type",$this->staffType);
			$stmt->bindParam(":staffProfnum",$this->staffProfNum);
			$stmt->bindParam(":staffEmploydate",$this->staffEmployDate);
			$stmt->bindParam(":staffNote",$this->staffNote);
			$stmt->bindParam(":staffPic",$this->staffPic);
			$stmt->bindParam(":added",$date);
			$stmt->bindParam(":recordHide",$this->recordHide);
			$stmt->bindParam(":userId",$_SESSION['user_id']);
			if ($stmt->execute()) {
				$this->log_activity($this->table,$this->dbConn->lastInsertId(),"Added New Staff");
				return trim($this->dbConn->lastInsertId());
			}
			else{
				die();
				}
		}
		// for update
		function update(){
			$sql="UPDATE $this->table SET staff_firstname=:staffFirstname,staff_middlename=:staffMiddlename,staff_lastname=:staffLastname,staff_tel=:staffTel,staff_emergency=:staffEmergency,staff_email=:staffEmail,staff_dob=:staffDob,staff_postal=:staffPostal,staff_housenum=:staffHousenum,staff_houseloc=:staffHouseloc,staff_type=:staff_type,staff_profnum=:staffProfnum,staff_employdate=:staffEmploydate,staff_note=:staffNote,staff_pic=:staffPic WHERE staff_id=:Id";

				$stmt = $this->dbConn->prepare($sql);
				$stmt->bindParam(":staffFirstname",$this->staffFirstName);
				$stmt->bindParam(":staffMiddlename",$this->staffMidName);
				$stmt->bindParam(":staffLastname",$this->staffLastName);
				$stmt->bindParam(":staffTel",$this->stafftel);
				$stmt->bindParam(":staffEmergency",$this->staffemergencytel);
				$stmt->bindParam(":staffEmail",$this->staffEmail);
				$stmt->bindParam(":staffDob",$this->staffDob);
				$stmt->bindParam(":staffPostal",$this->staffPostal);
				$stmt->bindParam(":staffHousenum",$this->staffHouseNum);
				$stmt->bindParam(":staffHouseloc",$this->staffHouseLoc);
				$stmt->bindParam(":staff_type",$this->staffType);
				$stmt->bindParam(":staffProfnum",$this->staffProfNum);
				$stmt->bindParam(":staffEmploydate",$this->staffEmployDate);
				$stmt->bindParam(":staffNote",$this->staffNote);
				$stmt->bindParam(":staffPic",$this->staffPic);
				$stmt->bindParam(":Id",$this->id);
				if ($stmt->execute()) {
					$this->log_activity($this->table,$this->id,"Updated Staff");
					return true;
				}
				else{
					return false;
					}

		}

		// check updated data 
		function staffCheck_id(){
			$sql="SELECT staff_pic FROM $this->table WHERE staff_id=:Id";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":Id",$this->id);
			if ($stmt->execute()) {
				$results= $stmt->fetch(PDO::FETCH_ASSOC);
				return $results["staff_pic"];
			}
			else{
				die();
				}
		}

		// for delete
		function delete(){
			$sql="UPDATE $this->table SET record_hide=:recordHide WHERE staff_id=:Id";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			$stmt->bindParam(":Id",$this->id);
			if ($stmt->execute()) {
				$this->log_activity($this->table,$this->id,"Deleted Staff");
				return true;
			}
			else{
				return false;
			}
		}


	// get users
		function get_staff(){
			$sql="SELECT * FROM $this->table WHERE record_hide=:recordHide ORDER BY staff_id DESC";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $results;
			}
			else{
				die();
				}

		}
	// count staff
		function count_staff(){
			$sql="SELECT * FROM $this->table WHERE record_hide=:recordHide";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			if ($stmt->execute()) {
				return $stmt->rowCount();
			}
			else{
				die();
				}

		}

	// get user
		function get_staff_by_id(){
			$sql="SELECT * FROM $this->table WHERE staff_id=:Id LIMIT 1";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":Id",$this->id);
			if ($stmt->execute()) {
				$results= $stmt->fetch(PDO::FETCH_ASSOC);
				return $results;
			}
			else{
				die();
				}
		}

	// get staff fullname
		function get_staffName($staffId){
				$sql="SELECT staff_firstname,staff_lastname,staff_pic FROM $this->table WHERE staff_id=:Id";
				$stmt = $this->dbConn->prepare($sql);
				$stmt->bindParam(":Id",$staffId);
				if ($stmt->execute()) {
					$results = $stmt->fetch(PDO::FETCH_ASSOC);
					return trim($results["staff_firstname"]." ".$results["staff_lastname"]."|".$results["staff_pic"]);
				}
				else{
					return false;
					}
		}
	// staff name only
		function get_staffName_only($staffId){
				$sql="SELECT staff_firstname,staff_lastname FROM $this->table WHERE staff_id=:Id";
				$stmt = $this->dbConn->prepare($sql);
				$stmt->bindParam(":Id",$staffId);
				if ($stmt->execute()) {
					$results = $stmt->fetch(PDO::FETCH_ASSOC);
					return trim($results["staff_firstname"]." ".$results["staff_lastname"]);
				}
				else{
					return false;
					}
		}

	// get staff by attorneys
		function get_staff_by_type($staffType){
			$returnRecords = '';
			$sql="SELECT staff_id,staff_firstname,staff_middlename,staff_lastname FROM $this->table WHERE record_hide=:recordHide AND staff_type=:staffType ORDER BY staff_id DESC";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			$stmt->bindParam(":staffType",$staffType);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				foreach ($results as $result) {
                    echo '<option value='.$result["staff_id"].'>'.$result["staff_firstname"]." ".$result["staff_lastname"].'</option>';
                }

                return $returnRecords;
			}
			else{
				die();
				}

		}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		function get_profile_details(){
				$sql="SELECT * FROM $this->table WHERE staff_id=:Id";
				$stmt = $this->dbConn->prepare($sql);
				$stmt->bindParam(":Id",$_SESSION['account_id']);
				if ($stmt->execute()) {
					$results = $stmt->fetch(PDO::FETCH_ASSOC);
					return $results;
				}
				else{
					exit();
					}
		}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function log_activity($tableName,$tableId,$activityName){
		$sql = "INSERT INTO activities (table_name,table_id,activity_name,account_id,account_name,user_id) VALUES (:tableName,:tableId,:activityName,:accountId,:accountName,:userId)";
		$stmt = $this->dbConn->prepare($sql);
		$stmt->bindParam(":tableName",$tableName);
		$stmt->bindParam(":tableId",$tableId);
		$stmt->bindParam(":tableId",$tableId);
		$stmt->bindParam(":activityName",$activityName);
		$stmt->bindParam(":accountId",$_SESSION['account_id']);
		$stmt->bindParam(":accountName",$_SESSION['account_name']);
		$stmt->bindParam(":userId",$_SESSION['user_id']);
		$stmt->execute();
	}


}

?>