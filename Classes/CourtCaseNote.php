<?php 
	date_default_timezone_set('Africa/Accra');
	class CourtCaseNote{
		// setting and getting variables
		private $id;
		private $viewNoteContent;
		private $courtCasId;
		private $added;
		private $dbConn;
		private $recordHide = "NO";
		private $table = "court_case_notes";

		function set_id($id) { $this->id = $id; }
		function set_viewNoteContent($viewNoteContent) { $this->viewNoteContent = $viewNoteContent; }
		function set_courtCasId($courtCasId) { $this->courtCasId = $courtCasId; }
		function set_recordHide($recordHide) { $this->recordHide = $recordHide; }

		public function __construct(){
			require_once("db/db.php");
			$db = new DbConnect();
			$this->dbConn = $db->connect();
		}

		// clean data for data input
		public function CleanData($data){
			$data = trim($data);
			$data=htmlentities($data,ENT_QUOTES, 'UTF-8');
			$data = filter_var($data,FILTER_SANITIZE_SPECIAL_CHARS);
			return $data;
		}

		// insert pages
		function insert(){
			$date = date("jS F Y \/ h:i:s A");
			$sql = "INSERT INTO $this->table (court_case_id,court_case_note_content,account_id,added,record_hide) VALUES (:courtCasId,:noteContent,:accountId,:added,:recordHide)";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":courtCasId",$this->courtCasId);
			$stmt->bindParam(":noteContent",$this->viewNoteContent);
			$stmt->bindParam(":accountId",$_SESSION['account_id']);
			$stmt->bindParam(":added",$date);
			$stmt->bindParam(":recordHide",$this->recordHide);
			if ($stmt->execute()) {
				$this->log_activity($this->table,$this->dbConn->lastInsertId(),"Added New Case Note");
				return true;
			}
			else{
				die();
				}
		}
		// for update
		function update(){
			$sql="UPDATE $this->table SET court_case_note_content=:noteContent WHERE account_id=:accountId AND court_case_note_id=:Id";
				$stmt = $this->dbConn->prepare($sql);
				$stmt->bindParam(":noteContent",$this->viewNoteContent);
				$stmt->bindParam(":accountId",$_SESSION['account_id']);
				$stmt->bindParam(":Id",$this->id);
				
				if ($stmt->execute()) {
					$this->log_activity($this->table,$this->id,"Updated Case Note");
					return true;
				}
				else{
					return false;
					}

		}
		// for delete
		function delete(){
			$sql="UPDATE $this->table SET record_hide=:recordHide WHERE court_case_note_id=:Id";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			$stmt->bindParam(":Id",$this->id);
			if ($stmt->execute()) {
				$this->log_activity($this->table,$this->id,"Deleted Case Note");
				return true;
			}
			else{
				return false;
			}
		}


	// get users
		function get_view_case_notes(){
			$returnRecords='';
			$sql="SELECT N.court_case_note_id,N.court_case_note_content,N.added,N.account_id,
			S.staff_firstname,S.staff_lastname
			FROM $this->table AS N 
			LEFT JOIN staff AS S ON N.account_id = S.staff_id
			WHERE N.record_hide=:recordHide 
			AND N.court_case_id=:courtCaseId ORDER BY court_case_note_id DESC";

			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			$stmt->bindParam(":courtCaseId",$this->courtCasId);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				foreach ($results as $result) {
					$returnRecords .= '<div style="background-color:#f4f4f4;" class="well">
											<h4><b>'.$result["court_case_note_content"].'</b></h4>
											<p><b>posted on :</b>'.$result["added"].'<b> BY </b>'.$result["staff_firstname"].' '.$result["staff_lastname"].'</p>';

					if ($_SESSION['account_id'] == $result["account_id"]) {

						$returnRecords .= '<button class="btn-info updateNote" id="'.$result["court_case_note_id"].'"><i class="fa fa-pencil"></i></button>
											<button class="btn-danger deleteNote" id="'.$result["court_case_note_id"].'"><i class="fa fa-trash"></i></button>';
					}

					$returnRecords .= '</div>';

				}
				return $returnRecords;
			}
			else{
				return false;
				}

		}


		function get_court_case_by_id(){
			$sql="SELECT * FROM $this->table WHERE court_case_note_id=:Id LIMIT 1";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":Id",$this->id);
			if ($stmt->execute()) {
				$results = $stmt->fetch(PDO::FETCH_ASSOC);
				return json_encode($results);
			}
			else{
				return false;
				}

		}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function log_activity($tableName,$tableId,$activityName){
		$sql = "INSERT INTO activities (table_name,table_id,activity_name,account_id,account_name,user_id) VALUES (:tableName,:tableId,:activityName,:accountId,:accountName,:userId)";
		$stmt = $this->dbConn->prepare($sql);
		$stmt->bindParam(":tableName",$tableName);
		$stmt->bindParam(":tableId",$tableId);
		$stmt->bindParam(":tableId",$tableId);
		$stmt->bindParam(":activityName",$activityName);
		$stmt->bindParam(":accountId",$_SESSION['account_id']);
		$stmt->bindParam(":accountName",$_SESSION['account_name']);
		$stmt->bindParam(":userId",$_SESSION['user_id']);
		$stmt->execute();
	}


}

 ?>