<?php 
	date_default_timezone_set('Africa/Accra');
	class BillExpenses{
		// setting and getting variables
		private $id;
		private $expenseCourtCaseId;
		private $expensesStaff;
		private $expensesActivity;
		private $expensesBillable;
		private $expensesDescribe;
		private $expensesDate;
		private $expensesCost;
		private $expensesQuantity;
		private $expensesTotal;
		private $expensesInvoiced;
		private $added;
		private $dbConn;
		private $recordHide = "NO";
		private $table = "bill_expenses";


		function set_id($id) { $this->id = $id; }
		function set_added($added) { $this->added = $added; }
		function set_recordHide($recordHide) { $this->recordHide = $recordHide; }
		function set_expenseCourtCaseId($expenseCourtCaseId) { $this->expenseCourtCaseId = $expenseCourtCaseId; }
		function set_expensesStaff($expensesStaff) { $this->expensesStaff = $expensesStaff; }
		function set_expensesActivity($expensesActivity) { $this->expensesActivity = $expensesActivity; }
		function set_expensesBillable($expensesBillable) { $this->expensesBillable = $expensesBillable; }
		function set_expensesDescribe($expensesDescribe) { $this->expensesDescribe = $expensesDescribe; }
		function set_expensesDate($expensesDate) { $this->expensesDate = $expensesDate; }
		function set_expensesCost($expensesCost) { $this->expensesCost = $expensesCost; }
		function set_expensesQuantity($expensesQuantity) { $this->expensesQuantity = $expensesQuantity; }
		function set_expensesTotal($expensesTotal) { $this->expensesTotal = $expensesTotal; }
		function set_expensesInvoiced($expensesInvoiced) { $this->expensesInvoiced = $expensesInvoiced; }

		public function __construct(){
			require_once("db/db.php");
			$db = new DbConnect();
			$this->dbConn = $db->connect();
		}

		// clean data for data input
		public function CleanData($data){
			$data = trim($data);
			$data=htmlentities($data,ENT_QUOTES, 'UTF-8');
			$data = filter_var($data,FILTER_SANITIZE_SPECIAL_CHARS);
			return $data;
		}

		// insert pages
		function insert(){
			$date = date("j-m-Y");
			// delete this
			$sql = "INSERT INTO $this->table (bill_expenses_case_id,bill_expenses_staff_id,bill_expenses_activity,bill_expenses_billable,bill_expenses_describe,bill_expenses_date,bill_expenses_cost,bill_expenses_quantity,bill_expenses_total,bill_expenses_invoiced,added,user_id,record_hide) 
			VALUES (:expenseCourtCaseId,:expensesStaff,:expensesActivity,:expensesBillable,:expensesDescribe,:expensesDate,:expensesCost,:expensesQuantity,:expensesTotal,:expensesInvoiced,:added,:userId,:recordHide)";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":expenseCourtCaseId",$this->expenseCourtCaseId);
			$stmt->bindParam(":expensesStaff",$this->expensesStaff);
			$stmt->bindParam(":expensesActivity",$this->expensesActivity);
			$stmt->bindParam(":expensesBillable",$this->expensesBillable);
			$stmt->bindParam(":expensesDescribe",$this->expensesDescribe);
			$stmt->bindParam(":expensesDate",$this->expensesDate);
			$stmt->bindParam(":expensesCost",$this->expensesCost);
			$stmt->bindParam(":expensesQuantity",$this->expensesQuantity);
			$stmt->bindParam(":expensesTotal",$this->expensesTotal);
			$stmt->bindParam(":expensesInvoiced",$this->expensesInvoiced);
			$stmt->bindParam(":added",$date);
			$stmt->bindParam(":userId",$_SESSION['user_id']);
			$stmt->bindParam(":recordHide",$this->recordHide);
			if ($stmt->execute()) {
				$this->log_activity($this->table,$this->dbConn->lastInsertId(),"Added New Expenses");
				return true;
			}
			else{
				die();
			}
		}
		// for update
		function update(){
			$sql="UPDATE $this->table SET bill_expenses_case_id=:expenseCourtCaseId,bill_expenses_activity=:expensesActivity,bill_expenses_billable=:expensesBillable,bill_expenses_describe=:expensesDescribe,bill_expenses_date=:expensesDate,bill_expenses_cost=:expensesCost,bill_expenses_quantity=:expensesQuantity,bill_expenses_total=:expensesTotal WHERE bill_expenses_id=:Id";
				$stmt = $this->dbConn->prepare($sql);
				$stmt->bindParam(":expenseCourtCaseId",$this->expenseCourtCaseId);
				$stmt->bindParam(":expensesActivity",$this->expensesActivity);
				$stmt->bindParam(":expensesBillable",$this->expensesBillable);
				$stmt->bindParam(":expensesDescribe",$this->expensesDescribe);
				$stmt->bindParam(":expensesDate",$this->expensesDate);
				$stmt->bindParam(":expensesCost",$this->expensesCost);
				$stmt->bindParam(":expensesQuantity",$this->expensesQuantity);
				$stmt->bindParam(":expensesTotal",$this->expensesTotal);
				$stmt->bindParam(":Id",$this->id);
				if ($stmt->execute()) {
					$this->log_activity($this->table,$this->id,"Updated Expenses");
					return true;
				}
				else{
					return false;
					}

		}

		// for delete
		function delete(){
			$sql="UPDATE $this->table SET record_hide=:recordHide WHERE bill_expenses_id=:Id";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			$stmt->bindParam(":Id",$this->id);
			if ($stmt->execute()) {
				$this->log_activity($this->table,$this->id,"Deleted Expenses");
				return true;
			}
			else{
				return false;
			}
		}

		function get_expenses(){
			$returnResults =[];
			$sql="SELECT * FROM $this->table WHERE record_hide=:recordHide ORDER BY bill_expenses_id DESC";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				foreach ($results as $result) {
					$result['caseName'] = $this->get_court_case_name($result['bill_expenses_case_id']);
					$result['bill_expenses_staff_id'] = $this->get_staff_name($result['bill_expenses_staff_id']);
					$returnResults[]=$result;
				}
				return $returnResults;
			}
			else{
				die();
			}
		}
		function get_expenses_by_id(){
			$sql="SELECT * FROM $this->table WHERE bill_expenses_id=:Id";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":Id",$this->id);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				return json_encode($results);
			}
			else{
				die();
			}
		}

		// grab all time entries by case
		function get_billable_expenses_by_case(){
			$returnResults =[];
			$sql="SELECT * FROM $this->table WHERE bill_expenses_billable=:expensesBillable AND bill_expenses_invoiced=:expensesInvoiced AND record_hide=:recordHide AND bill_expenses_case_id=:caseId";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":expensesBillable",$this->expensesBillable);
			$stmt->bindParam(":expensesInvoiced",$this->expensesInvoiced);
			$stmt->bindParam(":recordHide",$this->recordHide);
			$stmt->bindParam(":caseId",$this->expenseCourtCaseId);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				foreach ($results as $result) {
					$result['bill_expenses_staff_id'] = $this->get_staff_name($result['bill_expenses_staff_id']);
					$returnResults[]=$result;
				}
				return json_encode($returnResults);
			}
			else{
				die();
			}
		}

	// get court case name
		function get_court_case_name($caseId){
				$sql="SELECT case_name,case_number FROM court_case WHERE case_id=:Id";
				$stmt = $this->dbConn->prepare($sql);
				$stmt->bindParam(":Id",$caseId);
				if ($stmt->execute()) {
					$results = $stmt->fetch(PDO::FETCH_ASSOC);
					return trim($results["case_name"]." (".$results["case_number"].")");
				}
				else{
					return false;
					}
		}

// get username
		function get_staff_name($staffId){
				$sql="SELECT staff_firstname,staff_lastname FROM staff WHERE staff_id=:Id";
				$stmt = $this->dbConn->prepare($sql);
				$stmt->bindParam(":Id",$staffId);
				if ($stmt->execute()) {
					$results = $stmt->fetch(PDO::FETCH_ASSOC);
					return trim($results["staff_firstname"]." ".$results["staff_lastname"]);
				}
				else{
					return false;
					}
		} 		

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function log_activity($tableName,$tableId,$activityName){
		$sql = "INSERT INTO activities (table_name,table_id,activity_name,account_id,account_name,user_id) VALUES (:tableName,:tableId,:activityName,:accountId,:accountName,:userId)";
		$stmt = $this->dbConn->prepare($sql);
		$stmt->bindParam(":tableName",$tableName);
		$stmt->bindParam(":tableId",$tableId);
		$stmt->bindParam(":tableId",$tableId);
		$stmt->bindParam(":activityName",$activityName);
		$stmt->bindParam(":accountId",$_SESSION['account_id']);
		$stmt->bindParam(":accountName",$_SESSION['account_name']);
		$stmt->bindParam(":userId",$_SESSION['user_id']);
		$stmt->execute();
	}
}
?>