<?php 
	date_default_timezone_set('Africa/Accra');
	class BillTimeEntry{
		// setting and getting variables
		private $id;
		private $timeEntriesCourtCaseId;
		private $timeEntryStaff;
		private $timeEntryActivity;
		private $timeEntryBillable;
		private $timeEntryDescribe;
		private $timeEntryDate;
		private $timeEntryRate;
		private $timeEntryRateType;
		private $timeEntryDuration;
		private $timeEntryTotal;
		private $timeEntryInvoiced;
		private $added;
		private $dbConn;
		private $recordHide = "NO";
		private $table = "bill_time_entry";


		function set_id($id) { $this->id = $id; }
		function set_added($added) { $this->added = $added; }
		function set_recordHide($recordHide) { $this->recordHide = $recordHide; }
		function set_timeEntriesCourtCaseId($timeEntriesCourtCaseId) { $this->timeEntriesCourtCaseId = $timeEntriesCourtCaseId; }
		function set_timeEntryStaff($timeEntryStaff) { $this->timeEntryStaff = $timeEntryStaff; }
		function set_timeEntryActivity($timeEntryActivity) { $this->timeEntryActivity = $timeEntryActivity; }
		function set_timeEntryBillable($timeEntryBillable) { $this->timeEntryBillable = $timeEntryBillable; }
		function set_timeEntryDescribe($timeEntryDescribe) { $this->timeEntryDescribe = $timeEntryDescribe; }
		function set_timeEntryDate($timeEntryDate) { $this->timeEntryDate = $timeEntryDate; }
		function set_timeEntryRate($timeEntryRate) { $this->timeEntryRate = $timeEntryRate; }
		function set_timeEntryRateType($timeEntryRateType) { $this->timeEntryRateType = $timeEntryRateType; }
		function set_timeEntryDuration($timeEntryDuration) { $this->timeEntryDuration = $timeEntryDuration; }
		function set_timeEntryTotal($timeEntryTotal) { $this->timeEntryTotal = $timeEntryTotal; }
		function set_timeEntryInvoiced($timeEntryInvoiced) { $this->timeEntryInvoiced = $timeEntryInvoiced; }

		public function __construct(){
			require_once("db/db.php");
			$db = new DbConnect();
			$this->dbConn = $db->connect();
		}

		// clean data for data input
		public function CleanData($data){
			$data = trim($data);
			$data=htmlentities($data,ENT_QUOTES, 'UTF-8');
			$data = filter_var($data,FILTER_SANITIZE_SPECIAL_CHARS);
			return $data;
		}

		// insert pages
		function insert(){
			$date = date("j-m-Y");
			// delete this
			$sql = "INSERT INTO $this->table (time_entry_case_id,time_entry_staff_id,time_entry_activity,time_entry_billable,time_entry_describe,time_entry_date,time_entry_rate,time_entry_rate_type,time_entry_duration,time_entry_total,time_entry_invoiced,added,user_id,record_hide) 
			VALUES (:timeEntriesCourtCaseId,:timeEntryStaff,:timeEntryActivity,:timeEntryBillable,:timeEntryDescribe,:timeEntryDate,:timeEntryRate,:timeEntryRateType,:timeEntryDuration,:timeEntryTotal,:timeEntryInvoiced,:added,:userId,:recordHide)";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":timeEntriesCourtCaseId",$this->timeEntriesCourtCaseId);
			$stmt->bindParam(":timeEntryStaff",$this->timeEntryStaff);
			$stmt->bindParam(":timeEntryActivity",$this->timeEntryActivity);
			$stmt->bindParam(":timeEntryBillable",$this->timeEntryBillable);
			$stmt->bindParam(":timeEntryDescribe",$this->timeEntryDescribe);
			$stmt->bindParam(":timeEntryDate",$this->timeEntryDate);
			$stmt->bindParam(":timeEntryRate",$this->timeEntryRate);
			$stmt->bindParam(":timeEntryRateType",$this->timeEntryRateType);
			$stmt->bindParam(":timeEntryDuration",$this->timeEntryDuration);
			$stmt->bindParam(":timeEntryTotal",$this->timeEntryTotal);
			$stmt->bindParam(":timeEntryInvoiced",$this->timeEntryInvoiced);
			$stmt->bindParam(":added",$date);
			$stmt->bindParam(":userId",$_SESSION['user_id']);
			$stmt->bindParam(":recordHide",$this->recordHide);
			if ($stmt->execute()) {
				$this->log_activity($this->table,$this->dbConn->lastInsertId(),"Added New Time Entry");
				return true;
			}
			else{
				die();
			}
		}
		// for update
		function update(){
			$sql="UPDATE $this->table SET time_entry_case_id=:timeEntriesCourtCaseId,time_entry_activity=:timeEntryActivity,time_entry_billable=:timeEntryBillable,time_entry_describe=:timeEntryDescribe,time_entry_date=:timeEntryDate,time_entry_rate=:timeEntryRate,time_entry_rate_type=:timeEntryRateType,time_entry_duration=:timeEntryDuration,time_entry_total=:timeEntryTotal,time_entry_invoiced=:timeEntryInvoiced WHERE time_entry_id=:Id";
				$stmt = $this->dbConn->prepare($sql);
				$stmt->bindParam(":timeEntriesCourtCaseId",$this->timeEntriesCourtCaseId);
				$stmt->bindParam(":timeEntryActivity",$this->timeEntryActivity);
				$stmt->bindParam(":timeEntryBillable",$this->timeEntryBillable);
				$stmt->bindParam(":timeEntryDescribe",$this->timeEntryDescribe);
				$stmt->bindParam(":timeEntryDate",$this->timeEntryDate);
				$stmt->bindParam(":timeEntryRate",$this->timeEntryRate);
				$stmt->bindParam(":timeEntryRateType",$this->timeEntryRateType);
				$stmt->bindParam(":timeEntryDuration",$this->timeEntryDuration);
				$stmt->bindParam(":timeEntryTotal",$this->timeEntryTotal);
				$stmt->bindParam(":timeEntryInvoiced",$this->timeEntryInvoiced);
				$stmt->bindParam(":Id",$this->id);
				if ($stmt->execute()) {
					$this->log_activity($this->table,$this->id,"Updated Time Entry");
					return true;
				}
				else{
					return false;
					}

		}

		// for delete
		function delete(){
			$sql="UPDATE $this->table SET record_hide=:recordHide WHERE time_entry_id=:Id";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			$stmt->bindParam(":Id",$this->id);
			if ($stmt->execute()) {
				$this->log_activity($this->table,$this->id,"Deleted Time Entry");
				return true;
			}
			else{
				return false;
			}
		}

		function get_time_entries(){
			$returnResults =[];
			$sql="SELECT * FROM $this->table WHERE record_hide=:recordHide ORDER BY time_entry_id DESC";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				foreach ($results as $result) {
					$result['caseName'] = $this->get_court_case_name($result['time_entry_case_id']);
					$result['time_entry_staff_id'] = $this->get_staff_name($result['time_entry_staff_id']);
					$returnResults[]=$result;
				}
				return $returnResults;
			}
			else{
				die();
			}
		}

		function get_time_entry_by_id(){
			$sql="SELECT * FROM $this->table WHERE time_entry_id=:Id";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":Id",$this->id);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				return json_encode($results);
			}
			else{
				die();
			}
		}

		// grab all time entries by case
		function get_billable_time_entry_by_case(){
			$returnResults =[];
			$sql="SELECT * FROM $this->table WHERE time_entry_billable=:timeEntryBillable AND time_entry_invoiced=:timeEntryInvoiced AND record_hide=:recordHide AND time_entry_case_id=:caseId";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":timeEntryBillable",$this->timeEntryBillable);
			$stmt->bindParam(":timeEntryInvoiced",$this->timeEntryInvoiced);
			$stmt->bindParam(":recordHide",$this->recordHide);
			$stmt->bindParam(":caseId",$this->timeEntriesCourtCaseId);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				foreach ($results as $result) {
					$result['time_entry_staff_id'] = $this->get_staff_name($result['time_entry_staff_id']);
					$returnResults[]=$result;
				}
				return json_encode($returnResults);
			}
			else{
				die();
			}
		}
// get court case name
		function get_court_case_name($caseId){
				$sql="SELECT case_name,case_number FROM court_case WHERE case_id=:Id";
				$stmt = $this->dbConn->prepare($sql);
				$stmt->bindParam(":Id",$caseId);
				if ($stmt->execute()) {
					$results = $stmt->fetch(PDO::FETCH_ASSOC);
					return trim($results["case_name"]." (".$results["case_number"].")");
				}
				else{
					return false;
					}
		}

// get username
		function get_staff_name($staffId){
			$sql="SELECT staff_firstname,staff_lastname FROM staff WHERE staff_id=:Id";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":Id",$staffId);
			if ($stmt->execute()) {
				$results = $stmt->fetch(PDO::FETCH_ASSOC);
				return trim($results["staff_firstname"]." ".$results["staff_lastname"]);
			}
			else{
				return false;
				}
		} 
		
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function log_activity($tableName,$tableId,$activityName){
		$sql = "INSERT INTO activities (table_name,table_id,activity_name,account_id,account_name,user_id) VALUES (:tableName,:tableId,:activityName,:accountId,:accountName,:userId)";
		$stmt = $this->dbConn->prepare($sql);
		$stmt->bindParam(":tableName",$tableName);
		$stmt->bindParam(":tableId",$tableId);
		$stmt->bindParam(":tableId",$tableId);
		$stmt->bindParam(":activityName",$activityName);
		$stmt->bindParam(":accountId",$_SESSION['account_id']);
		$stmt->bindParam(":accountName",$_SESSION['account_name']);
		$stmt->bindParam(":userId",$_SESSION['user_id']);
		$stmt->execute();
	}
// ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// calculate per day and year total
	function get_time_period_total_month(){
		$total = 0.0;

		$currentdate = date("d-m-Y");
		$d2 = new DateTime($currentdate);

		$this->timeEntryBillabl="on";
		$this->timeEntryInvoiced="NO";
		$sql="SELECT time_entry_total,added 
		FROM $this->table 
		WHERE time_entry_staff_id=:timeEntryStaff 
		AND time_entry_billable=:timeEntryBillable
		AND time_entry_invoiced=:timeEntryInvoiced
		AND record_hide=:recordHide";

		$stmt = $this->dbConn->prepare($sql);
		$stmt->bindParam(":timeEntryStaff",$_SESSION['staff_id']);
		$stmt->bindParam(":timeEntryBillable",$this->timeEntryBillable);
		$stmt->bindParam(":timeEntryInvoiced",$this->timeEntryInvoiced);
		$stmt->bindParam(":recordHide",$this->recordHide);
		if ($stmt->execute()) {
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			foreach ($results as $result) {
				$d1 = new DateTime($result['added']);
				$interval = $d2->diff($d1);
				$interval->format('%m months');

				if (($interval->y == 0) && ($interval->m > 1) && ($interval->m < 2)) {
					$total += $result["time_entry_total"];
				}
			}
			
			echo $total;
		}
		else{
			die();
		}
	}



}
?>