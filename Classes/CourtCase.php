<?php 
	date_default_timezone_set('Africa/Accra');
	class CourtCase{
		// setting and getting variables
		private $id;
		private $caseContactIds;
		private $caseName;
		private $caseSuitNumber;
		private $caseNumber;
		private $casePracticeArea;
		private $caseStage;
		private $caseDateOpened;
		private $caseDescription;
		private $caseStatueLimitation;
		private $caseBillingContact;
		private $caseBillingMethod;
		private $caseLeadAttorney;
		private $caseStaffIds;
		private $caseStatus = "OPEN";
		private $added;
		private $dbConn;
		private $recordHide = "NO";
		private $table = "court_case";
		private $currentCourtMain;
		private $currentCourtSub;
		private $currentCourtNum;
		private $nextCourtMain;
		private $nextCourtSub;
		private $nextCourtNum;
		private $nextCourtDate;

		function set_id($id) { $this->id = $id; }
		function set_added($added) { $this->added = $added; }
		function set_recordHide($recordHide) { $this->recordHide = $recordHide; }
		function set_caseContactIds($caseContactIds) { $this->caseContactIds = $caseContactIds; }
		function set_caseName($caseName) { $this->caseName = $caseName; }
		function set_caseSuitNumber($caseSuitNumber) { $this->caseSuitNumber = $caseSuitNumber; }
		function set_caseNumber($caseNumber) { $this->caseNumber = $caseNumber; }
		function set_casePracticeArea($casePracticeArea) { $this->casePracticeArea = $casePracticeArea; }
		function set_caseStage($caseStage) { $this->caseStage = $caseStage; }
		function set_caseDateOpened($caseDateOpened) { $this->caseDateOpened = $caseDateOpened; }
		function set_caseDescription($caseDescription) { $this->caseDescription = $caseDescription; }
		function set_caseStatueLimitation($caseStatueLimitation) { $this->caseStatueLimitation = $caseStatueLimitation; }
		function set_caseBillingContact($caseBillingContact) { $this->caseBillingContact = $caseBillingContact; }
		function set_caseBillingMethod($caseBillingMethod) { $this->caseBillingMethod = $caseBillingMethod; }
		function set_caseLeadAttorney($caseLeadAttorney) { $this->caseLeadAttorney = $caseLeadAttorney; }
		function set_caseStaffIds($caseStaffIds) { $this->caseStaffIds = $caseStaffIds; }
		function set_caseStatus($caseStatus) { $this->caseStatus = $caseStatus; }
		function set_currentCourtMain($currentCourtMain) { $this->currentCourtMain = $currentCourtMain; }
		function set_currentCourtSub($currentCourtSub) { $this->currentCourtSub = $currentCourtSub; }
		function set_currentCourtNum($currentCourtNum) { $this->currentCourtNum = $currentCourtNum; }
		function set_nextCourtMain($nextCourtMain) { $this->nextCourtMain = $nextCourtMain; }
		function set_nextCourtSub($nextCourtSub) { $this->nextCourtSub = $nextCourtSub; }
		function set_nextCourtNum($nextCourtNum) { $this->nextCourtNum = $nextCourtNum; }
		function set_nextCourtDate($nextCourtDate) { $this->nextCourtDate = $nextCourtDate; }


		public function __construct(){
			require_once("db/db.php");
			$db = new DbConnect();
			$this->dbConn = $db->connect();
		}

		// clean data for data input
		public function CleanData($data){
			$data = trim($data);
			$data=htmlentities($data,ENT_QUOTES, 'UTF-8');
			$data = filter_var($data,FILTER_SANITIZE_SPECIAL_CHARS);
			return $data;
		}

		// insert pages
		function insert(){
			$date = date("d-m-Y");
			$sql = "INSERT INTO $this->table (case_contactIds,case_name,case_suit_number,case_number,case_practice_area,case_stage,case_date_opened,case_description,case_billing_contact,case_billing_method,case_lead_attorney,case_staffIds,case_status,user_id,added,record_hide,current_court_main_id,current_court_sub_id,current_court_num,next_court_main_id,next_court_sub_id,next_court_num,next_court_date) 
			VALUES (:caseContactIds,:caseName,:caseSuitNumber,:caseNumber,:casePracticeArea,:caseStage,:caseDateOpened,:caseDescription,:caseBillingContact,:caseBillingMethod,:caseLeadAttorney,:caseStaffIds,:caseStatus,:userId,:added,:recordHide,:currentCourtMain,:currentCourtSub,:currentCourtNum,:nextCourtMain,:nextCourtSub,:nextCourtNum,:nextCourtDate)";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":caseContactIds",$this->caseContactIds);
			$stmt->bindParam(":caseName",$this->caseName);
			$stmt->bindParam(":caseSuitNumber",$this->caseSuitNumber);
			$stmt->bindParam(":caseNumber",$this->caseNumber);
			$stmt->bindParam(":casePracticeArea",$this->casePracticeArea);
			$stmt->bindParam(":caseStage",$this->caseStage);
			$stmt->bindParam(":caseDateOpened",$this->caseDateOpened);
			$stmt->bindParam(":caseDescription",$this->caseDescription);
			$stmt->bindParam(":caseBillingContact",$this->caseBillingContact);
			$stmt->bindParam(":caseBillingMethod",$this->caseBillingMethod);
			$stmt->bindParam(":caseLeadAttorney",$this->caseLeadAttorney);
			$stmt->bindParam(":caseStaffIds",$this->caseStaffIds);
			$stmt->bindParam(":caseStatus",$this->caseStatus);
			$stmt->bindParam(":userId",$_SESSION['user_id']);
			$stmt->bindParam(":added",$date);
			$stmt->bindParam(":recordHide",$this->recordHide);
			$stmt->bindParam(":currentCourtMain",$this->currentCourtMain);
			$stmt->bindParam(":currentCourtSub",$this->currentCourtSub);
			$stmt->bindParam(":currentCourtNum",$this->currentCourtNum);
			$stmt->bindParam(":nextCourtMain",$this->nextCourtMain);
			$stmt->bindParam(":nextCourtSub",$this->nextCourtSub);
			$stmt->bindParam(":nextCourtNum",$this->nextCourtNum);
			$stmt->bindParam(":nextCourtDate",$this->nextCourtDate);
			if ($stmt->execute()) {
				$this->log_activity($this->table,$this->dbConn->lastInsertId(),"Added New Court Case");
				return trim($this->dbConn->lastInsertId());
			}
			else{
				die();
				}
		}
		// for update
		function update(){
			$sql="UPDATE $this->table SET case_contactIds=:caseContactIds,case_name=:caseName,case_suit_number=:caseSuitNumber,case_number=:caseNumber,case_practice_area=:casePracticeArea,case_stage=:caseStage,case_date_opened=:caseDateOpened,case_description=:caseDescription,case_billing_contact=:caseBillingContact,case_billing_method=:caseBillingMethod,case_lead_attorney=:caseLeadAttorney,case_staffIds=:caseStaffIds,case_status=:caseStatus,current_court_main_id=:currentCourtMain,current_court_sub_id=:currentCourtSub,current_court_num=:currentCourtNum,next_court_main_id=:nextCourtMain,next_court_sub_id=:nextCourtSub,next_court_num=:nextCourtNum,next_court_date=:nextCourtDate WHERE case_id=:Id";
				$stmt = $this->dbConn->prepare($sql);
				$stmt->bindParam(":caseContactIds",$this->caseContactIds);
				$stmt->bindParam(":caseName",$this->caseName);
				$stmt->bindParam(":caseSuitNumber",$this->caseSuitNumber);
				$stmt->bindParam(":caseNumber",$this->caseNumber);
				$stmt->bindParam(":casePracticeArea",$this->casePracticeArea);
				$stmt->bindParam(":caseStage",$this->caseStage);
				$stmt->bindParam(":caseDateOpened",$this->caseDateOpened);
				$stmt->bindParam(":caseDescription",$this->caseDescription);
				$stmt->bindParam(":caseBillingContact",$this->caseBillingContact);
				$stmt->bindParam(":caseBillingMethod",$this->caseBillingMethod);
				$stmt->bindParam(":caseLeadAttorney",$this->caseLeadAttorney);
				$stmt->bindParam(":caseStaffIds",$this->caseStaffIds);
				$stmt->bindParam(":caseStatus",$this->caseStatus);
				$stmt->bindParam(":currentCourtMain",$this->currentCourtMain);
				$stmt->bindParam(":currentCourtSub",$this->currentCourtSub);
				$stmt->bindParam(":currentCourtNum",$this->currentCourtNum);
				$stmt->bindParam(":nextCourtMain",$this->nextCourtMain);
				$stmt->bindParam(":nextCourtSub",$this->nextCourtSub);
				$stmt->bindParam(":nextCourtNum",$this->nextCourtNum);
				$stmt->bindParam(":nextCourtDate",$this->nextCourtDate);
				$stmt->bindParam(":Id",$this->id);
				if ($stmt->execute()) {
					$this->log_activity($this->table,$this->id,"Updated Court Case");
					return true;
				}
				else{
					return false;
					}

		}
		// for delete
		function delete(){
			$sql="UPDATE $this->table SET record_hide=:recordHide WHERE case_id=:Id";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			$stmt->bindParam(":Id",$this->id);
			if ($stmt->execute()) {
				$this->log_activity($this->table,$this->id,"Deleted Court Case");
				return true;
			}
			else{
				return false;
			}
		}

		// get cases staff being added to
		function get_cases(){
			$returnResults = '';
			$sql="SELECT * FROM $this->table WHERE record_hide=:recordHide ORDER BY case_id DESC";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				// check if member is part of the staff selected for case
				foreach ($results as $result) {
					$addedStaffIds = json_decode($result["case_staffIds"]);
					if (in_array($_SESSION["account_id"], $addedStaffIds)) {
							$returnResults .= '<option alt="'.$result["case_number"].'" value='.$result["case_id"].'>'.$result["case_name"].' ( '.$result["case_number"].' )</option>';
						}
				}
				return $returnResults;
			}
			else{
				die();
				}

		}

		// get profile cases
		function get_staff_profile_cases(){
			$returnResults = '';
			$sql="SELECT * FROM $this->table WHERE record_hide=:recordHide ORDER BY case_id DESC";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				// check if member is part of the staff selected for case
				foreach ($results as $result) {
					$addedStaffIds = json_decode($result["case_staffIds"]);
					if (in_array($_SESSION["account_id"], $addedStaffIds)) {
							$returnResults .= '
			                                    <tr>
			                                      <td><i class="fa fa-briefcase activity-icon"></i></td>
			                                      <td>'.$result["case_name"].'</td>
			                                      <td>'.$result["case_number"].'</td>
			                                      <td>'.$result["added"].'</td>
			                                      <td>'.$result["case_stage"].'</td>
			                                      <td>'.$result["case_status"].'</td>
			                                    </tr>
			                                  ';
						}
				}
				return $returnResults;
			}
			else{
				die();
				}

		}
		// count cases
		function count_cases(){
			$sql="SELECT * FROM $this->table WHERE record_hide=:recordHide";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			if ($stmt->execute()) {
				return $stmt->rowCount();
			}
			else{
				die();
				}

		}
		// for filter results
		function court_case_filter(){
			$returnResults =[];
			$sql="SELECT * FROM $this->table WHERE case_practice_area=:practiceArea AND case_stage=:stage AND case_lead_attorney=:leadAttor AND record_hide=:recordHide ORDER BY case_id DESC";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":practiceArea",$this->casePracticeArea);
			$stmt->bindParam(":stage",$this->caseStage);
			$stmt->bindParam(":leadAttor",$this->caseLeadAttorney);
			$stmt->bindParam(":recordHide",$this->recordHide);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				// check if member is part of the staff selected for case
				foreach ($results as $result) {
					 $result['case_lead_attorney']= $this->get_caseStaff_name($result["case_lead_attorney"]);
					 $result['user_id']= $this->get_userName($result["user_id"]);
					 $returnResults[]=$result;
				}
				return json_encode($returnResults);
			}
			else{
				die();
				}

		}

		function get_contact_cases(){
			$returnResults =[];
			$sql="SELECT * FROM $this->table WHERE record_hide=:recordHide ORDER BY case_id DESC";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				// check if member is part of the staff selected for case
				foreach ($results as $result) {
					$addedContactIds = json_decode($result["case_contactIds"]);
					if (in_array($_SESSION["account_id"], $addedContactIds)) {
							$returnResults[] = $result;
						}
				}
				return $returnResults;
			}
			else{
				die();
				}

		}

		// get all list of court cases
		function get_cases_all(){
			$returnRecords ='';
			$sql="SELECT C.case_name,C.case_suit_number,C.case_number,C.case_stage,C.case_lead_attorney,C.added,C.user_id,C.case_id,C.case_staffIds,
			S.staff_firstname,S.staff_lastname, 
			U.user_name
			FROM $this->table AS C
			LEFT JOIN staff AS S
			ON C.case_lead_attorney = S.staff_id
			LEFT JOIN users AS U
			ON C.user_id = U.users_id
			WHERE C.record_hide=:recordHide ORDER BY case_id DESC";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				foreach ($results as $result) {
					 $returnRecords	 .= '
		                                  <tr>
		                                    <td>'.$result["case_name"].'</td>
		                                    <td>'.$result["case_suit_number"].'</td>
		                                    <td>'.$result["case_number"].'</td>
		                                    <td>'.$result["case_stage"].'</td>
		                                    <td>'.$result["staff_firstname"].' '.$result["staff_lastname"].'</td>
		                                    <td>'.$result["added"].' <b>by</b> '.$result["user_name"].'</td>
		                                    <td><button class="btn-default view_case_details" id="'.$result["case_id"].'"><i class="fa fa-eye"></i></button>';
		                                    // check if user is part of case staff
		                                    if (in_array($_SESSION['account_id'], json_decode($result["case_staffIds"],true))) {

		                                    	 $returnRecords	 .= '<button class="btn-primary update_case_details" id="'.$result["case_id"].'"><i class="fa fa-pencil"></i></button> 
		                                      		<button class="btn-danger del_case_data" id="'.$result["case_id"].'"><i class="fa fa-trash"></i></button>';
		                                    }
		                                      
		                                    

		             $returnRecords	 .= '	</td> 
		             					</tr>';
				}

				return $returnRecords;
			}
			else{
				die();
				}

		}

	// get case by id
		function get_case_by_id(){
			$sql="SELECT * FROM $this->table WHERE case_id=:Id LIMIT 1";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":Id",$this->id);
			if ($stmt->execute()) {
				$results= $stmt->fetch(PDO::FETCH_ASSOC);
				return json_encode($results);
			}
			else{
				die();
				}
		}


	// get all case details for court case view modal 
		// MCC - MAIN CURRENT COURT
		// MNC - MAIN NEXT COURT
		function get_case_view_details_by_id(){
			$returnRecords = [];
			$sql="SELECT C.case_id,C.case_contactIds,C.case_name,C.case_suit_number,C.case_number,C.case_practice_area,C.case_stage,C.case_date_opened,C.current_court_main_id,C.current_court_sub_id,C.current_court_num,C.next_court_main_id,C.next_court_sub_id,C.next_court_num,C.next_court_date,C.case_description,C.case_statue_limit,C.case_billing_contact,C.case_billing_method,C.case_lead_attorney,C.case_staffIds,C.case_status,C.case_closed_user_id,C.case_closed_date,C.user_id,C.added,C.record_hide,C.last_updated,
			PA.practice_area_name,
			MCC.main_court_name AS main_current_court,
			MNC.main_court_name AS main_next_court,
			ST.staff_firstname,ST.staff_lastname

			FROM $this->table AS C
			LEFT JOIN practice_area AS PA ON C.case_practice_area = PA.practice_area_id
			LEFT JOIN main_court AS MCC ON C.current_court_main_id = MCC.main_court_id 
			LEFT JOIN main_court AS MNC ON C.next_court_main_id = MNC.main_court_id 
			LEFT JOIN staff AS ST ON C.case_lead_attorney = ST.staff_id

			WHERE C.case_id=:Id LIMIT 1";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":Id",$this->id);
			if ($stmt->execute()) {
				$results= $stmt->fetch(PDO::FETCH_ASSOC);

				// attached staff details
				$returnRecords["caseContacts"] = $this->get_view_court_case_contact($results["case_contactIds"]);
				// attached staff details 
				$returnRecords["caseStaffs"] = $this->get_view_court_case_staff($results["case_staffIds"]);
				// insert all results
				$returnRecords["allResults"] = $results;
				// insert all events records
				$returnRecords["caseEvents"] = $this->get_court_case_events();
				// insert all court case documents
				$returnRecords["caseDocuments"] = $this->get_view_court_case_docs();
				// insert all court case tasks
				$returnRecords["caseTasks"] = $this->get_view_court_case_tasks();
				// insert all court case messages
				$returnRecords["caseMessages"] = $this->get_view_court_case_messages();
				// insert all court case notes
				$returnRecords["caseNotes"] = $this->get_view_court_case_notes();

				return json_encode($returnRecords);
			}
			else{
				die();
				}
		}
	// get case contacts and staff
		function get_case_contacts_and_staff(){
			$returnRecords='';
			$sql="SELECT case_staffIds,case_contactIds FROM $this->table WHERE case_id=:Id LIMIT 1";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":Id",$this->id);
			if ($stmt->execute()) {
				$results= $stmt->fetch(PDO::FETCH_ASSOC);
				return $results;
			}
			else{
				die();
				}
		}
	// get cases name only 
		function get_case_name($caseId){
				$sql="SELECT case_name,case_number FROM $this->table WHERE case_id=:Id";
				$stmt = $this->dbConn->prepare($sql);
				$stmt->bindParam(":Id",$caseId);
				if ($stmt->execute()) {
					$results = $stmt->fetch(PDO::FETCH_ASSOC);
					return trim($results["case_name"]." (".$results["case_number"].")");
				}
				else{
					return false;
					}
		}


	// get all court case events
		function get_court_case_events(){ 
			$returnRecords = "";
			$sql="SELECT EV.event_name,EV.event_location,EV.event_start_date,EV.event_end_Date,EV.added,
			U.user_name 
			FROM events AS EV
			LEFT JOIN users AS U 
			ON EV.user_id = U.users_id
			WHERE EV.record_hide=:recordHide AND EV.event_court_case_id=:caseId ORDER BY event_id DESC";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			$stmt->bindParam(":caseId",$this->id);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				foreach ($results as $result) {
					$returnRecords .= "<tr>
										<td>".$result['event_name']."</td>
										<td>".$result['added']."</td>
										<td>".$result['event_location']."</td>
										<td>".$result['event_start_date']."<b> TO </b>".$result['event_end_Date']."</td>
										<td>".$result['user_name']."</td>
									</tr>";
				}
				return $returnRecords;
			}
			else{
				return false;
				}

		}
	
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// get all cases of contacts
		function get_contacts_cases($contactId){
			$returnResults=[];
			$sql="SELECT case_name,case_number,case_contactIds FROM $this->table WHERE record_hide=:recordHide ORDER BY case_id DESC";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			if ($stmt->execute()) {
				$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
				foreach ($results as $result) {
					// get case contacts ids and decode
					$caseContactsIds = json_decode($result['case_contactIds']);
					if (in_array($contactId, $caseContactsIds)) {
						$returnResults[] = "<p>".$result['case_name']." (".$result['case_number'].")</p>";
					}
				}
				// return all cases by contacts
				return $returnResults;
			}
			else{
				die();
				}

		}

		// get all case documents
		function get_view_court_case_docs(){
			$returnResults = '';
			$sql="SELECT D.case_doc_id,D.doc_court_case_id,D.doc_folder_name,D.doc_files_uploaded,D.doc_assigned_date,D.doc_assigned_date,D.doc_description,D.added,
			U.user_name
			FROM case_document AS D
			LEFT JOIN users AS U
			ON D.user_id = U.users_id
			WHERE D.record_hide=:recordHide AND D.doc_court_case_id = :caseId
			ORDER BY D.case_doc_id DESC";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			$stmt->bindParam(":caseId",$this->id);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				// loop through entire results
				foreach ($results as $result) {
					// loop through folder to get the content
					foreach (json_decode($result["doc_files_uploaded"],true) as $document) {
						$returnResults .= '<tr>
											<td>'.$result["doc_folder_name"].'</td>
											<td>'.$document.'</td>
											<td>'.$result["added"].'</td>
											<td>'.$result["user_name"].'</td>
											<td><button class="btn-info readFile" id="'.$result["doc_folder_name"].'/'.$document.'"><i class="fa fa-eye"></i> Read</button></td>
										</tr>';
					}
					
				}
				return $returnResults;
			}
			else{
				die();
			}
		}

		// get case view tasks
		function get_view_court_case_tasks(){
			$returnRecords='';
			$sql="SELECT T.task_name,T.task_priority,T.task_due_date,T.task_status,T.task_complete_user,T.task_complete_date,T.user_id,T.added,
			U.user_name AS completedUser,
			USR.user_name AS addedUser
			FROM tasks AS T
			LEFT JOIN users AS U ON T.user_id = U.users_id
			LEFT JOIN users AS USR ON T.user_id = U.users_id
			WHERE T.record_hide=:recordHide 
			AND T.court_case_id=:caseId";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			$stmt->bindParam(":caseId",$this->id);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				foreach ($results as $result) {
					$returnRecords .= '<tr>
										<td>'.$result["task_name"].'</td>
										<td>'.$result["task_priority"].'</td>
										<td>'.$result["task_due_date"].'</td>
										<td>'.$result["task_status"].'</td>
										<td>'.$result["task_complete_date"].'<b> BY </b>'. $result["completedUser"].'</td>
										<td>'.$result["added"].'</td>
										<td>'.$result["addedUser"].'</td>
									</tr>';
				}

				return $returnRecords;
			}
			else{
				return false;
			}
		}

		// get all case messages

		function get_view_court_case_messages(){
			$returnRecords = '';
			$sql="SELECT M.message_sender_accountName,M.message_sender_accounId,M.message_receive_contacts,M.message_receive_staff,M.message_subject,M.added,
			S.staff_firstname,S.staff_lastname,
			C.contact_First_name,C.contact_last_name
			FROM messages AS M
			LEFT JOIN staff AS S ON M.message_sender_accounId = S.staff_id
			LEFT JOIN contact AS C ON M.message_sender_accounId = C.contact_id
			WHERE M.message_courtCaseId=:caseId";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":caseId",$this->id);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				foreach ($results as $result) {
					switch ($result['message_sender_accountName']) {
						case 'staff':
							$returnRecords .= '<tr>
												<td>'.$result["message_subject"].'</td>
												<td>'.$result["added"].'</td>
												<td>'.$result["message_sender_accountName"].'</td>
												<td>'.$result["staff_firstname"].' '.$result["staff_lastname"].'</td>
											</tr>';
						break;
						case 'contact':
							$returnRecords .= '<tr>
												<td>'.$result["message_subject"].'</td>
												<td>'.$result["added"].'</td>
												<td>'.$result["message_sender_accountName"].'</td>
												<td>'.$result["contact_First_name"].' '.$result["contact_last_name"].'</td>
											</tr>';
						break;
					}
				}
				return $returnRecords;
			}
			else{
				return false;
				}
		}


		// get all court case notes
		function get_view_court_case_notes(){
			$returnRecords='';
			$sql="SELECT N.court_case_note_id,N.court_case_note_content,N.added,N.account_id,
			S.staff_firstname,S.staff_lastname
			FROM court_case_notes AS N 
			LEFT JOIN staff AS S ON N.account_id = S.staff_id
			WHERE N.record_hide=:recordHide 
			AND N.court_case_id=:courtCaseId 
			ORDER BY N.court_case_note_id DESC";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			$stmt->bindParam(":courtCaseId",$this->id);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				foreach ($results as $result) {
					
					$returnRecords .= '<div style="background-color:#f4f4f4;" class="well">
									<h4><b>'.$result["court_case_note_content"].'</b></h4>
									<p><b>Posted on :</b>'.$result["added"].'<b> BY </b>'.$result["staff_firstname"].' '.$result["staff_lastname"].'</p>';

					if ($_SESSION['account_id'] == $result["account_id"]) {

						$returnRecords .= '<button class="btn-info updateNote" id="'.$result["court_case_note_id"].'"><i class="fa fa-pencil"></i></button>
										<button class="btn-danger deleteNote" id="'.$result["court_case_note_id"].'"><i class="fa fa-trash"></i></button>';
					}

					$returnRecords .= '</div>';

				}
				return $returnRecords;
			}
			else{
				return false;
				}

		}


	// get attached contacts detail
		function get_view_court_case_contact($caseContactJsonArray){
			$returnRecords='';
			if (!empty($caseContactJsonArray)) {
				$caseContactJsonArray = json_decode($caseContactJsonArray,true);

				$sql="SELECT CON.contact_First_name,CON.contact_last_name,CON.contact_cell,
				CONGRP.contact_group_name 
				FROM contact AS CON
				LEFT JOIN contacts_group AS CONGRP
				ON CON.contact_group_id = CONGRP.contact_group_id
				WHERE CON.contact_id=:Id AND CON.record_hide=:recordHide LIMIT 1";
				$stmt = $this->dbConn->prepare($sql);
				
				foreach ($caseContactJsonArray as $contactId) {
					$stmt->bindParam(":Id",$contactId);
					$stmt->bindParam(":recordHide",$this->recordHide);
					$results = $stmt->fetch(PDO::FETCH_ASSOC);
					if ($stmt->execute()) {
						$returnRecords .= '<tr>
												<td><i class="fa fa-user"></i></td>
												<td>'.$results["contact_First_name"].' '.$results["contact_last_name"].'</td>
												<td>'.$results["contact_cell"].'</td>
												<td>'.$results["contact_group_name"].'</td>
											</tr>';
						
					}
					else{
						return false;
					}
					
				}

				return $returnRecords;
			}
			else{
				return false;
			}
		}


	// get attached staff details
		function get_view_court_case_staff($caseStaffJsonArray){
			$returnRecords='';
			if (!empty($caseStaffJsonArray)) {

				$sql="SELECT staff_firstname,staff_lastname,staff_tel,staff_type
				FROM staff
				WHERE staff_id=:Id AND record_hide=:recordHide LIMIT 1";
				$stmt = $this->dbConn->prepare($sql);
				$caseStaffJsonArray = json_decode($caseStaffJsonArray,true);
				foreach ($caseStaffJsonArray as $staffId) {
					$stmt->bindParam(":Id",$staffId);
					$stmt->bindParam(":recordHide",$this->recordHide);
					$results = $stmt->fetch(PDO::FETCH_ASSOC);
					if ($stmt->execute()) {
						$returnRecords .= '<tr>
												<td><i class="fa fa-user"></i></td>
												<td>'.$results["staff_firstname"].' '.$results["staff_lastname"].'</td>
												<td>'.$results["staff_tel"].'</td>
												<td>'.$results["staff_type"].'</td>
											</tr>';
						
					}
					else{
						return false;
					}
					
				}

				return $returnRecords;
			}
			else{
				return false;
			}
		}


		function get_staffName_only($staffId){
				$sql="SELECT staff_firstname,staff_lastname FROM $this->table WHERE staff_id=:Id";
				$stmt = $this->dbConn->prepare($sql);
				$stmt->bindParam(":Id",$staffId);
				if ($stmt->execute()) {
					$results = $stmt->fetch(PDO::FETCH_ASSOC);
					return trim($results["staff_firstname"]." ".$results["staff_lastname"]);
				}
				else{
					return false;
					}
		}



	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// close case
		function close_court_case(){
			$sql="UPDATE $this->table SET case_status=:caseStatus,case_closed_user_id=:caseClosedUserId,case_closed_date=:caseClosedDate WHERE case_id=:Id";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":caseStatus",$this->caseStatus);
			$stmt->bindParam(":caseClosedUserId",$_SESSION['user_id']);
			$stmt->bindParam(":caseClosedDate",date("d-m-Y h:i:sa"));
			$stmt->bindParam(":Id",$this->id);
			if ($stmt->execute()) {
				
				return true;
			}
			else{
				return false;
			}
		}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function log_activity($tableName,$tableId,$activityName){
		$sql = "INSERT INTO activities (table_name,table_id,activity_name,account_id,account_name,user_id) VALUES (:tableName,:tableId,:activityName,:accountId,:accountName,:userId)";
		$stmt = $this->dbConn->prepare($sql);
		$stmt->bindParam(":tableName",$tableName);
		$stmt->bindParam(":tableId",$tableId);
		$stmt->bindParam(":tableId",$tableId);
		$stmt->bindParam(":activityName",$activityName);
		$stmt->bindParam(":accountId",$_SESSION['account_id']);
		$stmt->bindParam(":accountName",$_SESSION['account_name']);
		$stmt->bindParam(":userId",$_SESSION['user_id']);
		$stmt->execute();
	}

}
?>