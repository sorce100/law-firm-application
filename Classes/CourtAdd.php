<?php 
	date_default_timezone_set('Africa/Accra');
	class CourtAdd{
		// setting and getting variables
		private $mainCourtId;
		private $subCourtId;
		private $added;
		private $dbConn;
		private $recordHide = "NO";
		private $maintable = "main_court";
		private $subtable = "sub_court";
		private $mainCourtName;
		private $mainCourtSelect;
		private $subCourtName;
		private $subCourtRegion;
		
		function set_id($id) { $this->id = $id; }
		function set_added($added) { $this->added = $added; }
		function set_recordHide($recordHide) { $this->recordHide = $recordHide; }
		function set_mainCourtName($mainCourtName) { $this->mainCourtName = $mainCourtName; }
		function set_mainCourtSelect($mainCourtSelect) { $this->mainCourtSelect = $mainCourtSelect; }
		function set_subCourtName($subCourtName) { $this->subCourtName = $subCourtName; }
		function set_mainCourtId($mainCourtId) { $this->mainCourtId = $mainCourtId; }
		function set_subCourtId($subCourtId) { $this->subCourtId = $subCourtId; }
		function set_subCourtRegion($subCourtRegion) { $this->subCourtRegion = $subCourtRegion; }

		public function __construct(){
			require_once("db/db.php");
			$db = new DbConnect();
			$this->dbConn = $db->connect();
		}

		// clean data for data input
		public function CleanData($data){
			$data = trim($data);
			$data=htmlentities($data,ENT_QUOTES, 'UTF-8');
			$data = filter_var($data,FILTER_SANITIZE_SPECIAL_CHARS);
			return $data;
		}

		// inserting main court
		function main_court_insert(){
			$date=date("jS F Y \/ h:i:s A");
			$sql = "INSERT INTO $this->maintable (main_court_name,added,record_hide,user_id) VALUES (:mainCourtName,:added,:recordHide,:userId)";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":mainCourtName",$this->mainCourtName);
			$stmt->bindParam(":added",$date);
			$stmt->bindParam(":recordHide",$this->recordHide);
			$stmt->bindParam(":userId",$_SESSION['user_id']);
			if ($stmt->execute()) {
				return true;
			}
			else{
				return false;
				}
		}
		// insert sub courts
		function sub_court_insert(){
			$date=date("jS F Y \/ h:i:s A");
			$sql = "INSERT INTO $this->subtable (main_court_select_id,sub_court_name,sub_court_region,added,record_hide,user_id) 
			VALUES (:mainCourtSelect,:subCourtName,:subCourtRegion,:added,:recordHide,:userId)";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":mainCourtSelect",$this->mainCourtSelect);
			$stmt->bindParam(":subCourtRegion",$this->subCourtRegion);
			$stmt->bindParam(":subCourtName",$this->subCourtName);
			$stmt->bindParam(":added",$date);
			$stmt->bindParam(":recordHide",$this->recordHide);
			$stmt->bindParam(":userId",$_SESSION['user_id']);
			if ($stmt->execute()) {
				return true;
			}
			else{
				return false;
				}
		}
		// get all sub division by main id
		function get_sub_division_by_main_id(){
			$returnRecords = '';
			$sql="SELECT sub_court_id,sub_court_name 
			FROM $this->subtable 
			WHERE record_hide=:recordHide 
			AND main_court_select_id=:mainCourtSelect
			ORDER BY sub_court_id DESC";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			$stmt->bindParam(":mainCourtSelect",$this->mainCourtSelect);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				foreach ($results as $result) {
					$returnRecords .= '<option value="'.$result["sub_court_id"].'">'.$result["sub_court_name"].'</option>';
				}
				return json_encode($returnRecords);
			}
			else{
				return false;
			}

		}

		// for main court
		function main_court_update(){
			$sql="UPDATE $this->maintable SET main_court_name=:mainCourtName WHERE main_court_id=:Id";
				$stmt = $this->dbConn->prepare($sql);
				$stmt->bindParam(":mainCourtName",$this->mainCourtName);
				$stmt->bindParam(":Id",$this->mainCourtId);
				if ($stmt->execute()) {
					
					return true;
				}
				else{
					return false;
					}

		}
		// for sub court
		function sub_court_update(){
			$sql="UPDATE $this->subtable SET main_court_select_id=:mainCourtSelect,sub_court_name=:subCourtName,sub_court_region=:subCourtRegion WHERE sub_court_id=:Id";
				$stmt = $this->dbConn->prepare($sql);
				$stmt->bindParam(":mainCourtSelect",$this->mainCourtSelect);
				$stmt->bindParam(":subCourtName",$this->subCourtName);
				$stmt->bindParam(":subCourtRegion",$this->subCourtRegion);
				$stmt->bindParam(":Id",$this->subCourtId);
				if ($stmt->execute()) {
					
					return true;
				}
				else{
					return false;
					}

		}
		// for delete
		function mainCourtdelete(){
			$sql="UPDATE $this->maintable SET record_hide=:recordHide WHERE main_court_id=:Id";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			$stmt->bindParam(":Id",$this->mainCourtId);
			if ($stmt->execute()) {
				
				return true;
			}
			else{
				return false;
			}
		}

		// for sub delete
		function subCourtDelete(){
			$sql="UPDATE $this->subtable SET record_hide=:recordHide WHERE sub_court_id=:Id";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			$stmt->bindParam(":Id",$this->subCourtId);
			if ($stmt->execute()) {
				
				return true;
			}
			else{
				return false;
			}
		}


	// get main list
		function get_main_courts(){
			$sql="SELECT * FROM $this->maintable WHERE record_hide=:recordHide ORDER BY main_court_id DESC";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $results;
			}
			else{
				return false;
			}

		}

	// main court options
		function get_main_courts_options(){
			$returnRecords='';
			$sql="SELECT * FROM $this->maintable WHERE record_hide=:recordHide ORDER BY main_court_id DESC";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			if ($stmt->execute()) {
				$details= $stmt->fetchAll(PDO::FETCH_ASSOC);
				foreach ($details as $detail) {
                  $returnRecords .='<option value='.$detail["main_court_id"].'>'.$detail["main_court_name"].'</option>';
              }
			
			return $returnRecords;
			
			}else{
				return false;
			}

		}

	// get sub list
		function get_sub_courts(){
			$sql="SELECT m.main_court_name,s.sub_court_id,s.main_court_select_id,s.sub_court_name,s.sub_court_region,s.added FROM 
			$this->subtable AS s
			RIGHT JOIN $this->maintable AS m
			ON s.main_court_select_id = m.main_court_id
			WHERE s.record_hide=:recordHide ORDER BY s.sub_court_id DESC";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $results;
			}
			else{
				return false;
			}

		}

	// get main court
		function get_main_court_by_id(){
			$sql="SELECT * FROM $this->maintable WHERE main_court_id=:Id";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":Id",$this->mainCourtId);
			if ($stmt->execute()) {
				$results= $stmt->fetch(PDO::FETCH_ASSOC);
				return json_encode($results);
			}
			else{
				return false;
				}
		}

	// get sub court
		function get_sub_court_by_id(){
			$sql="SELECT * FROM $this->subtable WHERE sub_court_id=:Id";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":Id",$this->subCourtId);
			if ($stmt->execute()) {
				$results= $stmt->fetch(PDO::FETCH_ASSOC);
				return json_encode($results);
			}
			else{
				return false;
				}
		}


	}

?>