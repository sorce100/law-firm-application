<?php 
	date_default_timezone_set('Africa/Accra');
	class Messages{
		// setting and getting variables
		private $id;
		private $message_courtCaseId;
		private $messageContactIds;
		private $messageStaffIds;
		private $messageSubject;
		private $messageContent;
		private $messageStatus;
		private $added;
		private $dbConn;
		private $recordHide = "NO";
		private $receiverRecordHide = "NO";
		private $senderRecordHide = "NO";
		private $table = "messages";

		function set_id($id) { $this->id = $id; }
		function set_added($added) { $this->added = $added; }
		function set_message_courtCaseId($message_courtCaseId) { $this->message_courtCaseId = $message_courtCaseId; }
		function set_messageContactIds($messageContactIds) { $this->messageContactIds = $messageContactIds; }
		function set_messageStaffIds($messageStaffIds) { $this->messageStaffIds = $messageStaffIds; }
		function set_messageSubject($messageSubject) { $this->messageSubject = $messageSubject; }
		function set_messageContent($messageContent) { $this->messageContent = $messageContent; }
		function set_messageStatus($messageStatus) { $this->messageStatus = $messageStatus; }
		function set_receiverRecordHide($receiverRecordHide) { $this->receiverRecordHide = $receiverRecordHide; }
		function set_senderRecordHide($senderRecordHide) { $this->senderRecordHide = $senderRecordHide; }

		public function __construct(){
			require_once("db/db.php");
			$db = new DbConnect();
			$this->dbConn = $db->connect();
		}

		// clean data for data input
		public function CleanData($data){
			$data = trim($data);
			$data=htmlentities($data,ENT_QUOTES, 'UTF-8');
			$data = filter_var($data,FILTER_SANITIZE_SPECIAL_CHARS);
			return $data;
		}

		// insert pages
		function insert(){
			$date = date("jS F Y \/ h:i:s A");
			$sql = "INSERT INTO $this->table (message_sender_accountName,message_sender_accounId,message_courtCaseId,message_receive_contacts,message_receive_staff,message_subject,message_content,message_status,added,receiver_record_hide,sender_record_hide) VALUES (:message_sender_accountName,:messageSender_accounId,:message_courtCaseId,:messageContactIds,:messageStaffIds,:messageSubject,:messageContent,:messageStatus,:added,:receiverRecordHide,:senderRecordHide)";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":message_sender_accountName",$_SESSION['account_name']);
			$stmt->bindParam(":messageSender_accounId",$_SESSION['account_id']);
			$stmt->bindParam(":message_courtCaseId",$this->message_courtCaseId);
			$stmt->bindParam(":messageContactIds",$this->messageContactIds);
			$stmt->bindParam(":messageStaffIds",$this->messageStaffIds);
			$stmt->bindParam(":messageSubject",$this->messageSubject);
			$stmt->bindParam(":messageContent",$this->messageContent);
			$stmt->bindParam(":messageStatus",$this->messageStatus);
			$stmt->bindParam(":added",$date);
			$stmt->bindParam(":receiverRecordHide",$this->receiverRecordHide);
			$stmt->bindParam(":senderRecordHide",$this->senderRecordHide);
			if ($stmt->execute()) {
				$this->log_activity($this->table,$this->dbConn->lastInsertId(),"New Message Sent");
				return trim($this->dbConn->lastInsertId());
			}
			else{
				die();
				}
		}
		// for update
		function update(){
			$sql="UPDATE $this->table SET message_receive_contacts=:messageContactIds,message_receive_staff=:messageStaffIds,message_subject=:messageSubject,message_content=:messageContent WHERE message_id=:Id";
				$stmt = $this->dbConn->prepare($sql);
				$stmt->bindParam(":messageContactIds",$this->messageContactIds);
				$stmt->bindParam(":messageStaffIds",$this->messageStaffIds);
				$stmt->bindParam(":messageSubject",$this->messageSubject);
				$stmt->bindParam(":messageContent",$this->messageContent);
				$stmt->bindParam(":Id",$this->id);
				if ($stmt->execute()) {
					return true;
				}
				else{
					return false;
					}

		}
		// for delete
		function receive_delete(){
			$sql="UPDATE $this->table SET receiver_record_hide=:recordHide WHERE message_id=:Id";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->receiverRecordHide);
			$stmt->bindParam(":Id",$this->id);
			if ($stmt->execute()) {
				
				return true;
			}
			else{
				return false;
			}
		}

		function sender_delete(){
			$sql="UPDATE $this->table SET sender_record_hide=:recordHide WHERE message_id=:Id";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->senderRecordHide);
			$stmt->bindParam(":Id",$this->id);
			if ($stmt->execute()) {
				
				return true;
			}
			else{
				return false;
			}
		}

		// get users
		function get_sent_messages(){
			$sql="SELECT * FROM $this->table WHERE sender_record_hide=:recordHide AND message_sender_accounId=:accountID AND message_sender_accountName=:accountName ORDER BY message_id DESC";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->senderRecordHide);
			$stmt->bindParam(":accountID",$_SESSION['account_id']);
			$stmt->bindParam(":accountName",$_SESSION['account_name']);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $results;
			}
			else{
				die();
				}

		}
///////////////////////////////////////STAFF///////////////////////////////////////////////////////////////
// for getting all messages received by staff
		function get_staff_received_messages(){
			$returnResults = [];
			// grab all messages thats not deleted
			$sql="SELECT * FROM $this->table WHERE receiver_record_hide=:recordHide ORDER BY message_id DESC";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->receiverRecordHide);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				// loop through all results and find one that matched account id then save
				foreach ($results as $result) {
					$receiveStaffIds = json_decode($result['message_receive_staff']);
					if (in_array($_SESSION['account_id'],$receiveStaffIds)) {
						$result['message_sender_accounId'] = $this->get_staff_name($result['message_sender_accounId']);
						// id account id is the array then add to array
						$returnResults[] = $result;
					}
				}
				// return account ids received messages
				return $returnResults;
			}
			else{
				die();
				}
		}

	// count unread
		function count_unread_staff_received_messages(){
			$returnResults = [];
			// grab all messages thats not deleted
			$sql="SELECT * FROM $this->table WHERE receiver_record_hide=:recordHide ORDER BY message_id DESC";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->receiverRecordHide);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				// loop through all results and find one that matched account id then save
				foreach ($results as $result) {
					$receiveStaffIds = json_decode($result['message_receive_staff']);
					if ((in_array($_SESSION['account_id'],$receiveStaffIds)) && ($result['message_status'] == "UNREAD")) {
						// id account id is the array then add to array
						$returnResults[] = $result;
					}
				}
				// return account ids received messages
				return sizeof($returnResults);
			}
			else{
				die();
				}
		}
//////////////////////////////////////////CLIENT////////////////////////////////////////////////////////////

	function get_contact_received_messages(){
			$returnResults = [];
			// grab all messages thats not deleted
			$sql="SELECT * FROM $this->table WHERE receiver_record_hide=:recordHide ORDER BY message_id DESC";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->receiverRecordHide);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				// loop through all results and find one that matched account id then save
				foreach ($results as $result) {
					$receiveContactIds = json_decode($result['message_receive_contacts']);
					if (in_array($_SESSION['account_id'],$receiveContactIds)) {
						// check if sender account is contact or staff
						if ($result['message_sender_accountName']=="staff") {
							$result['message_sender_accounId'] = $this->get_staff_name($result['message_sender_accounId']);
							$returnResults[] = $result;
						}
						elseif ($result['message_sender_accountName']=="contact") {
							$result['message_sender_accounId'] = $this->get_contact_Name($result['message_sender_accounId']);
							$returnResults[] = $result;
						}
					}
				}
				// return account ids received messages
				return $returnResults;
			}
			else{
				die();
				}
		}



//////////////////////////////////////////////////////////////////////////////////////////////////////

	// get case tasks
		function get_court_case_messages(){
			$returnResults = [];
			$sql="SELECT message_sender_accountName,message_sender_accounId,message_receive_contacts,message_receive_staff,message_subject,added FROM $this->table WHERE message_courtCaseId=:caseId";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":caseId",$this->message_courtCaseId);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				foreach ($results as $result) {
					if ($result['message_sender_accountName']=="staff") {
						$result['message_sender_accounId'] = $this->get_staff_name($result['message_sender_accounId']);
						$returnResults[] = $result;
					}
					elseif ($result['message_sender_accountName']=="contact") {
						$result['message_sender_accounId'] = $this->get_contact_Name($result['message_sender_accounId']);
						$returnResults[] = $result;
					}
				}
				return $returnResults;
			}
			else{
				die();
				}
		}

		function get_messages_by_id(){
			$sql="SELECT * FROM $this->table WHERE message_id=:id";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":id",$this->id);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				return json_encode($results);
			}
			else{
				die();
				}
		}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		function get_staff_name($staffId){
				$sql="SELECT staff_firstname,staff_lastname FROM staff WHERE staff_id=:Id";
				$stmt = $this->dbConn->prepare($sql);
				$stmt->bindParam(":Id",$staffId);
				if ($stmt->execute()) {
					$results = $stmt->fetch(PDO::FETCH_ASSOC);
					return trim($results["staff_firstname"]." ".$results["staff_lastname"]);
				}
				else{
					return false;
					}
		}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// get contact name
	function get_contact_Name($contactId){
		$sql="SELECT contact_First_name,contact_last_name FROM contact WHERE contact_id=:Id";
		$stmt = $this->dbConn->prepare($sql);
		$stmt->bindParam(":Id",$contactId);
		if ($stmt->execute()) {
			$results = $stmt->fetch(PDO::FETCH_ASSOC);
			return trim($results["contact_First_name"]." ".$results["contact_last_name"]);
		}
		else{
			return false;
			}
	}


	// get case name

		function get_caseDetails($caseId){
			$sql="SELECT case_name FROM court_case WHERE case_id=:Id";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":Id",$caseId);
			if ($stmt->execute()) {
				$results= $stmt->fetch(PDO::FETCH_ASSOC);
				return $results["case_name"];
			}
			else{
				die();
				}
		}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function log_activity($tableName,$tableId,$activityName){
		$sql = "INSERT INTO activities (table_name,table_id,activity_name,account_id,account_name,user_id) VALUES (:tableName,:tableId,:activityName,:accountId,:accountName,:userId)";
		$stmt = $this->dbConn->prepare($sql);
		$stmt->bindParam(":tableName",$tableName);
		$stmt->bindParam(":tableId",$tableId);
		$stmt->bindParam(":tableId",$tableId);
		$stmt->bindParam(":activityName",$activityName);
		$stmt->bindParam(":accountId",$_SESSION['account_id']);
		$stmt->bindParam(":accountName",$_SESSION['account_name']);
		$stmt->bindParam(":userId",$_SESSION['user_id']);
		$stmt->execute();
	}

}
?>