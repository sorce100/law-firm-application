 <?php 
	class Activity{
		private $dbConn;
		private $table = "activities";

		public function __construct(){
			require_once("db/db.php");
			$db = new DbConnect();
			$this->dbConn = $db->connect();
		}

		function get_user_activity_limit_10(){
			$returnResults = [];
			$sql="SELECT * FROM $this->table WHERE account_id=:accountId AND account_name=:accountName AND user_id=:userId ORDER BY activity_id DESC LIMIT 10";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":accountId",$_SESSION['account_id']);
			$stmt->bindParam(":accountName",$_SESSION['account_name']);
			$stmt->bindParam(":userId",$_SESSION['user_id']);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				foreach ($results as $result) {
					if ($result['account_name'] == "staff") {
						$result['user_id'] = $this->get_staff_name($result['account_id']);
						$returnResults[]=$result;
					}
					elseif ($result['account_name'] == "contact") {
						$returnResults[]=$result;
					}
				}
				return $returnResults;
			}
			else{
				die();
			}

		}

		function get_user_activity(){
			$returnResults = [];
			$sql="SELECT * FROM $this->table WHERE account_id=:accountId AND account_name=:accountName AND user_id=:userId ORDER BY activity_id DESC";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":accountId",$_SESSION['account_id']);
			$stmt->bindParam(":accountName",$_SESSION['account_name']);
			$stmt->bindParam(":userId",$_SESSION['user_id']);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				foreach ($results as $result) {
					if ($result['account_name'] == "staff") {
						$result['user_id'] = $this->get_staff_name($result['account_id']);
						$returnResults[]=$result;
					}
					elseif ($result['account_name'] == "contact") {
						$returnResults[]=$result;
					}
				}
				return $returnResults;
			}
			else{
				die();
			}

		}
// all activities
		function get_user_activity_all(){
			$returnResults = [];
			$sql="SELECT * FROM $this->table ORDER BY activity_id";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":accountId",$_SESSION['account_id']);
			$stmt->bindParam(":accountName",$_SESSION['account_name']);
			$stmt->bindParam(":userId",$_SESSION['user_id']);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				foreach ($results as $result) {
					if ($result['account_name'] == "staff") {
						$result['user_id'] = $this->get_staff_name($result['account_id']);
						$returnResults[]=$result;
					}
					elseif ($result['account_name'] == "contact") {
						$returnResults[]=$result;
					}
				}
				return $returnResults;
			}
			else{
				die();
			}

		}
	// get table activity
		function get_table_activity($tableName){
			$returnResults = [];
			$sql="SELECT ac.account_name,ac.activity_name,ac.date_done,ac.account_id,st.staff_type,CONCAT(st.staff_firstname,' ',st.staff_lastname) AS fullName
			FROM $this->table AS ac
			JOIN staff AS st
			ON ac.account_id = st.staff_id
			WHERE ac.table_name=:tableName";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":tableName",$tableName);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				foreach ($results as $result) {
					if ($result["account_name"] == "staff") {
						$returnResults[]=$result;
					}
				}
				return $returnResults;
			}		
			else{
				die();
				}
		}

		function get_staff_name($staffId){
			$sql="SELECT staff_firstname,staff_lastname FROM staff WHERE staff_id=:Id";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":Id",$staffId);
			if ($stmt->execute()) {
				$results= $stmt->fetch(PDO::FETCH_ASSOC);
				return $results["staff_firstname"]." ".$results["staff_lastname"];
			}
			else{
				die();
				}
		}

		// get contact name
		function get_contactName($contactId){
			$sql="SELECT contact_First_name,contact_last_name FROM contact WHERE contact_id=:Id";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":Id",$contactId);
			if ($stmt->execute()) {
				$results = $stmt->fetch(PDO::FETCH_ASSOC);
				return trim($results["contact_First_name"]." ".$results["contact_last_name"]);
			}
			else{
				return false;
				}
		}

}

?>