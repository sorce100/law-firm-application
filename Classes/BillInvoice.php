<?php 
	date_default_timezone_set('Africa/Accra');
	class BillInvoice{
		// setting and getting variables
		private $id;
		private $invoiceNum;
		private $invoiceDate;
		private $invoiceCourtCaseId;
		private $invoiceContact;
		private $invoiceAddress;
		private $invoiceBillable;
		private $timeEnteryInvoiceAdd;
		private $timeEntryTotal;
		private $expensesInvoiceAdd;
		private $expensesTotal;
		private $mainTotal;
		private $added;
		private $dbConn;
		private $recordHide = "NO";
		private $table = "bill_invoice";


		function set_id($id) { $this->id = $id; }
		function set_added($added) { $this->added = $added; }
		function set_recordHide($recordHide) { $this->recordHide = $recordHide; }
		function set_invoiceNum($invoiceNum) { $this->invoiceNum = $invoiceNum; }
		function set_invoiceDate($invoiceDate) { $this->invoiceDate = $invoiceDate; }
		function set_invoiceCourtCaseId($invoiceCourtCaseId) { $this->invoiceCourtCaseId = $invoiceCourtCaseId; }
		function set_invoiceContact($invoiceContact) { $this->invoiceContact = $invoiceContact; }
		function set_invoiceAddress($invoiceAddress) { $this->invoiceAddress = $invoiceAddress; }
		function set_invoiceBillable($invoiceBillable) { $this->invoiceBillable = $invoiceBillable; }
		function set_timeEnteryInvoiceAdd($timeEnteryInvoiceAdd) { $this->timeEnteryInvoiceAdd = $timeEnteryInvoiceAdd; }
		function set_timeEntryTotal($timeEntryTotal) { $this->timeEntryTotal = $timeEntryTotal; }
		function set_expensesInvoiceAdd($expensesInvoiceAdd) { $this->expensesInvoiceAdd = $expensesInvoiceAdd; }
		function set_expensesTotal($expensesTotal) { $this->expensesTotal = $expensesTotal; }
		function set_mainTotal($mainTotal) { $this->mainTotal = $mainTotal; }

		public function __construct(){
			require_once("db/db.php");
			$db = new DbConnect();
			$this->dbConn = $db->connect();
		}

		// clean data for data input
		public function CleanData($data){
			$data = trim($data);
			$data=htmlentities($data,ENT_QUOTES, 'UTF-8');
			$data = filter_var($data,FILTER_SANITIZE_SPECIAL_CHARS);
			return $data;
		}

		// insert pages
		function insert(){
			$date = date("j-m-Y");
			// delete this
			$sql = "INSERT INTO $this->table (bill_invoice_staff,bill_invoice_number,bill_invoice_date,bill_invoice_case_id,bill_invoice_contact_id,bill_invoice_address,bill_invoice_billable,bill_invoice_timeEntry_ids,bill_invoice_timeEntry_total,bill_invoice_expenses_ids,bill_invoice_expenses_total,bill_invoice_main_total,added,user_id,record_hide) VALUES (:invoiceStaff,:invoiceNum,:invoiceDate,:invoiceCourtCaseId,:invoiceContact,:invoiceAddress,:invoiceBillable,:timeEnteryInvoiceAdd,:timeEntryTotal,:expensesInvoiceAdd,:expensesTotal,:mainTotal,:added,:userId,:recordHide)";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":invoiceStaff",$_SESSION['account_id']);
			$stmt->bindParam(":invoiceNum",$this->invoiceNum);
			$stmt->bindParam(":invoiceDate",$this->invoiceDate);
			$stmt->bindParam(":invoiceCourtCaseId",$this->invoiceCourtCaseId);
			$stmt->bindParam(":invoiceContact",$this->invoiceContact);
			$stmt->bindParam(":invoiceAddress",$this->invoiceAddress);
			$stmt->bindParam(":invoiceBillable",$this->invoiceBillable);
			$stmt->bindParam(":timeEnteryInvoiceAdd",$this->timeEnteryInvoiceAdd);
			$stmt->bindParam(":timeEntryTotal",$this->timeEntryTotal);
			$stmt->bindParam(":expensesInvoiceAdd",$this->expensesInvoiceAdd);
			$stmt->bindParam(":expensesTotal",$this->expensesTotal);
			$stmt->bindParam(":mainTotal",$this->mainTotal);
			$stmt->bindParam(":added",$date);
			$stmt->bindParam(":userId",$_SESSION['user_id']);
			$stmt->bindParam(":recordHide",$this->recordHide);
			if ($stmt->execute()) {
				$this->update_timeEntry_invoiced($this->timeEnteryInvoiceAdd,$this->invoiceNum,"YES");
				$this->update_expenses_invoiced($this->expensesInvoiceAdd,$this->invoiceNum,"YES");
				$this->log_activity($this->table,$this->dbConn->lastInsertId(),"Added New Invoice");
				return true;
			}
			else{
				die();
			}
		}
		// for update
		function update(){
			$sql="UPDATE $this->table SET bill_invoice_number=:invoiceNum,bill_invoice_date=:invoiceDate,bill_invoice_case_id=:invoiceCourtCaseId,bill_invoice_contact_id=:invoiceContact,bill_invoice_address=:invoiceAddress,bill_invoice_billable=:invoiceBillable,bill_invoice_timeEntry_ids=:timeEnteryInvoiceAdd,bill_invoice_timeEntry_total=:timeEntryTotal,bill_invoice_expenses_ids=:expensesInvoiceAdd,bill_invoice_expenses_total=:expensesTotal,bill_invoice_main_total=:mainTotal WHERE bill_invoice_id=:Id";
				$stmt = $this->dbConn->prepare($sql);
				$stmt->bindParam(":invoiceNum",$this->invoiceNum);
				$stmt->bindParam(":invoiceDate",$this->invoiceDate);
				$stmt->bindParam(":invoiceCourtCaseId",$this->invoiceCourtCaseId);
				$stmt->bindParam(":invoiceContact",$this->invoiceContact);
				$stmt->bindParam(":invoiceAddress",$this->invoiceAddress);
				$stmt->bindParam(":invoiceBillable",$this->invoiceBillable);
				$stmt->bindParam(":timeEnteryInvoiceAdd",$this->timeEnteryInvoiceAdd);
				$stmt->bindParam(":timeEntryTotal",$this->timeEntryTotal);
				$stmt->bindParam(":expensesInvoiceAdd",$this->expensesInvoiceAdd);
				$stmt->bindParam(":expensesTotal",$this->expensesTotal);
				$stmt->bindParam(":mainTotal",$this->mainTotal);
				$stmt->bindParam(":Id",$this->id);
				if ($stmt->execute()) {
					$this->log_activity($this->table,$this->id,"Updated Invoice");
					return true;
				}
				else{
					return false;
					}

		}

		// for delete
		function delete(){
			$sql="UPDATE $this->table SET record_hide=:recordHide WHERE bill_invoice_id=:Id";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			$stmt->bindParam(":Id",$this->id);
			if ($stmt->execute()) {
				$this->log_activity($this->table,$this->id,"Deleted Invoice");
				return true;
			}
			else{
				return false;
			}
		}

		function get_invoices(){
			$returnResults =[];
			$sql="SELECT * FROM $this->table WHERE record_hide=:recordHide ORDER BY bill_invoice_id DESC";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				foreach ($results as $result) {
					$result['bill_invoice_case_id'] = $this->get_court_case_name($result['bill_invoice_case_id']);
					$result['bill_invoice_staff'] = $this->get_staff_name($result['bill_invoice_staff']);
					$result['bill_invoice_contact_id'] = $this->get_contact_name($result['bill_invoice_contact_id']);
					$returnResults[]=$result;
				}
				return $returnResults;
			}
			else{
				die();
			}
		}

		function get_case_doc_by_id(){
			$sql="SELECT * FROM $this->table WHERE bill_invoice_id=:Id";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":Id",$this->id);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				return json_encode($results);
			}
			else{
				die();
			}
		}

		// get court case name
		function get_court_case_name($caseId){
				$sql="SELECT case_name,case_number FROM court_case WHERE case_id=:Id";
				$stmt = $this->dbConn->prepare($sql);
				$stmt->bindParam(":Id",$caseId);
				if ($stmt->execute()) {
					$results = $stmt->fetch(PDO::FETCH_ASSOC);
					return trim($results["case_name"]." (".$results["case_number"].")");
				}
				else{
					return false;
					}
		}

// get staff name
		function get_staff_name($staffId){
				$sql="SELECT staff_firstname,staff_lastname FROM staff WHERE staff_id=:Id";
				$stmt = $this->dbConn->prepare($sql);
				$stmt->bindParam(":Id",$staffId);
				if ($stmt->execute()) {
					$results = $stmt->fetch(PDO::FETCH_ASSOC);
					return trim($results["staff_firstname"]." ".$results["staff_lastname"]);
				}
				else{
					return false;
					}
		}

// get contact name
		function get_contact_name($contactId){
			$sql="SELECT contact_First_name,contact_last_name FROM contact WHERE contact_id=:Id";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":Id",$contactId);
			if ($stmt->execute()) {
				$results = $stmt->fetch(PDO::FETCH_ASSOC);
				return trim($results["contact_First_name"]." ".$results["contact_last_name"]);
			}
			else{
				return false;
				}
		}
// grab all invoices that are not fully payed
		function get_unpayed_invoices(){
			$sql="SELECT * FROM $this->table";
			$stmt = $this->dbConn->prepare($sql);
			if ($stmt->execute()) {
				$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $results;
			}
			else{
				return false;
				}
		}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// update timeentry invoiced
		function update_timeEntry_invoiced($timeEntryId,$invoiceNum,$invoiced){
			$sql="UPDATE bill_time_entry SET time_entry_invoiced=:timeEntryInvoiced,time_entry_invoice_num=:timeinvoiceNum WHERE time_entry_id=:Id";
			// decode entry time ids and loop through theme to update
			$entryTimeIds = json_decode($timeEntryId);
			for ($i=0; $i < sizeof($entryTimeIds) ; $i++) { 
				$stmt = $this->dbConn->prepare($sql);
				$stmt->bindParam(":timeEntryInvoiced",$invoiced);
				$stmt->bindParam(":timeinvoiceNum",$invoiceNum);
				$stmt->bindParam(":Id",$entryTimeIds[$i]);
				$stmt->execute();
			}
		}
		
		// update expenses invoiced
		function update_expenses_invoiced($expensesId,$invoiceNum,$invoiced){
			$sql="UPDATE bill_expenses SET bill_expenses_invoiced=:expensesInvoiced,bill_expenses_invoice_num=:expensesinvoiceNum WHERE bill_expenses_id=:Id";
			// decode entry time ids and loop through theme to update
			$expensesIds = json_decode($expensesId);
			for ($i=0; $i < sizeof($expensesIds) ; $i++) { 
				$stmt = $this->dbConn->prepare($sql);
				$stmt->bindParam(":expensesInvoiced",$invoiced);
				$stmt->bindParam(":expensesinvoiceNum",$invoiceNum);
				$stmt->bindParam(":Id",$expensesIds[$i]);
				$stmt->execute();
			}
		}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function log_activity($tableName,$tableId,$activityName){
		$sql = "INSERT INTO activities (table_name,table_id,activity_name,account_id,account_name,user_id) VALUES (:tableName,:tableId,:activityName,:accountId,:accountName,:userId)";
		$stmt = $this->dbConn->prepare($sql);
		$stmt->bindParam(":tableName",$tableName);
		$stmt->bindParam(":tableId",$tableId);
		$stmt->bindParam(":tableId",$tableId);
		$stmt->bindParam(":activityName",$activityName);
		$stmt->bindParam(":accountId",$_SESSION['account_id']);
		$stmt->bindParam(":accountName",$_SESSION['account_name']);
		$stmt->bindParam(":userId",$_SESSION['user_id']);
		$stmt->execute();
	}
}
?>