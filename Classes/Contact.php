<?php 
	date_default_timezone_set('Africa/Accra');
	class Contact{
		// setting and getting variables
		private $id;
		private $contactFirstName;
		private $contactMidName;
		private $contactLastName;
		private $contactEmail;
		private $contactCompany;
		private $contactGroup;
		private $contactPortal;
		private $contactCell;
		private $contactWrkPhone;
		private $contactAddress;
		private $contactCity;
		private $contactRegion;
		private $contactCountry;
		private $added;
		private $dbConn;
		private $recordHide = "NO";
		private $table = "contact";

		function set_id($id) { $this->id = $id; }
		function set_contactFirstName($contactFirstName) { $this->contactFirstName = $contactFirstName; }
		function set_contactMidName($contactMidName) { $this->contactMidName = $contactMidName; }
		function set_contactLastName($contactLastName) { $this->contactLastName = $contactLastName; }
		function set_contactEmail($contactEmail) { $this->contactEmail = $contactEmail; }
		function set_contactCompany($contactCompany) { $this->contactCompany = $contactCompany; }
		function set_contactGroup($contactGroup) { $this->contactGroup = $contactGroup; }
		function set_contactPortal($contactPortal) { $this->contactPortal = $contactPortal; }
		function set_contactCell($contactCell) { $this->contactCell = $contactCell; }
		function set_contactWrkPhone($contactWrkPhone) { $this->contactWrkPhone = $contactWrkPhone; }
		function set_contactAddress($contactAddress) { $this->contactAddress = $contactAddress; }
		function set_contactCity($contactCity) { $this->contactCity = $contactCity; }
		function set_contactRegion($contactRegion) { $this->contactRegion = $contactRegion; }
		function set_contactCountry($contactCountry) { $this->contactCountry = $contactCountry; }
		function set_added($added) { $this->added = $added; }
		function set_recordHide($recordHide) { $this->recordHide = $recordHide; }

		public function __construct(){
			require_once("db/db.php");
			$db = new DbConnect();
			$this->dbConn = $db->connect();
		}

		// clean data for data input
		public function CleanData($data){
			$data = trim($data);
			$data=htmlentities($data,ENT_QUOTES, 'UTF-8');
			$data = filter_var($data,FILTER_SANITIZE_SPECIAL_CHARS);
			return $data;
		}

		// insert pages
		function insert(){
			$date = date("jS F Y \/ h:i:s A");
			$sql = "INSERT INTO $this->table (contact_First_name,contact_mid_name,contact_last_name,contact_email,contact_company,contact_group_id,contact_portal,contact_cell,contact_wrk_phone,contact_address,contact_city,contact_region,contact_country,user_id,added,record_hide) VALUES (:contactFirstName,:contactMidName,:contactLastName,:contactEmail,:contactCompany,:contactGroup,:contactPortal,:contactCell,:contactWrkPhone,:contactAddress,:contactCity,:contactRegion,:contactCountry,:userId,:added,:recordHide)";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":contactFirstName",$this->contactFirstName);
			$stmt->bindParam(":contactMidName",$this->contactMidName);
			$stmt->bindParam(":contactLastName",$this->contactLastName);
			$stmt->bindParam(":contactEmail",$this->contactEmail);
			$stmt->bindParam(":contactCompany",$this->contactCompany);
			$stmt->bindParam(":contactGroup",$this->contactGroup);
			$stmt->bindParam(":contactPortal",$this->contactPortal);
			$stmt->bindParam(":contactCell",$this->contactCell);
			$stmt->bindParam(":contactWrkPhone",$this->contactWrkPhone);
			$stmt->bindParam(":contactAddress",$this->contactAddress);
			$stmt->bindParam(":contactCity",$this->contactCity);
			$stmt->bindParam(":contactRegion",$this->contactRegion);
			$stmt->bindParam(":contactCountry",$this->contactCountry);
			$stmt->bindParam(":userId",$_SESSION['user_id']);
			$stmt->bindParam(":added",$date);
			$stmt->bindParam(":recordHide",$this->recordHide);
			if ($stmt->execute()) {
				$this->log_activity($this->table,$this->dbConn->lastInsertId(),"Added New Contact");
				return trim($this->dbConn->lastInsertId());
			}
			else{
				die();
				}
		}
		// for update
		function update(){
			$sql="UPDATE $this->table SET contact_First_name=:contactFirstName,contact_mid_name=:contactMidName,contact_last_name=:contactLastName,contact_email=:contactEmail,contact_company=:contactCompany,contact_group_id=:contactGroup,contact_portal=:contactPortal,contact_cell=:contactCell,contact_wrk_phone=:contactWrkPhone,contact_address=:contactAddress,contact_city=:contactCity,contact_region=:contactRegion,contact_country=:contactCountry WHERE contact_id=:Id";
				$stmt = $this->dbConn->prepare($sql);
				$stmt->bindParam(":contactFirstName",$this->contactFirstName);
				$stmt->bindParam(":contactMidName",$this->contactMidName);
				$stmt->bindParam(":contactLastName",$this->contactLastName);
				$stmt->bindParam(":contactEmail",$this->contactEmail);
				$stmt->bindParam(":contactCompany",$this->contactCompany);
				$stmt->bindParam(":contactGroup",$this->contactGroup);
				$stmt->bindParam(":contactPortal",$this->contactPortal);
				$stmt->bindParam(":contactCell",$this->contactCell);
				$stmt->bindParam(":contactWrkPhone",$this->contactWrkPhone);
				$stmt->bindParam(":contactAddress",$this->contactAddress);
				$stmt->bindParam(":contactCity",$this->contactCity);
				$stmt->bindParam(":contactRegion",$this->contactRegion);
				$stmt->bindParam(":contactCountry",$this->contactCountry);
				$stmt->bindParam(":Id",$this->id);
				if ($stmt->execute()) {
					$this->log_activity($this->table,$this->id,"Updated Contact");
					return true;
				}
				else{
					return false;
					}

		}
		// for delete
		function delete(){
			$sql="UPDATE $this->table SET record_hide=:recordHide WHERE contact_id=:Id";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			$stmt->bindParam(":Id",$this->id);
			if ($stmt->execute()) {
				$this->log_activity($this->table,$this->id,"Deleted Contact");
				return true;
			}
			else{
				return false;
			}
		}

		// count contacts
		function count_contacts(){
			$sql="SELECT * FROM $this->table WHERE record_hide=:recordHide";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			if ($stmt->execute()) {
				return $stmt->rowCount();
			}
			else{
				die();
				}

		}

		// get contacts
		function get_contacts(){
			$sql="SELECT * FROM $this->table WHERE record_hide=:recordHide ORDER BY contact_id DESC";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $results;
			}
			else{
				die();
				}

		}

	// get user
		function get_contact_by_id(){
			$sql="SELECT * FROM $this->table WHERE contact_id=:Id LIMIT 1";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":Id",$this->id);
			if ($stmt->execute()) {
				$results= $stmt->fetch(PDO::FETCH_ASSOC);
				return $results;
			}
			else{
				die();
				}
		}

	// get contact name
	function get_contactName($contactId){
		$sql="SELECT contact_First_name,contact_last_name FROM $this->table WHERE contact_id=:Id";
		$stmt = $this->dbConn->prepare($sql);
		$stmt->bindParam(":Id",$contactId);
		if ($stmt->execute()) {
			$results = $stmt->fetch(PDO::FETCH_ASSOC);
			return trim($results["contact_First_name"]." ".$results["contact_last_name"]);
		}
		else{
			return false;
			}
	}

	// get all contact groups
		function get_contactGroup_contacts($groupId){
			$sql="SELECT * FROM $this->table WHERE contact_group_id=:groupId";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":groupId",$groupId);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				return sizeof($results);
			}
			else{
				die();
				}

		}

	// get company contacts
		function get_company_contacts($companyId){
			$sql="SELECT * FROM $this->table WHERE contact_company=:companyId";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":companyId",$companyId);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				return sizeof($results);
			}
			else{
				die();
				}

		}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// get all cases of contacts
		function get_contacts_cases($contactId){
			$returnResults=[];
			$sql="SELECT case_name,case_number,case_contactIds FROM court_case WHERE record_hide=:recordHide ORDER BY case_id DESC";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			if ($stmt->execute()) {
				$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
				foreach ($results as $result) {
					// get case contacts ids and decode
					$caseContactsIds = json_decode($result['case_contactIds']);
					if (in_array($contactId, $caseContactsIds)) {
						$returnResults[] = "<p>".$result['case_name']." (".$result['case_number'].")</p>";
					}
				}
				// return all cases by contacts
				return $returnResults;
			}
			else{
				die();
				}

		}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function log_activity($tableName,$tableId,$activityName){
		$sql = "INSERT INTO activities (table_name,table_id,activity_name,account_id,account_name,user_id) VALUES (:tableName,:tableId,:activityName,:accountId,:accountName,:userId)";
		$stmt = $this->dbConn->prepare($sql);
		$stmt->bindParam(":tableName",$tableName);
		$stmt->bindParam(":tableId",$tableId);
		$stmt->bindParam(":tableId",$tableId);
		$stmt->bindParam(":activityName",$activityName);
		$stmt->bindParam(":accountId",$_SESSION['account_id']);
		$stmt->bindParam(":accountName",$_SESSION['account_name']);
		$stmt->bindParam(":userId",$_SESSION['user_id']);
		$stmt->execute();
	}
}
?>