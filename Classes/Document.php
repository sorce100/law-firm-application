<?php 
	date_default_timezone_set('Africa/Accra');
	class Document{
		// setting and getting variables
		private $id;
		private $courtCaseId;
		private $docFolderName;
		private $docFiles;
		private $docAssignedDate;
		private $docDescription;
		private $added;
		private $dbConn;
		private $recordHide = "NO";
		private $table = "case_document";


		function set_id($id) { $this->id = $id; }
		function set_added($added) { $this->added = $added; }
		function set_recordHide($recordHide) { $this->recordHide = $recordHide; }
		function set_courtCaseId($courtCaseId) { $this->courtCaseId = $courtCaseId; }
		function set_docFolderName($docFolderName) { $this->docFolderName = $docFolderName; }
		function set_docFiles($docFiles) { $this->docFiles = $docFiles; }
		function set_docAssignedDate($docAssignedDate) { $this->docAssignedDate = $docAssignedDate; }
		function set_docDescription($docDescription) { $this->docDescription = $docDescription; }

		public function __construct(){
			require_once("db/db.php");
			$db = new DbConnect();
			$this->dbConn = $db->connect();
		}

		// clean data for data input
		public function CleanData($data){
			$data = trim($data);
			$data=htmlentities($data,ENT_QUOTES, 'UTF-8');
			$data = filter_var($data,FILTER_SANITIZE_SPECIAL_CHARS);
			return $data;
		}

		// insert pages
		function insert(){
			$date = date("j-m-Y");
			// delete this
			$sql = "INSERT INTO $this->table (doc_court_case_id,doc_folder_name,doc_files_uploaded,doc_assigned_date,doc_description,added,user_id,account_type,record_hide) VALUES (:courtCaseId,:docFolderName,:docFiles,:docAssignedDate,:docDescription,:added,:userId,:accountType,:recordHide)";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":courtCaseId",$this->courtCaseId);
			$stmt->bindParam(":docFolderName",$this->docFolderName);
			$stmt->bindParam(":docFiles",$this->docFiles);
			$stmt->bindParam(":docAssignedDate",$this->docAssignedDate);
			$stmt->bindParam(":docDescription",$this->docDescription);
			$stmt->bindParam(":added",$date);
			$stmt->bindParam(":userId",$_SESSION['user_id']);
			$stmt->bindParam(":accountType",$_SESSION['account_name']);
			$stmt->bindParam(":recordHide",$this->recordHide);
			if ($stmt->execute()) {
				$this->log_activity($this->table,$this->dbConn->lastInsertId(),"Added New Case Document");
				return true;
			}
			else{
				die();
			}
		}
		// for update
		function update(){
			$sql="UPDATE $this->table SET doc_files_uploaded=:docFiles,doc_assigned_date=:docAssignedDate,doc_description=:docDescription WHERE case_doc_id=:Id";
				$stmt = $this->dbConn->prepare($sql);
				$stmt->bindParam(":docFiles",$this->docFiles);
				$stmt->bindParam(":docAssignedDate",$this->docAssignedDate);
				$stmt->bindParam(":docDescription",$this->docDescription);
				$stmt->bindParam(":Id",$this->id);
				if ($stmt->execute()) {
					$this->log_activity($this->table,$this->id,"Updated Case Document");
					return true;
				}
				else{
					return false;
					}

		}

		function update_case_docs(){
			$sql="UPDATE $this->table SET doc_files_uploaded=:docFiles WHERE case_doc_id=:Id";
				$stmt = $this->dbConn->prepare($sql);
				$stmt->bindParam(":docFiles",$this->docFiles);
				$stmt->bindParam(":Id",$this->id);
				if ($stmt->execute()) {
					
					return true;
				}
				else{
					return false;
					}
		}

		// for delete
		function delete(){
			$sql="UPDATE $this->table SET record_hide=:recordHide WHERE case_doc_id=:Id";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			$stmt->bindParam(":Id",$this->id);
			if ($stmt->execute()) {
				$this->log_activity($this->table,$this->id,"Deleted Document");
				return true;
			}
			else{
				return false;
			}
		}

		function get_case_documents(){
			$returnResults = '';
			$sql="SELECT D.case_doc_id,D.doc_court_case_id,D.doc_folder_name,D.doc_files_uploaded,D.doc_assigned_date,D.doc_assigned_date,D.doc_description,D.added,C.case_name
			FROM $this->table AS D
			LEFT JOIN court_case AS C
			ON D.doc_court_case_id = C.case_id
			WHERE D.record_hide=:recordHide ORDER BY D.case_doc_id DESC";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				foreach ($results as $result) {
					$filesSize=sizeof(json_decode($result["doc_files_uploaded"]));
					$returnResults .= '
					                  <tr>
					                    <td>'.$result["case_name"].'</td>
					                    <td>'.$result["doc_folder_name"].'</td>
					                    <td>'.$filesSize.'</td>
					                    <td>'.$result["doc_assigned_date"].'</td>
					                    <td>'.$result["added"].'</td>
					                    <td>
					                      <button class="btn-default document_list" id="'.$result["case_doc_id"].'"><i class="fa fa-eye"></i></button>
					                      <button class="btn-primary update_docs" id="'.$result["case_doc_id"].'"><i class="fa fa-pencil"></i></button> 
					                      <button class="btn-danger del_data" id="'.$result["case_doc_id"].'"><i class="fa fa-trash"></i></button>
					                    </td>
					                  </tr>';
				}
				return $returnResults;
			}
			else{
				die();
			}
		}


		function get_case_doc_by_id(){
			$sql="SELECT * FROM $this->table WHERE case_doc_id=:Id LIMIT 1";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":Id",$this->id);
			if ($stmt->execute()) {
				$results= $stmt->fetch(PDO::FETCH_ASSOC);
				return json_encode($results);
			}
			else{
				die();
			}
		}

		// list of documents
		function get_document_list(){
			$returnRecords = '';
			$sql="SELECT doc_files_uploaded,doc_folder_name FROM $this->table WHERE case_doc_id=:Id LIMIT 1";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":Id",$this->id);
			if ($stmt->execute()) {
				$results= $stmt->fetch(PDO::FETCH_ASSOC);
				if (!empty($results)) {
					$decodedFiles = json_decode($results['doc_files_uploaded'],true);
					foreach ($decodedFiles as $file) {
						$returnRecords .= '<tr>
											<td width="55%">'.$file.'</td>
											<td width="25%"><button type="button" id="'.$results["doc_folder_name"].'/'.$file.'" class="btn-default downloadDoc">Download <i class="fa fa-download"></i></button></td>
											<td width="20%"><button type="button" id="'.$results["doc_folder_name"].'/'.$file.'" class="btn-primary readFile">Read <i class="fa fa-eye"></i></button></td>
										  </tr>';
					}
				}
				return $returnRecords;
				
			}
			else{
				die();
			}
		}
	////////////////////////////////////////////////////////////
		// get case documents
		function get_case_document_list(){
			$sql="SELECT * FROM $this->table WHERE record_hide=:recordHide AND doc_court_case_id=:caseId";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			$stmt->bindParam(":caseId",$this->courtCaseId);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				return json_encode($results);
			}
			else{
				die();
			}
		}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function log_activity($tableName,$tableId,$activityName){
		$sql = "INSERT INTO activities (table_name,table_id,activity_name,account_id,account_name,user_id) VALUES (:tableName,:tableId,:activityName,:accountId,:accountName,:userId)";
		$stmt = $this->dbConn->prepare($sql);
		$stmt->bindParam(":tableName",$tableName);
		$stmt->bindParam(":tableId",$tableId);
		$stmt->bindParam(":tableId",$tableId);
		$stmt->bindParam(":activityName",$activityName);
		$stmt->bindParam(":accountId",$_SESSION['account_id']);
		$stmt->bindParam(":accountName",$_SESSION['account_name']);
		$stmt->bindParam(":userId",$_SESSION['user_id']);
		$stmt->execute();
	}

	// error logs
	function log_error($errorMessage){
			$date=date("jS F Y \/ h:i:s A");
			$sql = "INSERT INTO error_logs (error_message,error_date,account_id,account_name,user_id) VALUES (:errorMessage,:errorDate,:accountId,:accountName,:userId)";
			$stmt = $this->dbConnect->prepare($sql);
			$stmt->bindParam(":errorMessage",$errorMessage);
			$stmt->bindParam(":errorDate",$date);
			$stmt->bindParam(":accountId",$_SESSION['account_id']);
			$stmt->bindParam(":accountName",$_SESSION['account_name']);
			$stmt->bindParam(":userId",$_SESSION['user_id']);
			$stmt->execute();
		}
	
		
}

?>