<?php 
	date_default_timezone_set('Africa/Accra');
	class SessionLogs{
		// setting and getting variables
		private $id;
		private $table = "users_session_log";
		function set_id($id) { $this->id = $id; }
		function get_id() { return $this->id; }

		public function __construct(){
			require_once("db/db.php");
			$db = new DbConnect();
			$this->dbConn = $db->connect();
		}

			function session_log_start(){
				$date = date("jS F Y \/ h:i:s A");
				$sql = "INSERT INTO $this->table (user_id,session_start) VALUES (:userId,:sessionStart)";
				$stmt = $this->dbConn->prepare($sql);
				$stmt->bindParam(":userId",$_SESSION['user_id']);
				$stmt->bindParam(":sessionStart",$date);
				if ($stmt->execute()) {
					$_SESSION['session_log_id'] = trim($this->dbConn->lastInsertId());

					$this->log_activity("users",$_SESSION['user_id'],"Loged In");
					return true;
				}
				else{
					die();
					}
			}

		// when the user logs out
			function session_log_end(){
				$date = date("jS F Y \/ h:i:s A");
				$sql = "UPDATE $this->table SET  session_end=:sessionEnd WHERE users_session_log_id=:usersSessionLogId";
				$stmt = $this->dbConn->prepare($sql);
				$stmt->bindParam(":sessionEnd",$date);
				$stmt->bindParam(":usersSessionLogId",$_SESSION['session_log_id']);
				if ($stmt->execute()) {
					return true;
					$this->log_activity("users",$_SESSION['user_id'],"Loged Out");
				}
				else{
					die();
					}
			}

		// get all logs
			function get_session(){
				$sql = "SELECT user_id,session_start,session_end FROM $this->table ORDER BY users_session_log_id DESC";
				$stmt = $this->dbConn->prepare($sql);
				if ($stmt->execute()) {
					$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
					return $results;
				}
				else{
					die();
					}
			}
		// get username of log
			function get_session_username($data){
				$sql = "SELECT user_name FROM users WHERE users_id=:Id";
				$stmt = $this->dbConn->prepare($sql);
				$stmt->bindParam(":Id",$data);
				if ($stmt->execute()) {
					$results = $stmt->fetch(PDO::FETCH_ASSOC);
					return $results["user_name"];
				}
				else{
					die();
					}
			}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function log_activity($tableName,$tableId,$activityName){
		$sql = "INSERT INTO activities (table_name,table_id,activity_name,account_id,account_name,user_id) VALUES (:tableName,:tableId,:activityName,:accountId,:accountName,:userId)";
		$stmt = $this->dbConn->prepare($sql);
		$stmt->bindParam(":tableName",$tableName);
		$stmt->bindParam(":tableId",$tableId);
		$stmt->bindParam(":tableId",$tableId);
		$stmt->bindParam(":activityName",$activityName);
		$stmt->bindParam(":accountId",$_SESSION['account_id']);
		$stmt->bindParam(":accountName",$_SESSION['account_name']);
		$stmt->bindParam(":userId",$_SESSION['user_id']);
		$stmt->execute();
	}
}

?>