<?php 
	date_default_timezone_set('Africa/Accra');
	class EventType{
		// setting and getting variables
		private $id;
		private $eventType;
		private $eventColorPicker;
		private $added;
		private $dbConn;
		private $recordHide = "NO";
		private $table = "event_type";
		 
		function set_id($id) { $this->id = $id; }
		function set_added($added) { $this->added = $added; }
		function set_recordHide($recordHide) { $this->recordHide = $recordHide; }
		function set_eventType($eventType) { $this->eventType = $eventType; }
		function set_eventColorPicker($eventColorPicker) { $this->eventColorPicker = $eventColorPicker; }


		public function __construct(){
			require_once("db/db.php");
			$db = new DbConnect();
			$this->dbConn = $db->connect();
		}

		// clean data for data input
		public function CleanData($data){
			$data = trim($data);
			$data=htmlentities($data,ENT_QUOTES, 'UTF-8');
			$data = filter_var($data,FILTER_SANITIZE_SPECIAL_CHARS);
			return $data;
		}

		// insert pages
		function insert(){
			$date=date("jS F Y \/ h:i:s A");
			$sql = "INSERT INTO $this->table (event_type_name,event_type_color,added,record_hide) VALUES (:eventType,:eventColorPicker,:added,:recordHide)";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":eventType",$this->eventType);
			$stmt->bindParam(":eventColorPicker",$this->eventColorPicker);
			$stmt->bindParam(":added",$date);
			$stmt->bindParam(":recordHide",$this->recordHide);
			if ($stmt->execute()) {
				return true;
			}
			else{
				return false;
				}
		}
		// for update
		function update(){
			$sql="UPDATE $this->table SET event_type_name=:eventType,event_type_color=:eventColorPicker WHERE event_type_id=:Id";
				$stmt = $this->dbConn->prepare($sql);
				$stmt->bindParam(":eventType",$this->eventType);
				$stmt->bindParam(":eventColorPicker",$this->eventColorPicker);
				$stmt->bindParam(":Id",$this->id);
				if ($stmt->execute()) {
					
					return true;
				}
				else{
					return false;
					}

		}
		// for delete
		function delete(){
			$sql="UPDATE $this->table SET record_hide=:recordHide WHERE event_type_id=:Id";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			$stmt->bindParam(":Id",$this->id);
			if ($stmt->execute()) {
				
				return true;
			}
			else{
				return false;
			}
		}


	// get users
		function get_event_types(){
			$sql="SELECT * FROM $this->table WHERE record_hide=:recordHide ORDER BY event_type_id DESC";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $results;
			}
			else{
				die();
				}

		}

	// get user
		function get_event_type_by_id(){
			$sql="SELECT * FROM $this->table WHERE event_type_id=:Id LIMIT 1";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":Id",$this->id);
			if ($stmt->execute()) {
				$results= $stmt->fetch(PDO::FETCH_ASSOC);
				return json_encode($results);
			}
			else{
				die();
				}
		}


	}

?>