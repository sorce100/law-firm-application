<?php 
  require_once("db/db.php");
  class SMS{

  private $dbConn;
  private $table= "sms";

  public function __construct(){
    $db = new DbConnect();
    $this->dbConn = $db->connect();
  }


    function send_sms($smsDestination,$smsMessage){
      $url ="http://206.225.81.36/ucm_api/index.php"; 
      $request_id = "UCM _" . preg_replace('/\D/', '', date('Y-m-d H:i:s'));
      $smsReference =  $_SESSION["user_id"]."_".date('Y-m-d H:i:s'); 
      $data = [  
          'reference' =>  $smsReference, // your own reference
          'message_type' => "1",
          'message' => $smsMessage, 
          'user_id' => "321", 
          'app_id' => "600161", 
          'org_id' => "143", 
          'src_address' => 'EL AGBEMAVA', //not more than 11 characters
          'dst_address' => $smsDestination,
          'service_id' => "", 
          'operation' => "send", 
          'timestamp' => preg_replace('/\D/', '', date("YYYYmmddHHiiss")), 
          'auth_key' => md5(600161 . date("YYYYmmddHHiiss") . "#-SU&r6jU") 
      ];

        $curl = curl_init($url); 
        curl_setopt($curl, CURLOPT_HEADER, false); 
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-type: application/json"));
        curl_setopt($curl, CURLOPT_POST, true); 
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
        $ucm_response = curl_exec($curl);
        if ($ucm_response) {
          $sql = "INSERT INTO $this->table (sms_message,user_id,sms_destination,sms_status) VALUES (:smsMessage,:userId,:smsDestination,:smsStatus)";
          $stmt = $this->dbConn->prepare($sql);
          $stmt->bindParam(":smsMessage",$smsMessage);
          $stmt->bindParam(":userId",$_SESSION["user_id"]);
          $stmt->bindParam(":smsDestination",$smsDestination);
          $stmt->bindParam(":smsStatus",$ucm_response);
          $stmt->execute();
          $this->log_activity("activities",$this->dbConn->lastInsertId(),"New SMS Sent");
        }

    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  function log_activity($tableName,$tableId,$activityName){
    $sql = "INSERT INTO activities (table_name,table_id,activity_name,account_id,account_name,user_id) VALUES (:tableName,:tableId,:activityName,:accountId,:accountName,:userId)";
    $stmt = $this->dbConn->prepare($sql);
    $stmt->bindParam(":tableName",$tableName);
    $stmt->bindParam(":tableId",$tableId);
    $stmt->bindParam(":tableId",$tableId);
    $stmt->bindParam(":activityName",$activityName);
    $stmt->bindParam(":accountId",$_SESSION['account_id']);
    $stmt->bindParam(":accountName",$_SESSION['account_name']);
    $stmt->bindParam(":userId",$_SESSION['user_id']);
    $stmt->execute();
  }



// get sms
  function get_sms_list(){
      $returnRecords='';
      $sql="SELECT S.sms_destination,S.sms_status,S.sms_message,S.user_id,S.added,U.user_name
      FROM $this->table AS S
      INNER JOIN users AS U
      ON S.user_id = U.users_id
      ORDER BY sms_id DESC";
      $stmt = $this->dbConn->prepare($sql);
      if ($stmt->execute()) {
        $results= $stmt->fetchAll(PDO::FETCH_ASSOC);
        if (!empty($results)) {
          foreach ($results as $result) {
            $statusMessage = json_decode($result["sms_status"]);
            $returnRecords .='
                          <tr>
                            <td>'.$result["sms_destination"].'</td>
                            <td>'.$result["sms_message"].'</td>
                            <td>'.$statusMessage->{'desc'}.'</td>
                            <td>'.$result["added"].'</td>
                            <td>'.$result["user_name"].'</td>
                          </tr>';
          }
        }

        return $returnRecords;
      }
      else{
        die();
        }

    }

}
?>