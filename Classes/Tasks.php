<?php 
	date_default_timezone_set('Africa/Accra');
	// array for straff tasks selects
	// $staffTasks= array();
	class Tasks{
		
		// setting and getting variables
		private $id;
		private $taskCourtCase;
		private $taskName;
		private $taskDueDate;
		private $taskCheckList;
		private $taskPriority;
		private $taskDescription;
		private $taskAssignStaff;
		private $taskStatus = 'INCOMPLETE';
		private $added;
		private $dbConn;
		private $recordHide = "NO";
		private $table = "tasks";

		function set_id($id) { $this->id = $id; }
		function set_added($added) { $this->added = $added; }
		function set_recordHide($recordHide) { $this->recordHide = $recordHide; }
		function set_taskCourtCase($taskCourtCase) { $this->taskCourtCase = $taskCourtCase; }
		function set_taskName($taskName) { $this->taskName = $taskName; }
		function set_taskDueDate($taskDueDate) { $this->taskDueDate = $taskDueDate; }
		function set_taskCheckList($taskCheckList) { $this->taskCheckList = $taskCheckList; }
		function set_taskPriority($taskPriority) { $this->taskPriority = $taskPriority; }
		function set_taskDescription($taskDescription) { $this->taskDescription = $taskDescription; }
		function set_taskAssignStaff($taskAssignStaff) { $this->taskAssignStaff = $taskAssignStaff; }
		function set_taskStatus($taskStatus) { $this->taskStatus = $taskStatus; }


		public function __construct(){
			require_once("db/db.php");
			$db = new DbConnect();
			$this->dbConn = $db->connect();
		}

		// clean data for data input
		public function CleanData($data){
			$data = trim($data);
			$data=htmlentities($data,ENT_QUOTES, 'UTF-8');
			$data = filter_var($data,FILTER_SANITIZE_SPECIAL_CHARS);
			return $data;
		}

		// insert pages
		function insert(){
			$date = date("d-m-Y");
			$sql = "INSERT INTO $this->table (court_case_id,task_name,task_due_date,task_checklist,task_priority,task_description,task_assign_staff,task_status,user_id,added,record_hide) VALUES (:taskCourtCase,:taskName,:taskDueDate,:taskCheckList,:taskPriority,:taskDescription,:taskAssignStaff,:taskStatus,:userId,:added,:recordHide)";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":taskCourtCase",$this->taskCourtCase);
			$stmt->bindParam(":taskName",$this->taskName);
			$stmt->bindParam(":taskDueDate",$this->taskDueDate);
			$stmt->bindParam(":taskCheckList",$this->taskCheckList);
			$stmt->bindParam(":taskPriority",$this->taskPriority);
			$stmt->bindParam(":taskDescription",$this->taskDescription);
			$stmt->bindParam(":taskAssignStaff",$this->taskAssignStaff);
			$stmt->bindParam(":taskStatus",$this->taskStatus);
			$stmt->bindParam(":userId",$_SESSION['user_id']);
			$stmt->bindParam(":added",$date);
			$stmt->bindParam(":recordHide",$this->recordHide);
			if ($stmt->execute()) {
				$this->log_activity($this->table,$this->dbConn->lastInsertId(),"Added New Tasks");
				return trim($this->dbConn->lastInsertId());
			}
			else{
				die();
				}
		}
		// for update
		function update(){
			$sql="UPDATE $this->table SET task_name=:taskName,task_due_date=:taskDueDate,task_checklist=:taskCheckList,task_priority=:taskPriority,task_description=:taskDescription,task_assign_staff=:taskAssignStaff WHERE task_id=:Id";
				$stmt = $this->dbConn->prepare($sql);
				$stmt->bindParam(":taskName",$this->taskName);
				$stmt->bindParam(":taskDueDate",$this->taskDueDate);
				$stmt->bindParam(":taskCheckList",$this->taskCheckList);
				$stmt->bindParam(":taskPriority",$this->taskPriority);
				$stmt->bindParam(":taskDescription",$this->taskDescription);
				$stmt->bindParam(":taskAssignStaff",$this->taskAssignStaff);
				$stmt->bindParam(":Id",$this->id);
				if ($stmt->execute()) {
					$this->log_activity($this->table,$this->id,"Updated Tasks");
					return true;
				}
				else{
					return false;
					}

		}
		// for delete
		function delete(){
			$sql="UPDATE $this->table SET record_hide=:recordHide WHERE task_id=:Id";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			$stmt->bindParam(":Id",$this->id);
			if ($stmt->execute()) {
				$this->log_activity($this->table,$this->id,"Deleted Tasks");
				return true;
			}
			else{
				return false;
			}
		}

		// get users
		function get_tasks(){
			$sql="SELECT * FROM $this->table WHERE record_hide=:recordHide ORDER BY task_id DESC";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $results;
			}
			else{
				die();
				}

		}

			// get staff fullnames

		function get_caseStaff_name($staffId){
			$sql="SELECT staff_firstname,staff_lastname FROM staff WHERE staff_id=:Id";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":Id",$staffId);
			if ($stmt->execute()) {
				$results= $stmt->fetch(PDO::FETCH_ASSOC);
				return $results["staff_firstname"]." ".$results["staff_lastname"];
			}
			else{
				die();
				}
		}

	// get case name

		function get_caseDetails($caseId){
			$sql="SELECT case_name FROM court_case WHERE case_id=:Id";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":Id",$caseId);
			if ($stmt->execute()) {
				$results= $stmt->fetch(PDO::FETCH_ASSOC);
				return $results["case_name"];
			}
			else{
				die();
				}
		}

		// all task list
		function get_tasks_list(){
			$returnRecords = '';
			$sql="SELECT T.task_status,T.task_name,T.task_assign_staff,T.task_id,T.task_checklist,T.task_priority,T.task_due_date,T.court_case_id,T.added,T.task_id,T.user_id,
				C.case_name
			FROM $this->table AS T
			INNER JOIN court_case AS C
			ON T.court_case_id = C.case_id
			WHERE T.record_hide=:recordHide ORDER BY task_id DESC";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				if (!empty($results)) {
					foreach ($results as $result) {

                        	// decode json of assigned to staff
							$assignedStaffArray = json_decode($result["task_assign_staff"]);
							// return $assignedStaffArray;exit();

                        	// display tasks assigned to user
                        	if ((in_array($_SESSION['account_id'], $assignedStaffArray)) ||( $_SESSION['user_id'] == $result['user_id'] )) { 
	                        		$returnRecords.='<tr>';
	                        			// check if task is completed or not
		                        		switch ($result["task_status"]) {
		                        			case 'INCOMPLETE':
		                        				$returnRecords.='<td><button type="button" class="btn btn-default MarkComplete" id="'.$result["task_id"].'">Mark Complete</button></td>';
		                        			break;
		                        			case 'COMPLETE':
		                        				$returnRecords.='<td><button type="button" class="btn btn-success">Completed</button></td>';
		                        			break;

		                        		}
	                                      
	                                    $returnRecords.='<td>'.$result["task_name"].' <span class="badge">'. sizeof(json_decode($result["task_checklist"])) .'</span></td>';
	                                      // colors per priority
	                                    switch ($result["task_priority"]) {
		                        			case 'High':
		                        				$returnRecords.='<td style="color:red;"><b>'.$result["task_priority"].'</b></td>';
		                        			break;
		                        			case 'Medium':
		                        				$returnRecords.='<td style="color:orange;"><b>'.$result["task_priority"].'</b></td>';
		                        			break;
		                        			case 'Low':
		                        				$returnRecords.='<td style="color:navy;"><b>'.$result["task_priority"].'</b></td>';
		                        			break;
		                        			case 'No Priority':
		                        				$returnRecords.='<td><b>'.$result["task_priority"].'</b></td>';
		                        			break;

		                        		}
	                                      
	                                    $returnRecords.='<td>'.$result["task_due_date"].'</td>
	                                      				 	<td><b>'.$result['case_name'].'</b></td>
	                                      				 	<td>'.$_SESSION["user_name"].'</td>
	                                      				 	<td>'.$result["added"].'</td>';
	                                    // check if the same account that created task or not
					                    switch ($result["user_id"]) {

						                    case $_SESSION['user_id']:
						                    	$returnRecords.='<td><button class="btn-default view_task" id="'.$result["task_id"].'"><i class="fa fa-eye"></i></button>
							                                        <button class="btn-primary update_task" id="'.$result["task_id"].'"><i class="fa fa-pencil"></i></button>
							                                        <button class="btn-danger del_data" id="'.$result["task_id"].'"><i class="fa fa-trash"></i></button>
							                                      </td>';
						                    break;
						                    	
						                    default:
						                    	$returnRecords.='<td><button class="btn-default view_task" id="'.$result["task_id"].'"><i class="fa fa-eye"></i></button></td>';
					                    }
					                    $returnRecords.='</tr>';

                        	}   
                        }
				}
				return $returnRecords;
			}
			else{
				die();
				}

		}
	// mark task completed
		function task_completed(){
			$date = date("d-m-Y | h:i:s A");
			$sql="UPDATE $this->table SET task_status=:taskStatus,task_complete_user=:taskCompleteUser,task_complete_date=:taskCompleteDate WHERE task_id=:Id";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":taskStatus",$this->taskStatus);
			$stmt->bindParam(":taskCompleteUser",$_SESSION['user_id']);
			$stmt->bindParam(":taskCompleteDate",$date);
			$stmt->bindParam(":Id",$this->id);
			if ($stmt->execute()) {
				$this->log_activity($this->table,$this->id,"Tasks Completed");
				return true;
			}
			else{
				return false;
			}
		}

	// get case tasks
		function get_tasks_by_id(){
			$sql="SELECT * FROM $this->table WHERE task_id=:Id";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":Id",$this->id);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				return json_encode($results);
			}
			else{
				die();
				}
		}
	////////////////////////////////////////////////////////////////////////////////////////////////
		// get case tasks
		function get_case_tasks(){
			$sql="SELECT * FROM $this->table WHERE record_hide=:recordHide AND court_case_id=:caseId";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			$stmt->bindParam(":caseId",$this->taskCourtCase);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				return json_encode($results);
			}
			else{
				die();
			}
		}

	// //////////////////////////////TASK COMPLETION STATUS/////////////////////////////////////////////////////////////////

		function get_tasks_complete_status($taskStatus){
			$returnRecords = '';
			$sql="SELECT T.task_status,T.task_name,T.task_assign_staff,T.task_id,T.task_checklist,T.task_priority,T.task_due_date,T.court_case_id,T.added,T.task_id,T.user_id,
				C.case_name
			FROM $this->table AS T
			INNER JOIN court_case AS C
			ON T.court_case_id = C.case_id
			WHERE T.task_status=:taskStatus AND T.record_hide=:recordHide
			ORDER BY task_id DESC";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			$stmt->bindParam(":taskStatus",$taskStatus);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				if (!empty($results)) {
					foreach ($results as $result) {

                        	// decode json of assigned to staff
							$assignedStaffArray = json_decode($result["task_assign_staff"]);
							// return $assignedStaffArray;exit();

                        	// display tasks assigned to user
                        	if ((in_array($_SESSION['account_id'], $assignedStaffArray)) ||( $_SESSION['user_id'] == $result['user_id'] )) { 
	                        		$returnRecords.='<tr>';
	                        			// check if task is completed or not
		                        		switch ($result["task_status"]) {
		                        			case 'INCOMPLETE':
		                        				$returnRecords.='<td><button type="button" class="btn btn-default MarkComplete" id="'.$result["task_id"].'">Mark Complete</button></td>';
		                        			break;
		                        			case 'COMPLETE':
		                        				$returnRecords.='<td><button type="button" class="btn btn-success">Completed</button></td>';
		                        			break;

		                        		}
	                                      
	                                    $returnRecords.='<td>'.$result["task_name"].' <span class="badge">'. sizeof(json_decode($result["task_checklist"])) .'</span></td>';
	                                      // colors per priority
	                                    switch ($result["task_priority"]) {
		                        			case 'High':
		                        				$returnRecords.='<td style="color:red;"><b>'.$result["task_priority"].'</b></td>';
		                        			break;
		                        			case 'Medium':
		                        				$returnRecords.='<td style="color:orange;"><b>'.$result["task_priority"].'</b></td>';
		                        			break;
		                        			case 'Low':
		                        				$returnRecords.='<td style="color:navy;"><b>'.$result["task_priority"].'</b></td>';
		                        			break;
		                        			case 'No Priority':
		                        				$returnRecords.='<td><b>'.$result["task_priority"].'</b></td>';
		                        			break;

		                        		}
	                                      
	                                    $returnRecords.='<td>'.$result["task_due_date"].'</td>
	                                      				 	<td><b>'.$result['case_name'].'</b></td>
	                                      				 	<td>'.$_SESSION["user_name"].'</td>
	                                      				 	<td>'.$result["added"].'</td>';
	                                    // check if the same account that created task or not
					                    switch ($result["user_id"]) {

						                    case $_SESSION['user_id']:
						                    	$returnRecords.='<td><button class="btn-default view_task" id="'.$result["task_id"].'"><i class="fa fa-eye"></i></button>
							                                        <button class="btn-primary update_task" id="'.$result["task_id"].'"><i class="fa fa-pencil"></i></button>
							                                        <button class="btn-danger del_data" id="'.$result["task_id"].'"><i class="fa fa-trash"></i></button>
							                                      </td>';
						                    break;
						                    	
						                    default:
						                    	$returnRecords.='<td><button class="btn-default view_task" id="'.$result["task_id"].'"><i class="fa fa-eye"></i></button></td>';
					                    }
					                    $returnRecords.='</tr>';

                        	}   
                        }
				}
				return $returnRecords;
			}
			else{
				die();
				}

		}

////////////////////////////////dashboard////////////////////////////////
		// upcoming tasks
		function dashboard_upcoming_task($staffId){
			$staffTasks= array();
			$sql="SELECT court_case_id,task_assign_staff,task_name,task_due_date,task_priority FROM $this->table WHERE task_status =:taskStatus LIMIT 5";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":taskStatus",$this->taskStatus);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				foreach ($results as $result) {
					$assignedIds = json_decode($result["task_assign_staff"]);
					for ($i=0; $i<sizeof($assignedIds); $i++) { 
						if ($assignedIds[$i] == $staffId) {
							 $staffTasks[] = $result;
						}
					}
				}
				// retrun tasks array
				return  $staffTasks;
			}
			else{
				die();
				}
		}

		// check if date is in a month
		function check_date_month($returnDate){
			$currentdate = date("d-m-Y");
            $d1 = new DateTime($returnDate);
            $d2 = new DateTime($currentdate);
            $interval = $d2->diff($d1);
            $interval->format('%m months');
            if(($interval->y ==0) && ($interval->m == 1) || ($interval->m == 0)) {return true;}
		}




	//////////////////////////////FILTER METHOD/////////////////////////////////////////////////////////////////////////////
		function task_filter(){
			$returnRecords = '';

			if (empty($this->taskAssignStaff)) {
				$this->taskAssignStaff = $_SESSION['account_id'];
			}

			$sql="SELECT T.task_status,T.task_name,T.task_assign_staff,T.task_id,T.task_checklist,T.task_priority,T.task_due_date,T.court_case_id,T.added,T.task_id,T.user_id,
				C.case_name
			FROM $this->table AS T
			INNER JOIN court_case AS C
			ON T.court_case_id = C.case_id
			WHERE (T.task_status=:taskStatus OR :taskStatus IS NULL)
			AND (T.task_priority=:taskPriority OR :taskPriority IS NULL)
			AND (T.court_case_id=:taskCourtCase OR :taskCourtCase IS NULL)
			AND T.record_hide=:recordHide
			ORDER BY T.task_id DESC";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":taskStatus",$this->taskStatus);
			$stmt->bindParam(":taskPriority",$this->taskPriority);
			$stmt->bindParam(":taskCourtCase",$this->taskCourtCase);
			$stmt->bindParam(":recordHide",$this->recordHide);
			
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				if (!empty($results)) {
					foreach ($results as $result) {
                        	// decode json of assigned to staff
							$assignedStaffArray = json_decode($result["task_assign_staff"]);
							// return $assignedStaffArray;exit();

                        	// display tasks assigned to user
                        	if ((in_array($this->taskAssignStaff, $assignedStaffArray)) ||( $_SESSION['user_id'] == $result['user_id'] )) { 
	                        		$returnRecords.='<tr>';
	                        			// check if task is completed or not
		                        		switch ($result["task_status"]) {
		                        			case 'INCOMPLETE':
		                        				$returnRecords.='<td><button type="button" class="btn btn-default MarkComplete" id="'.$result["task_id"].'">Mark Complete</button></td>';
		                        			break;
		                        			case 'COMPLETE':
		                        				$returnRecords.='<td><button type="button" class="btn btn-success">Completed</button></td>';
		                        			break;

		                        		}
	                                      
	                                    $returnRecords.='<td>'.$result["task_name"].' <span class="badge">'. sizeof(json_decode($result["task_checklist"])) .'</span></td>';
	                                      // colors per priority
	                                    switch ($result["task_priority"]) {
		                        			case 'High':
		                        				$returnRecords.='<td style="color:red;"><b>'.$result["task_priority"].'</b></td>';
		                        			break;
		                        			case 'Medium':
		                        				$returnRecords.='<td style="color:orange;"><b>'.$result["task_priority"].'</b></td>';
		                        			break;
		                        			case 'Low':
		                        				$returnRecords.='<td style="color:navy;"><b>'.$result["task_priority"].'</b></td>';
		                        			break;
		                        			case 'No Priority':
		                        				$returnRecords.='<td><b>'.$result["task_priority"].'</b></td>';
		                        			break;

		                        		}
	                                      
	                                    $returnRecords.='<td>'.$result["task_due_date"].'</td>
	                                      				 	<td><b>'.$result['case_name'].'</b></td>
	                                      				 	<td>'.$_SESSION["user_name"].'</td>
	                                      				 	<td>'.$result["added"].'</td>';
	                                    // check if the same account that created task or not
					                    switch ($result["user_id"]) {

						                    case $_SESSION['user_id']:
						                    	$returnRecords.='<td><button class="btn-default view_task" id="'.$result["task_id"].'"><i class="fa fa-eye"></i></button>
							                                        <button class="btn-primary update_task" id="'.$result["task_id"].'"><i class="fa fa-pencil"></i></button>
							                                        <button class="btn-danger del_data" id="'.$result["task_id"].'"><i class="fa fa-trash"></i></button>
							                                      </td>';
						                    break;
						                    	
						                    default:
						                    	$returnRecords.='<td><button class="btn-default view_task" id="'.$result["task_id"].'"><i class="fa fa-eye"></i></button></td>';
					                    }
					                    $returnRecords.='</tr>';

                        	}   
                        }
				}
				return $returnRecords;
			}
			else{
				return false;
				}

		}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		function log_activity($tableName,$tableId,$activityName){
			$sql = "INSERT INTO activities (table_name,table_id,activity_name,account_id,account_name,user_id) VALUES (:tableName,:tableId,:activityName,:accountId,:accountName,:userId)";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":tableName",$tableName);
			$stmt->bindParam(":tableId",$tableId);
			$stmt->bindParam(":tableId",$tableId);
			$stmt->bindParam(":activityName",$activityName);
			$stmt->bindParam(":accountId",$_SESSION['account_id']);
			$stmt->bindParam(":accountName",$_SESSION['account_name']);
			$stmt->bindParam(":userId",$_SESSION['user_id']);
			$stmt->execute();
		}

}
?>