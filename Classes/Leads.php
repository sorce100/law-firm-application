<?php 
	date_default_timezone_set('Africa/Accra');
	class Leads{
		// setting and getting variables
		private $id;
		private $added;
		private $dbConn;
		private $recordHide = "NO";
		private $table = "leads";
		private $leadFirstName;
		private $leadMidName;
		private $leadLastName;
		private $leadEmail;
		private $leadTel;
		private $leadOfficeLocation;
		private $leadTown;
		private $leadRegion;
		private $leadCaseMatter;
		private $leadCaseDescription;
		private $leadCaseStatus;
		private $leadCasePracticeArea;
		private $leadCaseAssignedTo;
		private $leadCaseCompletionDate;
		private $leadCaseHourlyRate;
		private $leadCaseMonthlyRetainer;
		private $leadCaseFeeFrequency;
		private $leadCaseSpecialArrangement;
		private $leadCaseAssociatedClients;
		private $leadCaseAdverseParties;
		private $leadCaseAssociatedFiles;
		private $status = "NEW";

		function set_id($id) { $this->id = $id; }
		function set_added($added) { $this->added = $added; }
		function set_recordHide($recordHide) { $this->recordHide = $recordHide; }
		function set_leadFirstName($leadFirstName) { $this->leadFirstName = $leadFirstName; }
		function set_leadMidName($leadMidName) { $this->leadMidName = $leadMidName; }
		function set_leadLastName($leadLastName) { $this->leadLastName = $leadLastName; }
		function set_leadEmail($leadEmail) { $this->leadEmail = $leadEmail; }
		function set_leadTel($leadTel) { $this->leadTel = $leadTel; }
		function set_leadOfficeLocation($leadOfficeLocation) { $this->leadOfficeLocation = $leadOfficeLocation; }
		function set_leadTown($leadTown) { $this->leadTown = $leadTown; }
		function set_leadRegion($leadRegion) { $this->leadRegion = $leadRegion; }
		function set_leadCaseMatter($leadCaseMatter) { $this->leadCaseMatter = $leadCaseMatter; }
		function set_leadCaseDescription($leadCaseDescription) { $this->leadCaseDescription = $leadCaseDescription; }
		function set_leadCaseStatus($leadCaseStatus) { $this->leadCaseStatus = $leadCaseStatus; }
		function set_leadCasePracticeArea($leadCasePracticeArea) { $this->leadCasePracticeArea = $leadCasePracticeArea; }
		function set_leadCaseAssignedTo($leadCaseAssignedTo) { $this->leadCaseAssignedTo = $leadCaseAssignedTo; }
		function set_leadCaseCompletionDate($leadCaseCompletionDate) { $this->leadCaseCompletionDate = $leadCaseCompletionDate; }
		function set_leadCaseHourlyRate($leadCaseHourlyRate) { $this->leadCaseHourlyRate = $leadCaseHourlyRate; }
		function set_leadCaseMonthlyRetainer($leadCaseMonthlyRetainer) { $this->leadCaseMonthlyRetainer = $leadCaseMonthlyRetainer; }
		function set_leadCaseFeeFrequency($leadCaseFeeFrequency) { $this->leadCaseFeeFrequency = $leadCaseFeeFrequency; }
		function set_leadCaseSpecialArrangement($leadCaseSpecialArrangement) { $this->leadCaseSpecialArrangement = $leadCaseSpecialArrangement; }
		function set_leadCaseAssociatedClients($leadCaseAssociatedClients) { $this->leadCaseAssociatedClients = $leadCaseAssociatedClients; }
		function set_leadCaseAdverseParties($leadCaseAdverseParties) { $this->leadCaseAdverseParties = $leadCaseAdverseParties; }
		function set_leadCaseAssociatedFiles($leadCaseAssociatedFiles) { $this->leadCaseAssociatedFiles = $leadCaseAssociatedFiles; }
		function set_status($status) { $this->status = $status; }

		public function __construct(){
			require_once("db/db.php");
			$db = new DbConnect();
			$this->dbConn = $db->connect();
		}

		// clean data for data input
		public function CleanData($data){
			$data = trim($data);
			$data=htmlentities($data,ENT_QUOTES, 'UTF-8');
			$data = filter_var($data,FILTER_SANITIZE_SPECIAL_CHARS);
			return $data;
		}

		// insert pages
		function insert(){
			$date=date("jS F Y \/ h:i:s A");
			$sql = "INSERT INTO $this->table (lead_first_name,lead_mid_name,lead_last_name,lead_email,lead_tel,lead_office_location,lead_town,lead_region,lead_case_matter,lead_case_description,lead_case_status,lead_case_practice_area,lead_case_assignedTo,lead_case_completion_date,lead_case_hourly_rate,
			lead_case_monthly_retainer,lead_case_fee_frequency,lead_case_special_arrangement,lead_case_associated_clients,lead_case_adverse_parties,lead_case_associated_files,added,record_hide,user_id,status)

			VALUES (:leadFirstName,:leadMidName,:leadLastName,:leadEmail,:leadTel,:leadOfficeLocation,:leadTown,:leadRegion,:leadCaseMatter,:leadCaseDescription,:leadCaseStatus,:leadCasePracticeArea,:leadCaseAssignedTo,:leadCaseCompletionDate,:leadCaseHourlyRate,:leadCaseMonthlyRetainer,:leadCaseFeeFrequency,:leadCaseSpecialArrangement,:leadCaseAssociatedClients,:leadCaseAdverseParties,:leadCaseAssociatedFiles,:added,:recordHide,:userId,:status)";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":leadFirstName",$this->leadFirstName);
			$stmt->bindParam(":leadMidName",$this->leadMidName);
			$stmt->bindParam(":leadLastName",$this->leadLastName);
			$stmt->bindParam(":leadEmail",$this->leadEmail);
			$stmt->bindParam(":leadTel",$this->leadTel);
			$stmt->bindParam(":leadOfficeLocation",$this->leadOfficeLocation);
			$stmt->bindParam(":leadTown",$this->leadTown);
			$stmt->bindParam(":leadRegion",$this->leadRegion);
			$stmt->bindParam(":leadCaseMatter",$this->leadCaseMatter);
			$stmt->bindParam(":leadCaseDescription",$this->leadCaseDescription);
			$stmt->bindParam(":leadCaseStatus",$this->leadCaseStatus);
			$stmt->bindParam(":leadCasePracticeArea",$this->leadCasePracticeArea);
			$stmt->bindParam(":leadCaseAssignedTo",$this->leadCaseAssignedTo);
			$stmt->bindParam(":leadCaseCompletionDate",$this->leadCaseCompletionDate);
			$stmt->bindParam(":leadCaseHourlyRate",$this->leadCaseHourlyRate);
			$stmt->bindParam(":leadCaseMonthlyRetainer",$this->leadCaseMonthlyRetainer);
			$stmt->bindParam(":leadCaseFeeFrequency",$this->leadCaseFeeFrequency);
			$stmt->bindParam(":leadCaseSpecialArrangement",$this->leadCaseSpecialArrangement);
			$stmt->bindParam(":leadCaseAssociatedClients",$this->leadCaseAssociatedClients);
			$stmt->bindParam(":leadCaseAdverseParties",$this->leadCaseAdverseParties);
			$stmt->bindParam(":leadCaseAssociatedFiles",$this->leadCaseAssociatedFiles);
			$stmt->bindParam(":added",$date);
			$stmt->bindParam(":recordHide",$this->recordHide);
			$stmt->bindParam(":userId",$_SESSION['user_id']);
			$stmt->bindParam(":status",$this->status);

			if ($stmt->execute()) {
				return true;
			}
			else{
				return false;
				}
		}
		// for update
		function update(){
			$sql="UPDATE $this->table SET lead_first_name=:leadFirstName,lead_mid_name=:leadMidName,lead_last_name=:leadLastName,lead_email=:leadEmail,lead_tel=:leadTel,lead_office_location=:leadOfficeLocation,lead_town=:leadTown,lead_region=:leadRegion,lead_case_matter=:leadCaseMatter,lead_case_description=:leadCaseDescription,lead_case_status=:leadCaseStatus,lead_case_practice_area=:leadCasePracticeArea,lead_case_assignedTo=:leadCaseAssignedTo,lead_case_completion_date=:leadCaseCompletionDate,lead_case_hourly_rate=:leadCaseHourlyRate,lead_case_monthly_retainer=:leadCaseMonthlyRetainer,lead_case_fee_frequency=:leadCaseFeeFrequency,lead_case_special_arrangement=:leadCaseSpecialArrangement,lead_case_associated_clients=:leadCaseAssociatedClients,lead_case_adverse_parties=:leadCaseAdverseParties,lead_case_associated_files=:leadCaseAssociatedFiles WHERE lead_id=:Id";
				$stmt = $this->dbConn->prepare($sql);
				$stmt->bindParam(":leadFirstName",$this->leadFirstName);
				$stmt->bindParam(":leadMidName",$this->leadMidName);
				$stmt->bindParam(":leadLastName",$this->leadLastName);
				$stmt->bindParam(":leadEmail",$this->leadEmail);
				$stmt->bindParam(":leadTel",$this->leadTel);
				$stmt->bindParam(":leadOfficeLocation",$this->leadOfficeLocation);
				$stmt->bindParam(":leadTown",$this->leadTown);
				$stmt->bindParam(":leadRegion",$this->leadRegion);
				$stmt->bindParam(":leadCaseMatter",$this->leadCaseMatter);
				$stmt->bindParam(":leadCaseDescription",$this->leadCaseDescription);
				$stmt->bindParam(":leadCaseStatus",$this->leadCaseStatus);
				$stmt->bindParam(":leadCasePracticeArea",$this->leadCasePracticeArea);
				$stmt->bindParam(":leadCaseAssignedTo",$this->leadCaseAssignedTo);
				$stmt->bindParam(":leadCaseCompletionDate",$this->leadCaseCompletionDate);
				$stmt->bindParam(":leadCaseHourlyRate",$this->leadCaseHourlyRate);
				$stmt->bindParam(":leadCaseMonthlyRetainer",$this->leadCaseMonthlyRetainer);
				$stmt->bindParam(":leadCaseFeeFrequency",$this->leadCaseFeeFrequency);
				$stmt->bindParam(":leadCaseSpecialArrangement",$this->leadCaseSpecialArrangement);
				$stmt->bindParam(":leadCaseAssociatedClients",$this->leadCaseAssociatedClients);
				$stmt->bindParam(":leadCaseAdverseParties",$this->leadCaseAdverseParties);
				$stmt->bindParam(":leadCaseAssociatedFiles",$this->leadCaseAssociatedFiles);
				$stmt->bindParam(":Id",$this->id);
				if ($stmt->execute()) {
					
					return true;
				}
				else{
					return false;
					}

		}
		// for delete
		function delete(){
			$sql="UPDATE $this->table SET record_hide=:recordHide WHERE lead_id=:Id";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			$stmt->bindParam(":Id",$this->id);
			if ($stmt->execute()) {
				
				return true;
			}
			else{
				return false;
			}
		}


	// get users
		function get_leads(){
			$sql="SELECT l.lead_id,CONCAT(l.lead_first_name,' ',l.lead_mid_name,' ',l.lead_last_name) AS clientName ,l.lead_case_matter,l.lead_last_name,l.lead_tel,l.lead_region,l.lead_case_status,l.lead_case_hourly_rate,l.added,pa.practice_area_name,CONCAT(st.staff_firstname,' ',st.staff_middlename,' ',st.staff_lastname) AS staffName 
			FROM $this->table AS l
			JOIN practice_area AS pa ON pa.practice_area_id = l.lead_case_practice_area
			JOIN staff AS st ON st.staff_id = l.lead_case_assignedTo
			WHERE l.record_hide=:recordHide ORDER BY lead_id DESC";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $results;
			}
			else{
				die();
				}

		}

	// count leads
		function count_leads(){
			$sql="SELECT * FROM $this->table WHERE record_hide=:recordHide";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			if ($stmt->execute()) {
				return $stmt->rowCount();
			}
			else{
				die();
				}

		}
	// get user
		function get_lead_by_id(){
			$sql="SELECT * FROM $this->table WHERE lead_id=:Id LIMIT 1";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":Id",$this->id);
			if ($stmt->execute()) {
				$results= $stmt->fetch(PDO::FETCH_ASSOC);
				return json_encode($results);
			}
			else{
				die();
				}
		}


	// row count for status

		function count_status($caseStatus){
			$sql="SELECT * FROM $this->table WHERE lead_case_status=:caseStatus";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":caseStatus",$caseStatus);
			if ($stmt->execute()) {
				$results= $stmt->rowCount();
				return $results;
			}
			else{
				die();
				}
		}

		// get status details
		function status_details($caseStatus){
			$sql="SELECT CONCAT(lead_first_name,' ',lead_last_name) AS fullName ,lead_email,lead_tel,added FROM $this->table WHERE lead_case_status=:caseStatus";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":caseStatus",$caseStatus);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $results;
			}
			else{
				die();
				}
		}


	}

?>