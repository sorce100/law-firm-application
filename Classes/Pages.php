<?php 
	date_default_timezone_set('Africa/Accra');
	class Pages{
		// setting and getting variables
		private $id;
		private $pageName;
		private $pageUrl;
		private $pageFileName;
		private $added;
		private $dbConn;
		private $recordHide = "NO";
		private $table = "pages";
		 // declaring arrary to save the pages name
		private	$accessPages=[];

		function set_id($id) { $this->id = $id; }
		function set_pageName($pageName) { $this->pageName = $pageName; }
		function set_pageUrl($pageUrl) { $this->pageUrl = $pageUrl; }
		function set_pageFileName($pageFileName) { $this->pageFileName = $pageFileName; }
		function set_added($added) { $this->added = $added; }
		function set_recordHide($recordHide) { $this->recordHide = $recordHide; }

		public function __construct(){
			require_once("db/db.php");
			$db = new DbConnect();
			$this->dbConn = $db->connect();
		}

		// clean data for data input
		public function CleanData($data){
			$data = trim($data);
			$data=htmlentities($data,ENT_QUOTES, 'UTF-8');
			$data = filter_var($data,FILTER_SANITIZE_SPECIAL_CHARS);
			return $data;
		}

		// insert pages
		function insert(){
			$date=date("jS F Y \/ h:i:s A");
			$sql = "INSERT INTO $this->table (pages_name,pages_url,page_file_name,added,record_hide) VALUES (:pagesName,:pagesUrl,:pageFileName,:added,:recordHide)";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":pagesName",$this->pageName);
			$stmt->bindParam(":pagesUrl",$this->pageUrl);
			$stmt->bindParam(":pageFileName",$this->pageFileName);
			$stmt->bindParam(":added",$date);
			$stmt->bindParam(":recordHide",$this->recordHide);
			if ($stmt->execute()) {
				return true;
			}
			else{
				return false;
				}
		}
		// for update
		function update(){
			$sql="UPDATE $this->table SET pages_name=:pagesName,pages_url=:pagesUrl,page_file_name=:pageFileName WHERE pages_id=:pagesId";
				$stmt = $this->dbConn->prepare($sql);
				$stmt->bindParam(":pagesName",$this->pageName);
				$stmt->bindParam(":pagesUrl",$this->pageUrl);
				$stmt->bindParam(":pageFileName",$this->pageFileName);
				$stmt->bindParam(":pagesId",$this->id);
				if ($stmt->execute()) {
					
					return true;
				}
				else{
					return false;
					}

		}
		// for delete
		function delete(){
			$sql="UPDATE $this->table SET record_hide=:recordHide WHERE pages_id=:pagesId";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			$stmt->bindParam(":pagesId",$this->id);
			if ($stmt->execute()) {
				
				return true;
			}
			else{
				return false;
			}
		}


	// get pages
		function get_pages(){
			$sql="SELECT * FROM $this->table WHERE record_hide=:recordHide ORDER BY pages_id DESC";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $results;
			}
			else{
				die();
				}

		}

	// get all pages list
		function get_pages_list(){
			$returnRecords='';
			$sql="SELECT * FROM $this->table WHERE record_hide=:recordHide ORDER BY pages_id DESC";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				if (!empty($results)) {
					foreach ($results as $result) {
						$returnRecords .='
                          <tr>
                            <td>'.$result["pages_name"].'</td>
                            <td>'.$result["pages_url"].'</td>
                            <td>'.$result["added"].'</td>
                            <td>
                              <button class="btn-primary update_data" id="'.$result["pages_id"].'"><i class="fa fa-pencil"></i></button>
                              <button class="btn-danger del_data" id="'.$result["pages_id"].'"><i class="fa fa-trash"></i></button>
                            </td>
                          </tr>';
					}
				}

				return $returnRecords;
			}
			else{
				die();
				}

		}

	// get user
		function get_page_by_id(){
			$sql="SELECT * FROM $this->table WHERE pages_id=:pagesId";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":pagesId",$this->id);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				return json_encode($results);
			}
			else{
				die();
				}
		}

		function get_menu_pages($pagesId){
			
			$sql="SELECT pages_url,page_file_name FROM $this->table WHERE pages_id=:Id";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":Id",$pagesId);
			if ($stmt->execute()) {
				$results= $stmt->fetch(PDO::FETCH_ASSOC);
				// add page to arrary then add the array to session
				$this->accessPages[]=$results["page_file_name"];
				$_SESSION['pagesAllowed']=$this->accessPages;
				
				print_r( $results["pages_url"]);
			}
			else{
				die();
				}
		}


	}

?>