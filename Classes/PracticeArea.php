<?php 
	date_default_timezone_set('Africa/Accra');
	class PracticeArea{
		// setting and getting variables
		private $id;
		private $practiceArea;
		private $added;
		private $dbConn;
		private $recordHide = "NO";
		private $table = "practice_area";

		function set_id($id) { $this->id = $id; }
		function set_practiceArea($practiceArea) { $this->practiceArea = $practiceArea; }
		function set_added($added) { $this->added = $added; }
		function set_recordHide($recordHide) { $this->recordHide = $recordHide; }

		public function __construct(){
			require_once("db/db.php");
			$db = new DbConnect();
			$this->dbConn = $db->connect();
		}

		// clean data for data input
		public function CleanData($data){
			$data = trim($data);
			$data=htmlentities($data,ENT_QUOTES, 'UTF-8');
			$data = filter_var($data,FILTER_SANITIZE_SPECIAL_CHARS);
			return $data;
		}

		// insert pages
		function insert(){
			$date = date("jS F Y \/ h:i:s A");
			$sql = "INSERT INTO $this->table (practice_area_name,added,record_hide) VALUES (:practiceAreaName,:added,:recordHide)";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":practiceAreaName",$this->practiceArea);
			$stmt->bindParam(":added",$date);
			$stmt->bindParam(":recordHide",$this->recordHide);
			if ($stmt->execute()) {
				$this->log_activity($this->table,$this->dbConn->lastInsertId(),"Added New Practice Area");
				return true;
			}
			else{
				die();
				}
		}
		// for update
		function update(){
			$sql="UPDATE $this->table SET practice_area_name=:practiceAreaName WHERE practice_area_id=:Id";
				$stmt = $this->dbConn->prepare($sql);
				$stmt->bindParam(":practiceAreaName",$this->practiceArea);
				$stmt->bindParam(":Id",$this->id);
				if ($stmt->execute()) {
					$this->log_activity($this->table,$this->id,"Updated Practice Area");
					return true;
				}
				else{
					return false;
					}

		}
		// for delete
		function delete(){
			$sql="UPDATE $this->table SET record_hide=:recordHide WHERE practice_area_id=:Id";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			$stmt->bindParam(":Id",$this->id);
			if ($stmt->execute()) {
				$this->log_activity($this->table,$this->id,"Deleted Practice Area");
				return true;
			}
			else{
				return false;
			}
		}


	// get practice ares
		function get_court_case_practice_areas(){
			$returnRecords ='';
			$sql="SELECT P.practice_area_id,P.practice_area_name,C.case_id
			FROM $this->table AS P
			LEFT JOIN court_case AS C
			ON P.practice_area_id = C.case_practice_area
			WHERE P.record_hide=:recordHide ORDER BY P.practice_area_id DESC";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				foreach ($results as $result) {
                    $returnRecords .= '<tr>
                                        <td>'.$result["practice_area_name"].'</td>
                                        <td>'.$result["case_id"].'</td>
                                      </tr>';
                }
                return $returnRecords;
			}
			else{
				return false;
				}

		}


		function get_practice_areas_options(){
			$returnRecords ='';
			$sql="SELECT * FROM $this->table WHERE record_hide=:recordHide ORDER BY practice_area_id DESC";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				foreach ($results as $result) {
                    $returnRecords .= '<option value='.$result["practice_area_id"].'>'.$result["practice_area_name"].'</option>';
                }

                return $returnRecords;
			}
			else{
				return false;
				}

		}
	// practice areas list
		function get_practice_areas_list(){
			$returnRecords ='';
			$sql="SELECT * FROM $this->table WHERE record_hide=:recordHide ORDER BY practice_area_id DESC";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				if ($results) {
					foreach ($results as $result) {
						$returnRecords .= '
				                          <tr>
				                            <td>'.$result["practice_area_name"].'</td>
				                            <td>'.$result["added"].'</td>
				                            <td>
				                              <button class="btn-primary update_data" id="'.$result["practice_area_id"].'"><i class="fa fa-pencil"></i></button>
				                              <button class="btn-danger del_data" id="'.$result["practice_area_id"].'"><i class="fa fa-trash"></i></button>
				                            </td>
				                          </tr>';
					}
				}

				return $returnRecords;
			}
			else{
				die();
				}

		}


	// get user
		function get_area_by_id(){
			$sql="SELECT * FROM $this->table WHERE practice_area_id=:Id";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":Id",$this->id);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				return json_encode($results);
			}
			else{
				die();
				}
		}

	// get all cases with practice area from court_Case table

		function get_practiceArea_courtCases($practiceId){
			$sql="SELECT * FROM court_case WHERE case_practice_area=:practiceId";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":practiceId",$practiceId);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				return count($results);
			}
			else{
				die();
				}
		}

	// get practice area name
		function get_practiceArea_name(){
			$sql="SELECT practice_area_name FROM $this->table WHERE practice_area_id=:Id";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":Id",$this->id);
			if ($stmt->execute()) {
				$results= $stmt->fetch(PDO::FETCH_ASSOC);
				return $results["practice_area_name"];
			}
			else{
				die();
				}
		}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function log_activity($tableName,$tableId,$activityName){
		$sql = "INSERT INTO activities (table_name,table_id,activity_name,account_id,account_name,user_id) VALUES (:tableName,:tableId,:activityName,:accountId,:accountName,:userId)";
		$stmt = $this->dbConn->prepare($sql);
		$stmt->bindParam(":tableName",$tableName);
		$stmt->bindParam(":tableId",$tableId);
		$stmt->bindParam(":tableId",$tableId);
		$stmt->bindParam(":activityName",$activityName);
		$stmt->bindParam(":accountId",$_SESSION['account_id']);
		$stmt->bindParam(":accountName",$_SESSION['account_name']);
		$stmt->bindParam(":userId",$_SESSION['user_id']);
		$stmt->execute();
	}

}


?>