<?php 
	date_default_timezone_set('Africa/Accra');
	class Events{
		// setting and getting variables
		private $id;
		private $courtCaseId;
		private $eventName;
		private $eventStartDate;
		private $eventEndDate;
		private $eventLocation;
		private $eventDescription;
		private $eventContactId;
		private $eventContactShare;
		private $eventStaffid;
		private $eventStaffShare;
		private $eventType;
		private $added;
		private $dbConn;
		private $recordHide = "NO";
		private $table = "events";

		function set_id($id) { $this->id = $id; }
		function set_added($added) { $this->added = $added; }
		function set_recordHide($recordHide) { $this->recordHide = $recordHide; }
		function set_courtCaseId($courtCaseId) { $this->courtCaseId = $courtCaseId; }
		function set_eventName($eventName) { $this->eventName = $eventName; }
		function set_eventStartDate($eventStartDate) { $this->eventStartDate = $eventStartDate; }
		function set_eventEndDate($eventEndDate) { $this->eventEndDate = $eventEndDate; }
		function set_eventLocation($eventLocation) { $this->eventLocation = $eventLocation; }
		function set_eventDescription($eventDescription) { $this->eventDescription = $eventDescription; }
		function set_eventContactId($eventContactId) { $this->eventContactId = $eventContactId; }
		function set_eventContactShare($eventContactShare) { $this->eventContactShare = $eventContactShare; }
		function set_eventStaffid($eventStaffid) { $this->eventStaffid = $eventStaffid; }
		function set_eventStaffShare($eventStaffShare) { $this->eventStaffShare = $eventStaffShare; }
		function set_eventType($eventType) { $this->eventType = $eventType; }

		public function __construct(){
			require_once("db/db.php");
			$db = new DbConnect();
			$this->dbConn = $db->connect();
		}

		// clean data for data input
		public function CleanData($data){
			$data = trim($data);
			$data=htmlentities($data,ENT_QUOTES, 'UTF-8');
			$data = filter_var($data,FILTER_SANITIZE_SPECIAL_CHARS);
			return $data;
		}

		// insert pages
		function insert(){
			$date = date("j-m-Y");
			$sql = "INSERT INTO $this->table (event_court_case_id,event_name,event_start_date,event_end_Date,event_location,event_description,event_contact_id,event_contact_share,event_staff_id,event_staff_share,user_id,added,record_hide,event_typeId) 
			VALUES (:courtCaseId,:eventName,:eventStartDate,:eventEndDate,:eventLocation,:eventDescription,:eventContactId,:eventContactShare,:eventStaffid,:eventStaffShare,:userId,:added,:recordHide,:eventTypeId)";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":courtCaseId",$this->courtCaseId);
			$stmt->bindParam(":eventName",$this->eventName);
			$stmt->bindParam(":eventStartDate",$this->eventStartDate);
			$stmt->bindParam(":eventEndDate",$this->eventEndDate);
			$stmt->bindParam(":eventLocation",$this->eventLocation);
			$stmt->bindParam(":eventDescription",$this->eventDescription);
			$stmt->bindParam(":eventContactId",$this->eventContactId);
			$stmt->bindParam(":eventContactShare",$this->eventContactShare);
			$stmt->bindParam(":eventStaffid",$this->eventStaffid);
			$stmt->bindParam(":eventStaffShare",$this->eventStaffShare);
			$stmt->bindParam(":userId",$_SESSION['user_id']);
			$stmt->bindParam(":added",$date);
			$stmt->bindParam(":recordHide",$this->recordHide);
			$stmt->bindParam(":eventTypeId",$this->eventType);
			if ($stmt->execute()) {
				$this->log_activity($this->table,$this->dbConn->lastInsertId(),"Added New Event");
				return trim($this->dbConn->lastInsertId());
			}
			else{
				die();
				}
		}
		// for update
		function update(){
			$sql="UPDATE $this->table SET event_name=:eventName,event_start_date=:eventStartDate,event_end_Date=:eventEndDate,event_location=:eventLocation,event_description=:eventDescription,event_contact_id=:eventContactId,event_contact_share=:eventContactShare,event_staff_id=:eventStaffid,event_staff_share=:eventStaffShare,event_typeId=:eventTypeId WHERE event_id=:Id";
				$stmt = $this->dbConn->prepare($sql);
				$stmt->bindParam(":eventName",$this->eventName);
				$stmt->bindParam(":eventStartDate",$this->eventStartDate);
				$stmt->bindParam(":eventEndDate",$this->eventEndDate);
				$stmt->bindParam(":eventLocation",$this->eventLocation);
				$stmt->bindParam(":eventDescription",$this->eventDescription);
				$stmt->bindParam(":eventContactId",$this->eventContactId);
				$stmt->bindParam(":eventContactShare",$this->eventContactShare);
				$stmt->bindParam(":eventStaffid",$this->eventStaffid);
				$stmt->bindParam(":eventStaffShare",$this->eventStaffShare);
				$stmt->bindParam(":eventTypeId",$this->eventType);
				$stmt->bindParam(":Id",$this->id);
				if ($stmt->execute()) {
					$this->log_activity($this->table,$this->id,"Updated Event");
					return true;
				}
				else{
					return false;
					}

		}
		// event drop update 
		function eventDrop(){
			$sql="UPDATE $this->table SET event_start_date=:eventStartDate,event_end_Date=:eventEndDate WHERE event_id=:Id";
				$stmt = $this->dbConn->prepare($sql);
				$stmt->bindParam(":eventStartDate",$this->eventStartDate);
				$stmt->bindParam(":eventEndDate",$this->eventEndDate);
				$stmt->bindParam(":Id",$this->id);
				if ($stmt->execute()) {
					
					return true;
				}
				else{
					return false;
					}
		}
		// for delete
		function delete(){
			$sql="UPDATE $this->table SET record_hide=:recordHide WHERE event_id=:Id";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			$stmt->bindParam(":Id",$this->id);
			if ($stmt->execute()) {
				$this->log_activity($this->table,$this->id,"Deleted Event");
				return true;
			}
			else{
				return false;
			}
		}

		// get users
		function get_events(){
			$sql="SELECT * FROM $this->table WHERE record_hide=:recordHide ORDER BY event_id DESC";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $results;
			}
			else{
				die();
				}

		}

		// function get_user_name($userId){
		// 	$sql="SELECT user_name FROM users WHERE users_id=:Id";
		// 	$stmt = $this->dbConn->prepare($sql);
		// 	$stmt->bindParam(":Id",$userId);
		// 	if ($stmt->execute()) {
		// 		$results = $stmt->fetch(PDO::FETCH_ASSOC);
		// 		return $results["user_name"];
		// 	}
		// 	else{
		// 		return false;
		// 		}
		// }
	// get all events list
		function get_events_list(){
			$returnRecords='';
			$sql="SELECT E.event_id,E.event_court_case_id,E.event_name,E.event_start_date,E.event_end_Date,E.event_typeId,E.event_location,E.event_description,E.event_contact_id,E.event_contact_share,E.event_staff_id,E.event_staff_share,E.user_id,E.added,E.record_hide,U.user_name
			 FROM $this->table AS E
			 LEFT JOIN users AS U
			 ON E.user_id = U.users_id
			 WHERE E.record_hide=:recordHide ORDER BY event_id DESC";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				foreach ($results as $result) {
					// $result['user_id'] = $this->get_user_name($result['user_id']);
					$returnRecords .= '
                                  <tr>
                                    <td>'.$result["event_name"].'</td>
                                    <td>'.$result["event_start_date"].'</td>
                                    <td>'.$result["event_end_Date"].'</td>
                                    <td>'.$result["added"].'<b> by </b>'.$result["user_name"].'</td>
                                    <td>
                                      <button class="btn-default view_event_detail" id="'.$result["event_id"].'"><i class="fa fa-eye"></i></button> ';

                                      if ($_SESSION['user_id'] == $result['user_id']) {
                                      	$returnRecords .= '<button class="btn-primary update_event_detail" id="'.$result["event_id"].'"><i class="fa fa-pencil"></i></button> 
                                      		<button class="btn-danger del_data" id="'.$result["event_id"].'"><i class="fa fa-trash"></i></button>';
                                      }
                                      

                    $returnRecords .= '</td>
                                  </tr>';
                                    
				}
				return $returnRecords;
			}
			else{
				die();
				}

		}

	// get events name date
			function get_case_events(){
				$sql="SELECT event_name,event_location,event_start_date,event_end_Date,added FROM $this->table WHERE record_hide=:recordHide AND event_court_case_id=:caseId";
				$stmt = $this->dbConn->prepare($sql);
				$stmt->bindParam(":recordHide",$this->recordHide);
				$stmt->bindParam(":caseId",$this->courtCaseId);
				if ($stmt->execute()) {
					$results= $stmt->fetch(PDO::FETCH_ASSOC);
					return ("<td>".$results['event_name']."</td><td>".$results['added']."</td><td>".$results['event_location']."</td><td>".$results['event_start_date']."<b> TO </b>".$results['event_end_Date']."</td>");
				}
				else{
					die();
					}

			}

	// get user
		function get_events_by_id(){
			$returnRecords = [];
			$caseStaffData ='';
			$caseContactData ='';

			$sql="SELECT E.event_id,E.event_court_case_id,E.event_name,E.event_start_date,E.event_end_Date,E.event_typeId,E.event_location,E.event_description,E.event_contact_id,E.event_contact_share,E.event_staff_id,E.event_staff_share,E.user_id,E.added,E.record_hide,C.case_contactIds,C.case_staffIds
			 FROM $this->table AS E
			 LEFT JOIN court_case AS C
			 ON E.event_court_case_id = C.case_id
			 WHERE E.event_id=:Id LIMIT 1";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":Id",$this->id);
			if ($stmt->execute()) {
				$results= $stmt->fetch(PDO::FETCH_ASSOC);
				// add entire results to the array
				$returnRecords['allResults'] = $results;
				// get staff shared with event
				$eventStaffShareArray = json_decode($results['event_staff_share'],true);
				// get case staff json array
				$caseStaffIdsArray = json_decode($results['case_staffIds'],true);
				// loop through the case staff id and check if user was added to share
				foreach ($caseStaffIdsArray as $staffData) {
					if (in_array($staffData, $eventStaffShareArray)) {
						$caseStaffData .= '<tr><td>
				      							<input checked type="checkbox" name="eventStaffShare[]" class="eventStaffShare" value="'.$staffData.'">
				      						</td></tr>';
					}
					elseif (!in_array($staffData, $eventStaffShareArray)) {
						$caseStaffData .= '<tr><td>
				      							<input type="checkbox" name="eventStaffShare[]" class="eventStaffShare" value="'.$staffData.'">
				      						</td></tr>';
					}
				}
				// add to return array
				$returnRecords['eventStaffsData'] = $caseStaffData;
				
				// /////////////////////////////////////////////////////////////////////////////////
				// get contacts shared for event
				$eventContactsShareArray = json_decode($results['event_contact_share'],true);
				// get contacts array
				$caseContactsIdsArray = json_decode($results['case_contactIds'],true);

				foreach ($caseContactsIdsArray as $contactData) {
					if (in_array($contactData, $eventStaffShareArray)) {
						$caseContactData .= '<tr><td>
				      							<input checked type="checkbox" name="eventContactShare[]" class="eventContactShare" value="'.$contactData.'">
				      						</td></tr>';
					}
					elseif (!in_array($contactData, $eventStaffShareArray)) {
						$caseContactData .= '<tr><td>
				      							<input type="checkbox" name="eventContactShare[]" class="eventContactShare" value="'.$contactData.'">
				      						</td></tr>';
					}
				}
				
				// add to return array
				$returnRecords['eventContactsData'] = $caseContactData;

				return json_encode($returnRecords);
			}
			else{
				die();
				}
		}

	// get contact name
	function get_contactName($contactId){
		$sql="SELECT contact_First_name,contact_last_name FROM $this->table WHERE event_id=:Id";
		$stmt = $this->dbConn->prepare($sql);
		$stmt->bindParam(":Id",$contactId);
		if ($stmt->execute()) {
			$results = $stmt->fetch(PDO::FETCH_ASSOC);
			return trim($results["contact_First_name"]." ".$results["contact_last_name"]);
		}
		else{
			return false;
			}
	}


	
////////////////////////////////dashboard////////////////////////////////
	function dashboard_upcoming_event(){
			$sql="SELECT event_court_case_id,event_name,event_start_date,event_end_Date,event_location,event_staff_id FROM $this->table WHERE record_hide=:recordHide ORDER BY event_id DESC LIMIT 5";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(":recordHide",$this->recordHide);
			if ($stmt->execute()) {
				$results= $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $results;
			}
			else{
				die();
				}

		}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	function log_activity($tableName,$tableId,$activityName){
		$sql = "INSERT INTO activities (table_name,table_id,activity_name,account_id,account_name,user_id) VALUES (:tableName,:tableId,:activityName,:accountId,:accountName,:userId)";
		$stmt = $this->dbConn->prepare($sql);
		$stmt->bindParam(":tableName",$tableName);
		$stmt->bindParam(":tableId",$tableId);
		$stmt->bindParam(":tableId",$tableId);
		$stmt->bindParam(":activityName",$activityName);
		$stmt->bindParam(":accountId",$_SESSION['account_id']);
		$stmt->bindParam(":accountName",$_SESSION['account_name']);
		$stmt->bindParam(":userId",$_SESSION['user_id']);
		$stmt->execute();
	}

}
?>