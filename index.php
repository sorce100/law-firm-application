<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>EL AGBEMAVA LAW OFFICE</title>
    <!-- ICONS -->
    <link rel="icon" type="image/jpg" sizes="96x96" href="pages/images/favicon.png">
    <!-- Bootstrap -->
    <link href="vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="vendors/animate.css/animate.min.css" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="build/css/custom.css" rel="stylesheet">
    <!-- include jquery -->
    <script src="vendors/jquery/dist/jquery.min.js"></script>
    <script src="vendors/bootstrap-growl.min.js"></script>
  </head>

  <body class="login">
    <div>
      <a class="hiddenanchor" id="signup"></a>

      
      <div class="login_wrapper">
        <div class="animate form login_form">
          <img src="pages/images/original.jpg" class="img img-responsive" width="100%" height="100%">
          <section class="login_content">
            <!--  -->
            <form id="login_form" method="POST">
              <h1>Login Form</h1>
              <div>
                <input type="text" class="form-control" id="userName" name="userName" placeholder="Enter Username"  required autofocus="true" autocomplete="off" />
              </div>
              <div>
                <input type="password" class="form-control" id="userPassword" name="userPassword" placeholder="Enter Password" required autocomplete="off" />
              </div>
              <!--  -->
              <br>
              <input type="hidden" name="mode" value="login">
              <div>
                <button class="btn btn-block submit" style="background:#1E8FB7; color: #fff;font-weight: bold;" id="loginBtn">Log in <i class="fa fa-sign-in"></i></button>
              </div>
            </form>
            <!--  -->
              <br>
              <div class="clearfix"></div>

              <div class="separator">
                <p class="change_link"><i class="fa fa-lock"></i> <a data-toggle="modal" data-target="#recoverPassChModal">Forgot password?</a></p>

                <div class="clearfix"></div>
                <br />

                <div>
                  <h1><i class="fa fa-briefcase"></i> EL AGBEMAVA LAW OFFICE</h1>
                  <p>© <?php echo date('Y');?> All Rights Reserved. <a href="http://theprismoid.com/" target="_blank">INGUZ SOLUTIONS</a></p>
                </div>
              </div>
            <!-- </form> -->
          </section>
        </div>
      </div>
    </div>
  </body>
</html>

<!-- for password change -->
<div class="modal fade" id="passChModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" id="bg">
         <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true" style="color: red;font-size: 25px;" class="btn-default">&times; </span></button>
        <h4 class="modal-title" id="subject">Change password</h4>
      </div>
      <div class="modal-body" id="bg">
          <form class="form-auth-small" id="changePass_form" method="POST">
            <input type="hidden" id="chPassUserName" name="chPassUserName">
            <input type="hidden" id="chPassUserPassword" name="chPassUserPassword">
            <div class="form-group input-group">
              <span class="input-group-addon"><i class="fa fa-lock"></i></span>
              <label for="signin-password" class="control-label sr-only">Password</label>
              <input type="password" class="form-control" id="newPassword" name="newPassword" placeholder="Enter Password &hellip;" required>
            </div>
            <div class="form-group input-group">
              <span class="input-group-addon"><i class="fa fa-lock"></i></span>
              <label for="signin-password" class="control-label sr-only">Password</label>
              <input type="password" class="form-control" id="newRetypePassword" name="newRetypePassword" placeholder="Retype Password &hellip;" required>
            </div>
            <input type="hidden" name="mode" value="changePass">
            <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal">Close <i class="fa fa-times"></i></button>
              <button type="submit" class="btn btn-primary" id="resetPass_btn">Reset Password <i class="fa fa-save"></i></button>
           </div>
          </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- for password recover -->
<div class="modal fade" id="recoverPassChModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" id="bg">
         <button type="button" class="close" data-dismiss="modal"  aria-label="Close"><span aria-hidden="true" style="color: red;font-size: 25px;" class="btn-default">&times; </span></button>
        <h4 class="modal-title" id="subject">Password Recovery</h4>
      </div>
      <div class="modal-body" id="bg">
          <form class="form-auth-small" id="recoverPass_form" method="POST">
      <div class="form-group input-group">
        <span class="input-group-addon"><i class="fa fa-user"></i></span>
        <label for="signin-username" class="control-label sr-only">Username</label>
        <input type="text" class="form-control" id="recvUserName" name="recvUserName" placeholder="Enter Username &hellip;" autofocus required autocomplete="off">
      </div>
      <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal">Close <i class="fa fa-times"></i></button>
              <button type="submit" class="btn btn-primary" id="recoverPass_btn">Recover Password <i class="fa fa-cog"></i></button>
           </div>
    </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>

<script src="login.js"></script>