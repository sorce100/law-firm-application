$(document).ready(function(){
  	// login
      $("#login_form").on("submit",function(e){
      e.preventDefault();
            $.ajax({
            url:"controllers/usersController.php",
            method:"POST",
            data:$("#login_form").serialize(),
            beforeSend:function(){ 
                $('#loginBtn').text("Loading ...").prop("disabled",true);    
            },
            success:function(data){
                // console.log(data);
                data = $.trim(data); 
              // if account is for staff and is successfull
                switch(data) {
                  case 'successStaff':
                    $.bootstrapGrowl("Login Successfull",{ type: 'success',align: 'center',allow_dismiss: false,width: 'auto' });
                    window.location.replace("pages/dashboard.php");
                    break;
                  case 'successContact':
                    $.bootstrapGrowl("Login Successfull",{ type: 'success',align: 'center',allow_dismiss: false,width: 'auto' });
                    window.location.replace("pages/client_home.php");
                    break;
                  case 'error':
                    $.bootstrapGrowl("Sorry! You have entered an invalid username or password",{ type: 'danger',align: 'center',allow_dismiss: false,width: 'auto' });
                    $('#userName').val("").prop('autofocus',true);
                    $('#userPassword').val("");
                    $('#loginBtn').text("Log in").prop("disabled",false);
                    break;  
                  default:
                    var fields = data.split('-');
                    var username = fields[0];
                    var passwd = fields[1];

                    $('#chPassUserName').val(username);
                    $('#chPassUserPassword').val(passwd);
                    $('#passChModal').modal('show');
                    $('#userName').val("");
                    $('#userPassword').val("");
                    $('#loginBtn').text("Log in").prop("disabled",false);
                }

            } 

            });  
        });
    });
   $(document).ready(function(){ 
    // change pass
    	$("#changePass_form").on("submit",function(e){
      	e.preventDefault();
            $.ajax({
            url:"controllers/usersController.php",
            method:"POST",
            data:$("#changePass_form").serialize(),
            beforeSend:function(){ 
            	$('#resetPass_btn').text("Loading ...").prop("disabled",true); 
                  
            },
            success:function(data){ 
                switch(data) {
                  case 'success':
                    $.bootstrapGrowl("Password Changed Successfully",{ type: 'success',align: 'center',allow_dismiss: false,width: 'auto' });
                    $("#passChModal").modal("hide");
                    break;
                  case 'error':
                    $.bootstrapGrowl("Sorry! Error Resetting password",{ type: 'danger',align: 'center',allow_dismiss: false,width: 'auto' });
                    $('#newPassword').val("");
                    $('#newRetypePassword').val("");
                    $('#resetPass_btn').text("Reset Password").prop("disabled",false);
                    break;
                  default:
                    $.bootstrapGrowl("Sorry! There was an error",{ type: 'danger',align: 'center',allow_dismiss: false,width: 'auto' });
                }
            } 

            });  
        });
   });  